-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.35-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for nibss_payplus_response
CREATE DATABASE IF NOT EXISTS `nibss_payplus_response` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `nibss_payplus_response`;

-- Dumping structure for table nibss_payplus_response.statuses
CREATE TABLE IF NOT EXISTS `statuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(15) NOT NULL COMMENT 'IP the data was received from',
  `xml` longtext NOT NULL COMMENT 'Raw XML sent by NIBSS',
  `sent` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1=Sent to CSS, 0=Not Sent',
  `send_count` tinyint(2) NOT NULL DEFAULT '0' COMMENT 'Number of times we''ve attempted to send this status to CSS',
  `css_response` varchar(100) NOT NULL DEFAULT '' COMMENT 'Response from CSS',
  `timereceived` datetime NOT NULL COMMENT 'Time it was received from NIBSS',
  `timesent` datetime NOT NULL DEFAULT '1001-01-01 00:00:00' COMMENT 'Time it was sent to CSS successfully',
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `sent` (`sent`),
  KEY `send_count` (`send_count`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT COMMENT='Stores the status of schedules sent by NIBSS';

-- Dumping data for table nibss_payplus_response.statuses: ~0 rows (approximately)
/*!40000 ALTER TABLE `statuses` DISABLE KEYS */;
/*!40000 ALTER TABLE `statuses` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
