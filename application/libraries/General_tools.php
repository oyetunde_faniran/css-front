<?php
/**
 * Handles all System Authorization operations
 * 
 * @package System_Administration
 *
 * @author Tunde
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class General_tools {
    
    function __construct(){
    }   //END __construct()
    

    public static function cleanMoneyField($field){
        $ci = &get_instance();
        $ci->load->library('form_validation');
        $ret_val = '';
        $length = strlen($field);
        for ($char = 0; $char < $length; $char++){
            $dis_char = substr($field, $char, 1);
            if ($ci->form_validation->numeric($dis_char) || $dis_char == '.'){
                $ret_val .= $dis_char;
            }
        }
        return $ret_val;
    }   //END cleanMoneyField()
    
    
    
    public static function getRandomString_AlphaNum($length){
        //Init the pool of characters by category
        $pool[0] = "ABCDEFGHJKLMNPQRSTUVWXYZ";
        $pool[1] = "23456789";
        return self::getRandomString_Generator($length, $pool);
    }   //END getRandomString_AlphaNum()
    
    
    
    public static function getRandomString_Num($length){
        //Init the pool of characters by category
        $pool[0] = "0123456789";
        return self::getRandomString_Generator($length, $pool);
    }   //END getRandomString_AlphaNum()
    
    
    
    public static function getRandomString_AlphaNumSigns($length){
        //Init the pool of characters by category
        $pool[0] = "ABCDEFGHJKLMNPQRSTUVWXYZ";
        $pool[1] = "abcdefghjkmnpqrstuvwxyz";
        $pool[2] = "23456789";
        $pool[3] = "-_[]{}()";
        return self::getRandomString_Generator($length, $pool);
    }   //END getRandomString_AlphaNumSigns()
    
    
    
    private static function getRandomString_Generator($length, $pools){
        $highest_pool_index = count($pools) - 1;
        //Now generate the string
        $finalResult = "";
        $length = abs((int)$length);
        for ($counter = 0; $counter < $length; $counter++){
            $whichPool = rand(0, $highest_pool_index);    //Randomly select the pool to use
            $maxPos = strlen($pools[$whichPool]) - 1;    //Get the max number of characters in the pool to be used
            $finalResult .= $pools[$whichPool][mt_rand(0, $maxPos)];
        }
        return $finalResult;
    }   //END getRandomString_Generator()
    
    
    
    
    
    public function getRandomString($min_length = 6, $max_length = 32){
        return $this->getRandomString_AlphaNum(rand($min_length, $max_length));
    }
    
    
    
    public function getUserCountryCode($user_ip){
            $user_ip_temp = '41.206.1.5';
//            $user_ip_temp = '8.8.8.8';
        $user_ip = (!empty($user_ip) && $user_ip != '::1' && $user_ip != '127.0.0.1') ? $user_ip : $user_ip_temp;
        if (!empty($user_ip)){
            $loc_deets = file_get_contents("https://freegeoip.net/json/$user_ip");
            $loc_deets_obj = json_decode($loc_deets);
            $dis_country_code = (string)$loc_deets_obj->country_code;
        } else {
            $dis_country_code = '';
        }
        return $dis_country_code;
    }   //END getUserCountryCode()
    
    
    
    
    public function parseResponseXML($xml, $needed_nodes, $uppercase_nodes = false){
        $ret_val = array();
        $parser = xml_parser_create();
        $parsed_xml = array();
        xml_parse_into_struct($parser, $xml, $parsed_xml);
//		die('<pre>' . print_r($needed_nodes, true) . print_r($parsed_xml, true));
		
        //Iterate through the result to pick what's needed
        $needed_node_count = count($needed_nodes);
        $found = 0;
        foreach ($parsed_xml as $node){
			$needed_node_key = $uppercase_nodes ? strtoupper($node['tag']) : $node['tag'];
			if (!isset($needed_nodes[$needed_node_key])){
//                echo "'$needed_node_key' NOT FOUND<br />";
				continue;
			}
//            echo "'$needed_node_key' FOUND++++++++++++++++++++<br />";
			$ret_val[$needed_node_key] = $node['value'];
			$found++;

			//If we have all that we need, then break out of the loop
			if ($found == $needed_node_count){
                break;
			}
        }	//END foreach()
        
        return $ret_val;
    }   //END parseResponseXML()
    
    
    
    public function internationalizeNumber($phone, $idd = '234'){
        $len = strlen($phone);
        if ($len == 11){
            $ret_val = $idd . substr($phone, 1);
        } elseif ($len == 13 || $len == 12){
            $ret_val = $phone;//'234' . substr($phone, 3);
        } else {
            $ret_val = '';
        }
        return $ret_val;
    }
    
    
    
}   //END class