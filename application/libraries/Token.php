<?php

/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 10/28/2016
 * Time: 3:16 PM
 */
class Token
{
    protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
    }

    /**
     * Generate a token of given length.
     *
     * @param int $length
     * @return mixed
     */
    public function generate_token($length)
    {
        $token = strtoupper($this->CI->basic_functions->create_random_string($length));

        return $token;
    }

    public function validate_token($token) {

    }
}