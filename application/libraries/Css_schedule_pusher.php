<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Css_schedule_pusher
 *
 * @author oyetunde.faniran
 */
class Css_schedule_pusher {
    
    
    function __construct() {
//        $this->ci = &get_instance();
        $this->client_id = CSS_CLIENT_ID;
        $this->secretkey = CSS_SECRET_KEY;
        $this->wsdl = CSS_WSDL;
    }
    
    
    public function createSchedule($sched_id, $bank_code, $accno){
        $salt = rand(1000, 1000000000);
        $str2hash = "{$this->client_id}-{$this->secretkey}-{$salt}";
        $mac = hash('sha512', $str2hash);
            
        $creat_sched_request = '<?xml version="1.0" encoding="UTF-8"?>
<PaymentRequestCommand>
  <ScheduleId>' . $sched_id . '</ScheduleId>
  <DebitDescription>' . $sched_id . '</DebitDescription>
  <DebitAccountNumber>' . $accno . '</DebitAccountNumber>
  <DebitBankCode>' . $bank_code . '</DebitBankCode>
  <ClientId>' . $this->client_id . '</ClientId>
  <Salt>' . $salt . '</Salt>
  <Mac>' . $mac . '</Mac>
</PaymentRequestCommand>';
        $ret_val = $this->_sendRequest($creat_sched_request, 'createPaymentSchedule');
        return $ret_val;
    }
    
    
    
    
    public function updateSchedule($sched_id, $beneficiaries, $narration, $pageno){
        $salt = rand(1000, 1000000000);
        $str2hash = "{$this->client_id}-{$this->secretkey}-{$salt}";
        $mac = hash('sha512', $str2hash);
        
        $payment_records = '';
        $sno = 0;
        foreach ($beneficiaries as $ben){
            $ben_name = (string)htmlspecialchars($ben['accname']);
            $ben_amount = round((real)($ben['amount']), 2);
            $ben_account = (string)htmlspecialchars($ben['accno']);
            $ben_bank = (string)htmlspecialchars($ben['bankcode']);
            $ben_narration = $ben['narration'];
            $ben_sno = ++$sno;

            $payment_records .= '<PaymentRecord>
  <Beneficiary>' . $ben_name . '</Beneficiary>
  <Amount>' . $ben_amount . '</Amount>
  <AccountNumber>' . $ben_account . '</AccountNumber>
  <BankCode>' . $ben_bank . '</BankCode>
  <Narration>' . $ben_narration . '</Narration>
  <SerialNo>' . $ben_sno . '</SerialNo>
</PaymentRecord>';
        }
            
        $update_sched_request = '<?xml version="1.0" encoding="UTF-8"?>
<PaymentRequestProcess>
  <ScheduleId>' . $sched_id . '</ScheduleId>
  <ClientId>' . $this->client_id . '</ClientId>
  <FileName>' . $narration . '</FileName>
  <PageNumber>' . $pageno . '</PageNumber>
  <Salt>' . $salt . '</Salt>
  <Mac>' . $mac . '</Mac>
  <PaymentRecords>'
     . $payment_records . 
  '</PaymentRecords>
</PaymentRequestProcess>';
        $ret_val = $this->_sendRequest($update_sched_request, 'updatePaymentSchedule');
        return $ret_val;
    }
    
    
    
    public function processSchedule($sched_id){
        $salt = rand(1000, 1000000000);
        $str2hash = "{$this->client_id}-{$this->secretkey}-{$salt}";
        $mac = hash('sha512', $str2hash);
            
        $creat_sched_request = '<?xml version="1.0" encoding="UTF-8"?>
<ProcessPaymentRequest>
  <ScheduleId>' . $sched_id . '</ScheduleId>
  <ClientId>' . $this->client_id . '</ClientId>
  <Salt>' . $salt . '</Salt>
  <Mac>' . $mac . '</Mac>
</ProcessPaymentRequest>';
        $ret_val = $this->_sendRequest($creat_sched_request, 'processPaymentSchedule');
        return $ret_val;
    }
    
    
    
    protected function _sendRequest($request, $method){
/*        $response = '<?xml version="1.0" encoding="UTF-8"?><PaymentResponse><Header><Status>16</Status></Header><HashValue></HashValue></PaymentResponse>';*/
//        $ret_val = array(
//            'status' => 16,
//            'response' => $response,
//            'request' => $request,
//            'response_obj' => $request
//        );
//        return $ret_val;
        
        
        
        $options = array(
            'trace' => 1,
            'exceptions' => 0
        );
        $client = new SoapClient($this->wsdl, $options);
        $response = $client->$method($request);
        $response_obj = @simplexml_load_string($response);
        $status = isset($response_obj->Header->Status) ? (string)$response_obj->Header->Status : -1;
        $ret_val = array(
            'status' => $status,
            'response' => $response,
            'request' => $request,
            'response_obj' => $response_obj
        );
        return $ret_val;
    }
    
    
    public function send2Nibss($data){
        //$sched_id, $bank_code, $accno, $beneficiaries, $narration, $pageno
        $sched_id = $data['sched_id'];
        $bank_code = $data['bank_code'];
        $accno = $data['accno'];
        $beneficiaries = $data['beneficiaries'];
        $narration = $data['narration'];
        $pageno = $data['pageno'];
        $this->client_id = $data['client_id'];
        $this->secretkey = $data['secretkey'];
        
        $create_response = $update_response = $process_response = array();
        
        try {
            $create_response = $this->createSchedule($sched_id, $bank_code, $accno);
            if($create_response['status'] != 16){
                throw new Exception('Create schedule failed', (int)$create_response['status']);
            }
            
            $update_response = $this->updateSchedule($sched_id, $beneficiaries, $narration, $pageno);
            if($update_response['status'] != 16){
                throw new Exception('Update schedule failed', (int)$create_response['status']);
            }
            
            $process_response = $this->processSchedule($sched_id);
            if($process_response['status'] != 16){
                throw new Exception('Process schedule failed', (int)$create_response['status']);
            }
            $ret_val = array(
                'status' => true,
                'create_response' => $create_response,
                'update_response' => $update_response,
                'process_response' => $process_response,
            );
        } catch (Exception $ex) {
            $ret_val = array(
                'status' => false,
                'create_response' => $create_response,
                'update_response' => $update_response,
                'process_response' => $process_response,
            );
        }
        return $ret_val;
    }
    
    
}
