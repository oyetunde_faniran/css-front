<?php

/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 11/16/2016
 * Time: 2:20 PM
 */
class Mini_css
{
    protected $CI;

    protected $css_webservice_url;
    protected $css_client_id;
    protected $css_secret_key;

    public function __construct()
    {
        $this->CI =& get_instance();

        if (empty($this->css_webservice_url)) {
            $this->css_webservice_url = CSS_WEBSERVICE_URL;
        }

        if (empty($this->css_client_id)) {
            $this->css_client_id = CSS_CLIENT_ID;
        }

        if (empty($this->css_secret_key)) {
            $this->css_secret_key = CSS_SECRET_KEY;
        }

        $this->CI->load->model('status_model', 'status');

        $this->CI->load->model('payment/payment_schedule_group_model', 'payment_schedule_group');
        $this->CI->load->model('payment/payment_schedule_model', 'payment_schedule');
        $this->CI->load->model('payment/payment_schedule_beneficiary_model', 'payment_schedule_beneficiary');

        $this->CI->load->model('payment/payment_file_group_model', 'payment_file_group');
        $this->CI->load->model('payment/payment_file_model', 'payment_file');
        $this->CI->load->model('payment/payment_file_beneficiary_model', 'payment_file_beneficiary');

        $this->CI->load->model('account-validation/validation_schedule_model', 'validation_schedule');
        $this->CI->load->model('account-validation/validation_account_model', 'validation_account');
        $this->CI->load->model('account-validation/validated_account_model', 'validated_account');
    }

    /**
     * Initiate payment for an authorized payment file group.
     *
     * @param $payment_file_group_id
     * @return bool
     */
    public function initiatePayment($payment_file_group_id)
    {
        $response = $this->preparePaymentSchedules($payment_file_group_id);

        /*if ($response['successful'])
            $this->uploadPendingPaymentSchedules();*/

        return $response;
    }

    /**
     * Upload all pending payment schedules.
     */
    public function uploadPendingPaymentSchedules()
    {
        set_time_limit(0);

        $status = $this->CI->status->findByDescription('PENDING');

        $pending_payment_schedules = $this->CI->payment_schedule->all(null, $status['status_id']);

        if (!empty($pending_payment_schedules)) {

            $successfully_uploaded = 0;
            $schedule_group_id = '';
            foreach ($pending_payment_schedules as $schedule) {

                //upload at a time only the schedules in a group
                if ($schedule_group_id != '' && $schedule['payment_schedule_group'] != $schedule_group_id)
                    break;

                $response = $this->uploadPaymentSchedule($schedule, false);

                //if Webservice Endpoint is down, don't attempt to upload any longer.
                /*if (!$response[0] && $response[1] == 'NIP Gateway DOWN!!!!')
                    break;*/

                if ($response[0])
                    $successfully_uploaded++;

                $schedule_group_id = $schedule['payment_schedule_group'];
            }

            //if at least 1 schedule was uploaded successfully, flag the schedule group as IN PROGRESS
            if ($successfully_uploaded > 0) {
                $status = $this->CI->status->findByDescription('IN PROGRESS');

                $data = array(
                    'status' => $status['status_id'],
                    'id' => $schedule_group_id
                );
                $this->CI->payment_schedule_group->save($data);
            }

            $msg = $successfully_uploaded > 0 ? $successfully_uploaded . " schedule(s) successfully uploaded" : $response[1];
        } else
            $msg = "NO PENDING PAYMENT SCHEDULES FOUND";

        return $msg;
    }

    /**
     * Upload payment schedule.
     *
     * @param $ref The schedule_ref or the entire schedule array
     * @param bool|true $is_ref
     * @return array
     */
    public function uploadPaymentSchedule($ref, $is_ref = true)
    {
        if ($is_ref) {
            $schedule = $this->CI->payment_schedule->findByScheduleRef($ref);
        } else {
            $schedule = $ref;
        }

        $schedule_beneficiaries = $this->CI->payment_schedule_beneficiary->all($schedule['payment_schedule_id'], true);

        $xml = $this->generateUploadPaymentScheduleXML($schedule, $schedule_beneficiaries);

        try {
            $response = $this->sendToNIBSS('uploadPaymentSchedule', $xml);

            if (!$response)
                throw new Exception('NIP Gateway DOWN!!!!');

            //check the response
            $response_obj = simplexml_load_string($response);
            if (!$response_obj)
                throw new Exception('NIP Gateway DOWN!!!!');

            $response_code = $response_obj->Header->Status;
            if (strlen($response_code) > 2)
                throw new Exception('NIP Gateway DOWN!!!!');

            //get the description for the status of the response
            $response_description = $this->getNibssPayPlusResponseDescription($response_code);

            if ($response_code == '16' || $response_code == '01') {
                $status = $this->CI->status->findByDescription('IN PROGRESS');
            } else {
                $status = $this->CI->status->findByDescription('FAILED');
            }

            //update the payment schedule record in db
            $data = array(
                'schedule_ref' => $schedule['schedule_ref'],
                'status' => $status['status_id'],
                'response_code' => $response_code,
                'response_description' => $response_description

            );
            $this->CI->payment_schedule->updateByRef($data);

            //update all beneficiaries in schedule
            $data = array(
                'payment_schedule_id' => $schedule['payment_schedule_id'],
                'status' => $status['status_id'],
                'response_code' => $response_code,
                'response_description' => $response_description
            );
            $this->CI->payment_schedule_beneficiary->updateAllInSchedule($data);

            $stat = $response_code == '16' ? true : false;

            $ret_val = array($stat, $response_description);

        } catch (Exception $ex) {
            $message = $ex->getMessage();

            $ret_val = array(false, !empty($message) ? $message : "Cannot Connect to Payment Gateway");
        }

        return $ret_val;
    }

    /**
     * Get status for all payment schedules previously sent.
     *
     * @return string
     */
    public function getAllPaymentScheduleStatus()
    {
        set_time_limit(0);

        $status = $this->CI->status->findByDescription('IN PROGRESS');

        $payment_schedules_in_progress = $this->CI->payment_schedule->all(null, $status['status_id']);

        if (!empty($payment_schedules_in_progress)) {

            $i = 0;
            $processed_schedules = 0;
            foreach ($payment_schedules_in_progress as $schedule) {

                $response = $this->getPaymentScheduleStatus($schedule, false);

                //if Webservice Endpoint is down, don't attempt to upload any longer.
                /*if (!$response[0] && $response[1] == 'NIP Gateway DOWN!!!!')
                    break;*/

                if ($response['successful'] && $response['extra'] == 'COMPLETED')
                    $processed_schedules++;

                $i++;

                //get status of 15 schedules at a time
                /*if ($i == 15)
                    break;*/
            }

            $msg = $processed_schedules > 0 ? $processed_schedules . " schedule(s) processed successfully" : $response['message'];
        } else
            $msg = "NO PAYMENT SCHEDULES IN PROGRESS";

        return $msg;
    }

    /**
     * @param string|array $ref Schedule Ref|Entire Schedule record
     * @param bool|true $is_ref
     * @return array
     */
    public function getPaymentScheduleStatus($ref, $is_ref = true)
    {
        if ($is_ref) {
            $schedule = $this->CI->payment_schdule->findByScheduleRef($ref);
        } else {
            $schedule = $ref;
        }

        $xml = $this->generateGetPaymentScheduleStatusXML($schedule);

        try {
            $response = $this->sendToNIBSS('getPaymentScheduleStatus', $xml);

            if (!$response) {
                throw new Exception('NIP Gateway DOWN!!!!');
            }

            //check the response
            $response_obj = simplexml_load_string($response);
            $response_code = $response_obj->Header->Status;

            if (strlen($response_code) > 2) {
                throw new Exception('NIP Gateway DOWN!!!!');
            }

            //get the description for the status of the response
            $response_description = $this->getNibssPayPlusResponseDescription($response_code);

            if (in_array($response_code, array('06', '12'))) {
                $status = $this->CI->status->findByDescription('IN PROGRESS');
            } elseif ($response_code == '00') {
                $status = $this->CI->status->findByDescription('COMPLETED');
            } else {
                $status = $this->CI->status->findByDescription('FAILED');
            }

            //update the payment schedule record in db
            $data = array(
                'schedule_ref' => $schedule['schedule_ref'],
                'status' => $status['status_id'],
                'response_code' => $response_code,
                'response_description' => $response_description
            );
            $this->CI->payment_schedule->updateByRef($data);

            //if schedule FAILED or is IN PROGRESS, update all beneficiaries in schedule accordingly
            if ($status['status_description'] == 'FAILED' || $status['status_description'] == 'IN PROGRESS') {
                $data = array(
                    'payment_schedule_id' => $schedule['payment_schedule_id'],
                    'status' => $status['status_id'],
                    'response_code' => $response_code,
                    'response_description' => $response_description
                );
                $this->CI->payment_schedule_beneficiary->updateAllInSchedule($data);
            }

            //if beneficiary payment records are sent back, update each beneficiary
            if (!empty($response_obj->PaymentRecord)) {
                //fetch all statuses
                $statuses = $this->CI->status->all();

                foreach ($response_obj->PaymentRecord as $payment_record) {

                    $response_code_2 = $payment_record->Status;
                    $response_description_2 = $this->getNibssPayPlusResponseDescription($response_code_2);

                    if ($response_code_2 == '00') {
                        foreach ($statuses as $beneficiary_status) {
                            if ($beneficiary_status['status_description'] == 'PAID')
                                break;
                        }
                    } elseif ($response_code_2 == '06') {
                        foreach ($statuses as $beneficiary_status) {
                            if ($beneficiary_status['status_description'] == 'IN PROGRESS')
                                break;
                        }
                    } else {
                        foreach ($statuses as $beneficiary_status) {
                            if ($beneficiary_status['status_description'] == 'FAILED')
                                break;
                        }
                    }

                    //update the schedule beneficiary's record
                    $data = array(
                        'payment_schedule_id' => $schedule['payment_schedule_id'],
                        'beneficiary_account_no' => $payment_record->AccountNumber,
                        'beneficiary_bank_code' => $payment_record->SortCode,
                        'status' => $beneficiary_status['status_id'],
                        'response_code' => $response_code_2,
                        'response_description' => $response_description_2
                    );
                    $this->CI->payment_schedule_beneficiary->updateOneInSchedule($data);
                }
            }

            //update the payment file corresponding to this payment schedule
            $this->CI->payment_file->updatePaymentStatus($schedule['payment_file'], $status['status_id']);

            //update the payment file beneficiary records
            $this->CI->payment_file_beneficiary->updatePaymentForAllInFile($schedule['payment_file']);

            $stat = $status['status_description'] == 'COMPLETED' || $status['status_description'] == 'IN PROGRESS' ? true : false;

            $ret_val = array('successful' => $stat, 'message' => $response_description, "extra" => $status);

        } catch (Exception $ex) {
            $message = $ex->getMessage();

            $ret_val = array('successful' => false, 'message' => (!empty($message) ? $message : "Cannot Connect to Payment Gateway"), "extra" => "");
        }

        return $ret_val;
    }

    /**
     * Validate the accounts in payment file.
     *
     * @param $file_id
     * @return mixed
     */
    public function validateAccounts($file_id)
    {
        //first validate against our DB
        $stats = $this->validateAccountsAgainstLocalDB($file_id);

        //if any accounts were not found in our DB, send these to NIBSS for validation
        //exclude any records where Account Name, Account Number, or Bank Code was not supplied
        if ($stats['records_found'] < $stats['total_records']) {

            $result = $this->CI->db
                ->select("bpb_account_name AS account_name, bpb_account_no AS account_no, bpb_bank_code AS bank_code", false)
                ->from("bulk_payment_beneficiaries")
                //->where("bulk_payment_file = {$file_id} AND bpb_av_response_code IS NULL")
                ->where("bulk_payment_file = {$file_id} AND bpb_av_response_code IS NULL AND bpb_account_name != '' AND bpb_account_no != '' AND bpb_bank_code != ''")
                ->get();

            if ($result && $result->num_rows() > 0) {
                $accounts = $result->result_array();

                $this->CI->validated_account->saveMany($accounts);

                //prepare the schedules; a cron job will send them
                $response = $this->prepareAccountsForValidation($accounts);

                //if validation schedules were successfully prepared for these accounts,
                if ($response['successful']) {
                    $status = $this->CI->status->findByDescription('IN PROGRESS');

                    //flag these accounts as IN PROGRESS for validation
                    $sql = "UPDATE bulk_payment_beneficiaries bpb
                            SET bpb_av_status = {$status['status_id']}
                            WHERE bulk_payment_file = {$file_id}
                              AND bpb_av_response_code IS NULL
                              AND bpb_account_name != ''
                              AND bpb_account_no != ''
                              AND bpb_bank_code != ''";
                    $this->CI->db->query($sql);
                }
            }
        } else {
            //check whether any of the accounts is still in progress
            $result = $this->CI->db
                ->select("COUNT(*) AS count")
                ->from("bulk_payment_beneficiaries")
                ->where(array("bulk_payment_file" => $file_id, "bpb_av_status" => 7))
                ->get();

            $accounts_in_progress = $result && $result->num_rows() > 0 ? $result->row()->count : 0;

            if ($accounts_in_progress > 0)
                $status = $this->CI->status->findByDescription('IN PROGRESS');
            else
                $status = $this->CI->status->findByDescription('COMPLETED');
        }
        $this->CI->bulk_payment_file->updateAccountValidationStatus($file_id, $status['status_id']);

        return $stats;
    }

    public function validateAccountsWithNIBSSPayPlus($accounts, $file_name, $file_description)
    {
        $schedule_id = $this->logAccountsForValidation($accounts, $file_name, $file_description);

        //first validate against our DB
        //$this->validateAccountsAgainstLocalDB($accounts);

        $schedule = $this->generateAccountValidationUploadSchedule($schedule_id);

        file_put_contents('account-validation-schedule.txt', $schedule);

        //make the webservice call
        try {
            $response = $this->sendToNIBSS('uploadNewVendors', $schedule);

            if (!$response) {
                throw new Exception('NIP Gateway DOWN!!!!');
            }

            //check the response
            $response_obj = simplexml_load_string($response);
            $response_code = $response_obj->Header->Status;

            if (strlen($response_code) > 2) {
                throw new Exception('NIP Gateway DOWN!!!!');
            }

            //get the description for the status of the response
            $response_description = $this->getNIBSSPayPlusResponseDescription($response_code);

            $status = $response_code != '16' ? '2' : '0';

            $data = array(
                'schedule_id' => $schedule_id,
                'status' => $status,
                'response_code' => $response_code,
                'response_description' => $response_description
            );
            $this->updateAccountValidationSchedule($data);

            if ($status == '2') {
                $this->updateAccountsInValidationSchedule($data);
            }

            $ret_val = array($response_code == '16' ? true : false, $response_description);

        } catch (Exception $e) {
            $ret_val = array(false, $e->getMessage());
        }

        return $ret_val;
    }

    public function resendAccountValidationSchedule($schedule_id)
    {
        $schedule = $this->generateAccountValidationUploadSchedule($schedule_id);

        //file_put_contents('account-validation-schedule.txt', $schedule);

        //make the webservice call
        try {
            $response = $this->sendToNIBSS('uploadNewVendors', $schedule);

            if (!$response) {
                throw new Exception('NIP Gateway DOWN!!!!');
            }

            //check the response
            $response_obj = simplexml_load_string($response);
            $response_code = $response_obj->Header->Status;

            if (strlen($response_code) > 2) {
                throw new Exception('NIP Gateway DOWN!!!!');
            }

            //get the description for the status of the response
            $response_description = $this->getNIBSSPayPlusResponseDescription($response_code);

            $status = $response_code != '16' ? '2' : '0';

            $data = array(
                'schedule_id' => $schedule_id,
                'status' => $status,
                'response_code' => $response_code,
                'response_description' => $response_description
            );
            $this->updateAccountValidationSchedule($data);

            if ($status == '2') {
                $this->updateAccountsInValidationSchedule($data);
            }

            $ret_val = array($response_code == '16' ? true : false, $response_description);

        } catch (Exception $e) {
            $ret_val = array(false, $e->getMessage());
        }

        return $ret_val;
    }

    public function getAccountValidationSchedule($schedule_id)
    {
        $resource = $this->CI->db->get_where('av_schedules', array('avs_schedule_id' => $schedule_id));

        return $resource->num_rows() > 0 ? $resource->row_array() : null;
    }

    /**
     * Retrieve details of valid accounts in a bulk payment file.
     *
     * Valid accounts include:
     *  1. Those of which details exactly match those in the bank as verified by NIBSS.
     *  2. Those of which account names have been swapped to match those in the bank as verified by NIBSS.
     *  3. Those that have been forcefully authorized.
     *
     * @param $file_id
     * @return mixed
     */
    public function getValidAccountsInFile($file_id)
    {
        $this->CI->load->model('status_model', 'status');

        $statuses = $this->CI->status->all();

        $status_ids = array();
        foreach ($statuses as $status) {
            if (in_array($status['status_description'], array('VALID', 'ACCOUNT NAME SWAPPED', 'ACCOUNT FORCED')))
                $status_ids[] = $status['status_id'];
        }

        $resource = $this->CI->db
            ->select('bpb_correct_account_name AS account_name, bpb_account_no AS account_no, bpb_bank_code AS bank_code, bpb_amount AS amount, bpb_unique_id AS unique_id')
            ->from('bulk_payment_beneficiaries')
            ->where_in('bpb_av_status', $status_ids)
            ->where('bulk_payment_file', $file_id)
            ->get();
        //die($this->CI->db->last_query());

        return $resource->result_array();
    }

    /**
     * Verify that each of the accounts in the file exists in our DB, and update its status accordingly.
     *
     * @param $file_id
     * @return array list of total accounts found in our DB and total valid ones
     */
    public function validateAccountsAgainstLocalDB($file_id)
    {
        $ret_val = array('total_records' => 0, 'records_found' => 0, 'records_valid' => 0);

        //first count total number of accounts in file
        $result = $this->CI->db->select("COUNT(*) AS total")->from("bulk_payment_beneficiaries")->where("bulk_payment_file", $file_id)->get();
        $ret_val['total_records'] = $result->row()->total;

        $sql = "UPDATE bulk_payment_beneficiaries AS bpb
                JOIN validated_accounts AS va ON bpb.bpb_account_no = va.account_no AND bpb.bpb_bank_code = va.bank_code
                SET
                  bpb.bpb_av_response_code = CASE
                      WHEN (va.response_code = '08' OR va.response_code = '00') AND bpb.bpb_account_name = va.account_name THEN va.response_code
                      WHEN (va.response_code = '08' OR va.response_code = '00') AND bpb.bpb_account_name != va.account_name AND bpb.bpb_account_name = va.error_reason THEN '00'
                      WHEN (va.response_code = '08' OR va.response_code = '00') AND bpb.bpb_account_name != va.account_name AND bpb.bpb_account_name != va.error_reason THEN '08'
                      ELSE va.response_code END,
                  bpb.bpb_av_response_description = CASE
                      WHEN (va.response_code = '08' OR va.response_code = '00') AND bpb.bpb_account_name = va.account_name THEN va.response_description
                      WHEN (va.response_code = '08' OR va.response_code = '00') AND bpb.bpb_account_name != va.account_name AND bpb.bpb_account_name = va.error_reason THEN 'SUCCESSFUL'
                      WHEN (va.response_code = '08' OR va.response_code = '00') AND bpb.bpb_account_name != va.account_name AND bpb.bpb_account_name != va.error_reason THEN 'FAILED NAME VALIDATION'
                      ELSE va.response_description END,
                  bpb.bpb_av_error_reason = va.error_reason,
                  bpb.bpb_av_status = CASE
                      WHEN bpb.bpb_av_response_code = '00' THEN 14
                      WHEN bpb.bpb_av_response_code = '06' THEN 7
                      WHEN bpb.bpb_av_response_code != '00' AND bpb.bpb_av_response_code != '06' AND bpb.bpb_av_status = 1 THEN 15
                      ELSE  bpb.bpb_av_status END
                WHERE bpb.bulk_payment_file = $file_id
                  AND bpb.bpb_account_name != ''
                  AND bpb.bpb_account_no != ''
                  AND bpb.bpb_bank_code != ''";

        $this->CI->db->query($sql);

        $result = $this->CI->db
            ->select("bpf.bpf_id,
                        bpb1.records_found,
                        bpb2.records_valid", false)
            ->from("bulk_payment_files bpf")
            ->join("(
                        SELECT bpb.bulk_payment_file, COUNT(va.id) AS records_found
                        FROM bulk_payment_beneficiaries bpb
                        JOIN validated_accounts va ON bpb.`bpb_account_no` = va.account_no AND bpb.bpb_bank_code = va.bank_code
                        GROUP BY bpb.`bulk_payment_file`
                    ) bpb1", "bpf.bpf_id = bpb1.bulk_payment_file", "INNER")
            ->join("(
                        SELECT bpb.bulk_payment_file, COUNT(bpb_id) AS records_valid
                        FROM bulk_payment_beneficiaries bpb
                        WHERE bpb.bpb_av_response_code = '00'
                        GROUP BY bpb.`bulk_payment_file`
                    ) bpb2", "bpf.bpf_id = bpb2.bulk_payment_file", "LEFT")
            ->where("bpf.bpf_id", $file_id)
            ->get();

        //die('<pre>' . $this->CI->db->last_query());
        if ($result && $result->num_rows() > 0) {
            $row = $result->row_array();
            $ret_val['records_found'] = $row['records_found'];
            $ret_val['records_valid'] = $row['records_valid'];
        }

        //die('<pre>'.print_r($ret_val,1));
        return $ret_val;
    }

    /**
     * On routine basis, validate accounts with NIBSSPayPlus.
     *
     * Validate all accounts in our local DB that we got any of the following responses for:
     * 19 - Beneficiary Bank Not Available
     * 24 - Cannot Verify Account
     * 16 - Request Accepted
     * 06 - In Progress
     */
    public function routineAccountValidationRequestWithNIBSSPayPlus()
    {
        try {
            $response_codes = array('19', '24', '16', '06');

            //fetch accounts to be validated
            $accounts_for_validation = $this->CI->validated_account->allWithResponseCode($response_codes);

            if (!$accounts_for_validation)
                throw new Exception('No accounts for validation');

            //prepare the validation schedules
            $response = $this->prepareAccountsForValidation($accounts_for_validation);

            if (!$response['successful'])
                throw new Exception("Accounts could not be prepared for validation");

            $uploaded_schedules = 0;

            //send the validation schedules
            foreach ($response['message'] as $schedule_id => $schedule_ref) {
                $upload_response = $this->uploadAccountValidationSchedule($schedule_ref);

                if ($upload_response['successful'])
                    $uploaded_schedules++;
            }

            $ret_val = array('successful' => true, 'message' => $uploaded_schedules);

        } catch (Exception $e) {
            $ret_val = array('successful' => false, 'message' => $e->getMessage());
        }

        return $ret_val;
    }

    /**
     * Upload any account validation schedules not yet sent.
     *
     * @return array
     */
    public function uploadPendingAccountValidationSchedules()
    {
        //fetch all schedules not yet sent to NIBSS
        $status = $this->CI->status->findByDescription("PENDING");

        $pending_schedules = $this->CI->validation_schedule->all($status['status_id']);

        if (!empty($pending_schedules)) {

            $uploaded_schedules = 0;
            foreach ($pending_schedules as $schedule) {

                $upload_response = $this->uploadAccountValidationSchedule($schedule, false);

                if ($upload_response['successful'])
                    $uploaded_schedules++;
            }

            $ret_val = array('successful' => true, 'message' => $uploaded_schedules);

        } else
            $ret_val = array('successful' => false, 'message' => 'No pending account validation schedules');

        return $ret_val;
    }

    /**
     * Upload account validation schedule.
     *
     * @param $ref The schedule_ref or the entire schedule array
     * @param bool|true $is_ref
     * @return array
     */
    public function uploadAccountValidationSchedule($ref, $is_ref = true)
    {
        if ($is_ref) {
            $schedule = $this->CI->validation_schedule->find($ref, false);
        } else {
            $schedule = $ref;
        }

        //fetch the accounts in the schedule
        $accounts = $this->CI->validation_account->all($schedule['schedule_id']);

        //generate the xml request
        $xml = $this->generateUploadNewVendorsXML($schedule, $accounts);

        //file_put_contents('xml-debugger.txt', $xml);

        try {
            //make the webservice call
            $response = $this->sendToNIBSS('uploadNewVendors', $xml);

            if (!$response)
                throw new Exception('NIP Gateway DOWN!!!!');

            //check the response
            $response_obj = simplexml_load_string($response);
            $response_code = $response_obj->Header->Status;

            if (strlen($response_code) > 2)
                throw new Exception('NIP Gateway DOWN!!!!');

            //get the description for the status of the response
            $response_description = $this->getNibssPayPlusResponseDescription($response_code);

            if ($response_code == '16' || $response_code == '01')
                $status = $this->CI->status->findByDescription('IN PROGRESS');
            else
                $status = $this->CI->status->findByDescription('FAILED');

            //update the schedule
            $data = array(
                'status' => $status['status_id'],
                'response_code' => $response_code,
                'response_description' => $response_description
            );
            $this->CI->validation_schedule->update($schedule['schedule_id'], $data);

            //update all accounts in the schedule
            $this->CI->validation_account->updateMany($schedule['schedule_id'], $data);

            $ret_val = array('successful' => ($response_code == '16' ? true : false), 'message' => $response_description);

        } catch (Exception $ex) {
            $message = $ex->getMessage();

            $ret_val = array('successful' => false, 'message' => !empty($message) ? $message : "Cannot Connect to Payment Gateway");
        }

        return $ret_val;
    }

    /**
     * On routine basis, get status for all account validation schedules still in progress.
     *
     * @return array
     */
    public function routineAccountValidationStatusCheck()
    {
        //fetch all schedules still in progress
        $status = $this->CI->status->findByDescription("IN PROGRESS");

        $schedules_in_progress = $this->CI->validation_schedule->all($status['status_id']);

        if (!empty($schedules_in_progress)) {

            foreach ($schedules_in_progress as $schedule) {

                //if schedule is still in progress after 3 attempts to get its status
                //delete it along with all the accounts in it.
                if ($schedule['schedule_requery_attempts'] >= 3) {
                    $this->CI->validation_schedule->delete($schedule['schedule_id']);
                    continue;
                }

                $response = $this->getAccountValidationScheduleStatus($schedule['schedule_ref']);
            }

            //update our local NIBSSPayPlus DB
            $this->CI->validated_account->updateAll();

            //delete all completed schedules
            $status = $this->CI->status->findByDescription("COMPLETED");

            $completed_schedules = $this->CI->validation_schedule->all($status['status_id']);

            if (!empty($completed_schedules)) {
                foreach ($completed_schedules as $schedule) {
                    $this->CI->validation_schedule->delete($schedule['schedule_id']);
                }
            }

            $ret_val = array('successful' => true, 'message' => count($schedules_in_progress));

        } else {
            $ret_val = array('successful' => false, 'message' => 'No Validation Schedules IN PROGRESS');
        }

        return $ret_val;
    }

    /**
     * Get the status of an account validation schedule.
     *
     * @param $ref
     * @param bool|true $is_ref
     * @return array
     */
    public function getAccountValidationScheduleStatus($ref, $is_ref = true)
    {
        if ($is_ref) {
            $schedule = $this->CI->validation_schedule->find($ref, false);
        } else {
            $schedule = $ref;
        }

        $xml = $this->generateGetNewVendorsStatusXML($schedule['schedule_ref']);

        try {
            //make the webservice call
            $response = $this->sendToNIBSS('getNewVendorsStatus', $xml);

            if (!$response)
                throw new Exception('NIP Gateway DOWN!!!!');

            //check the response
            $response_obj = simplexml_load_string($response);
            if (!$response_obj)
                throw new Exception('NIP Gateway DOWN!!!!');

            $response_code = $response_obj->Header->Status;
            if (strlen($response_code) > 2)
                throw new Exception('NIP Gateway DOWN!!!!');

            //fetch all statuses in DB
            $statuses = $this->CI->status->all();

            //fetch all NIBSSPayPlus responses
            $npp_responses = $this->getNIBSSPayPlusResponseDescription();

            //get the appropriate NIBSSPayPlus description for this response code
            foreach ($npp_responses as $npp_response) {
                if ($npp_response['code'] == $response_code) {
                    $response_description = $npp_response['description'];
                    break;
                }
            }

            if (!in_array($response_code, array('00', '12'))) {

                if ($response_code == '06') {
                    foreach ($statuses as $status) {
                        if ($status['status_description'] == 'IN PROGRESS')
                            break;
                    }
                } else {
                    foreach ($statuses as $status) {
                        if ($status['status_description'] == 'FAILED')
                            break;
                    }
                }

                //update the schedule
                $data = array(
                    'status' => $status['status_id'],
                    'response_code' => $response_code,
                    'response_description' => $response_description,
                    'log_attempts' => true
                );
                $this->CI->validation_schedule->update($schedule['schedule_id'], $data);

                //update all accounts in the schedule
                $this->CI->validation_account->updateMany($schedule['schedule_id'], $data);

            } else {

                //check the existence of vendor accounts
                if (empty($response_obj->Vendor)) {
                    throw new Exception("strange: No vendors returned in response!");
                }

                $any_account_still_in_progress = false;

                foreach ($response_obj->Vendor as $vendor_record) {

                    $code_2 = $vendor_record->Status;

                    //get the description for this response code from the list of responses
                    foreach ($npp_responses as $npp_response) {
                        if ($npp_response['code'] == $code_2) {
                            $desc_2 = $npp_response['description'];
                            break;
                        }
                    }

                    //get the appropriate status to flag
                    if ($code_2 == '00') {
                        foreach ($statuses as $status) {
                            if ($status['status_description'] == 'VALID')
                                break;
                        }
                    } elseif ($code_2 == '06') {
                        foreach ($statuses as $status) {
                            if ($status['status_description'] == 'IN PROGRESS')
                                break;
                        }

                        $any_account_still_in_progress = true;
                    } else {
                        foreach ($statuses as $status) {
                            if ($status['status_description'] == 'INVALID')
                                break;
                        }
                    }

                    $data = array(
                        'status' => $status['status_id'],
                        'response_code' => $code_2,
                        'response_description' => $desc_2,
                        'error_reason' => !empty($vendor_record->ErrorReason) ? $vendor_record->ErrorReason : null
                    );

                    $details = array(
                        'account_no' => $vendor_record->AccountNumber,
                        'account_name' => $vendor_record->AccountName,
                        'bank_code' => $vendor_record->SortCode
                    );
                    $this->CI->validation_account->updateByDetails($details, $data);
                }

                if ($any_account_still_in_progress) {
                    foreach ($statuses as $status) {
                        if ($status['status_description'] == 'IN PROGRESS')
                            break;
                    }
                } else {
                    foreach ($statuses as $status) {
                        if ($status['status_description'] == 'COMPLETED')
                            break;
                    }
                }

                //update the schedule
                $data = array(
                    'status' => $status['status_id'],
                    'response_code' => $response_code,
                    'response_description' => $response_description,
                    'log_attempts' => true
                );
                $this->CI->validation_schedule->update($schedule['schedule_id'], $data);
            }

            $ret_val = array('successful' => (in_array($response_code, array('00', '06', '12')) ? true : false), 'message' => $response_description);

        } catch (Exception $e) {
            $ret_val = array('successful' => false, 'message' => $e->getMessage());
        }

        return $ret_val;
    }

    /**
     * Generate payment schedules for all payment files in a payment file group.
     *
     * @param $payment_file_group_id
     * @return array
     */
    public function preparePaymentSchedules($payment_file_group_id, $user_id = 0)
    {
        $userId = !empty($user_id) ? $user_id : $_SESSION['myuserId'];

        try {
            $this->CI->db->trans_begin();

            //save the payment schedule_group
            $data = array(
                'payment_file_group' => $payment_file_group_id,
                'status' => 1,
                'created_by' => $userId
            );
            $psg_id = $this->CI->payment_schedule_group->save($data);

            //generate a payment schedule for each payment file in the group
            $payment_files = $this->CI->payment_file->all($payment_file_group_id);

            foreach ($payment_files as $file) {
                $schedule_ref = $this->CI->basic_functions->create_random_number_string(12);

                $data = array(
                    'payment_schedule_group' => $psg_id,
                    'payment_file' => $file['payment_file_id'],
                    'source_account' => $file['source_account_id'],
                    'schedule_ref' => "$schedule_ref",
                    'status' => 1 //PENDING
                );
                $ps_id = $this->CI->payment_schedule->save($data);

                $payment_file_ids[] = $file['payment_file_id'];
                $include_commission_accounts = $userId == 1 ? true : false;
                $payment_files_beneficiaries = $this->CI->payment_file_beneficiary->all($payment_file_ids, $include_commission_accounts);

                //save the beneficiaries in each payment file
                $schedule_beneficiaries = array();
                foreach ($payment_files_beneficiaries as $beneficiary) {
                    if ($beneficiary['payment_file'] == $file['payment_file_id']) {

                        $schedule_beneficiaries[] = array(
                            'payment_schedule' => $ps_id,
                            'payment_beneficiary' => $beneficiary['beneficiary_id'],
                            'account_name' => $beneficiary['beneficiary_account_name'],
                            'account_no' => $beneficiary['beneficiary_account_no'],
                            'bank_code' => $beneficiary['beneficiary_bank_code'],
                            'amount' => $beneficiary['beneficiary_amount'],
                            'commission_account' => !empty($beneficiary['beneficiary_is_commission_account']) ? true : false,
                            'payment_status' => 1
                        );
                    }
                }
                $this->CI->payment_schedule_beneficiary->saveMany($schedule_beneficiaries);
            }

            $this->CI->db->trans_commit();

            $retVal = array('successful' => true, 'message' => $psg_id);
        } catch (Exception $ex) {
            $this->CI->db->trans_rollback();
            $retVal = array('successful' => false, 'message' => $ex->getMessage());
        }

        return $retVal;
    }

    /**
     * Prepare validation schedules for accounts to be validated with NIBSSPayPlus.
     *
     * @param $accounts
     * @param int $schedule_size max number of accounts to put in 1 schedule
     * @return array
     */
    protected function prepareAccountsForValidation($accounts, $schedule_size = 1000)
    {
        try {
            $schedule_refs = array();

            $this->CI->db->trans_begin();

            for ($i = 0; $i < count($accounts); $i += $schedule_size) {

                //create the schedule
                $schedule_id_and_ref = $this->CI->validation_schedule->create();
                if (!$schedule_id_and_ref) {
                    throw new Exception("Could not create Schedule");
                }

                $schedule_refs[$schedule_id_and_ref[0]] = $schedule_id_and_ref[1];

                //add accounts to this schedule
                $schedule_accounts = array();
                foreach ($accounts as $key => $account) {

                    if ($key >= $i) {
                        $account['schedule_id'] = $schedule_id_and_ref[0];
                        $schedule_accounts[] = $account;

                        if (count($schedule_accounts) == $schedule_size) {
                            $this->CI->validation_account->createMany($schedule_accounts);
                            $schedule_accounts = array();
                            break;
                        }
                    }
                }

                if (!empty($schedule_accounts)) {
                    $this->CI->validation_account->createMany($schedule_accounts);
                }
            }

            $this->CI->db->trans_commit();

            $ret_val = array('successful' => true, 'message' => $schedule_refs);

        } catch (Exception $e) {
            $this->CI->db->trans_rollback();

            $ret_val = array('successful' => false, 'message' => $e->getMessage());
        }

        return $ret_val;
    }

    protected function logAccountsForValidation($accounts_list, $file_name, $file_description)
    {
        //generate a schedule ID and log it against the file name
        $schedule_id = $this->CI->basic_functions->create_random_number_string();

        $this->CI->db->insert('av_schedules', array(
            'avs_file_name' => $file_name,
            'avs_file_description' => $file_description,
            'avs_schedule_id' => $schedule_id,
            'avs_created_at' => date('Y-m-d H:i:s')
        ));

        $avs_id = $this->CI->db->insert_id();

        //log every account on the list against the schedule id
        if ($avs_id > 0) {
            $_data = array();

            foreach ($accounts_list as $account) {
                $_data[] = array(
                    'avs_id' => $avs_id,
                    'ava_vendor_no' => !empty($account['unique_id']) ? $account['unique_id'] : $this->CI->basic_functions->create_random_number_string(5),
                    'ava_account_name' => $account['account_name'],
                    'ava_account_no' => $account['account_no'],
                    'ava_bank_code' => $account['bank_code'],
                    'ava_created_at' => date('Y-m-d H:i:s')
                );
            }

            $this->CI->db->insert_batch('av_accounts', $_data);
        }

        return $schedule_id;
    }

    /**
     * Generate the XML for uploading payment schedule,
     * ie. using NIBSSPayPlus webservice method uploadPaymentSchedule.
     *
     * @param $schedule
     * @param $beneficiaries
     * @return string
     */
    public function generateUploadPaymentScheduleXML($schedule, $beneficiaries)
    {
        $this->CI->load->helper('fluid_xml');

        $salt = rand(100000, 1000000000);
        $str2hash = "{$this->css_client_id}-{$this->css_secret_key}-{$salt}";
        $mac = hash('sha512', $str2hash);

        $xml = new \FluidXml\FluidXml('PaymentRequest');

        $xml->addChild('Header', true)
            ->addChild('FileName', $schedule['schedule_ref'])
            ->addChild('ScheduleId', $schedule['schedule_ref'])
            ->addChild('DebitSortCode', $schedule['source_account_bank_code'])
            ->addChild('DebitAccountNumber', $schedule['source_account_no'])
            ->addChild('ClientId', $this->css_client_id)
            ->addChild('Salt', $salt)
            ->addChild('Mac', $mac);

        foreach ($beneficiaries as $beneficiary) {
            $xml->addChild('PaymentRecord', true)
                ->addChild('Beneficiary', $beneficiary['beneficiary_account_name'])
                ->addChild('Amount', $beneficiary['beneficiary_amount'])
                ->addChild('AccountNumber', $beneficiary['beneficiary_account_no'])
                ->addChild('SortCode', $beneficiary['beneficiary_bank_code'])
                ->addChild('Narration', $schedule['narration']);
        }

        $xml->addChild('HashValue', ' ');

        return $xml->xml();
    }

    /**
     * Generate the XML for requesting status of payment schedule,
     * ie. using NIBSSPayPlus webservice method getPaymentScheduleStatus.
     *
     * @param $schedule
     * @return string
     */
    public function generateGetPaymentScheduleStatusXML($schedule)
    {
        $this->CI->load->helper('fluid_xml');

        $salt = rand(100000, 1000000000);
        $str2hash = "{$this->css_client_id}-{$this->css_secret_key}-{$salt}";
        $mac = hash('sha512', $str2hash);

        $xml = new \FluidXml\FluidXml('StatusRequest');

        $xml->addChild('Header', true)
            ->addChild('ScheduleId', $schedule['schedule_ref'])
            ->addChild('ClientId', $this->css_client_id)
            ->addChild('Salt', $salt)
            ->addChild('Mac', $mac);

        $xml->addChild('HashValue', ' ');

        return $xml->xml();
    }

    /**
     * Generate the XML for uploading accounts for validation,
     * ie. using the webservice method uploadNewVendors.
     *
     * @param $schedule
     * @param $accounts
     * @return string
     */
    public function generateUploadNewVendorsXML($schedule, $accounts)
    {
        $this->CI->load->helper('fluid_xml');

        $salt = rand(100000, 1000000000);
        $str2hash = "{$this->css_client_id}-{$this->css_secret_key}-{$salt}";
        $mac = hash('sha512', $str2hash);

        $xml = new \FluidXml\FluidXml('VendorUploadRequest');

        $xml->addChild('Header', true)
            ->addChild('ScheduleId', $schedule['schedule_ref'])
            ->addChild('ClientId', $this->css_client_id)
            ->addChild('Salt', $salt)
            ->addChild('Mac', $mac);

        foreach ($accounts as $account) {
            $xml->addChild('Vendor', true)
                ->addChild('VendorNumber', $account['vendor_no'])
                ->addChild('AccountName', $account['account_name'])
                ->addChild('AccountNumber', $account['account_no'])
                ->addChild('SortCode', $account['bank_code']);
        }

        $xml->addChild('HashValue', ' ');

        return $xml->xml();
    }

    /**
     * Generate XML for requesting the status of accounts sent for validation,
     * ie. using NIBSSPayPlus webservice method getNewVendorsStatus.
     *
     * @param $schedule_ref
     * @return mixed|string
     */
    public function generateGetNewVendorsStatusXML($schedule_ref)
    {
        $this->CI->load->helper('fluid_xml');

        $salt = rand(100000, 1000000000);
        $str2hash = "{$this->css_client_id}-{$this->css_secret_key}-{$salt}";
        $mac = hash('sha512', $str2hash);

        $xml = new \FluidXml\FluidXml('StatusRequest');

        $xml->addChild('Header', true)
            ->addChild('ScheduleId', $schedule_ref)
            ->addChild('ClientId', $this->css_client_id)
            ->addChild('Salt', $salt)
            ->addChild('Mac', $mac);

        $xml->addChild('HashValue', ' ');

        return $xml->xml();
    }

    protected function getAccountsForValidationInSchedule($schedule_id)
    {
        $resource = $this->CI->db
            ->select('ava.*')
            ->from('av_accounts AS ava')
            ->join('av_schedules AS avs', "avs.avs_id = ava.avs_id AND avs.avs_schedule_id = '{$schedule_id}'")
            ->get();

        return $resource->num_rows() > 0 ? $resource->result_array() : null;
    }

    /**
     * Make webservice call to NIBSS and log the call.
     *
     * @param $method
     * @param $request
     * @return string
     */
    public function sendToNIBSS($method, $request)
    {
        set_time_limit(0);

        try {
            $client = new SoapClient($this->css_webservice_url, array("trace" => 1, "exceptions" => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
            $response = $client->$method($request);

            //log the Webservice call request and response
            $log_data = array('url' => $this->css_webservice_url, 'method' => $method, 'request' => $request, 'response' => $response);
            $this->logWebserviceCall($log_data);

        } catch (Exception $e) {
            $response = $e->getMessage();
        }

        return $response;
    }

    /**
     * Get the description for a NIBSSPayPlus response code or several response codes.
     *
     * @param $code
     * @return null
     */
    protected function getNIBSSPayPlusResponseDescription($code = null)
    {
        if ($code) {
            if (is_array($code))
                $this->CI->db->where_in("response_code", $code);
            else
                $this->CI->db->where("response_code", $code);
        }

        $result = $this->CI->db->select('response_code AS code, description')->from('nibsspayplus_responses')->get();

        if ($result && $result->num_rows() > 0)
            $ret_val = $result->num_rows() == 1 ? $result->row()->description : $result->result_array();
        else
            $ret_val = null;

        return $ret_val;
    }

    /**
     * Log a webservice call to NIBSSPayPlus.
     *
     * @param $data
     * @return mixed
     */
    protected function logWebserviceCall($data)
    {
        return $this->CI->db->insert('payments_webservice_log', array(
            'url' => $data['url'],
            'method' => $data['method'],
            'request' => $data['request'],
            'response' => $data['response'],
        ));
    }

    protected function updateAccountValidationSchedule($data)
    {
        $update_data = array('avs_status' => $data['status'], 'avs_response_code' => $data['response_code'], 'avs_response_description' => $data['response_description']);

        return $this->CI->db->update("av_schedules", $update_data, array('avs_schedule_id' => $data['schedule_id']));
    }

    protected function updateAccountsInValidationSchedule($data)
    {
        $sql = "UPDATE av_accounts AS ava
                JOIN av_schedules AS avs ON ava.avs_id = avs.avs_id AND avs.avs_schedule_id = ?
                SET ava.ava_status = ?, ava.ava_response_code = ?, ava.ava_response_description = ?";

        $values = array($data['schedule_id'], $data['status'], $data['response_code'], $data['response_description']);

        return $this->CI->db->query($sql, $values);
    }

    protected function updateAccount($data)
    {
        $sql = "UPDATE av_accounts AS ava
                JOIN av_schedules AS avs ON ava.avs_id = avs.avs_id AND avs.avs_schedule_id = ?
                SET ava.ava_status = ?, ava.ava_response_code = ?, ava.ava_response_description = ?, ava.ava_error_reason = ?
                WHERE ava.ava_account_no = ?";

        $values = array($data['schedule_id'], $data['status'], $data['response_code'], $data['response_description'], $data['error_reason'], $data['account_number']);

        return $this->CI->db->query($sql, $values);
    }
}