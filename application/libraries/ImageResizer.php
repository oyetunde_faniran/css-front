<?php


class ImageResizer{

	public $oldImage;	//The full path to the original file name including the file name and extension
	public $newImage;	//The path where the new image will be saved with the file name only (no extension)

	public function __construct($oldImage="", $newImage=""){
		$this->oldImage = $oldImage;
		$this->newImage = $newImage;
	}


	protected function aspectRatioDimension($dimArray, $maxWidth, $maxHeight){//$maxHeight, $maxWidth
		$width = $dimArray["width"];
		$height = $dimArray["height"];

		if ($width <= $maxWidth && $height <= $maxHeight){
			//Original Image OK
			$result = array (
							"width" => $width,
							"height" => $height
						);
		} else {
				$tries = 1;
				do{
					switch ($tries){
						case 1:	//Resize by width
								$newWidth = $width > $maxWidth ? $maxWidth : $width;
								$ratio = (float)$height / (float)$width;
								$newHeight = round($newWidth * $ratio);
								$result = array (
												"width" => $newWidth,
												"height" => $newHeight
											);
								break;
						case 2:	//Resize by height
								$newHeight = $height > $maxHeight ? $maxHeight : $height;
								$ratio = (float)$width / (float)$height;
								$newWidth = round($newHeight * $ratio);
								$result = array (
												"width" => $newWidth,
												"height" => $newHeight
											);
								break;
						case 3:	//Resize by forcing the large side in the actual image to be the smaller side in the max dimension
								$g = $maxHeight < $maxWidth ? $maxHeight : $maxWidth;	//Get the lower side in the maximum dimension
//								$g = $height > $width ? $height : $width;
								//Get the larger side in the actual dimension
								if ($height > $width){
									$gSide = "height";
									$lSide = "width";
								} else {
										$gSide = "width";
										$lSide = "height";
									}
								//Now resize
								$ratio = (float)$$lSide / (float)$$gSide;
								$newLow = round ($g * $ratio);
								$result = array (
												$gSide => $g,
												$lSide => $newLow
											);
					}	//END switch

					$tries++;

				} while (($newWidth > $maxWidth || $newHeight > $maxHeight) && $tries <= 3);
			}

		return $result;

	}



	public function resize ($maxWidth, $maxHeight, $extension = "jpg"){
        //die("$maxWidth, $maxHeight, $extension = jpg....***");
		if (empty($this->oldImage) || empty($this->newImage) || !file_exists($this->oldImage))
			die ("Please, set the path to the image to be resized and where to store the output.");

		if (empty($maxHeight) || empty($maxWidth))
			die ("Invalid arguments: Arguments must be greater than zero (0).");

//		$f = fopen($this->oldImage, "r");//$f = fopen($name, "r");
		if (@file_exists($this->oldImage))
			$x = @file_get_contents($this->oldImage);

		if(isset($x)){
//			while ($s = fread($f, 1024))
//				$x .= $s;
			@ini_set("memory_limit", "128M");
			$im = @imagecreatefromstring($x);
			if($im){
				$d = array(
							"width" => @imagesx($im),
							"height" => @imagesy($im)
						);

				//Get the new image size with the aspect ratio maintained
				$dim = $this->aspectRatioDimension($d, $maxWidth, $maxHeight); //$dim = aspectRatioDimension($d);

				$nim = @imagecreatetruecolor($dim["width"], $dim["height"]);
                //die ($nim);
                //die($this->newImage . "|||" . trim($extension, '.'));
				if(@imagecopyresampled($nim, $im, 0, 0, 0, 0, $dim["width"], $dim["height"], @imagesx($im), @imagesy($im))){
					switch (trim($extension, '.')){
						case "jpg":
							@imagejpeg($nim, $this->newImage . ".jpg");
							break;
						case "gif":
							@imagegif($nim, $this->newImage . ".gif");
							break;
						case "png":
							@imagepng($nim, $this->newImage . ".png");
							break;
						default:
							@imagejpeg($nim, $this->newImage . ".jpg");
							break;
					}
				}
				return true;
			} else return false;
		} else return false;
	}
}