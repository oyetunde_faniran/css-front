<?php
/**
 * Handles all report formatting functions
 *
 * @package System_Administration
 *
 * @author Tunde
 */
if (!defined('BASEPATH')){
    exit('No direct script access allowed');
}

class Report_formatter {

    function __construct() {
		$ci = &get_instance();
		$ci->load->library('number_word');
		$this->number_word = $ci->number_word;
    }

	/**Handles the report for "All Accounts => Latest total balance in all accounts"
		@params Array $report	An array containing the initial report data fetched from the database
	*/
	public function _prepareTotalTotal($report){
		//Add the total amount in words
		$report['amt_in_words'] = $this->number_word->mynumber_word($report['total']);
		return $report;
	}	//END _prepareTotalTotal()
	
	
	
	/**Handles the report for "Per Account => Latest total"
		@params Array $report	An array containing the initial report data fetched from the database
	*/
	public function _prepareAccountTotal($report){
		//Add the total amount in words
		$report_temp['data'] = $report;
		$report_temp['account_count'] = count($report);
		$report_temp['latest_date'] = $report[0]['latest_date'];
		$report_temp['number_word'] = $this->number_word;	//Send the number to word converter object for use in the view
		
		//Form the data for the charts
		$chart_data_temp = '';
		foreach ($report as $acc){
			$chart_data_temp .= '{label: "' . $acc['date'] . '", value: "' . $acc['trans_value'] . '"},';
		}
		$chart_data = trim($chart_data_temp, ',');
//        die($chart_data);
		$report_temp['additional_js'] = '/*Morris.Donut({	//Pie Chart
										  element: "all-accounts-pie-chart",
										  data: [
											' . $chart_data . '
										  ]
										});*/
										Morris.Bar({	//Bar Chart
										  element: "all-accounts-bar-chart",
										  data: [
											' . $chart_data . '
										  ],
										  xkey: "label",
										  ykeys: ["value"],
										  labels: ["Transactions"]
										});';
		$report = $report_temp;
		return $report;
	}	//END _prepareAccountTotal()

	
	
	
	/**Handles the report for "Per Account => Latest total"
		@params Array $report	An array containing the initial report data fetched from the database
	*/
	public function _prepareBankTotal($report){
		//Add the total amount in words
		$report_temp['data'] = $report;
		$report_temp['account_count'] = count($report);
		$report_temp['latest_date'] = $report[0]['latest_date'];
		$report_temp['number_word'] = $this->number_word;	//Send the number to word converter object for use in the view
		
		//Form the data for the charts
		$chart_data_temp = '';
		foreach ($report as $acc){
			$chart_data_temp .= '{label: "' . $acc['bank_name'] . '", value: "' . $acc['bank_amount'] . '"},';
		}
		$chart_data = trim($chart_data_temp, ',');
		$report_temp['additional_js'] = 'Morris.Donut({	//Pie Chart
										  element: "all-accounts-pie-chart",
										  data: [
											' . $chart_data . '
										  ]
										});
										Morris.Bar({	//Bar Chart
										  element: "all-accounts-bar-chart",
										  data: [
											' . $chart_data . '
										  ],
										  xkey: "label",
										  ykeys: ["value"],
										  labels: ["Account Balance"]
										});';
		$report = $report_temp;
		return $report;
	}	//END _prepareBankTotal()
	
	
	
	
	
	/**Handles the report for "Per Account => Latest total"
		@params Array $report	An array containing the initial report data fetched from the database
	*/
	public function _prepareTrendView($report){
		//Add the total amount in words
		$report_temp['data'] = $report;
		//$report_temp['account_count'] = count($report);
		$report_temp['latest_date'] = $report[0]['latest_date'];
		//$report_temp['number_word'] = $this->number_word;	//Send the number to word converter object for use in the view
		
		//Form the data for the charts
		$chart_data_temp = '';
		foreach ($report as $acc){
			$chart_data_temp .= '{label: "' . $acc['accbal_datetime'] . '", amount: ' . $acc['accbal_amount'] . '},';
		}
		$chart_data = trim($chart_data_temp, ',');

		$report_temp['additional_js'] = "Morris.Line({
										  element: 'all-accounts-bar-chart',
										  data: [
											$chart_data
										  ],
										  xkey: 'label',
										  ykeys: ['amount'],
										  labels: ['Account Balance']
										});";
		$report = $report_temp;
		return $report;
	}	//END _prepareCPH()
	
	
	

}	//END class