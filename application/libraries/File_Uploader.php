<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of File_Uploader
 *
 * @author Tunde
 */
class File_Uploader {

    public $max_size;
    public $max_size_display;

    function __construct($max_size = 0, $max_size_display = ''){
        $this->max_size = $max_size != 0 ? $max_size : 1048576;
        $this->max_size_display = !empty($max_size_display) ? $max_size_display : '1MB';
        $this->ci = &get_instance();
        $this->ci->load->library('ImageResizer');
    }



    public function doUpload($up_file, $destination, $type = 'image', $delete = true, $max_width = 100, $max_height = 100){
        //die($this->max_size. '<pre>' . print_r($up_file, true) . ", $destination, $type = 'image', $delete = true, $max_width = 100, $max_height = 100");
        try {
            //Error State
            if ($up_file['error'] != 0){
                throw new Exception('File not uploaded successfully.');
            }

            //File Size
            if ($up_file['size'] > $this->max_size){
                throw new Exception('The file size cannot be more than ' . $this->max_size_display . '.');
            }

            switch($type){
                case 'image':
                    $ext = $this->uploadImage($up_file, $destination, $max_width, $max_height);
                    break;
                default:
                    throw new Exception('Unknown upload type');
            }   //END switch

            //Delete the file if required
            $tmp_name = isset($up_file['tmp_name']) ? $up_file['tmp_name'] : '';
            if ($delete && file_exists($tmp_name)){
                @unlink($tmp_name);
            }

            $ret_val = array(
                'status' => true,
                'msg' => 'ok',
                'ext' => $ext
            );
        } catch (Exception $ex) {
//            die($ex->getMessage());
            $ret_val = array(
                'status' => false,
                'msg' => $ex->getMessage(),
                'ext' => ''
            );
        }
        //die('<pre>' . print_r($ret_val, true));
        return $ret_val;
    }



    protected function uploadImage($up_file, $destination, $max_width = 100, $max_height = 100){
        //die(print_r($up_file, true) . "....$destination, $max_width = 100, $max_height = 100");
        //Get the needed variables
        $tmp_name = isset($up_file['tmp_name']) ? $up_file['tmp_name'] : '';

        //Confirm that the uploaded file is a true image file and that it's either a PNG or JPG file
        $image_info = getimagesize($tmp_name);
        $image_type = !empty($image_info['mime']) ? $image_info['mime'] : '';
        switch($image_type){
            case 'image/jpeg':  //JPEG
            case 'image/pjpeg':
                $ext = '.jpg';
                break;
            case 'image/png':   //PNG
                $ext = '.png';
                break;
            default:
                throw new Exception('Sorry, only JPEG and PNG image files are allowed.');
        }   //END switch

        $this->imageresizer->oldImage = $tmp_name;
        $this->imageresizer->newImage = $destination;
        $this->imageresizer->resize($max_width, $max_height, $ext);
        return $ext;
    }   //END uploadImage()
    
    
    
    protected function uploadCSV($file_loc){
        return $file_loc;
    }


}   //END class