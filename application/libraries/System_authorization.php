<?php
/**
 * Handles all System Authorization operations
 *
 * @package System_Administration
 *
 * @author Tunde
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class System_authorization {

    function __construct(){
        $this->ci = &get_instance();
    }   //END __construct()




    /**
     * Checks if a user has the correct privilege to visit a particular page
     * @param int $min_uri_segments The minimum number of segments in every URL
     * @param array $free2view      An array that contains all the pages that can be visited without been logged in
     * @return boolean              Returns TRUE if the user has the privilege, FALSE otherwise
     * @throws Exception
     */
    public function authorize($min_uri_segments = 2, $free2view = array()){
        try {
//            $segs = $this->ci->uri->segment_array();
//            die('<pre>' . print_r($segs, true));
//            $dis_request = $ci->uri->ruri_string();
//            echo "<h1>$dis_request</h1>";

//            return true;
            //Get the URI string and extract the folder name, controller & method without parameters
            $dis_request = $this->ci->uri->ruri_string();
//            echo('<pre>' . print_r($_SESSION, true));
//            die($dis_request);
            $request_array = explode('/', $dis_request);
            //echo('<pre>BEFORE - ' . print_r($request_array, true));
            if($request_array[0] == 'sys-admin'){
                $start_index = 1;
                $stop_index = $min_uri_segments + 1;
            } else {
                $start_index = 0;
                $stop_index = $min_uri_segments;
            }
            //die("\n\n<pre>AFTER - " . print_r($request_array, true));
            $request_temp = '';
            for($counter = $start_index; $counter < $stop_index; $counter++){
                $request_temp .= !empty($request_array[$counter]) ? '/' . $request_array[$counter] : '';
            }
            $request = ltrim($request_temp, '/');
            //echo ($request . ' --- ' . uri_string());

            //If it's a free-to-view page, then ALLOW
//            $free2view = array('',                                    //Home Page
//                               'css/index',                                    //Home Page
//                                'sys-admin/access_denied',            //Access Denied Page
//                                'sys-admin/login/',                   //Log-In Form-Submission Handler
//                                'sys-admin/login/index',              //Log-In Form-Submission Handler
//                                'sys-admin/login/login_page',         //Log-In Form-Submission Handler
//                                'sys-admin/login/forgot_password',    //Forgot Password
//                                'sys-admin/login/logout'              //Log Out
//                         );
            if (in_array($request, $free2view)){
                throw new Exception('', 1);
            }

            //Get needed session variables and confirm that they are available
            $user_id = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : 0;
            $user_privileges = isset($_SESSION['user_privileges']) ? $_SESSION['user_privileges'] : array();
            if (empty($user_id) || empty($user_privileges)){
                redirect('/');
            }

            //Search the privileges array for the current request
            $found = FALSE;
            //die("\n\n<pre>" . print_r($user_privileges, true));
            foreach ($user_privileges as $priv){
                if ($priv['url'] == $request){
                    $found = TRUE;
                    break;
                }
            }
            $ret_val = $found;
        } catch (Exception $e) {
            $code = $e->getCode();
            $ret_val = $code == 1 ? TRUE : FALSE;
        }
        return $ret_val;
    }   //END authorize()



    public function isCaptchaOK($captcha_text, $captcha_token){
        $token_md5 = md5($captcha_token);
        $captcha_expected = !empty($_SESSION[session_id() . "cod_confirmimage_$token_md5"]) ? $_SESSION[session_id() . "cod_confirmimage_$token_md5"] : '';
        if(empty($captcha_expected) || $captcha_text != $captcha_expected){
            $ret_val = false;
        } else {
            $ret_val = true;
        }
        return $ret_val;
    }   //END isCaptchaOK()
    
    
    
    
    public function hasAccess($priv_code){
        $ret_val = !empty($_SESSION['user_privileges'][$priv_code]);
        return $ret_val;
    }


}   //END class