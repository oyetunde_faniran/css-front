<?php

/**
 * Description of base
 *
 * @author Segun Ojo
 */
class Basic_functions
{

    private $ci;

    public function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->load->database();
    }


    public function create_random_number_string($length = 10)
    {
        $characters = '1234567890';
        $string = '';
        for ($p = 0; $p < $length; $p++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }
        return $string;
    }


    public function create_random_name($length = 10)
    {
        $characters = 'abcdefghijklmnpqrstuvwxyz123456789';
        $string = '';
        for ($p = 0; $p < $length; $p++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }
        return $string;
    }

    public function create_random_string($length = 10)
    {
        $characters = 'abcdefghijklmnpqrstuvwxyz';
        $string = '';
        for ($p = 0; $p < $length; $p++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }
        return $string;
    }

    /**
     * This is used to upload files.
     * @param string $input_name - the name of the file input
     * @param array $accepted_types - an array of the acceptable mime types
     * @param bool $image_only - Indicates whether it is only image files that should be allowed, default is false. You can set it to true to restrict files to image only
     * @param string $destination_folder - The relative path to the folder the file will be stored
     * @return array - status is boolean indicating success or failure. msg is string, communicating the result of the operation. uploaded_file_name is string - the new name of the uploaded file
     */
    public function uploadFile($destination_folder = "documents/", $input_name = "doc", $accepted_types = array("image/jpeg", "image/jpg", "image/pjpeg", "image/pjpg", "video/flv", "video/x-flv", 'video/mp4', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'), $image_only = FALSE)
    {
        $report['status'] = FALSE;
        $report['msg'] = 'No upload file found';
        if (!empty($_FILES[$input_name]['tmp_name'])) {

            if ($image_only) {
                $properties = getimagesize($_FILES[$input_name]['name']);

                if (!$properties) {
                    return $report['msg'] = "File you are trying to upload is not an image file";
                }
            }
            $mime_type = $_FILES[$input_name]['type'];
            if ((!empty($accepted_types)) && (!in_array($mime_type, $accepted_types))) {//Start mime type check
                $report['msg'] = "The file you are trying to upload is not in the acceptable format. It is $mime_type";
            } else {
                if (filesize($_FILES[$input_name]['tmp_name']) > 20000000) {
                    $report['msg'] = "File is too large - more than 20 Mb";
                } else {
                    $file_parts = pathinfo($_FILES[$input_name]['name']);
                    $file_extension = $file_parts['extension'];
                    $random_name = $this->create_random_name();
                    $final_name = $random_name . "." . $file_extension;
                    $destination_path = $destination_folder . $final_name;
                    if (file_exists($destination_path)) {
                        unlink($destination_path);
                    }
                    if (move_uploaded_file($_FILES[$input_name]['tmp_name'], $destination_path)) {
                        $report['msg'] = "File successfully uploaded.";
                        $report['status'] = TRUE;
                        $report['uploaded_file_name'] = $final_name;
                        $report['mime_type'] = $mime_type;
//                        return $report;
                    } else {
                        $report['msg'] = "System Error! Unable to add the selectde file. Please retry.";
                    }
                }
            }//mime type check ends here
        }
        return $report;
    }


    /**
     * Deletes a given file with the filename given if it exists
     * @param string $filename - The name of the file to be deleted with the full directory path
     * @return array $report - msg - string - result of the operation. status - boolean - Whether or not the operation was successfull. TRUE if deleted. Otherwise, FALSE;
     */
    public function delete_file($filename)
    {
        $report['msg'] = "No file deleted";
        $report['status'] = FALSE;
        if (file_exists($filename)) {
            if (unlink($filename)) {
                $report['status'] = TRUE;
                $report['msg'] = "File successfully deleted";
            }
        }
        return $report;
    }


    public function updateLastActivity()
    {
        if (isset($_SESSION['myuserId'])) {
            $now = date('Y-m-d H:i:s');
            $user_id = $_SESSION['myuserId'];
            $data = array('userlog_last_activity' => "$now");
            $this->ci->db->where('user_id', "$user_id");
            $this->ci->db->update('upl_users_login', $data);

        }
        return;
    }

    public function formatDateLogin($date)
    {
        if ($date == "0000-00-00 00:00:00") {
            return "Never";
        }
        $date = trim($date);
        $retVal = "";
        $date_time_array = explode(" ", $date);
        $time = $date_time_array[1];
        $time_array = explode(":", $time);


        $date_array = explode("-", "$date");
        $day = $date_array['2'];
        $month = $date_array['1'];
        $year = $date_array['0'];
        if ($year > 0) {
            @ $ddate = mktime(12, 12, 12, $month, $day, $year);
            @ $retVal = date("j M Y", $ddate);

        }
        if (!empty($time)) {
            $hr = $time_array[0];
            $min = $time_array[1];
            $sec = $time_array[2];
            @ $ddate = mktime($hr, $min, $sec, $month, $day, $year);
            @ $retVal = date("j M Y, H:i", $ddate);


        }
        return $retVal;
    }

    public function formatDate($date, $withTime = TRUE)
    {
        if ($date == "0000-00-00 00:00:00") {
            return "Never";
        }
        $date = trim($date);
        $retVal = "";
        $date_time_array = explode(" ", $date);
        $time = $date_time_array[1];
        $time_array = explode(":", $time);


        $date_array = explode("-", "$date");
        $day = $date_array['2'];
        $month = $date_array['1'];
        $year = $date_array['0'];
        if ($year > 0) {
            @ $ddate = mktime(12, 12, 12, $month, $day, $year);
            @ $retVal = date("j M Y", $ddate);

        }
        if (!empty($time)) {
            $hr = $time_array[0];
            $min = $time_array[1];
            $sec = $time_array[2];
            @ $ddate = mktime($hr, $min, $sec, $month, $day, $year);
            @ $retVal = date("j M Y, H:i", $ddate);
            if (!$withTime) {
                @ $retVal = date("j M Y", $ddate);
            }


        }
        return $retVal;
    }

    public function sendAuthorizationRequestMail($sentmessage, $authenticatingTaskId)
    {
        return;
        $subject = "PAYCHOICE - Request For Authorization";

        $messageFooter = "<br><br>We advise that you delete this mail if it contains sensitive contents such as:
Login Password, Transaction Password, Answers to your Security Questions, etc.<br>
FCMB will NEVER ask you for these details.<br><br>
For more information, please call us on 01-4540083 send an email to clientaccess@fcmb.com<br><br>
Thank you for choosing FCMB... My Bank and I<br>";

        $headers = "From: " . PAYCHOICE_EMAIL_ADDRESS . "\r\n";
        $headers .= "Reply-To: " . PAYCHOICE_EMAIL_ADDRESS . "\r\n";
        $headers .= "BCC: segun.ojo@upperlink.ng\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

        $merchantId = $_SESSION['mymerchantId'];
        $whereArray = array(
            'p.task_id' => "$authenticatingTaskId",
            'u.user_active' => '1',
            'u.user_deleted' => '0',
            'g.usergroup_enabled' => '1',
            'u.merchantId' => "$merchantId"
        );

        $query = $this->ci->db->select("l.user_id, u.user_firstname, l.userlog_email")
            ->from('upl_users_login l')
            ->join('upl_users u', 'u.user_id = l.user_id', 'INNER')
            ->join('upl_permission p', 'p.usergroup_id = l.usergroup_id', 'INNER')
            ->join('upl_usergroups g', 'g.usergroup_id = l.usergroup_id', 'INNER')
            ->where($whereArray)
            ->get();
        // echo $this->ci->db->last_query();
        $path_to_logo = "http://74.208.224.205/images/logo.png";
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $email = $row->userlog_email;
                $firstname = $row->user_firstname;

                $message = '<!DOCTYPE html><html><title>Paychoice</title>
<style>
table{
padding: 10px;
border: 1px solid #FCB831;
}
.t-left{
text-align: left;
}
</style>
</head>
<body>';

                $message .= '<table >';
                $message .= "<tr style='background: #eee;'><td class='t-left'>";
                $message .= '<img src="' . $path_to_logo . '" alt="' . PAYCHOICE_APPLICATION_NAME . '" />';
                $message .= "</td></tr>";
                $message .= "<tr><td style='background: #19164D; height: 20px; text-align: middle; color:white;'><strong>" . $subject . "</strong></td></tr>";
                $message .= "<tr><td class='t-left'>Dear " . $firstname . ",<br><br>";
                $message .= "$sentmessage";
                $message .= "$messageFooter";
                $message .= "</td></tr></table>";
                $message .= "</body></html>";

                $this->ci->email->from(PAYCHOICE_EMAIL_ADDRESS, PAYCHOICE_APPLICATION_NAME);
                $this->ci->email->to($email);
                $this->ci->email->set_mailtype('html');
                $this->ci->email->subject($subject);
                $this->ci->email->message($message);
                $hasSent = $_SERVER['HTTP_HOST'] != 'localhost' ? $this->ci->email->send() : FALSE;
                $this->ci->email->clear();

                //log mail temporarily
                $insertData = array(
                    'email' => "$email",
                    'message' => "$message",
                    'subject' => "$subject",
                    'headers' => "$headers"
                );

                $this->ci->db->insert('mail_log', $insertData);
            }
        }
    }

    public function encryptGetData($value = "")
    {
        return $value;

        $retVal = "";
        if (!empty($value)) {
            for ($i = 0; $i <= 3; $i++) {
                $value = base64_encode($value);
            }
            $prefix = $this->create_random_name(20);
            $suffix = $this->create_random_name(20);
            $retVal = $prefix . $value . $suffix;

        }

        return $retVal;
    }

    public function decryptGetData($value = "")
    {
        return $value;

        $retVal = "";
        if (!empty($value)) {
            //count the number of characters
            $numCharacter = strlen($value);
            if ($numCharacter > 40) {
                $realValueLength = $numCharacter - 40;
                $realValue = substr($value, 20, $realValueLength);
                for ($i = 0; $i <= 3; $i++) {
                    $realValue = base64_decode($realValue);
                }
                $retVal = $realValue;
            }
        }

        return $retVal;
    }

    public function getBanks($enabled_only = true, $bankId = '0')
    {
        if ($enabled_only) {
            $this->ci->db->where('bankStatus', '1');
        }

        if ($bankId != '0') {
            $this->ci->db->where('bankId', "$bankId");
        }

        $result = $this->ci->db->order_by('bankName')
            ->get('banks');

        if ($this->ci->db->affected_rows() > 0) {
            $ret_val = $result->result_array();
        } else {
            $ret_val = array();
        }

        return $ret_val;
    }

    public function getBanksNIBSSCodes($enabled_only = true, $bankId = '0')
    {
        if ($enabled_only) {
            $this->ci->db->where('bankStatus', '1');
        }

        if ($bankId != '0') {
            $this->ci->db->where('bankId', "$bankId");
        }
        /*else {
            $this->ci->db->where('bankId >', "1");
        }*/

        $result = $this->ci->db->select('bankNIBSSCode')->order_by('bankName')->get('banks');

        $ret_val = array();
        if ($this->ci->db->affected_rows() > 0) {
            foreach ($result->result_array() as $row) {

                $ret_val[] = $row['bankNIBSSCode'];
            }

        }

        return $ret_val;
    }

    public function getBanksNIBSSCodesAndId($enabled_only = true, $bankId = '0')
    {
        if ($enabled_only) {
            $this->ci->db->where('bankStatus', '1');
        }

        if ($bankId != '0') {
            $this->ci->db->where('bankId', "$bankId");
        }
        /*else {
            $this->ci->db->where('bankId >', "1");
        }*/

        $result = $this->ci->db->select("bankId, bankNIBSSCode")->order_by('bankName')->get('banks');

        $ret_val = array();

        if ($this->ci->db->affected_rows() > 0) {
            foreach ($result->result_array() as $row) {
                $ret_val[$row['bankId']] = $row['bankNIBSSCode'];
            }

        }

        return $ret_val;
    }

    public function getBanksInterswitchCodes($enabled_only = true, $bankId = '0')
    {
        if ($enabled_only) {
            $this->ci->db->where('bankStatus', '1');
        }

        if ($bankId != '0') {
            $this->ci->db->where('bankId', "$bankId");
        } else {
            $this->ci->db->where('bankId >', "1");
        }

        $result = $this->ci->db->select('bankInterswitchCode')->order_by('bankName')->get('banks');

        if ($this->ci->db->affected_rows() > 0) {
            $ret_val = $result->result_array();
        } else {
            $ret_val = array();
        }

        return $ret_val;
    }

    public function getSalaryPeriodDetails($salaryPeriodId)
    {
        $retVal = array();

        $merchantId = $_SESSION['mymerchantId'];
        $where_array = array(
            'salaryPeriodId' => "$salaryPeriodId",
            'merchantId' => "$merchantId"
        );

        $result = $this->ci->db
            ->select("MONTHNAME(s.salaryMonth) salaryMonth, YEAR(s.salaryMonth) salaryYear, approvalStatus,  currentStatus, s.salaryMonth salaryDate, s.salaryPeriodId", FALSE)
            ->from('salary_period s')
            ->where($where_array)
            ->get();

        $period_array = $result->row_array();

        if ($this->ci->db->affected_rows() == 1) {
            $retVal = $period_array;
        }

        return $retVal;
    }

    public function getDetailsWithDesktopId($tableName, $merchantId, $desktopId, $column1 = "")
    {
        $retVal = array();

        if (!empty($column1)) {
            $result = $this->ci->db->get_where("$tableName", array("$column1" => "$merchantId", 'desktopId' => "$desktopId"));

        } else {
            $result = $this->ci->db->get_where("$tableName", array('merchantId' => "$merchantId", 'desktopId' => "$desktopId"));
        }

        if ($result->num_rows()) {
            $retVal = $result->row_array();
        }

        return $retVal;
    }

    public function getTwoLevelId($desktopId, $desktopTable, $merchantId, $merchantTable, $joinColumn)
    {
        //$query = "SELECT * FROM levels l, scale c WHERE l.desktopId = '9' AND l.scaleId = c.scaleId AND c.merchantId = '15'";
        $where = array("$desktopTable.desktopId" => "$desktopId", "$merchantTable.merchantId" => "$merchantId");
        $result = $this->ci->db->select('*')
            ->from("$desktopTable")
            ->join("$merchantTable", "$desktopTable.$joinColumn = $merchantTable.$joinColumn", 'INNER')
            ->where($where)
            ->get();

        if ($result->num_rows()) {
            $retVal = $result->row_array();
        }

        return $retVal;
    }

    public function getThreeLevelId($desktopId, $desktopTable, $merchantId, $merchantTable, $joinColumn, $middleTable, $middleColumn)
    {
        //$query = "SELECT * FROM levels l, scale c WHERE l.desktopId = '9' AND l.scaleId = c.scaleId AND c.merchantId = '15'";
        //$query = "SELECT * FROM steps s, levels l, scale c WHERE s.desktopId = '9' AND s.levelId = l.levelId AND l.scaleId = c.scaleId AND c.merchantId = '15'";
        $where = array("$desktopTable.desktopId" => "$desktopId", "$merchantTable.merchantId" => "$merchantId");
        $result = $this->ci->db
            ->select('*')
            ->from("$desktopTable")
            ->join("$middleTable", "$middleTable.$middleColumn = $desktopTable.$middleColumn", 'INNER')
            ->join("$merchantTable", "$middleTable.$joinColumn = $merchantTable.$joinColumn", 'INNER')
            ->where($where)
            ->get();

        if ($result->num_rows()) {
            $retVal = $result->row_array();
        }

        return $retVal;
    }

    public function getStates($state_id = '')
    {
        $retVal = array();
        if ($state_id != '') {
            $query = $this->ci->db->get_where("states", "state_id = $state_id");
            if ($query->num_rows() == 1) {
                $retVal = $query->row_array();
            }
        } else {
            $query = $this->ci->db->get('states');
            // print_r ($query->row_array());
            $retVal = $query->result_array();
        }

        return $retVal;
    }

    public function formatMaritalStatus($maritalStatusId)
    {
        switch ($maritalStatusId) {
            case '0':
                $name = "Single";
                break;
            case '1':
                $name = 'Married';
                break;
            case '2':
                $name = 'Divorced';
                break;
            case '3':
                $name = 'Widowed';
                break;
            default:
                $name = "";
        }
        return $name;
    }

    public function card_encrypt($text)
    {
        $retVal = "";
        if (!empty($text))
            $retVal = trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, PAYCHOICE_CARD_SALT, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB), MCRYPT_RAND))));

        return $retVal;
    }

    public function card_decrypt($text)
    {
        $retVal = "";
        if (!empty($text))
            $retVal = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, PAYCHOICE_CARD_SALT, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB), MCRYPT_RAND)));
        return $retVal;
    }


    public function maskCardPAN($number, $maskingCharacter = '*')
    {
        //return $number;
        return substr($number, 0, 4) . str_repeat($maskingCharacter, strlen($number) - 5) . substr($number, -4);
    }

    /**
     * Send SMS.
     *
     * @param $recipient
     * @param $sender
     * @param $msg
     * @return int
     */
    public function sendSMS($recipient, $sender, $msg)
    {
        $un = 'bamsms';
        $up = 'fF0}cA3{yP2*eO9]';
        $msg = stripslashes($msg);

        /* production of the required URL */
        $url = "http://www.mysmslink.com/sms_send_out.php?"
            . "un=$un"
            . "&up=" . urlencode($up)
            . "&recipient=" . urlencode($recipient)        //A comma-separated list of phone numbers in international format
            . "&sender=" . urlencode($sender)            //The sender name to use
            . "&msg=" . urlencode($msg);                //The actual SMS body

        if (($f = @fopen($url, "r"))) {
            $sendResponse = fgets($f, 255);
        } else $sendResponse = 0;

        return $sendResponse;
    }   //END sendSMS()

}
