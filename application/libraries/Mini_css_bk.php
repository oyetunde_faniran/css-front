<?php

/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 11/16/2016
 * Time: 2:20 PM
 */
class Mini_css
{
    protected $CI;

    protected $css_webservice_url = 'http://192.163.224.76/v1prod/wsdl';
    protected $css_client_id = 'JH5CWZSD86';
    protected $css_secret_key = 'nruXAQTyU*5uuL8P0mDukWTAZHGHraq=X*|*-Ggz&llw1pxr*';

    public function __construct()
    {
        $this->CI =& get_instance();

        if (empty($this->css_webservice_url)) {
            $this->css_webservice_url = CSS_WEBSERVICE_URL;
        }

        if (empty($this->css_client_id)) {
            $this->css_client_id = CSS_CLIENT_ID;
        }

        if (empty($this->css_secret_key)) {
            $this->css_secret_key = CSS_SECRET_KEY;
        }
    }

    /**
     * Validate the accounts in payment file.
     * 
     * @param $file_id
     * @return mixed
     */
    public function validateAccounts($file_id)
    {
        //first validate against our DB
        $stats = $this->validateAccountsAgainstLocalDB($file_id);

        if ($stats['records_found'] > 0) {
            $validation_response_code = $stats['records_valid'] == $stats['records_found'] ? '00' : '12';
            $validation_status = '0';
        } else {
            $validation_response_code = '';
            $validation_status = '2';
        }

        $validation_response_description = !empty($validation_response_code) ? $this->getNIBSSPayPlusResponseDescription($validation_response_code) : null;

        $sql = "UPDATE bp_files
                SET file_av_status = ?, file_av_response_code = ?, file_av_response_description = ?
                WHERE file_id = ?";

        return $this->CI->db->query($sql, array($validation_status, $validation_response_code, $validation_response_description, $file_id));
    }

    public function validateAccountsWithNIBSSPayPlus($accounts, $file_name, $file_description)
    {
        $schedule_id = $this->logAccountsForValidation($accounts, $file_name, $file_description);

        //first validate against our DB
        //$this->validateAccountsAgainstLocalDB($accounts);

        $schedule = $this->generateAccountValidationUploadSchedule($schedule_id);

        file_put_contents('account-validation-schedule.txt', $schedule);

        //make the webservice call
        try {
            $response = $this->sendToNIBSS('uploadNewVendors', $schedule);

            if (!$response) {
                throw new Exception('NIP Gateway DOWN!!!!');
            }

            //check the response
            $response_obj = simplexml_load_string($response);
            $response_code = $response_obj->Header->Status;

            if (strlen($response_code) > 2) {
                throw new Exception('NIP Gateway DOWN!!!!');
            }

            //get the description for the status of the response
            $response_description = $this->getNIBSSPayPlusResponseDescription($response_code);

            $status = $response_code != '16' ? '2' : '0';

            $data = array(
                'schedule_id' => $schedule_id,
                'status' => $status,
                'response_code' => $response_code,
                'response_description' => $response_description
            );
            $this->updateAccountValidationSchedule($data);

            if ($status == '2') {
                $this->updateAccountsInValidationSchedule($data);
            }

            $ret_val = array($response_code == '16' ? true : false, $response_description);

        } catch (Exception $e) {
            $ret_val = array(false, $e->getMessage());
        }

        return $ret_val;
    }

    public function resendAccountValidationSchedule($schedule_id)
    {
        $schedule = $this->generateAccountValidationUploadSchedule($schedule_id);

        file_put_contents('account-validation-schedule.txt', $schedule);

        //make the webservice call
        try {
            $response = $this->sendToNIBSS('uploadNewVendors', $schedule);

            if (!$response) {
                throw new Exception('NIP Gateway DOWN!!!!');
            }

            //check the response
            $response_obj = simplexml_load_string($response);
            $response_code = $response_obj->Header->Status;

            if (strlen($response_code) > 2) {
                throw new Exception('NIP Gateway DOWN!!!!');
            }

            //get the description for the status of the response
            $response_description = $this->getNIBSSPayPlusResponseDescription($response_code);

            $status = $response_code != '16' ? '2' : '0';

            $data = array(
                'schedule_id' => $schedule_id,
                'status' => $status,
                'response_code' => $response_code,
                'response_description' => $response_description
            );
            $this->updateAccountValidationSchedule($data);

            if ($status == '2') {
                $this->updateAccountsInValidationSchedule($data);
            }

            $ret_val = array($response_code == '16' ? true : false, $response_description);

        } catch (Exception $e) {
            $ret_val = array(false, $e->getMessage());
        }

        return $ret_val;
    }

    public function getAccountValidationStatus($schedule_id)
    {
        $schedule = $this->generateAccountValidationStatusSchedule($schedule_id);

        //file_put_contents('account-validation-schedule.txt', $schedule);

        //make the webservice call
        try {
            $response = $this->sendToNIBSS('getNewVendorsStatus', $schedule);

            if (!$response) {
                throw new Exception('NIP Gateway DOWN!!!!');
            }

            //check the response
            $response_obj = simplexml_load_string($response);

            $response_code = $response_obj->Header->Status;

            if (strlen($response_code) > 2) {
                throw new Exception('NIP Gateway DOWN!!!!');
            }

            //get the description for the status of the response
            $response_description = $this->getNIBSSPayPlusResponseDescription($response_code);

            $status = in_array($response_code, array('00', '06', '12')) ? '0' : '2';

            $data = array(
                'schedule_id' => $schedule_id,
                'status' => $status,
                'response_code' => $response_code,
                'response_description' => $response_description
            );
            $this->updateAccountValidationSchedule($data);

            if ($status == '2') {
                $this->updateAccountsInValidationSchedule($data);
            }

            //check the existence of vendor accounts
            if (!empty($response_obj->Vendor)) {
                foreach ($response_obj->Vendor as $vendor_record) {

                    $code_2 = $vendor_record->Status;
                    $desc_2 = $this->getNIBSSPayPlusResponseDescription($code_2);
                    $status = $code_2 == '00' ? '0' : ($code_2 == "06" ? "1" : '2');

                    $data = array(
                        'schedule_id' => $schedule_id,
                        'status' => $status,
                        'response_code' => $code_2,
                        'response_description' => $desc_2,
                        'error_reason' => !empty($vendor_record->ErrorReason) ? $vendor_record->ErrorReason : null,
                        'account_number' => $vendor_record->AccountNumber
                    );
                    $this->updateAccount($data);
                }
            }

            //if the schedule status is good, update our localNibssPayPlus db
            //update the details of any account previously validated, and insert any new one.
            if (in_array($response_code, array('00', '12'))) {
                $sql = "INSERT INTO validated_accounts (va_vendor_no, va_account_name, va_account_no, va_bank_code, va_response_code, va_response_description, va_error_reason, va_created_at)
                        (SELECT ava_vendor_no, ava_account_name, ava_account_no, ava_bank_code, ava_response_code, ava_response_description, ava_error_reason, NOW()
                          FROM av_accounts
                          JOIN av_schedules ON av_accounts.avs_id = av_schedules.avs_id AND av_schedules.avs_schedule_id = '{$schedule_id}'
                          WHERE ava_response_code IS NOT NULL)
                        ON DUPLICATE KEY UPDATE
                          va_vendor_no = ava_vendor_no,
                          va_account_name = ava_account_name,
                          va_response_code = ava_response_code,
                          va_response_description = ava_response_description,
                          va_error_reason = ava_error_reason";
                $this->CI->db->query($sql);
            }

            $ret_val = array($status == '0' ? true : false, $response_description);

        } catch (Exception $e) {
            $ret_val = array(false, $e->getMessage());
        }

        return $ret_val;
    }

    public function getAccountValidationSchedule($schedule_id)
    {
        $resource = $this->CI->db->get_where('av_schedules', array('avs_schedule_id' => $schedule_id));

        return $resource->num_rows() > 0 ? $resource->row_array() : null;
    }

    public function getValidAccountsInFile($file_id)
    {
        $resource = $this->CI->db
            ->select('bpd_correct_account_name AS account_name, bpd_account_no AS account_no, bpd_bank_code AS bank_code, bpd_amount AS amount, bpd_unique_id AS unique_id')
            ->from('bp_details')
            ->where_in('bpd_av_status', array('0', '4', '5'))->where('bpf_id', $file_id)
            ->get();

        return $resource->result_array();
    }

    /**
     * Verify that each of the accounts in the file exists in our DB, and update its status accordingly.
     * 
     * @param $file_id
     * @return array list of total accounts found in our DB and total valid ones
     */
    public function validateAccountsAgainstLocalDB($file_id)
    {
        $sql = "UPDATE bp_details AS bpd
                JOIN validated_accounts AS va ON bpd.bpd_account_no = va.va_account_no AND bpd.bpd_bank_code = va.va_bank_code
                SET
                  bpd.bpd_av_response_code = CASE
                      WHEN (va.va_response_code = '08' OR va.va_response_code = '00') AND bpd.bpd_account_name = va.va_account_name THEN va.va_response_code
                      WHEN (va.va_response_code = '08' OR va.va_response_code = '00') AND bpd.bpd_account_name = va.va_error_reason THEN '00'
                      WHEN (va.va_response_code = '08' OR va.va_response_code = '00') AND bpd.bpd_account_name != va.va_account_name AND bpd.bpd_account_name != va.va_error_reason THEN '08'
                      ELSE va.va_response_code END,
                  bpd.bpd_av_response_description = CASE
                      WHEN (va.va_response_code = '08' OR va.va_response_code = '00') AND bpd.bpd_account_name = va.va_account_name THEN va.va_response_description
                      WHEN (va.va_response_code = '08' OR va.va_response_code = '00') AND bpd.bpd_account_name = va.va_error_reason THEN 'SUCCESSFUL'
                      WHEN (va.va_response_code = '08' OR va.va_response_code = '00') AND bpd.bpd_account_name != va.va_account_name AND bpd.bpd_account_name != va.va_error_reason THEN 'FAILED NAME VALIDATION'
                      ELSE va.va_response_description END,
                  bpd.bpd_av_error_reason = CASE
                      WHEN (va.va_response_code = '08' OR va.va_response_code = '00') AND bpd.bpd_account_name = va.va_account_name THEN va.va_error_reason
                      WHEN (va.va_response_code = '08' OR va.va_response_code = '00') AND bpd.bpd_account_name = va.va_error_reason THEN va_error_reason
                      WHEN (va.va_response_code = '08' OR va.va_response_code = '00') AND bpd.bpd_account_name != va.va_account_name AND bpd.bpd_account_name != va.va_error_reason THEN va_error_reason
                      ELSE va.va_error_reason END,
                  bpd.bpd_av_status = CASE
                      WHEN bpd.bpd_av_response_code = '00' THEN '0'
                      ELSE '2' END
                WHERE bpd.bpf_id = $file_id";

        $this->CI->db->query($sql);

        $ret_val = array();
        $ret_val['records_found'] = $this->CI->db->affected_rows();

        $resource = $this->CI->db->select('COUNT(bpd_id) AS valid')->from('bp_details')->where(array('bpf_id' => $file_id, 'bpd_av_response_code' => '00'))->get();
        $ret_val['records_valid'] = $resource->row()->valid;

        return $ret_val;
    }

    protected function logAccountsForValidation($accounts_list, $file_name, $file_description)
    {
        //generate a schedule ID and log it against the file name
        $schedule_id = $this->CI->basic_functions->create_random_number_string();

        $this->CI->db->insert('av_schedules', array(
            'avs_file_name' => $file_name,
            'avs_file_description' => $file_description,
            'avs_schedule_id' => $schedule_id,
            'avs_created_at' => date('Y-m-d H:i:s')
        ));

        $avs_id = $this->CI->db->insert_id();

        //log every account on the list against the schedule id
        if ($avs_id > 0) {
            $_data = array();

            foreach ($accounts_list as $account) {
                $_data[] = array(
                    'avs_id' => $avs_id,
                    'ava_vendor_no' => !empty($account['unique_id']) ? $account['unique_id'] : $this->CI->basic_functions->create_random_number_string(5),
                    'ava_account_name' => $account['account_name'],
                    'ava_account_no' => $account['account_no'],
                    'ava_bank_code' => $account['bank_code'],
                    'ava_created_at' => date('Y-m-d H:i:s')
                );
            }

            $this->CI->db->insert_batch('av_accounts', $_data);
        }

        return $schedule_id;
    }

    protected function generateAccountValidationUploadSchedule($schedule_id)
    {
        $accounts = $this->getAccountsForValidationInSchedule($schedule_id);

        $this->CI->load->helper('fluid_xml');

        $salt = rand(100000, 1000000000);
        $str2hash = "{$this->css_client_id}-{$this->css_secret_key}-{$salt}";
        $mac = hash('sha512', $str2hash);

        $xml = new \FluidXml\FluidXml('VendorUploadRequest');

        $xml->addChild('Header', true)
            ->addChild('ScheduleId', $schedule_id)
            ->addChild('ClientId', $this->css_client_id)
            ->addChild('Salt', $salt)
            ->addChild('Mac', $mac);

        foreach ($accounts as $account) {
            $xml->addChild('Vendor', true)
                ->addChild('VendorNumber', $account['ava_vendor_no'])
                ->addChild('AccountName', $account['ava_account_name'])
                ->addChild('AccountNumber', $account['ava_account_no'])
                ->addChild('SortCode', $account['ava_bank_code']);
        }

        $xml->addChild('HashValue', ' ');

        return $xml->xml();
    }

    protected function generateAccountValidationStatusSchedule($schedule_id)
    {
        $this->CI->load->helper('fluid_xml');

        $salt = rand(100000, 1000000000);
        $str2hash = "{$this->css_client_id}-{$this->css_secret_key}-{$salt}";
        $mac = hash('sha512', $str2hash);

        $xml = new \FluidXml\FluidXml('StatusRequest');

        $xml->addChild('Header', true)
            ->addChild('ScheduleId', $schedule_id)
            ->addChild('ClientId', $this->css_client_id)
            ->addChild('Salt', $salt)
            ->addChild('Mac', $mac);

        $xml->addChild('HashValue', ' ');

        return $xml->xml();
    }

    protected function getAccountsForValidationInSchedule($schedule_id)
    {
        $resource = $this->CI->db
            ->select('ava.*')
            ->from('av_accounts AS ava')
            ->join('av_schedules AS avs', "avs.avs_id = ava.avs_id AND avs.avs_schedule_id = '{$schedule_id}'")
            ->get();

        return $resource->num_rows() > 0 ? $resource->result_array() : null;
    }

    protected function sendToNIBSS($method, $request)
    {
        set_time_limit(0);

        try {
            $client = new SoapClient($this->css_webservice_url, array("trace" => 1, "exceptions" => 1, 'cache_wsdl' => WSDL_CACHE_MEMORY));
            $response = $client->$method($request);

            //log the Webservice call request and response
            $log_data = array('url' => $this->css_webservice_url, 'method' => $method, 'request' => $request, 'response' => $response);
            $this->logWebserviceCall($log_data);

        } catch (Exception $e) {
            $response = $e->getMessage();
        }

        return $response;
    }

    protected function getNIBSSPayPlusResponseDescription($code)
    {
        $resource = $this->CI->db->select('description')->from('nibsspayplus_responses')->where('response_code', $code)->get();

        return $resource->num_rows() > 0 ? $resource->row()->description : null;
    }

    protected function logWebserviceCall($data)
    {
        return $this->CI->db->insert('payments_webservice_log', array(
            'url' => $data['url'],
            'method' => $data['method'],
            'request' => $data['request'],
            'response' => $data['response'],
        ));
    }

    protected function updateAccountValidationSchedule($data)
    {
        $update_data = array('avs_status' => $data['status'], 'avs_response_code' => $data['response_code'], 'avs_response_description' => $data['response_description']);

        return $this->CI->db->update("av_schedules", $update_data, array('avs_schedule_id' => $data['schedule_id']));
    }

    protected function updateAccountsInValidationSchedule($data)
    {
        $sql = "UPDATE av_accounts AS ava
                JOIN av_schedules AS avs ON ava.avs_id = avs.avs_id AND avs.avs_schedule_id = ?
                SET ava.ava_status = ?, ava.ava_response_code = ?, ava.ava_response_description = ?";

        $values = array($data['schedule_id'], $data['status'], $data['response_code'], $data['response_description']);

        return $this->CI->db->query($sql, $values);
    }

    protected function updateAccount($data)
    {
        $sql = "UPDATE av_accounts AS ava
                JOIN av_schedules AS avs ON ava.avs_id = avs.avs_id AND avs.avs_schedule_id = ?
                SET ava.ava_status = ?, ava.ava_response_code = ?, ava.ava_response_description = ?, ava.ava_error_reason = ?
                WHERE ava.ava_account_no = ?";

        $values = array($data['schedule_id'], $data['status'], $data['response_code'], $data['response_description'], $data['error_reason'], $data['account_number']);

        return $this->CI->db->query($sql, $values);

        //file_put_contents('sql-debugger.txt', $this->CI->db->last_query() . "\n\n", FILE_APPEND);
    }
}