<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE') OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ') OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESCTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS') OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


/*BEGIN: Constants for UPL SYS ADMIN*/
/**The prefix of DB tables for UPL Sys-Admin*/
define('UPL_DB_TABLE_PREFIX', 'upl_');

/**The CSS class to use for success messages.*/
define('UPL_MSG_STYLE_SUCCESS', 'alert alert-success alert-dismissable');

/**The CSS class to use for error messages.*/
define('UPL_MSG_STYLE_ERROR', 'alert alert-danger alert-dismissable');

/**The CSS class to use for warning messages.*/
define('UPL_MSG_STYLE_WARNING', 'alert alert-warning alert-dismissable');

/**The CSS class to use for buttons.*/
define('UPL_BUTTON_STYLE', '');

/**The CSS class to use for tables.*/
define('UPL_STYLE_TABLE', 'table-style');

/**The CSS class to use for table headers.*/
define('UPL_STYLE_TABLE_HEADER', 'table-header');

/**The CSS class to use for table headers.*/
define('UPL_STYLE_TABLE_SUB_HEADER', 'table-sub-header-style');

/**The first CSS class to use for table rows. (This is necessary for styling adjacent table rows differently)*/
define('UPL_STYLE_TABLE_ROW1', 'table-row1');

/**The second CSS class to use for table rows. (This is necessary for styling adjacent table rows differently)*/
define('UPL_STYLE_TABLE_ROW2', 'table-row2');
/*END: Constants for UPL SYS ADMIN*/


define('RECORDS_PER_PAGE', 100);


define('DEFAULT_TITLE', 'Central Settlement System');
define('APP_NAME', 'Central Settlement System');
define('DEFAULT_SCHOOL_USERGROUP', 2);

define('FROM_EMAIL', 'mailer@css.ng');
define('EMAIL_SENDER_NAME', 'C.S.S.');


//define('ACC_BALANCE_SERVICE_SALT', 'rD7@zT5@iY6{mS9[uP1*zZ1}vP7[mE5]yR7]jO1*bU9[fW0@');
//define('ACC_BALANCE_SERVICE_REQUESTID', '999103201511240159123456789012');
//define('ACC_BALANCE_SERVICE_INS_CODE', '555001');
/*LIVE URL : http://41.86.152.181/nchannelserverlive/bams-api/getUserAccounts //getAccountBalance

Best Reg*/
//    define('ACC_BALANCE_SERVICE_GET_ACC_URL', 'http://41.86.152.181/nchannelservertest/bams-api/getUserAccounts');
//    define('ACC_BALANCE_SERVICE_GET_BAL_URL', 'http://41.86.152.181/nchannelservertest/bams-api/getAccountBalance');

//define('ACC_BALANCE_SERVICE_GET_ACC_URL', 'http://41.86.152.181/nchannelserverlive/bams-api/getUserAccounts');
//define('ACC_BALANCE_SERVICE_GET_BAL_URL', 'http://41.86.152.181/nchannelserverlive/bams-api/getAccountBalance');


define('IMAGE_UPLOAD_DIR', './assets/images/a/');


//Payment Status Constants
define('PAYMENT_PENDING', '0');
define('PAYMENT_REJECTED', '1');
define('PAYMENT_APPROVED', '2');
define('PAYMENT_AUTHORIZED', '3');
define('PAYMENT_FORWARDED', '4');

define('DEBUG_FOLDER', './debug-f6rdghr879fjdh325579ifhdfgh/');

//define('FUNDS_TRANSFER_TRANSACTION_CHARGE', 50);


/**The parameters for settlement using CSS*/
//define('CSS_WEBSERVICE_URL', 'http://192.163.224.76/v1prod/wsdl');
//define('CSS_CLIENT_NAME', 'BAMS Lagos State');
//define('CSS_CLIENT_ID', 'JH5CWZSD86');
//define('CSS_SECRET_KEY', 'nruXAQTyU*5uuL8P0mDukWTAZHGHraq=X*|*-Ggz&llw1pxr*');

/** The maximum number of times failed GA entry attempts are allowed */
define('GA_MAX_FAILED_ATTEMPTS', 3);

//define('SUSPENSE_ACC_NO', '1001649788');
//define('SUSPENSE_ACC_NAME', 'Upperlink Suspense Account');
//define('SUSPENSE_ACC_BANK_CODE', '082');
define('SUSPENSE_ACC_NO', '0123456789');
define('SUSPENSE_ACC_NAME', 'Upperlink Suspense Account');
define('SUSPENSE_ACC_BANK_CODE', '050');


define('COMM_BENEFICIARY_USERGROUP', 2);


define('NIBSS_USERNAME', 'username here');
define('NIBSS_PASSWORD', 'password here');

define('ACC_BALANCE_SERVICE_INS_CODE', '555001');

define('CSS_CLIENT_ID', 'PJ6X8VZ2M4');
define('CSS_SECRET_KEY', '3Py3j*M*DzSN9UpG5^FS437JSsSwLth*FYghr;HG7vZDbf5RUNCJkZ=N6]HKG73');
define('CSS_WSDL', 'http://css.ng/v1/css_api_v2/wsdl');

define('BANKDEPOSIT_UPLOAD_DIR', './uploaded-bankdeposit-jd7839djdgns6y8dj/');

define('TRANS_REPORT_UPLOAD_DIR', './trans-report-7y32fdi/');

/**Minimum amount that entries (for a client ID) from a bank statement must sum up to before a commission for splitting is created from them. */
define('MIN_COMM_AMOUNT', 4000);
define('NIBSS_TRANS_CHARGE', 10.5);