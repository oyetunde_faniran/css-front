<?php
/**
 * Contains methods for manipulating user group data
 *
 * @author Faniran, Oyetunde
 */
class Usergroup_model extends UPL_Model {

    function __construct(){
        parent::__construct();
        $this->usergroup_table = UPL_DB_TABLE_PREFIX . 'usergroups';
    }


    /**
     * Gets a single user group whose ID is specified
     * @param int $group_id         The ID of the user group to fetch
     * @param boolean $enabled_only When TRUE, the user group is returned only if it is active
     * @return array                The details of the user group if found or an empty array on failure
     */
    public function getUserGroup($group_id, $enabled_only = true){
        $group_id = (int)$group_id;
        $where_array = array (
            'usergroup_id' => "$group_id"
        );
        if ($enabled_only){
            $where_array['usergroup_enabled'] = '1';
        }
        $result = $this->db->get_where($this->usergroup_table, $where_array);
        //die('<pre>' . $this->db->last_query());
        if ($this->db->affected_rows() == 1){
            $ret_val = $result->row_array();
        } else {
            $ret_val = array();
        }
        return $ret_val;
    }   //END getUserGroup()



    /**
     * Gets the list of existing user groups
     * @param boolean $enabled_only When TRUE, the only active user groups are returned
     * @return array                The list of user groups sorted by name
     */
    public function getUserGroups($enabled_only = true){
        if ($enabled_only){
            $this->db->where('usergroup_enabled', '1');
        }
        $result = $this->db->order_by('usergroup_name')
                           ->get($this->usergroup_table);
        if ($this->db->affected_rows() > 0){
            $ret_val = $result->result_array();
        } else {
            $ret_val = array();
        }
        return $ret_val;
    }   //END getUserGroups()



    /**
     * Adds a user group
     * @param string $name          The name of the user group to add
     * @param string $description   The description of the user group to add
     * @return int                  The ID of the user group or 0 on failure
     */
    public function addUserGroup($name, $description){
        $data_array = array (
            'usergroup_name' => "$name",
            'usergroup_description' => "$description"
        );
        $this->db->insert($this->usergroup_table, $data_array);
        return $this->db->insert_id();
    }   //END addUserGroup()



    /**
     * Update the details of a user group
     * @param int $group_id         The ID of the group to update
     * @param string $name          The name of the user group
     * @param string $description   The description of the user group
     * @return boolean              TRUE on success, else FALSE
     */
    public function updateUserGroup($group_id, $name, $description, $enabled){
        $group_id =(int)$group_id;
        $enabled = $enabled == 1 ? '1' : '0';
        $data_array = array (
            'usergroup_name' => "$name",
            'usergroup_description' => "$description",
            'usergroup_enabled' => "$enabled"
        );
        $where_array['usergroup_id'] = $group_id;
        $this->db->where($where_array)
                 ->update($this->usergroup_table, $data_array);
        return $this->db->affected_rows() == 1;
    }   //END addUserGroup()



    /**
     * Checks if a user group is unique excluding the user group with the ID specified in the check.
     * @param string $group     The name of the group to check for uniqueness
     * @param int $group_id     The ID of the group to be excluded in the search
     * @return boolean          Returns TRUE if unique, FALSE otherwise
     */
    public function isUnique($group, $group_id){
        $group_id = (int)$group_id;
        $where_array = array(
            'usergroup_name' => "$group",
            'usergroup_id <>' => "$group_id"
        );
        $this->db->get_where($this->usergroup_table, $where_array);
        if ($this->db->affected_rows() > 0){
            $ret_val = false;
        } else {
            $ret_val = true;
        }
        return $ret_val;
    }   //END isUnique()

}   //END class
