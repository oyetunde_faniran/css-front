<?php
/**
 * Contains methods for manipulating module data
 *
 * @author Faniran, Oyetunde
 */
class Module_model extends CI_Model {

    function __construct(){
        parent::__construct();
        $this->module_table = UPL_DB_TABLE_PREFIX . 'modules';
    }


    /**
     * Gets a single task whose ID is specified
     * @param int $module_id        The ID of the module to fetch
     * @param boolean $enabled_only When TRUE, the module is returned only if it is active
     * @return array                The details of the module if found or an empty array on failure
     */
    public function getModule($module_id, $enabled_only = true){
        $module_id = (int)$module_id;
        $where_array = array (
            'module_id' => "$module_id"
        );
        if ($enabled_only){
            $where_array['module_enabled'] = '1';
        }
        $result = $this->db->get_where($this->module_table, $where_array);
        //die('<pre>' . $this->db->last_query());
        if ($this->db->affected_rows() == 1){
            $ret_val = $result->row_array();
        } else {
            $ret_val = array();
        }
        return $ret_val;
    }   //END getTask()



    /**
     * Gets the list of existing modules
     * @param boolean $enabled_only When TRUE, only active modules are returned
     * @return array                The list of tasks sorted by modules, task description
     */
    public function getModules($enabled_only = true){
        if ($enabled_only){
            $this->db->where('module_enabled', '1');
        }
        $result = $this->db->order_by('module_name')
                           ->get($this->module_table);
        if ($this->db->affected_rows() > 0){
            $ret_val = $result->result_array();
        } else {
            $ret_val = array();
        }
        return $ret_val;
    }   //END getModules()



    /**
     * Adds a new module
     * @param int $module       The name of the module to add
     * @param int $description  A short description of the module
     * @param int $active       Active = 1, Inactive = 0
     * @return int              The ID of the inserted module
     */
    public function addModule($module, $description, $active){
        $active = $active == 1 ? '1' : '0';
        $data_array = array (
            'module_name' => "$module",
            'module_description' => "$description",
            'module_enabled' => "$active"
        );
        $this->db->insert($this->module_table, $data_array);
        return $this->db->insert_id();
    }   //END addModule()



    /**
     * Updates the details of a module
     * @param int $module_id        The ID of the module to update
     * @param string $module        The name of the module
     * @param string $description   A short description of the module
     * @param int $active           Active = 1, Inactive = 0
     * @return int                  Returns true if the operation was succesful, false otherwise
     */
    public function updateTask($module_id, $module, $description, $active){
        $module_id =(int)$module_id;
        $active = $active == 1 ? '1' : '0';
        $data_array = array (
            'module_name' => "$module",
            'module_description' => "$description",
            'module_enabled' => "$active"
        );
        $where_array['module_id'] = $module_id;
        $this->db->where($where_array)
                 ->update($this->module_table, $data_array);
        return $this->db->affected_rows() == 1;
    }   //END updateTask()



    /**
     * Checks if a module is unique excluding the module with the ID specified in the check.
     * @param string $module      The module to check for uniqueness
     * @param int $module_id      The ID of the module to be excluded in the search
     * @return boolean            Returns TRUE if unique, FALSE otherwise
     */
    public function isUniqueModule($module, $module_id){
        $module_id = (int)$module_id;
        $where_array = array(
            'module_name' => "$module",
            'module_id <>' => "$module_id"
        );
        $this->db->get_where($this->module_table, $where_array);
        if ($this->db->affected_rows() > 0){
            $ret_val = false;
        } else {
            $ret_val = true;
        }
        return $ret_val;
    }   //END isUniqueModule()

}   //END class