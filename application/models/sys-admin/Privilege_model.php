<?php
/**
 * Contains methods for manipulating user group data
 *
 * @author Faniran, Oyetunde
 */
class Privilege_model extends CI_Model {

    function __construct(){
        parent::__construct();
        $this->privilege_table = UPL_DB_TABLE_PREFIX . 'permission';
        $this->task_table = UPL_DB_TABLE_PREFIX . 'tasks';
    }


    /**
     * Saves the list of privileges for a particular user group
     * @param int $group_id         The ID of the user group for which privileges would be configured
     * @param array $privileges     An array containing the IDs of the privileges to configure for the user group under consideration
     * @return int                  The number of inserted privileges or 0 (zero) on failure
     */
    public function configure($group_id, $privileges){
        $group_id = (int)$group_id;
        $priv_array = is_array($privileges) ? $privileges : array();
        $data_array = array();

        //Format the privileges into a data array for insertion
        foreach ($priv_array as $priv){
            $priv = (int)$priv;
            if (!empty($priv)){
                $data_array[] = array(
                    'usergroup_id' => "$group_id",
                    'task_id' => "$priv"
                );
            }
        }

        //Continue iff valid privileges were found
        if (!empty($data_array)){
            $this->emptyPrivileges($group_id);
            $this->db->insert_batch($this->privilege_table, $data_array);
            $ret_val = $this->db->affected_rows();
        } else {
            $ret_val = 0;
        }

        return $ret_val;
    }   //END configure()



    /**
     * Deletes all the privileges configured for a user group
     * @param int $group_id     When TRUE, only active tasks are returned
     * @return boolean          Returns TRUE if successful, else FALSE
     */
    protected function emptyPrivileges($group_id){//Gets the list of existing privileges for a user group
        $group_id = (int)$group_id;
        return $this->db->where(array('usergroup_id' => "$group_id"))
                        ->delete($this->privilege_table);
    }   //END emptyPrivileges()




    /**
     * Gets the list of privileges for a particular user group
     * @param int $group_id     The ID of the user group under consideration
     * @return array            The list of privileges currently assigned to a particular user group
     */
    public function getPrivileges($group_id){
        $group_id = (int)$group_id;
//        $result = $this->db->where('usergroup_id', "$group_id")
//                           ->get($this->privilege_table);
        $result = $this->db->select('p.*, t.*')
                           ->from($this->privilege_table . ' p')
                           ->join($this->task_table . ' t', 'p.task_id = t.task_id', 'INNER')
                           ->where('usergroup_id', "$group_id")
                           ->get();
//        die($this->db->last_query());
        if ($this->db->affected_rows() > 0){
            $ret_val = $result->result_array();
        } else {
            $ret_val = array();
        }
        return $ret_val;
    }   //END getPrivileges()


}   //END class