<?php
/**
 * Description of branch
 *
 * @author Tunde
 */
class Country_model extends UPL_Model {

    function __construct() {
        parent::__construct();
        $table = 'countries';
        $table_pk = 'country_id';
        $this->setTable($table);
        $this->setTablePK($table_pk);
    }

    public function getCountries(){
        $ret_val = $this->loadMultiple(0, 0, '', "\$this->db->order_by('country_name');");
//        die('<pre>' . print_r($ret_val, 1));
        return $ret_val;
    }

}
