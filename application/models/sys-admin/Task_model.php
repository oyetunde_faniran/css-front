<?php
/**
 * Contains methods for manipulating user group data
 *
 * @author Faniran, Oyetunde
 */
class Task_model extends CI_Model {

    function __construct(){
        parent::__construct();
        $this->task_table = UPL_DB_TABLE_PREFIX . 'tasks';
        $this->module_table = UPL_DB_TABLE_PREFIX . 'modules';
    }


    /**
     * Gets a single task whose ID is specified
     * @param int $task_id          The ID of the task to fetch
     * @param boolean $enabled_only When TRUE, the task is returned only if it is active
     * @return array                The details of the task if found or an empty array on failure
     */
    public function getTask($task_id, $enabled_only = true){
        $task_id = (int)$task_id;
        $where_array = array (
            'task_id' => "$task_id"
        );
        if ($enabled_only){
            $where_array['task_enabled'] = '1';
        }
        $result = $this->db->get_where($this->task_table, $where_array);
        //die('<pre>' . $this->db->last_query());
        if ($this->db->affected_rows() == 1){
            $ret_val = $result->row_array();
        } else {
            $ret_val = array();
        }
        return $ret_val;
    }   //END getTask()



    /**
     * Gets the list of existing tasks
     * @param boolean $enabled_only When TRUE, only active tasks are returned
     * @return array                The list of tasks sorted by modules, task description
     */
    public function getTasks($enabled_only = true){
        if ($enabled_only){
            $this->db->where('t.task_enabled', '1');
        }
        /*
         * SELECT t.*, m.module_name 'module', IFNULL(t2.task_description, 'No Parent') 'parent'
FROM `upl_tasks` t
LEFT JOIN upl_modules m ON t.module_id = m.module_id
LEFT JOIN upl_tasks t2 ON t.task_parent_id = t2.task_id
ORDER BY `module_id`, `task_description`

         */
        $result = $this->db->select("t.*, m.module_name 'module', IFNULL(t2.task_description, 'No Parent') 'parent'", false)
                           ->from($this->task_table . ' t')
                           ->join($this->module_table . ' m', 't.module_id = m.module_id', 'LEFT')
                           ->join($this->task_table . ' t2', 't.task_parent_id = t2.task_id', 'LEFT')
                           ->order_by('m.module_id, t.task_description')
                           ->get();
//        die('<pre>' . $this->db->last_query());
        if ($this->db->affected_rows() > 0){
            $ret_val = $result->result_array();
        } else {
            $ret_val = array();
        }
        return $ret_val;
    }   //END getTasks()



    /**
     * Adds a new task
     * @param int $module_id    The ID of the module the task belongs to
     * @param int $parent_id    The ID of the task that is a parent to this task
     * @param string $code      A unique code assigned to the task, which can be used to reference that task in the source code. (N.B: Using the ID of the task is not safe as it may change for whatever reason.)
     * @param string $url       The controller/method (or folder/controller/method) that serves as the URL of the task
     * @param string $desc      A description of the task. (N.B: This description would appear on the privilege management page where tasks are assigned to different user groups.)
     * @param int $active       1 = Active, 0 = Inactive
     * @param int $show         1 = Show in the navigation bar, 0 = Don't show in the navigation bar
     * @return int              The ID of the inserted task is returned
     */
    public function addTask($module_id, $parent_id, $code, $url, $desc, $active, $show){
        $active = $active == 1 ? '1' : '0';
        $show = $show == 1 ? '1' : '0';
        $data_array = array (
            'module_id' => "$module_id",
            'task_parent_id' => "$parent_id",
            'task_code' => "$code",
            'task_url' => "$url",
            'task_description' => "$desc",
            'task_enabled' => "$active",
            'task_show_in_nav_bar' => "$show"
        );
        $this->db->insert($this->task_table, $data_array);
        return $this->db->insert_id();
    }   //END addTask()



    /**
     * Updates the details of a task
     * @param int $task_id      The ID of the task whose details would be updated
     * @param int $module_id    The ID of the module the task belongs to
     * @param int $parent_id    The ID of the task that is a parent to this task
     * @param string $code      A unique code assigned to the task, which can be used to reference that task in the source code. (N.B: Using the ID of the task is not safe as it may change for whatever reason.)
     * @param string $url       The controller/method (or folder/controller/method) that serves as the URL of the task
     * @param string $desc      A description of the task. (N.B: This description would appear on the privilege management page where tasks are assigned to different user groups.)
     * @param int $active       1 = Active, 0 = Inactive
     * @param int $show         1 = Show in the navigation bar, 0 = Don't show in the navigation bar
     * @return boolean          Returns true if the operation was successful, else false
     */
    public function updateTask($task_id, $module_id, $parent_id, $code, $url, $desc, $active, $show){
        $task_id =(int)$task_id;
        $active = $active == 1 ? '1' : '0';
        $data_array = array (
            'module_id' => "$module_id",
            'task_parent_id' => "$parent_id",
            'task_code' => "$code",
            'task_url' => "$url",
            'task_description' => "$desc",
            'task_enabled' => "$active",
            'task_show_in_nav_bar' => "$show"
        );
        $where_array['task_id'] = $task_id;
        $this->db->where($where_array)
                 ->update($this->task_table, $data_array);
        return $this->db->affected_rows() == 1;
    }   //END updateTask()



    /**
     * Checks if a user group is unique excluding the user group with the ID specified in the check.
     * @param string $task_code The task code to check for uniqueness
     * @param int $task_id      The ID of the task to be excluded in the search
     * @return boolean          Returns TRUE if unique, FALSE otherwise
     */
    public function isUniqueTaskCode($task_code, $task_id){
        $task_id = (int)$task_id;
        $where_array = array(
            'task_code' => "$task_code",
            'task_id <>' => "$task_id"
        );
        $this->db->get_where($this->task_table, $where_array);
        if ($this->db->affected_rows() > 0){
            $ret_val = false;
        } else {
            $ret_val = true;
        }
        return $ret_val;
    }   //END isUniqueTaskCode()



}   //END class
