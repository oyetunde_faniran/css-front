<?php
/**
 * Description of branch
 *
 * @author Tunde
 */
class State_model extends UPL_Model {

    function __construct() {
        parent::__construct();
        $table = UPL_DB_TABLE_PREFIX . 'states';
        $table_pk = 'state_id';
        $this->setTable($table);
        $this->setTablePK($table_pk);
    }

    public function getStates(){
        return $this->loadMultiple(0, 0, '', "\$this->db->order_by('state_name');");
    }

}
