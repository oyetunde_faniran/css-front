<?php

/**
 * Contains methods for manipulating user group data
 *
 * @author Faniran, Oyetunde
 */
class User_model extends UPL_Model {

    function __construct(){
        parent::__construct();
        $this->user_table = UPL_DB_TABLE_PREFIX . 'users';
        $this->userlogin_table = UPL_DB_TABLE_PREFIX . 'users_login';
        $this->usergroup_table = UPL_DB_TABLE_PREFIX . 'usergroups';
        $this->task_table = UPL_DB_TABLE_PREFIX . 'tasks';
        $this->module_table = UPL_DB_TABLE_PREFIX . 'modules';
        $this->permission_table = UPL_DB_TABLE_PREFIX . 'permission';
        $this->load->model('sys-admin/Audit_trail_model', 'audit');
    }


    /**
     * Gets a single user whose ID is specified
     * @param int $user_id          The ID of the user to fetch
     * @param boolean $enabled_only When TRUE, the user is returned only if it is active
     * @param string $email         The email of the user to fetch
     * @return array                The details of the user if found or an empty array on failure
     */
    public function getUser($user_id, $enabled_only = true, $email = ''){
        if (!empty($email)){
            $where_array = array (
                'ul.userlog_email' => "$email",
                'u.user_deleted' => "0"
            );
        } else {
            $user_id = (int)$user_id;
            $where_array = array (
                'u.user_id' => "$user_id",
                'u.user_deleted' => "0"
            );
        }
        if ($enabled_only){
            $where_array['u.user_active'] = '1';
        }
        $result = $this->db->select("u.*, ul.*, DATE_FORMAT(ul.userlog_datecreated, '%M %d, %Y') 'datecreated', DATE_FORMAT(ul.userlog_lastlogin, '%M %d, %Y') 'lastlogin', ug.usergroup_name", false)
                           ->from("{$this->user_table} u")
                           ->join("{$this->userlogin_table} ul", 'u.user_id = ul.user_id', 'INNER')
                           ->join("{$this->usergroup_table} ug", 'ul.usergroup_id = ug.usergroup_id', 'LEFT')
                           ->where($where_array)
                           ->get();
        //die('<pre>' . $this->db->last_query());
        if ($this->db->affected_rows() == 1){
            $ret_val = $result->row_array();
        } else {
            $ret_val = array();
        }
        return $ret_val;
    }   //END getUser()



    /**
     * Gets the list of existing users
     * @param boolean $enabled_only When TRUE, only active users are returned
     * @return array                The list of users sorted by surname, firstname
     */
    public function getUsers($enabled_only = true){
        $where_array['user_deleted'] = "0";
        if ($enabled_only){
            $where_array['user_active'] = "1";
        }
        $result = $this->db->select("u.*, ul.*, DATE_FORMAT(ul.userlog_datecreated, '%M %d, %Y') 'datecreated',
                                    DATE_FORMAT(ul.userlog_lastlogin, '%M %d, %Y') 'lastlogin', ug.usergroup_name", false)
                           ->from("{$this->user_table} u")
                           ->join("{$this->userlogin_table} ul", 'u.user_id = ul.user_id', 'INNER')
                           ->join("{$this->usergroup_table} ug", 'ul.usergroup_id = ug.usergroup_id', 'LEFT')
                           ->where($where_array)
                           ->order_by("u.user_surname, u.user_firstname")
                           ->get();
        if ($this->db->affected_rows() > 0){
            $ret_val = $result->result_array();
        } else {
            $ret_val = array();
        }
        return $ret_val;
    }   //END getUsers()



    /**
     * Adds a user
     * @param string $firstname     The first name of the user to add
     * @param string $surname       The surname of the user to add
     * @param string $phone         The phone number of the user to add
     * @param string $email         The email address of the user to add
     * @param string $username      The username of the user to add
     * @param string $password      The password of the user to add
     * @param string $group_id      The ID of the user group the user belongs to
     * @param string $court_id      The ID of the court the user is assigned to
     * @return int                  The ID of the user or 0 on failure
     */
    public function addUser($firstname, $surname, $phone, $email, $username, $password, $lga_temp, $group_id_temp, $ga_secret = ''){
        try {
            //Begin a database transaction
            $this->db->trans_begin();

            //Some cleaning up
            $lga = (int)$lga_temp;
            $group_id = (int)$group_id_temp;

            //Insert into the main users table
            $data_array = array (
                'user_firstname' => "$firstname",
                'user_surname' => "$surname",
                'user_phone' => "$phone",
                'sch_id' => "$group_id",
                'lga_id' => "$lga"
            );
            $this->db->insert($this->user_table, $data_array);
            if ($this->db->affected_rows() == 0){
                throw new Exception();
            }
            $user_id = $this->db->insert_id();

            $login_inserted = $this->saveUserLoginData($user_id, $email, $username, $group_id, $ga_secret);
            if (!$login_inserted){
                throw new Exception();
            }

            $this->db->trans_commit();  //Transaction was successful, so commit

            //Save the password and send it to the user's email addy
            $this->saveUserPassword($user_id, $password);
            $this->sendPassword2EMail($password, $email, $firstname, $surname, true);


            //Log Audit Trail
            $task_id = isset($_SESSION['user_privileges']['upl-user-add']['id']) ? $_SESSION['user_privileges']['upl-user-add']['id'] : 0;
            $value_before = array();
            $value_after = $this->getUser($user_id);
            $audit_ipaddress = $_SERVER['REMOTE_ADDR'];
            $this->audit->logUserAction($task_id, $value_before, $value_after, $user_id, $audit_ipaddress);


        } catch (Exception $ex) {
            $this->db->trans_rollback();    //Transaction failed, so rollback
            $user_id = 0;
        }
        return $user_id;
    }   //END addUser()



    /**
     * Saves the login details of the user
     * @param string $user_id       The user ID of the user under consideration
     * @param string $email         The email address of the user to add
     * @param string $username      The username of the user to add
     * @param string $group_id_temp      The ID of the user group the user belongs to
     * @return int                  The ID of the user or 0 on failure
     */
    protected function saveUserLoginData($user_id, $email, $username, $group_id_temp, $ga_secret)
    {
        $group_id = (int)$group_id_temp;
        $data_array = array(
            'user_id' => "$user_id",
            'usergroup_id' => "$group_id",
            'userlog_username' => "$username",
            'userlog_email' => "$email",
            'user_ga_secretkey' => "$ga_secret"
        );
        return $this->db->insert($this->userlogin_table, $data_array);
    }   //END saveUserLoginData()




    /**
     * Updates the login details of the user
     * @param string $user_id       The user ID of the user under consideration
     * @param string $email         The email address of the user to update
     * @param string $username      The username of the user to update
     * @param string $password      The password of the user to update
     * @param string $group_id      The ID of the user group the user belongs to
     * @return boolean              true on success or false on failure
     */
    protected function updateUserLoginData($user_id, $email, $username, $group_id = 0){
        $group_id = (int)$group_id;
        $data_array = array (
            'userlog_username' => "$username",
            'userlog_email' => "$email"
        );
        if ($group_id != 0){
            $data_array['usergroup_id'] = $group_id;
        }
        $where_array = array (
            'user_id' => "$user_id"
        );
        return $this->db->where($where_array)
                        ->update($this->userlogin_table, $data_array);
    }   //END saveUserLoginData()



    /**
     * Saves the password of the user
     * @param string $password  The password of the user
     * @return boolean          true on success or false on failure
     */
    protected function saveUserPassword($user_id, $password, $pass_changed = false)
    {
        $final_password = md5(md5($password) . '|' . md5($user_id) . '|' . $user_id);
        //die("-->'$final_password' -->" . $password . '|' . md5($user_id) . '|' . $user_id);
        $data_array = array(
            'userlog_password' => "$final_password",
        );
        if ($pass_changed){
            $data_array['user_password_changed'] = "1";
        }
        $where_array = array(
            'user_id' => "$user_id"
        );
        return $this->db->where($where_array)
            ->update($this->userlogin_table, $data_array);
    }   //END saveUserPassword()


    /**
     * Sends the user's password to the user's email address
     * @param string $password      The password to send
     * @param string $email         The email address of the user
     * @param string $firstname     The first name of the user
     * @param string $surname       The last name of the user
     */
    public function sendPassword2EMail($password, $email, $firstname, $surname, $new_user = false){
        $subject = 'Your Login Details on ' . DEFAULT_TITLE;

        if($new_user){
            $msg = "Hello {$firstname} {$surname},

Your profile has been created on " . DEFAULT_TITLE . " (" . site_url() . ").
Please, find below your log-in details.

USERNAME/E-MAIL: Your email address
PASSWORD: {$password}

You can change your password by updating your profile after logging in.

Please, note that you would also be required to set up your Google Authenticator app with the QR code / secret key that would be displayed to you on logging in.

Yours,
" . DEFAULT_TITLE . " Team.";
        } else {
            $msg = "Hello {$firstname} {$surname},

Your password was reset on " . DEFAULT_TITLE . " (" . site_url() . ").
Please, find below your new log-in details.

USERNAME/E-MAIL: Your email address
PASSWORD: {$password}

You can change your password by updating your profile after logging in.

Yours,
" . DEFAULT_TITLE . " Team.";
        }
        $this->_sendEmail(FROM_EMAIL, EMAIL_SENDER_NAME, $email, $subject, $msg);
    }   //END sendPassword2EMail()



    /**
     * Update the main details of a user
     * @param int $user_id          The ID of the user whose details would be updated
     * @param string $firstname     The first name of the user to update
     * @param string $surname       The surname of the user to update
     * @param string $phone         The phone number of the user to update
     * @param string $email         The email address of the user to update
     * @param string $username      The username of the user to update
     * @param string $password      The password of the user to update
     * @param string $group_id      The ID of the user group the user belongs to
     * @param int $active           Active = 1, Inactive = 0
     * @param int $court_id         The ID of the court the user is assigned to
     * @return boolean              updated = true, not updated = false
     */
    public function updateUser($user_id, $firstname, $surname, $phone, $email, $username, $password, $lga, $group_id, $active){
        try {
            $updated = 0;

            //Begin a database transaction
            $this->db->trans_begin();

            //Update the main users table
            $data_array = array (
                'user_firstname' => "$firstname",
                'user_surname' => "$surname",
                'user_phone' => "$phone",
                'lga_id' => "$lga",
                'user_active' => "$active"
            );
            $where_array = array(
                'user_id' => "$user_id"
            );
            $this->db->where($where_array)
                     ->update($this->user_table, $data_array);
            if ($this->db->affected_rows() == 1){
                $updated++;
            }

            //Save the login data
            $login_inserted = $this->updateUserLoginData($user_id, $email, $username, $group_id);
            if ($login_inserted){
                $updated++;
            }

            //Save the password and send it to the user's email addy if it was changed
            if (!empty($password)){
                $pass_saved = $this->saveUserPassword($user_id, $password);
                $this->sendPassword2EMail($password, $email, $firstname, $surname);
                if ($pass_saved){
                    $updated++;
                }
            }

            //Log Audit Trail
            $task_id = ($_SESSION['user_privileges']['upl-user-edit']['id']) ? $_SESSION['user_privileges']['upl-user-edit']['id'] : 0;
            $value_before = array();
            $value_after = $this->getUser($user_id);
            $audit_ipaddress = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
            $this->audit->logUserAction($task_id, $value_before, $value_after, $user_id, $audit_ipaddress);

            //Transaction was successful, so commit
            $this->db->trans_commit();

        } catch (Exception $ex) {
            $this->db->trans_rollback();    //Transaction failed, so rollback
        }
        return $updated > 0;
    }   //END updateUser()





    /**
     * Update the main details of the currently logged-in user
     * @param int $user_id          The ID of the user whose details would be updated
     * @param string $firstname     The first name of the user to update
     * @param string $surname       The surname of the user to update
     * @param string $phone         The phone number of the user to update
     * @param string $email         The email address of the user to update
     * @param string $username      The username of the user to update
     * @param string $password      The password of the user to update
     * @return boolean              updated = true, not updated = false
     */
    public function updateUserPersonalProfile($user_id, $firstname, $surname, $phone, $email, $username, $password){
        try {
            $updated = 0;

            //Begin a database transaction
            $this->db->trans_begin();

            //Update the main users table
            $data_array = array (
                'user_firstname' => "$firstname",
                'user_surname' => "$surname",
                'user_phone' => "$phone",
            );
            $where_array = array(
                'user_id' => "$user_id"
            );
            $this->db->where($where_array)
                     ->update($this->user_table, $data_array);
            if ($this->db->affected_rows() == 1){
                $updated++;
            }

            //Save the login data
            $login_inserted = $this->updateUserLoginData($user_id, $email, $username);
            if ($login_inserted){
                $updated++;
            }

            //Save the password and send it to the user's email addy if it was changed
            if (!empty($password)){
                $pass_saved = $this->saveUserPassword($user_id, $password);
                $this->sendPassword2EMail($password, $email, $firstname, $surname);
                if ($pass_saved){
                    $updated++;
                }
            }

            $this->db->trans_commit();  //Transaction was successful, so commit

        } catch (Exception $ex) {
            $this->db->trans_rollback();    //Transaction failed, so rollback
        }
        return $updated > 0;
    }   //END updateUser()




    /**
     * Checks if an email is unique excluding the user with the ID specified in the check.
     * @param string $email     The name of the email to check for uniqueness
     * @param int $user_id      The ID of the user to be excluded in the search
     * @return boolean          Returns TRUE if unique, FALSE otherwise
     */
    public function isUniqueEmail($email, $user_id){
        $user_id = (int)$user_id;
        $where_array = array(
            'userlog_email' => "$email",
            'user_id <>' => "$user_id"
        );
        $this->db->get_where($this->userlogin_table, $where_array);
        if ($this->db->affected_rows() > 0){
            $ret_val = false;
        } else {
            $ret_val = true;
        }
        return $ret_val;
    }   //END isUnique()



    public function loginUser($email_or_username, $password, $email_is_username = true, $update_last_login = true){
        $user = $email_is_username ? 'ul.userlog_email' : 'ul.userlog_username';
        $password_md5 = md5($password);
        $where_array = array(
            $user => "'$email_or_username'",
            'ul.userlog_password' => "MD5(CONCAT_WS('|', '$password_md5', md5(ul.user_id), ul.user_id))",
            //'u.sch_id' => SCHOOL_ID,
            'u.user_deleted' => "'0'",
            'u.user_active' => "'1'"
        );

        $result = $this->db->select("ul.*, u.*, ug.usergroup_name 'usergroup'")
                           ->from($this->userlogin_table . ' ul')
                           ->join($this->user_table . ' u', 'u.user_id = ul.user_id', 'INNER')
                           ->join($this->usergroup_table . ' ug', 'ul.usergroup_id = ug.usergroup_id', 'LEFT')
                           ->where($where_array, null, false)
                           ->get();
//        die('<pre>' . $this->db->last_query());

        //If the login was successful, then update the last log-in date
        $ret_val = $result->row_array();
        if ($this->db->affected_rows() == 1){
            if ($update_last_login){
                $this->updateLastLoginDate($ret_val['user_id']);
            }
        }
        //die('<pre>' . print_r($ret_val, true));
        return $ret_val;
    }   //END loginUser()




    private function updateLastLoginDate($user_id){
        $current_time = date('Y-m-d h:i:s');
        $data_array = array (
            'userlog_lastlogin' => "$current_time"
        );
        $where_array = array(
            'user_id' => "$user_id"
        );
        return $this->db->where($where_array)
                        ->update($this->userlogin_table, $data_array);
    }   //END updateLastLoginDate()





    public function doPasswordRemind($email){
        $user_deets = $this->getUser(0, true, $email);
        if (empty($user_deets)){
            throw new Exception('Invalid e-mail address supplied.');
        }

        //Generate a new password
        $this->load->library('general_tools');
        $password = $this->general_tools->getRandomString(rand(10, 20));

        //Save the newly generated password into the DB
        $ret_val = $this->saveUserPassword($user_deets['user_id'], $password);

        //Now, send the newly-generated password to the user
        $this->sendPassword2EMail($password, $email, $user_deets['user_firstname'], $user_deets['user_surname']);

        return $ret_val;
    }   //END doPasswordRemind()



    protected function getMenu4UserGroup($group_id){
        $group_id = (int)$group_id;
        $where_array = array(
            't.task_show_in_nav_bar' => "1",
            'p.usergroup_id' => "$group_id",
            'm.module_show_in_nav_bar' => "1"
        );
        $result = $this->db->select('m.module_name, m.module_cssclass, t.*')
                           ->from($this->task_table . ' t')
                           ->join($this->module_table . ' m', 't.module_id = m.module_id', 'INNER')
                           ->join($this->permission_table . ' p', 'p.task_id = t.task_id', 'INNER')
                           ->where($where_array)
                           ->order_by('m.module_order, m.module_dateadded DESC, t.task_order, t.task_dateadded DESC')
                           ->get();
        return $result->result_array();
    }   //END getMenu4UserGroup()



    public function loadMenu($group_id){
        $menu_array = $this->getMenu4UserGroup($group_id);
        $ret_val = '<nav class="navbar-side" role="navigation">
                        <div class="navbar-collapse sidebar-collapse collapse">
                            <ul id="side" class="nav navbar-nav side-nav">
                                <li class="active">' .
                                    anchor('', '<i class="fa fa-home"></i> Home') .
                                '</li>';
        $last_module = '';
        $is_first_run = true;

        foreach ($menu_array as $menu){
            if ($last_module != $menu['module_name']){
                if ($last_module != '' && !$is_first_run){
                    $ret_val .= '   </ul>
                                 </li>';
                }
                $is_first_run = false;
                $ret_val .= '<li class="panel">
                                <a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#' . $menu['module_id'] . '">
                                    <i class="' . $menu['module_cssclass'] . '"></i>' . $menu['module_name'] . '<i class="fa fa-caret-down"></i>
                                </a>
                                <ul class="collapse nav" id="' . $menu['module_id'] . '">';
            }
            $ret_val .= '<li>' . anchor($menu['task_url'], '<i class="fa fa-angle-double-right"></i>' . $menu['task_label']) . '</li>';
            $last_module = $menu['module_name'];
        }   //END foreach()
        if (!$is_first_run){
            $ret_val .= '   </ul>
                         </li>';
        }
        $ret_val .= '           <li class="active">' .
                                    anchor('sys-admin/login/logout', '<i class="fa fa-arrow-left"></i> Log Out') .
                                '</li>
                            </ul>
                        </div>
                    </nav>';
        return $ret_val;
    }   //END loadMenu()
    
    
    
    public function lockAccount($user_id){
        $where_array = array('user_id' => $user_id);
        $data_array = array('user_active' => '0');
        $this->db->where($where_array)
                 ->update('upl_users', $data_array);
        $ret_val = $this->db->affected_rows();
        return $ret_val;
    }   //END lockAccount()



}   //END class