<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of audit_trail
 *
 * @author Tunde
 */
class Audit_trail_model extends CI_Model {
    function __construct(){
        parent::__construct();
        $this->audit_table = UPL_DB_TABLE_PREFIX . 'audit_trail';
        $this->user_table = UPL_DB_TABLE_PREFIX . 'users';
        $this->task_table = UPL_DB_TABLE_PREFIX . 'tasks';
    }


    public function logUserAction($task_id, $value_before, $value_after, $user_id, $audit_ipaddress){
        $task_id = (int)$task_id;
        $user_id = (int)$user_id;
        $value_before = is_array($value_before) ? serialize($value_before) : $value_before;
        $value_after = is_array($value_after) ? serialize($value_after) : $value_after;
        $data = array(
                    'task_id' => "$task_id",
                    'user_id' => "$user_id",
                    'audit_value_before' => "$value_before",
                    'audit_value_after' => "$value_after",
                    'audit_ipaddress' => "$audit_ipaddress"
                );
        return $this->db->insert($this->audit_table, $data);
    }   //END logUserAction()


    public function getAuditTrailEntry($audit_trail_id){
        $audit_trail_id = (int)$audit_trail_id;
        $result = $this->db->select("a.*, t.task_description, CONCAT_WS(' ', u.user_firstname, u.user_surname)", false)
                           ->from($this->audit_table . ' a')
                           ->join($this->task_table . ' t', 'a.task_id = t.task_id', 'LEFT')
                           ->join($this->user_table . ' u', 'a.user_id = u.user_id', 'LEFT')
                           ->where(array('audit_trail_id' => $audit_trail_id))
                           ->get();
        return $result->row_array();
    }   //END getAuditTrailEntry()



    public function getAuditTrailLogs($limit = 0, $start = 0, $user_id = 0, $task_id = 0, $start_date = '', $end_date = ''){
        $where_array = array();

        //Apply the USER filter if need be
        if (!empty($user_id)){
            $where_array['user_id'] = $user_id;
        }

        //Apply the TASK filter if need be
        if (!empty($task_id)){
            $where_array['task_id'] = $task_id;
        }

        //Apply the DATE filter if need be
        if (!empty($start_date) && !empty($end_date)){
            $where_array['audit_trail_time_logged BETWEEN '] = "$start_date AND $end_date";
        }

        //Limit the number of rows if required
        if (!empty($limit)){
            if (!empty($start)){
                $this->db->limit($limit, $start);
            } else {
                $this->db->limit($limit);
            }
        }

        $result = $this->db->select("a.*, t.task_description, CONCAT_WS(' ', u.user_firstname, u.user_surname) 'user_name', DATE_FORMAT(audit_trail_time_logged, '%M %d, %Y %h:%i:%s') 'date_logged'", false)
                           ->from($this->audit_table . ' a')
                           ->join($this->user_table . ' u', 'a.user_id = u.user_id', 'INNER')
                           ->join($this->task_table . ' t', 'a.task_id = t.task_id', 'INNER')
                           ->where($where_array)
                           ->order_by('audit_trail_time_logged DESC')
                           ->get();
        return $result->result_array();
    }   //END getAuditTrailLogs()


}   //END class