<?php

class Account_model extends UPL_Model
{

    function __construct()
    {
        parent::__construct();
        $table = 'bank_accounts';
        $table_pk = 'acc_id';
        $this->setTable($table);
        $this->setTablePK($table_pk);
    }


    public function getAccounts($enabled_only = true, $order_by_temp = 'acc_name', $cat_id = 0)
    {
        //WHERE clause thingz
        if ($enabled_only) {
            $where_array['acc_enabled'] = '1';
        }
        if ($cat_id != 0) {
            $where_array['accat_id'] = $cat_id;
        }
        if (!empty($where_array)) {
            $this->db->where($where_array);
        }

        $order_by = $order_by_temp == 'acc_name' ? 'a.acc_name' : 'b.bank_name, a.acc_name';
        $result = $this->db->select('a.*, b.bank_name')
            ->from('bank_accounts a')
            ->join('banks b', 'a.bank_id = b.bank_id', 'INNER')
            ->order_by($order_by)
            ->get();
        //die('<pre>' . $this->db->last_query());
        $ret_val = $result->result_array();
        return $ret_val;
    }    //END getAccounts()


    public function getAccount($acc_id_temp)
    {
        $acc_id = (int)$acc_id_temp;
        $result = $this->db->select('a.*, b.bank_name')
            ->from('bank_accounts a')
            ->join('banks b', 'a.bank_id = b.bank_id', 'INNER')
            ->where(array('acc_id' => "$acc_id"))
            ->get();
        //die('<pre>' . $this->db->last_query());
        $ret_val = $result->row_array();
        return $ret_val;
    }    //END getAccounts()


    public function getClientAccounts($client_id, $enabled_only = true)
    {
        $where_array['a.client_id'] = "$client_id";
        if ($enabled_only) {
            $where_array['a.acc_enabled'] = '1';
        }
        $result = $this->db->select('a.*, b.bank_name, b.bank_code')
            ->from('bank_accounts a')
            ->join('banks b', 'a.bank_id = b.bank_id', 'INNER')
            ->where($where_array)
            ->get();
//        die('<pre>' . $this->db->last_query());
        $ret_val = $result->result_array();
        return $ret_val;
    }    //END getAccounts()


    public function addAccount($client_id, $bank_id, $acc_name, $acc_number)
    {
        //Data to be inserted or updated
//        $data_array = array(
//            'client_id' => "$client_id",
//            'bank_id' => "$bank_id",
//            'acc_name' => "$acc_name",
//            'acc_number' => "$acc_number",
//        );

        //Where array to check for the existence of the account
//        $where_array = array(
//            'client_id' => "$client_id",
//            'bank_id' => "$bank_id",
//            'acc_number' => "$acc_number",
//        );

        //die('<pre>' . print_r($data_array, true) . print_r($where_array, true));

        //Check if the account exists already
//        $result = $this->db->where($where_array)
//            ->get('bank_accounts');
        $dis_query = "SELECT * FROM bank_accounts 
                        WHERE `client_id` = '$client_id'
                            AND `bank_id` = '$bank_id'
                            AND `acc_number` = '$acc_number'";
        $result = $this->db->query($dis_query);
        $row = $result->row_array();

        try {
            //Account exists, throw exception so that it would be updated instead
            if (!empty($row)) {
                echo '<pre style="color: #000;">IT EXISTS: ' . $this->db->last_query() . '</pre>';
                throw new Exception();
            }
            echo '<pre style="color: #000;">IT DOESN\'T EXIST: ' . $this->db->last_query() . '</pre>';

            //Do a fresh add of the account
//            $ret_val = $this->db->insert('bank_accounts', $data_array);
            $insert_query = "INSERT INTO bank_accounts
                                SET client_id = '$client_id',
                                    bank_id = '$bank_id'
                                    acc_number = '$acc_number'
                                    acc_name = '$acc_name'";
            $ret_val = $this->db->query($insert_query);

            echo '<pre style="color: #00F;">INSERTED: ' . $this->db->last_query() . '</pre><hr />';

        } catch (Exception $ex) {
            //Update the details of the account if need be
//            $current_acc_name = !empty($row['acc_name']) ? $row['acc_name'] : '';
//            if ($acc_name != $current_acc_name) {
//                $ret_val = $this->db->where($where_array)
//                    ->update($data_array);
//                echo '<pre style="color: #0F0;">UPDATED: ' . print_r($data_array, true) . "\n" . $this->db->last_query() . '</pre><hr />';
//            } else {
//                echo '<pre style="color: #F00;">NOTHING DONE: ' . print_r($data_array, true) . '</pre><hr />';
//            }
//            $ret_val = $this->db->where($where_array)
//                                ->update($data_array);
            $update_query = "UPDATE bank_accounts
                                SET acc_name = '$acc_name'
                                WHERE `client_id` = '$client_id'
                                    AND `bank_id` = '$bank_id'
                                    AND `acc_number` = '$acc_number'";
            $ret_val = $this->db->query($update_query);
            echo '<pre style="color: #0F0;">UPDATED: ' . $this->db->last_query() . '</pre><hr />';
            
        }   //END try..catch

        return $ret_val;
    }   //END addAccount()


    public function addAccountToCategory($cat_id, $acc_ids)
    {
        $acc_ids_string = implode(',', $acc_ids);
        $ret_val = $this->db->where("acc_id IN ($acc_ids_string)")
            ->update('bank_accounts', array('accat_id' => $cat_id));
        return $ret_val;
    }   //END addAccountToCategory()


}    //END class()