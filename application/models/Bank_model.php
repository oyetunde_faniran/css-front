<?php

class Bank_model extends UPL_Model
{

    function __construct()
    {
        parent::__construct();
        $table = 'banks';
        $table_pk = 'bank_id';
        $this->setTable($table);
        $this->setTablePK($table_pk);
    }    //END __construct()


    public function getAccountBanks($state_id = 12)
    {
        /*
        SELECT DISTINCT b.*
        FROM banks bs
            INNER JOIN bank_accounts a ON b.bank_id = a.bank_id
        WHERE b.bank_enabled = '1' AND a.state_id = 12
        */
        $where_array = array(
            'b.bank_enabled' => "1",
            'a.state_id' => "$state_id"
        );

        $result = $this->db->select("DISTINCT b.*", false)
            ->from('banks b')
            ->join('bank_accounts a', 'b.bank_id = a.bank_id', 'INNER')
            ->where($where_array)
            ->order_by('b.bank_name')
            ->get();
        $ret_val = $result->result_array();
        return $ret_val;
    }    //END getAccountBanks()


    public function getBankFromBankCode($bank_code)
    {
        $result = $this->db->query("SELECT * FROM banks WHERE bank_code = '$bank_code'");
        //die('<pre>' . $this->db->last_query());
        $ret_val = $result->row_array();
        return $ret_val;
    }   //END getBankFromBankCode()


    public function getBank($id)
    {
        $result = $this->db->get_where("banks", array("bank_id" => $id));

        return $result->num_rows() > 0 ? $result->row_array() : null;
    }


}    //END class()