<?php

/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 12/1/2016
 * Time: 5:42 AM
 */
class Validated_account_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Retrieve all validated accounts.
     *
     * @param null $id
     * @return null
     */
    public function all($id = null)
    {
        /*if ($id) {
            if (is_array($id))
                $this->db->where_in("va.vs_id", $id);
            else
                $this->db->where("va.vs_id", $id);
        }*/

        $result = $this->db
            ->select("va.*, b.bank_name, b.bank_id", false)
            ->from("validated_accounts va")
            ->join("banks b", "va.bank_code = b.bank_code", "LEFT")
            ->get();

        $ret_Val = $result && $result->num_rows() > 0 ? $result->result_array() : null;

        return $ret_Val;
    }

    /**
     * Retrieve all validated accounts in a bank.
     *
     * @param $id
     * @param bool|true $use_bank_code
     * @return null
     */
    public function allInBank($id, $use_bank_code = true)
    {
        $result = $this->db
            ->select("va.*, b.bank_name, b.bank_id", false)
            ->from("validated_accounts va")
            ->join("banks b", "va.bank_code = b.bank_code", "LEFT")
            ->where(array(($use_bank_code ? 'va.bank_code' : 'b.bank_id') => $id))
            ->get();

        $ret_Val = $result && $result->num_rows() > 0 ? $result->result_array() : null;

        return $ret_Val;
    }

    /**
     * Retrieve all validated accounts having or not having a particular response code.
     *
     * @param $code
     * @param bool|true $include
     * @return null
     */
    public function allWithResponseCode($code, $include = true)
    {
        if (is_array($code)) {
            if ($include)
                $this->db->where_in("va.response_code", $code);
            else
                $this->db->where_not_in("va.response_code", $code);
        } else {
            if ($include)
                $this->db->where("va.response_code", $code);
            else
                $this->db->where("va.response_code !=", $code);
        }

        $result = $this->db
            ->select("va.*, b.bank_name, b.bank_id", false)
            ->from("validated_accounts va")
            ->join("banks b", "va.bank_code = b.bank_code", "LEFT")
            ->order_by("va.response_code")
            ->get();

        $ret_Val = $result && $result->num_rows() > 0 ? $result->result_array() : null;

        return $ret_Val;
    }

    /**
     * Retrieve validated account by ID.
     *
     * @param $id
     * @return null
     */
    public function find($id)
    {
        $result = $this->db
            ->select("va.*, b.bank_name, b.bank_id", false)
            ->from("validated_accounts va")
            ->join("banks b", "va.bank_code = b.bank_code", "LEFT")
            ->where("va.id", $id)
            ->get();

        $ret_Val = $result && $result->num_rows() > 0 ? $result->row_array() : null;

        return $ret_Val;
    }

    /**
     * Retrieve validated account by Account Name.
     *
     * @param $account_name
     * @return null
     */
    public function findByAccountName($account_name)
    {
        $result = $this->db
            ->select("va.*, b.bank_name, b.bank_id", false)
            ->from("validated_accounts va")
            ->join("banks b", "va.bank_code = b.bank_code", "LEFT")
            ->where("va.account_name", $account_name)
            ->get();

        $ret_Val = $result && $result->num_rows() > 0 ? $result->row_array() : null;

        return $ret_Val;
    }

    /**
     * Retrieve validated account by Account Number.
     *
     * @param $account_no
     * @return null
     */
    public function findByAccountNo($account_no)
    {
        $result = $this->db
            ->select("va.*, b.bank_name, b.bank_id", false)
            ->from("validated_accounts va")
            ->join("banks b", "va.bank_code = b.bank_code", "LEFT")
            ->where("va.account_no", $account_no)
            ->get();

        $ret_Val = $result && $result->num_rows() > 0 ? $result->row_array() : null;

        return $ret_Val;
    }

    /**
     * Retrieve validated account by Account Name.
     *
     * @param $account_name
     * @return null
     */
    public function searchByAccountName($account_name)
    {
        $result = $this->db
            ->select("va.*, b.bank_name, b.bank_id", false)
            ->from("validated_accounts va")
            ->join("banks b", "va.bank_code = b.bank_code", "LEFT")
            ->like("va.account_name", $account_name)
            ->get();

        $ret_Val = $result && $result->num_rows() > 0 ? $result->result_array() : null;

        return $ret_Val;
    }

    /**
     * Retrieve validated account by Account Number.
     *
     * @param $account_no
     * @return null
     */
    public function searchByAccountNo($account_no)
    {
        $result = $this->db
            ->select("va.*, b.bank_name, b.bank_id", false)
            ->from("validated_accounts va")
            ->join("banks b", "va.bank_code = b.bank_code", "LEFT")
            ->like("va.account_no", $account_no)
            ->get();

        $ret_Val = $result && $result->num_rows() > 0 ? $result->result_array() : null;

        return $ret_Val;
    }

    /**
     * Add many accounts.
     *
     * @param $accounts
     * @return mixed
     */
    public function saveMany($accounts)
    {
        $_data = array();

        foreach ($accounts as $account) {
            $_data[] = array(
                'account_name' => $account['account_name'],
                'account_no' => $account['account_no'],
                'bank_code' => $account['bank_code'],
                'created_at' => date("Y-m-d H:i:s")
            );
        }

        $this->db->insert_ignore_batch("validated_accounts", $_data);

        return $this->db->affected_rows();
    }

    /**
     * Update Local NIBSSPayPlus DB with responses from validation schedules.
     *
     * @return mixed
     */
    public function updateAll()
    {
        //Update our local NIBSSPayPlus db
        $sql = "UPDATE validated_accounts v
                JOIN (
                    SELECT
                      va.va_account_name AS vendor_account_name,
                      va.va_account_no AS vendor_account_no,
                      va.va_bank_code AS vendor_bank_code,
                      va.va_vendor_no AS vendor_no,
                      va.va_response_code AS vendor_response_code,
                      va.va_response_description AS vendor_response_description,
                      va.va_error_reason AS vendor_error_reason,
                      vs.vs_ref AS schedule_ref
                    FROM validation_accounts va
                    JOIN validation_schedules vs ON va.vs_id = vs.vs_id
                ) va2 ON v.account_name = va2.vendor_account_name AND v.account_no = va2.vendor_account_no AND v.bank_code = va2.vendor_bank_code
                SET
                  v.response_code = va2.vendor_response_code,
                  v.response_description = va2.vendor_response_description,
                  v.error_reason = va2.vendor_error_reason,
                  v.schedule_ref = va2.schedule_ref,
                  v.vendor_no = va2.vendor_no";

        $this->db->query($sql);

        return $this->db->affected_rows();
    }
}