<?php

/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 12/1/2016
 * Time: 5:19 AM
 */
class Validation_schedule_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Retrieve all validation schedules, optionally of a particular status.
     *
     * @param null $status
     * @return null
     */
    public function all($status = null)
    {
        if ($status)
            $this->db->where("vs.vs_status", $status);

        $result = $this->db
            ->select("vs.vs_id AS schedule_id,
                        vs.vs_ref AS schedule_ref,
                        vs.vs_status AS schedule_status,
                        s.status_description AS schedule_status_description,
                        vs.vs_response_code AS schedule_response_code,
                        vs.vs_response_description AS schedule_response_description,
                        vs.vs_attempts AS schedule_requery_attempts", false)
            ->from("validation_schedules vs")
            ->join("statuses s", "vs.vs_status = s.status_id", "LEFT")
            ->get();

        $ret_Val = $result && $result->num_rows() > 0 ? $result->result_array() : null;

        return $ret_Val;
    }

    /**
     * Retrieve validation schedule by ID or Ref.
     *
     * @param $id
     * @param bool|true $use_id_row
     * @return null
     */
    public function find($id, $use_id_row = true)
    {
        $result = $this->db
            ->select("vs.vs_id AS schedule_id,
                        vs.vs_ref AS schedule_ref,
                        vs.vs_status AS schedule_status,
                        s.status_description AS schedule_status_description,
                        vs.vs_response_code AS schedule_response_code,
                        vs.vs_response_description AS schedule_response_description,
                        vs.vs_attempts AS schedule_requery_attempts", false)
            ->from("validation_schedules vs")
            ->join("statuses s", "vs.vs_status = s.status_id", "LEFT")
            ->where(array(($use_id_row ? 'vs.vs_id' : 'vs.vs_ref') => $id))
            ->get();

        $ret_Val = $result && $result->num_rows() > 0 ? $result->row_array() : null;

        return $ret_Val;
    }

    /**
     * Create validation schedule.
     *
     * @return array|null array containing the auto_increment ID and the schedule_ref just generated
     */
    public function create()
    {
        $schedule_ref = $this->basic_functions->create_random_number_string(10);

        $data = array(
            'vs_ref' => $schedule_ref,
            'vs_created_at' => date('Y-m-d H:i:s')
        );

        $result = $this->db->insert("validation_schedules", $data);

        return $result && $this->db->affected_rows() > 0 ? array($this->db->insert_id(), $schedule_ref) : null;
    }

    /**
     * Update validation schedule.
     *
     * @param $id
     * @param $data
     * @param bool|true $use_id
     * @return mixed
     */
    public function update($id, $data, $use_id = true)
    {
        $this->db->set("vs_status", $data['status']);
        $this->db->set("vs_response_code", $data['response_code']);
        $this->db->set("vs_response_description", $data['response_description']);

        if (!empty($data['log_attempts']))
            $this->db->set("vs_attempts", "vs_attempts + 1", false);

        $this->db->where(array(($use_id ? "vs_id" : "vs_ref") => $id));

        return $this->db->update("validation_schedules");
    }

    /**
     * Retrieve all
     * @return null
     */
    public function inProgress()
    {
        $result = $this->db
            ->select("vs.*")
            ->from("validation_schedules")
            ->join("statuses s", "vs.vs_status = s.status_id")
            ->where("s.status_description == 'IN PROGRESS'")
            ->get();

        return $result && $result->num_rows() > 0 ? $result->result_array() : null;
    }

    /**
     * Delete schedule and all accounts in it.
     *
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        $result = $this->db->where("vs_id", $id)->delete(array("validation_schedules", "validation_accounts"));

        return $result ? $this->db->affected_rows() : 0;
    }
}