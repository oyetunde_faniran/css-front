<?php

/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 12/1/2016
 * Time: 5:31 AM
 */
class Validation_account_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Retrieve all validation accounts, in 1 or more validation schedules.
     *
     * @param null $id
     * @return null
     */
    public function all($id = null)
    {
        if ($id) {
            if (is_array($id))
                $this->db->where_in("va.vs_id", $id);
            else
                $this->db->where("va.vs_id", $id);
        }

        $result = $this->db
            ->select("va.va_id AS account_id,
                        va.vs_id AS schedule_id,
                        va.va_vendor_no AS vendor_no,
                        va.va_account_name AS account_name,
                        va.va_account_no AS account_no,
                        va.va_bank_code AS bank_code,
                        b.bank_name AS bank_name,
                        b.bank_id AS bank_id,
                        va.va_status AS validation_status,
                        s.status_description AS validation_status_description,
                        va.va_response_code AS validation_response_code,
                        va.va_response_description AS validation_response_description,
                        va.va_error_reason AS validation_error_reason", false)
            ->from("validation_accounts va")
            ->join("statuses s", "va.va_status = s.status_id", "LEFT")
            ->join("banks b", "va.va_bank_code = b.bank_code", "LEFT")
            ->get();

        $ret_Val = $result && $result->num_rows() > 0 ? $result->result_array() : null;

        return $ret_Val;
    }

    /**
     * Retrieve validation account by ID.
     *
     * @param $id
     * @return null
     */
    public function find($id)
    {
        $result = $this->db
            ->select("va.va_id AS account_id,
                        va.vs_id AS schedule_id,
                        va.va_vendor_no AS vendor_no,
                        va.va_account_name AS account_name,
                        va.va_account_no AS account_no,
                        va.va_bank_code AS bank_code,
                        b.bank_name AS bank_name,
                        b.bank_id AS bank_id,
                        va.va_status AS account_status,
                        s.status_description AS account_status_description,
                        va.va_response_code AS account_response_code,
                        va.va_response_description AS account_response_description,
                        va.va_error_reason AS account_error_reason", false)
            ->from("validation_accounts va")
            ->join("statuses s", "va.va_status = s.status_id", "LEFT")
            ->join("banks b", "va.va_bank_code = b.bank_code", "LEFT")
            ->where("va.va_id", $id)
            ->get();

        $ret_Val = $result && $result->num_rows() > 0 ? $result->row_array() : null;

        return $ret_Val;
    }

    public function createMany($data)
    {
        $_data = array();

        foreach ($data as $item) {
            $_data[] = array(
                'vs_id' => $item['schedule_id'],
                'va_vendor_no' => $this->basic_functions->create_random_number_string(5),
                'va_account_name' => $item['account_name'],
                'va_account_no' => $item['account_no'],
                'va_bank_code' => $item['bank_code'],
                'va_created_at' => date('Y-m-d H:i:s')
            );
        }

        return $this->db->insert_batch("validation_accounts", $_data);
    }

    /**
     * Update account.
     *
     * @param null $id
     * @param $data
     * @return mixed
     */
    public function update($id, $data)
    {
        $this->db->set("va_status", $data['status']);
        $this->db->set("va_response_code", $data['response_code']);
        $this->db->set("va_response_description", $data['response_description']);

        if (!empty($data['error_reason']))
            $this->db->set("va_error_reason", $data['error_reason']);

        $this->db->where("va_id", $id);

        return $this->db->update("validation_accounts");
    }

    /**
     * Update accounts 1 schedule.
     *
     * @param null $id
     * @param $data
     * @return mixed
     */
    public function updateMany($id = null, $data)
    {
        $this->db->set("va_status", $data['status']);
        $this->db->set("va_response_code", $data['response_code']);
        $this->db->set("va_response_description", $data['response_description']);

        if (!empty($data['error_reason']))
            $this->db->set("va_error_reason", $data['error_reason']);

        $this->db->where("vs_id", $id);

        return $this->db->update("validation_accounts");
    }

    /**
     * Update account by using some details to search.
     *
     * @param $details
     * @param $data
     * @return mixed
     */
    public function updateByDetails($details, $data)
    {
        $this->db->set("va_status", $data['status']);
        $this->db->set("va_response_code", $data['response_code']);
        $this->db->set("va_response_description", $data['response_description']);

        if (!empty($data['error_reason']))
            $this->db->set("va_error_reason", $data['error_reason']);

        $where = array(
            'va_account_no' => $details['account_no'],
            'va_account_name' => $details['account_name'],
            'va_bank_code' => $details['bank_code']
        );

        $this->db->where($where);

        return $this->db->update("validation_accounts");
    }

    /**
     * Delete all accounts in a validation schedule.
     *
     * @param $schedule_id
     * @return int
     */
    public function deleteAllInSchedule($schedule_id)
    {
        $result = $this->db->delete("validation_accounts", array("vs_id" => $schedule_id));

        return $result ? $this->db->affected_rows() : 0;
    }
}