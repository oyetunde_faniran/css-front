<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Clients
 *
 * @author oyetunde.faniran
 */
class Clients_model extends UPL_Model {
    
    
    function __construct() {
        parent::__construct();
        $table = 'api_clients';
        $table_pk = 'client_id';
        $this->setTable($table);
        $this->setTablePK($table_pk);
    }
    
    
    
    public function getClients($enabled_only = true){
        if($enabled_only){
            $where_array = array('client_enabled' => "1");
            $this->db->where($where_array);
        }
        $result = $this->db->order_by('client_name')
                           ->get('api_clients');
        $ret_val = $result->result_array();
        return $ret_val;
    }   //END getClients()
    
    
    
    
    public function getClient($client_id){
        $where_array = array('client_id' => "$client_id");
        $result = $this->db->where($where_array)
                           ->order_by('client_name')
                           ->get('api_clients');
        $ret_val = $result->row_array();
        return $ret_val;
    }   //END getClient()
    
    
    
}   //END class