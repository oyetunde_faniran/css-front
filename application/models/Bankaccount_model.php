<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Bankaccount_model
 *
 * @author tundefaniran
 */
class Bankaccount_model extends UPL_Model {
    
    
    
    function __construct(){
        parent::__construct();
        $table = 'bankdeposits';
        $table_pk = 'bdeposit_id';
        $this->setTable($table);
        $this->setTablePK($table_pk);
    }
    
    
    
    public function getBankDeposits($where_clause){
        $result = $this->db->select("*, b.bank_name, CONCAT_WS(' ', u.user_firstname, u.user_surname) username, CONCAT_WS(' ', u2.user_firstname, u2.user_surname) approver, DATE_FORMAT(bdeposit_datedeposited, '%M %d, %Y') depositdate, DATE_FORMAT(bdeposit_timelogged, '%M %d, %Y %H:%i:%s') timelogged, DATE_FORMAT(bdeposit_timelogged, '%Y-%m-%d') datelogged", false)
                           ->from('bankdeposits bd')
                           ->join('banks b', 'b.bank_id = bd.bank_id', 'INNER')
                           ->join('upl_users u', 'u.user_id = bd.user_id', 'INNER')
                           ->join('upl_users u2', 'u2.user_id = bd.user_id_approver', 'LEFT')
                           ->where($where_clause)
                           ->get();
        $ret_val = $result->result_array();
        return $ret_val;
    }
    
    
    public function saveKeystoneBankStatement($data, $deposit_id){
        $data_array = [];
        foreach ($data as $d){
            $d['bdeposit_id'] = $deposit_id;
            $data_array[] = $d;
        }
        $this->db->insert_batch('bankdeposits_contents', $data_array);
    }
    
    
    
    public function saveBankStatementProjectAssignment($id, $project_id){
        $data_array = ['project_id' => "$project_id"];
        $where_array = ['bdeets_id' => "$id"];
        $this->db->where($where_array)
                 ->update('bankdeposits_contents', $data_array);
    }
    
    
    
    
    public function getKeystoneBankStatement($deposit_id, $where_array = []){
        $where_array['bdeposit_id'] = "$deposit_id";
        $result = $this->db->where($where_array)
                           ->order_by('bdeets_id')
                           ->get('bankdeposits_contents');
        $ret_val = $result->result_array();
        return $ret_val;
    }
    
    
    public function updateBankStatement($deposit_id, $data_array = []){
        $where_array['bdeposit_id'] = "$deposit_id";
        $ret_val = $this->db->where($where_array)
                            ->update('bankdeposits', $data_array);
        return $ret_val;
    }


    public function updateBankDepositContentStatus($bdeposit_content_ids_temp){
        $bdeposit_content_ids = trim($bdeposit_content_ids_temp, ',');
        $bdeposit_content_ids_array = explode(',', $bdeposit_content_ids);
        return $this->db->where_in('bdeets_id', $bdeposit_content_ids_array)
                    ->update('bankdeposits_contents', ['commission_generated' => "1"]);
    }
    
    
    
    public function getStatementRecords4CommissionInit($deposit_id, $project_id = 0, $where_array = [], $monthly_aggregation = 0){
        if ($monthly_aggregation == 0){
            $where_array['bdc.bdeposit_id'] = "$deposit_id";
        } else {
            $this->db->where_in('p.prj_when2split', ['1', '2']);
        }
        $where_array['bdc.transtype'] = "1";
        $where_array['bdc.commission_generated'] = "0";
        $where_array['bd.bdeposit_status'] = "4";

        if($project_id > 0){
            $where_array['bdc.project_id'] = $project_id;
        }

//        die('wait');
        $result = $this->db->select('bdc.project_id, SUM(bdc.amount) total_amount, GROUP_CONCAT(bdc.bdeets_id) bdeets_id')
                            ->from('bankdeposits_contents bdc')
                            ->join('comm_projects p', 'bdc.project_id = p.prj_id', 'INNER')
                            ->join('bankdeposits bd', 'bd.bdeposit_id = bdc.bdeposit_id', 'INNER')
                            ->where($where_array)
                            ->group_by('bdc.project_id')
                            ->get();
//        die('<pre>' . $this->db->last_query() . '</pre><p>&nbsp;</p>');
        $ret_val = $result->result_array();
        return $ret_val;
    }
    
    
    /**
     * Get statements that have been approved and are yet to be converted to commission entries
     * @return array
     */
    public function getStatements4CommissionInit($deposit_id = 0, $project_id = 0){
        $where_array = [
            'bd.bdeposit_status' => "4",        //Approved bank deposit batch
            'p.prj_when2split <>' => "3",       //Project is not configured as "Never Split"
            'bdc.commission_generated' => "0",  //Bank deposit entry is yet to be used to generate a commission
            'bdc.transtype' => "1"              //Trans type = credit
        ];
        if($deposit_id > 0){
            $where_array['bd.bdeposit_id'] = $deposit_id;
        }
        if($project_id > 0){
            $where_array['bdc.project_id'] = $project_id;
        }
//        $result = $this->db->from('bankdeposits bd')
//                ->join('banks b', 'b.bank_id = bd.bank_id', 'INNER')
//                ->where($where_array)
//                ->get();
        $result = $this->db->from('bankdeposits bd')
            ->join('banks b', 'b.bank_id = bd.bank_id', 'INNER')
            ->join('bankdeposits_contents bdc', 'bdc.bdeposit_id = bd.bdeposit_id')
            ->join('comm_projects p', 'p.prj_id = bdc.project_id', 'INNER')
            ->where($where_array)
            ->get();
        //die('<pre>' . $this->db->last_query() . '</pre><p>&nbsp;</p>');
        $ret_val = $result->result_array();
        return $ret_val;
    }
    
    
}
