<?php

class Account_transfer_model extends UPL_Model
{

    function __construct()
    {
        parent::__construct();
        $table = 'transfer_requests';
        $table_pk = 'transreq_id';
        $this->setTable($table);
        $this->setTablePK($table_pk);
    }


    public function getTransfers()
    {
        $result = $this->db->select("tr.*,
									a_from.acc_name 'acc_name_from',
									a_from.acc_number 'acc_no_from',
									a_from.bank_id 'bank_id_from',
									b_from.bank_name 'bank_name_from',
									a_to.acc_name 'acc_name_to',
									a_to.acc_number 'acc_no_to',
									a_to.bank_id 'bank_id_to',
									b_to.bank_name 'bank_name_to',
									DATE_FORMAT(tr.transreq_dateinitiated, '%M %d, %Y %H:%i:%s') date_formatted,
									CONCAT_WS(' ', u.user_firstname, u.user_surname) 'name_initiator'", false)
            ->from('transfer_requests tr')
            ->join('bank_accounts a_from', 'tr.acc_id_from = a_from.acc_id', 'INNER')
            ->join('bank_accounts a_to', 'tr.acc_id_to = a_to.acc_id', 'INNER')
            ->join('banks b_from', 'a_from.bank_id = b_from.bank_id', 'INNER')
            ->join('banks b_to', 'a_to.bank_id = b_to.bank_id', 'INNER')
            ->join('upl_users u', 'u.user_id = tr.user_id_initiator', 'INNER')
            ->order_by('tr.transreq_dateinitiated DESC')
            ->get();
        $ret_val = $result->result_array();
        //die('<pre>---' . print_r($ret_val, true));
        return $ret_val;
    }    //END getAccounts()

}    //END class()