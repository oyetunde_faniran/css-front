<?php

class Vendor_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();

    }

    public function addVendor($vendorName, $accountName, $bankId, $accountNo)
    {
        try {
            //Begin a database transaction
            $this->db->trans_begin();
            $now = date("Y-m-d H:i:s");
            $merchantId = $_SESSION['mymerchantId'];
            //Insert into the main merchants table
            $data_array = array(
                'vendorName' => "$vendorName",
                'merchantId' => "$merchantId",
                'status' => "0",
                'createdDate' => "$now",
                'createdBy' => $_SESSION['myuserId'],
                'modifiedBy' => $_SESSION['myuserId'],
                'dateModified' => "$now"
            );
            $this->db->insert("vendors", $data_array);

            if ($this->db->affected_rows() == 0) {
                throw new Exception();
            }
            $vendorId = $this->db->insert_id();

            $account_array = array(
                'vendorId' => "$vendorId",
                'accountName' => "$accountName",
                'bankId' => "$bankId",
                'accountNo' => "$accountNo",
                'createdDate' => "$now",
                'createdBy' => $_SESSION['myuserId'],
                'modifiedBy' => $_SESSION['myuserId'],
                'dateModified' => "$now"
            );
            $this->db->insert("vendor_account", $account_array);
            $this->db->trans_commit();
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            $vendorId = 0;
        }

        return $vendorId;
    }

    public function updateVendor($vendorName, $vendorId, $vendorAccountId, $accountName, $accountNo, $bankId)
    {
        try {
            $this->db->trans_begin();
            $where_array = array('vendorId' => "$vendorId");
            $now = date("Y-m-d H:i:s");
            $data_array = array(
                'vendorName' => "$vendorName",
                'status' => "0",
                'modifiedBy' => $_SESSION['myuserId'],
                'dateModified' => "$now"
            );

            $this->db->update("vendors", $data_array, $where_array);
            $where_account = array('vendorAccountId' => "$vendorAccountId");
            $account_array = array(
                'vendorId' => "$vendorId",
                'accountName' => "$accountName",
                'bankId' => "$bankId",
                'accountNo' => "$accountNo",
                'createdDate' => "$now",
                'createdBy' => $_SESSION['myuserId'],
                'modifiedBy' => $_SESSION['myuserId'],
                'dateModified' => "$now"
            );
            $this->db->update("vendor_account", $account_array, $where_account);
            $this->db->trans_commit();
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            $vendorId = 0;
        }
        $where_array = array('vendorId' => "$vendorId");
        $now = date("Y-m-d H:i:s");
        $data_array = array(
            'vendorName' => "$vendorName",
            'status' => "0",
            'modifiedBy' => $_SESSION['myuserId'],
            'dateModified' => "$now"
        );
        return $this->db->update("vendors", $data_array, $where_array);
    }

    public function getVendor($vendorId)
    {
        $retVal = array();
        $whereArray = array("v.vendorId" => "$vendorId");
        $result = $this->db->select('v.*, a.vendorAccountId, a.accountName, a.bankId, a.accountNo, b.bankName', FALSE)
            ->from('vendors v')
            ->join('vendor_account a', "a.vendorId = v.vendorId ", 'LEFT')
            ->join('banks b', "b.bankId = a.bankId", 'LEFT')
            ->where($whereArray)
            ->get();

        if ($result->num_rows()) {
            $retVal = $result->row_array();
        }

        return $retVal;
    }

    /**
     * Retrieve vendor's details.
     *
     * @param $vendorId
     * @return array
     */
    public function getVendorDetail($vendorId)
    {
        $retVal = array();

        $result = $this->db
            ->select("a.vendorId, a.vendorName, a.status, a.createdDate, a.activatedDate, a.dateModified, v.vendorAccountId, v.accountName, v.bankId, v.accountNo, b.bankName,
                        (SELECT CONCAT_WS(' ', u.user_firstname, u.user_surname) FROM upl_users u WHERE u.user_id = a.activatedBy ) AS 'authorizer',
                        (SELECT CONCAT_WS(' ', u.user_firstname, u.user_surname) FROM upl_users u WHERE u.user_id = a.modifiedBy ) AS 'editor'", false)
            ->from('vendors a')
            ->join('vendor_account v', "a.vendorId = v.vendorId ", 'LEFT')
            ->join('banks b', "b.bankId = v.bankId", 'LEFT')
            ->where('a.vendorId', "$vendorId")
            ->get();

        if ($result->num_rows()) {
            $retVal = $result->row_array();
        }

        return $retVal;
    }

    /**
     * Retrieve vendors.
     *
     * @param bool|true $enabledOnly
     * @return bool
     */
    public function fetchVendors($enabledOnly = true)
    {
        $merchantId = $_SESSION['mymerchantId'];
        $retVal = false;
        $where = "(`merchantId` = '$merchantId')";
        if ($enabledOnly) {
            $where_array = array(
                'status' => "1"
            );
        } else {
            $where_array = array();
        }

        $result = $this->db
            ->select("v.*, CONCAT(u.user_firstname, ' ', u.user_surname) AS creator, CONCAT(u2.user_firstname, ' ', u2.user_surname) AS activator", false)
            ->from("vendors AS v")
            ->join("upl_users As u", "v.createdBy = u.user_id", "left")
            ->join("upl_users As u2", "v.activatedBy = u2.user_id", "left")
            ->where($where, NULL, FALSE)
            ->where($where_array)
            ->order_by('vendorName', 'ASC')
            ->get();

        //die($this->db->last_query());
        if ($result->num_rows() > 0) {
            $retVal = $result->result_array();
        }

        return $retVal;
    }

    public function disable($vendorId = 0)
    {
        $vendorId = (int)$vendorId;
        $now = date('Y-m-d H:i:s');
        if ($vendorId != 0) {
            $data_array = array(
                'status' => "2",
                'activatedBy' => $_SESSION['myuserId'],
                'activatedDate' => "$now"
            );
            $where_array = array(
                'vendorId' => "$vendorId"
            );

            return $this->db->where($where_array)->update('vendors', $data_array);
        }
    }

    public function enable($vendorId)
    {
        $vendorId = (int)$vendorId;
        $now = date('Y-m-d H:i:s');
        if ($vendorId != 0) {
            $data_array = array(
                'status' => "1",
                'activatedBy' => $_SESSION['myuserId'],
                'activatedDate' => "$now"
            );
            $where_array = array(
                'vendorId' => "$vendorId"
            );

            return $this->db->where($where_array)->update('vendors', $data_array);
        }
    }

    public function isUniqueVendorName($vendorName, $vendorId = 0)
    {
        $merchantId = $_SESSION['mymerchantId'];
        if ($vendorId == 0) {
            $where_array = array('merchantId' => "$merchantId",
                'vendorName' => "$vendorName"
            );
            $this->db->where($where_array);
            $this->db->get('vendors');
        } else {
            $where_array = array(
                'vendorName' => "$vendorName",
                'merchantId' => "$merchantId",
                'vendorId<>' => "$vendorId"
            );
            $this->db->get_where("vendors", $where_array);

        }

        if ($this->db->affected_rows() > 0) {
            $ret_val = false;
        } else {
            $ret_val = true;
        }

        return $ret_val;
    }
}