<?php
class Account_category_model extends UPL_Model {
	
	function __construct() {
        parent::__construct();
        $table = 'bank_accounts_category';
        $table_pk = 'accat_id';
        $this->setTable($table);
        $this->setTablePK($table_pk);
    }
	
}	//END class()