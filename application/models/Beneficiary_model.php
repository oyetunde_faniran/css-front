<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Clients
 *
 * @author oyetunde.faniran
 */
class Beneficiary_model extends UPL_Model {
    
    
    function __construct() {
        parent::__construct();
        $table = 'comm_beneficiaries';
        $table_pk = 'ben_id';
        $this->setTable($table);
        $this->setTablePK($table_pk);
    }
    
    
    
    public function getBeneficiaries($enabled_only = true){
        if($enabled_only){
            $where_array = array('ben_enabled' => "1");
            $this->db->where($where_array);
        }
        $result = $this->db->from('comm_beneficiaries bn')
                           ->join('banks bk', 'bk.bank_id = bn.bank_id', 'INNER')
                           ->order_by('ben_name')
                           ->get();
        $ret_val = $result->result_array();
        return $ret_val;
    }   //END getClients()
    
    
    
    
    public function getBeneficiary($client_id){
        $where_array = array('ben_id' => "$client_id");
        $result = $this->db->where($where_array)
                           ->join('banks bk', 'bk.bank_id = bn.bank_id', 'INNER')
                           ->get('comm_beneficiaries bn');
        $ret_val = $result->row_array();
        return $ret_val;
    }   //END getClient()
    
    
    
    
    public function updateBeneficiary($ben_id, $data_array){
        $where_array = array('ben_id' => $ben_id);
        $this->db->where($where_array)
                 ->update('comm_beneficiaries', $data_array);
    }
    
    
    
    public function getBeneficiaryFromUserID($user_id){
        $where_array = array('ben_user_id' => "$user_id");
        $result = $this->db->where($where_array)
                           ->get('comm_beneficiaries');
//        die('<pre>' . $this->db->last_query());
        $row = $result->row_array();
        return $row;
    }
    
    
    
}   //END class