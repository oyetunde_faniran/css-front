<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Transaction
 *
 * @author oyetunde.faniran
 */
class Transaction_model extends UPL_Model {
    
    
    function __construct() {
        parent::__construct();
    }
    
    
    
    public function getTransactions($where = array()){
        if(!empty($where)){
            $this->db->where($where);
        }
        $result = $this->db->from('schedules s')
                           ->join('api_clients c', 's.client_id = c.client_id', 'INNER')
                           ->join('banks b', 's.sch_source_bank_code = b.bank_code', 'LEFT')
                           ->get();
//        die($where);
        $ret_val = $result->result_array();
        return $ret_val;
    }



    public function getTransactionBeneficiaries($sch_id){
        if(!empty($where)){
            $this->db->where($where);
        }
        $result = $this->db->where(['sch_id' => "$sch_id"])
            ->get('schedules_beneficiaries');
//        die($where);
        $ret_val = $result->result_array();
        return $ret_val;
    }
    
    
    public function updateTransaction($sch_id, $data_array){
        $where_array = array(
            'sch_id' => $sch_id
        );
        $this->db->where($where_array)
                 ->update('schedules', $data_array);
    }


    public function getNPPResponses($for_beneficiaries_only = true){
        if($for_beneficiaries_only){
            $where_array = array('for_beneficiaries_only' => "1");
            $this->db->where($where_array);
        }
        $result = $this->db->order_by('description')
            ->get('nibsspayplus_responses');
        $ret_val = $result->result_array();
        return $ret_val;
    }



    public function getNPPResponse($id){
        $result = $this->db->where(['id' => $id])
            ->get('nibsspayplus_responses');
        $ret_val = $result->row_array();
        return $ret_val;
    }



    public function updateTransactionBenStatus($ben_id_array, $status_code, $reason){
        $ben_ids_csv = implode(',', $ben_id_array);
        $data_array = array(
            'ben_statuscode' => "$status_code",
            'ben_reason' => "$reason",
        );
        return $this->db->where_in('ben_id', $ben_id_array)
                        ->update('schedules_beneficiaries', $data_array);
    }



    public function updateTransCompletedBeneficiaryCount($sch_id, $count, $column){
        return $this->db->query("UPDATE schedules
                SET $column = $column + $count
                WHERE sch_id = $sch_id");
    }



}
