<?php

class Report_model extends UPL_Model
{

    function __construct()
    {
        parent::__construct();
        $table = 'bank_accounts_balance';
        $table_pk = 'accbal_id';
        $this->setTable($table);
        $this->setTablePK($table_pk);
    }


    public function getLastUpdateTime($state_id = 12)
    {
        $result = $this->db->where(array('tracker_status' => 2))
                            ->order_by('tracker_updatehour DESC')
                            ->limit(1, 0)
                            ->get('bank_accounts_update_script_tracker');
        $row = $result->row_array();
        $ret_val = !empty($row['tracker_updatehour']) ? $row['tracker_updatehour'] : '0000-00-00 00:00:00';
//        die($ret_val);
        return $ret_val;
//        $where_array = array(
//            'a.state_id' => "$state_id",
//            'a.acc_enabled' => '1'
//        );
//        $result = $this->db->select('MAX(b.accbal_datetime) last_time')
//            ->from('bank_accounts_balance b')
//            ->join('bank_accounts a', 'a.acc_id = b.acc_id', 'INNER')
//            ->where($where_array)
//            ->get();
//        $row = $result->row_array();
//        $ret_val = $row['last_time'];
////        die("--> $ret_val");
//        return $ret_val;
    }


    public function getTotalTotal($state_id = 12, $bank_id_temp = 0, $acc_id_temp = 0, $acc_cat_id = 0)
    {
        //die('go there');
        /* SELECT SUM(b.accbal_amount), COUNT(*) 'account_count', DATE_FORMAT(b.accbal_datetime, '%M %d, %Y %H:%i:%s') 'latest_date'
        FROM bank_accounts_balance b
            INNER JOIN bank_accounts a ON b.acc_id = a.acc_id
        WHERE a.state_id = '12'
            AND b.accbal_datetime = (SELECT MAX(b2.accbal_datetime) FROM bank_accounts_balance b2) */

        $latest_update_time = $this->getLastUpdateTime($state_id);
        $where_array = array(
            'a.state_id' => "$state_id",
            'b.accbal_datetime' => "$latest_update_time",
            'a.acc_enabled' => '1'
        );

        if ($acc_cat_id != 0) {
            $where_array['a.accat_id'] = $acc_cat_id;
        }

        //Add bank or account to the where clause if any was specified
        if ($acc_id_temp != 0) {
            $acc_id = (int)$acc_id_temp;
            $where_array['a.acc_id'] = "$acc_id";
        } elseif ($bank_id_temp != 0) {
            $bank_id = (int)$bank_id_temp;
            $where_array['a.bank_id'] = "$bank_id";
        }

        $result = $this->db->select("SUM(b.accbal_amount) total, COUNT(*) 'account_count', DATE_FORMAT(b.accbal_datetime, '%M %d, %Y %H:%i:%s') 'latest_date'", false)
            ->from("bank_accounts_balance b")
            ->join('bank_accounts a', 'a.acc_id = b.acc_id', 'INNER')
            ->where($where_array)
            ->get();
        //die('<pre>' . $this->db->last_query());
        $ret_val = $result->row_array();
        return $ret_val;
    }    //END getTotalTotal()


    public function getAccountTotal($state_id = 12, $acc_cat_id = 0)
    {
        /* SELECT a.acc_id, ba.bank_name, a.acc_name, b.accbal_amount, DATE_FORMAT(b.accbal_datetime, '%M %d, %Y %H:%i:%s') 'latest_date'
        FROM bank_accounts_balance b
            INNER JOIN bank_accounts a ON b.acc_id = a.acc_id
            INNER JOIN banks ba ON a.bank_id = ba.bank_id
        WHERE a.state_id = '12'
            AND b.accbal_datetime = (SELECT MAX(b2.accbal_datetime) FROM bank_accounts_balance b2)
            AND acc_enabled = '1' */

        $latest_update_time = $this->getLastUpdateTime($state_id);
//        die("====$latest_update_time---");
        $where_array = array(
            'a.state_id' => "$state_id",
            'b.accbal_datetime' => "$latest_update_time",
            'a.acc_enabled' => '1'
        );
        if ($acc_cat_id != 0) {
            $where_array['a.accat_id'] = $acc_cat_id;
        }

        $result = $this->db->select("a.acc_id, ba.bank_name, a.acc_name, a.acc_number, b.accbal_amount, b.accbal_datetime_fetched, b.accbal_balance_nevergotten, DATE_FORMAT(b.accbal_datetime, '%M %d, %Y %H:%i:%s') 'latest_date'", false)
            ->from("bank_accounts_balance b")
            ->join('bank_accounts a', 'a.acc_id = b.acc_id', 'INNER')
            ->join('banks ba', 'a.bank_id = ba.bank_id', 'INNER')
            ->where($where_array)
            ->order_by('ba.bank_name, a.acc_name')
            ->get();
//        die('<pre>' . $this->db->last_query());
        $ret_val = $result->result_array();
        return $ret_val;
    }    //END getTotalTotal()


    public function getBankTotal($state_id = 12)
    {
        /*SELECT a.acc_id, ba.bank_name, COUNT(*) acc_count, SUM(b.accbal_amount) accbal_amount, DATE_FORMAT(b.accbal_datetime, '%M %d, %Y %H:%i:%s') 'latest_date'
        FROM bank_accounts_balance b
            INNER JOIN bank_accounts a ON b.acc_id = a.acc_id
            INNER JOIN banks ba ON a.bank_id = ba.bank_id
        WHERE a.state_id = '12'
            AND b.accbal_datetime = (SELECT MAX(b2.accbal_datetime) FROM bank_accounts_balance b2)
            AND acc_enabled = '1'
        GROUP BY a.bank_id*/

        $latest_update_time = $this->getLastUpdateTime($state_id);
        $where_array = array(
            'a.state_id' => "$state_id",
            'b.accbal_datetime' => "$latest_update_time",
            'a.acc_enabled' => '1'
        );
        $result = $this->db->select("a.acc_id, ba.bank_name, COUNT(*) acc_count, SUM(b.accbal_amount) bank_amount, DATE_FORMAT(b.accbal_datetime, '%M %d, %Y %H:%i:%s') 'latest_date'", false)
            ->from("bank_accounts_balance b")
            ->join('bank_accounts a', 'a.acc_id = b.acc_id', 'INNER')
            ->join('banks ba', 'a.bank_id = ba.bank_id', 'INNER')
            ->where($where_array)
            ->group_by('a.bank_id')
            ->order_by('ba.bank_name')
            ->get();
        $ret_val = $result->result_array();
        return $ret_val;
    }    //END getTotalTotal()


    public function getCphTrend($state_id = 12, $fromdate_temp, $todate_temp, $account, $bank, $acc_cat_id = 0)
    {
        //die("PARAMS: $state_id = 12, $fromdate_temp, $todate_temp, $account, $bank");
        /*SELECT SUM(b.accbal_amount), b.accbal_datetime
            FROM bank_accounts_balance b
                INNER JOIN bank_accounts a ON a.acc_id = b.acc_id

            WHERE a.state_id = '12'
            #			AND b.accbal_datetime = '2015-11-01 23:00:00'
                        AND acc_enabled = '1'
            GROUP BY b.accbal_datetime
            ORDER BY b.accbal_datetime*/

        $latest_update_time = $this->getLastUpdateTime($state_id);
        $where_array = array(
            'a.state_id' => "'$state_id'",
            'b.accbal_datetime <=' => "'$latest_update_time'",
            'a.acc_enabled' => "'1'"
        );

        if ($acc_cat_id != 0) {
            $where_array['a.accat_id'] = $acc_cat_id;
        }

        //Limit results to an account or bank if specified
        if ($account != 0) {
            $where_array['a.acc_id'] = "$account";
        } elseif ($bank != 0) {
            $where_array['a.bank_id'] = "$bank";
        }

        // $fromdate = !empty($fromdate_temp) ? $fromdate_temp : date('Y-m-d H:i:s');
        // $todate = !empty($todate_temp) ? $todate_temp : date('Y-m-d H:i:s');
        if (!empty($fromdate_temp) && !empty($todate_temp)) {
            $where_array['b.accbal_datetime BETWEEN '] = "'$fromdate_temp 00:00:00' AND '$todate_temp 23:59:59'";
        }

        $result = $this->db->select("SUM(b.accbal_amount) accbal_amount, b.accbal_datetime, DATE_FORMAT(b.accbal_datetime, '%M %d, %Y %H:%i:%s') 'formatted_date'", false)
            ->from("bank_accounts_balance b")
            ->join('bank_accounts a', 'a.acc_id = b.acc_id', 'INNER')
            ->join('banks ba', 'a.bank_id = ba.bank_id', 'INNER')
            ->where($where_array, null, false)
            ->group_by('b.accbal_datetime')
            ->order_by('b.accbal_datetime')
            ->get();
        //die('<pre>' . $this->db->last_query());
        $ret_val = $result->result_array();
        //die('<pre>' . print_r($ret_val, true));
        return $ret_val;
    }    //END getTotalTotal()


    public function getCpdTrend($state_id = 12, $fromdate_temp, $todate_temp, $account, $bank, $acc_cat_id = 0)
    {
        //die("PARAMS: $state_id = 12, $fromdate_temp, $todate_temp, $account, $bank");
        /*SELECT SUM(b.accbal_amount) amount, b.accbal_datetime actual_time, DATE_FORMAT(b.accbal_datetime, '%Y-%m-%d') 'accbal_datetime', DATE_FORMAT(b.accbal_datetime, '%M %d, %Y %H:%i:%s') 'latest_date'
            FROM `bank_accounts_balance` `b`
            INNER JOIN `bank_accounts` `a` ON `a`.`acc_id` = `b`.`acc_id`
            INNER JOIN `banks` `ba` ON `a`.`bank_id` = `ba`.`bank_id`
            WHERE a.state_id = '12'
            AND b.accbal_datetime = (SELECT MAX(b2.accbal_datetime) FROM bank_accounts_balance b2 WHERE b2.accbal_datetime LIKE CONCAT(DATE_FORMAT(b.accbal_datetime, '%Y-%m-%d'), '%'))
            AND a.acc_enabled = '1'
            #AND b.accbal_datetime BETWEEN '2015-11-01 00:00:00' AND '2015-11-01 23:59:59'
            GROUP BY DATE_FORMAT(`b`.`accbal_datetime`, '%Y-%m-%d')
            ORDER BY `b`.`accbal_datetime`*/

        //$latest_update_time = $this->getLastUpdateTime($state_id);
        $where_array = array(
            'a.state_id' => "'$state_id'",
            'b.accbal_datetime' => "(SELECT MAX(b2.accbal_datetime) FROM bank_accounts_balance b2 WHERE b2.accbal_datetime LIKE CONCAT(DATE_FORMAT(b.accbal_datetime, '%Y-%m-%d'), '%'))",
            'a.acc_enabled' => "'1'"
        );

        if ($acc_cat_id != 0) {
            $where_array['a.accat_id'] = $acc_cat_id;
        }

        //Limit results to an account or bank if specified
        if ($account != 0) {
            $where_array['a.acc_id'] = "$account";
        } elseif ($bank != 0) {
            $where_array['a.bank_id'] = "$bank";
        }

        // $fromdate = !empty($fromdate_temp) ? $fromdate_temp : date('Y-m-d H:i:s');
        // $todate = !empty($todate_temp) ? $todate_temp : date('Y-m-d H:i:s');
        if (!empty($fromdate_temp) && !empty($todate_temp)) {
            $where_array['b.accbal_datetime BETWEEN '] = "'$fromdate_temp 00:00:00' AND '$todate_temp 23:59:59'";
        }

        $result = $this->db->select("SUM(b.accbal_amount) accbal_amount, b.accbal_datetime actual_time, DATE_FORMAT(b.accbal_datetime, '%Y-%m-%d') 'accbal_datetime', DATE_FORMAT(b.accbal_datetime, '%M %d, %Y') 'formatted_date'", false)
            ->from("bank_accounts_balance b")
            ->join('bank_accounts a', 'a.acc_id = b.acc_id', 'INNER')
            ->join('banks ba', 'a.bank_id = ba.bank_id', 'INNER')
            ->where($where_array, null, false)
            ->group_by("DATE_FORMAT(`b`.`accbal_datetime`, '%Y-%m-%d')")
            ->order_by('accbal_datetime')
            ->get();
        //die('<pre>' . $this->db->last_query());
        $ret_val = $result->result_array();
        //die('<pre>' . print_r($ret_val, true));
        return $ret_val;
    }    //END getTotalTotal()


    public function getCpmTrend($state_id = 12, $fromdate_temp, $todate_temp, $account, $bank, $acc_cat_id = 0)
    {
        //die("PARAMS: $state_id = 12, $fromdate_temp, $todate_temp, $account, $bank");
        /*SELECT SUM(b.accbal_amount) amount, b.accbal_datetime actual_time, DATE_FORMAT(b.accbal_datetime, '%Y-%m-%d') 'accbal_datetime', DATE_FORMAT(b.accbal_datetime, '%M %d, %Y %H:%i:%s') 'latest_date'
            FROM `bank_accounts_balance` `b`
            INNER JOIN `bank_accounts` `a` ON `a`.`acc_id` = `b`.`acc_id`
            INNER JOIN `banks` `ba` ON `a`.`bank_id` = `ba`.`bank_id`
            WHERE a.state_id = '12'
            AND b.accbal_datetime = (SELECT MAX(b2.accbal_datetime) FROM bank_accounts_balance b2 WHERE b2.accbal_datetime LIKE CONCAT(DATE_FORMAT(b.accbal_datetime, '%Y-%m-%d'), '%'))
            AND a.acc_enabled = '1'
            #AND b.accbal_datetime BETWEEN '2015-11-01 00:00:00' AND '2015-11-01 23:59:59'
            GROUP BY DATE_FORMAT(`b`.`accbal_datetime`, '%Y-%m-%d')
            ORDER BY `b`.`accbal_datetime`*/

        //$latest_update_time = $this->getLastUpdateTime($state_id);
        $where_array = array(
            'a.state_id' => "'$state_id'",
            'b.accbal_datetime' => "(SELECT MAX(b2.accbal_datetime) FROM bank_accounts_balance b2 WHERE b2.accbal_datetime LIKE CONCAT(DATE_FORMAT(b.accbal_datetime, '%Y-%m'), '%'))",
            'a.acc_enabled' => "'1'"
        );

        if ($acc_cat_id != 0) {
            $where_array['a.accat_id'] = $acc_cat_id;
        }

        //Limit results to an account or bank if specified
        if ($account != 0) {
            $where_array['a.acc_id'] = "$account";
        } elseif ($bank != 0) {
            $where_array['a.bank_id'] = "$bank";
        }

        // $fromdate = !empty($fromdate_temp) ? $fromdate_temp : date('Y-m-d H:i:s');
        // $todate = !empty($todate_temp) ? $todate_temp : date('Y-m-d H:i:s');
        if (!empty($fromdate_temp) && !empty($todate_temp)) {
            $where_array['b.accbal_datetime BETWEEN '] = "'$fromdate_temp 00:00:00' AND '$todate_temp 23:59:59'";
        }

        $result = $this->db->select("SUM(b.accbal_amount) accbal_amount, b.accbal_datetime actual_time, DATE_FORMAT(b.accbal_datetime, '%Y-%m') 'accbal_datetime', DATE_FORMAT(b.accbal_datetime, '%M %Y') 'formatted_date'", false)
            ->from("bank_accounts_balance b")
            ->join('bank_accounts a', 'a.acc_id = b.acc_id', 'INNER')
            ->join('banks ba', 'a.bank_id = ba.bank_id', 'INNER')
            ->where($where_array, null, false)
            ->group_by("DATE_FORMAT(`b`.`accbal_datetime`, '%Y-%m')")
            ->order_by('accbal_datetime')
            ->get();
        //die('<pre>' . $this->db->last_query());
        $ret_val = $result->result_array();
        //die('<pre>' . print_r($ret_val, true));
        return $ret_val;
    }    //END getCpmTrend()


    public function getCpyTrend($state_id = 12, $fromdate_temp, $todate_temp, $account, $bank, $acc_cat_id = 0)
    {
        //$latest_update_time = $this->getLastUpdateTime($state_id);
        $where_array = array(
            'a.state_id' => "'$state_id'",
            'b.accbal_datetime' => "(SELECT MAX(b2.accbal_datetime) FROM bank_accounts_balance b2 WHERE b2.accbal_datetime LIKE CONCAT(DATE_FORMAT(b.accbal_datetime, '%Y'), '%'))",
            'a.acc_enabled' => "'1'"
        );

        //Limit results to an account or bank if specified
        if ($account != 0) {
            $where_array['a.acc_id'] = "$account";
        } elseif ($bank != 0) {
            $where_array['a.bank_id'] = "$bank";
        }

        if ($acc_cat_id != 0) {
            $where_array['a.accat_id'] = $acc_cat_id;
        }

        // $fromdate = !empty($fromdate_temp) ? $fromdate_temp : date('Y-m-d H:i:s');
        // $todate = !empty($todate_temp) ? $todate_temp : date('Y-m-d H:i:s');
        if (!empty($fromdate_temp) && !empty($todate_temp)) {
            $where_array['b.accbal_datetime BETWEEN '] = "'$fromdate_temp 00:00:00' AND '$todate_temp 23:59:59'";
        }

        $result = $this->db->select("SUM(b.accbal_amount) accbal_amount, b.accbal_datetime actual_time, DATE_FORMAT(b.accbal_datetime, '%Y') 'accbal_datetime', DATE_FORMAT(b.accbal_datetime, '%Y') 'formatted_date'", false)
            ->from("bank_accounts_balance b")
            ->join('bank_accounts a', 'a.acc_id = b.acc_id', 'INNER')
            ->join('banks ba', 'a.bank_id = ba.bank_id', 'INNER')
            ->where($where_array, null, false)
            ->group_by("DATE_FORMAT(`b`.`accbal_datetime`, '%Y')")
            ->order_by('accbal_datetime')
            ->get();
        //die('<pre>' . $this->db->last_query());
        $ret_val = $result->result_array();
        //die('<pre>' . print_r($ret_val, true));
        return $ret_val;
    }    //END getCpyTrend()



}    //END class()