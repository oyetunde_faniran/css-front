<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Clients
 *
 * @author oyetunde.faniran
 */
class Deductions_model extends UPL_Model {
    
    
    function __construct() {
        parent::__construct();
        $table = 'comm_deductions';
        $table_pk = 'ded_id';
        $this->setTable($table);
        $this->setTablePK($table_pk);
    }
    
    
    
    public function getDeductions($enabled_only = true, $optional_only = true){
        $where_array = [];
        if($enabled_only){
            $where_array['ded_enabled'] = "1";
        }
        if($optional_only){
            $where_array['ded_mandatory'] = "0";
        }
        
        if(!empty($where_array)){
            $this->db->where($where_array);
        }
        
        $result = $this->db->from('comm_deductions')
                           ->order_by('ded_mandatory ASC, ded_description')
                           ->get();
        $ret_val = $result->result_array();
        return $ret_val;
    }   //END getClients()
    
    
    
    
    public function getDeduction($id, $enabled_only = true){
        $where_array = array(
            'ded_id' => $id
        );
        if($enabled_only){
            $where_array['ded_enabled'] = "1";
        }
        $result = $this->db->from('comm_deductions')
                           ->where($where_array)
                           ->get();
        $ret_val = $result->row_array();
        return $ret_val;
    }
    
    
    
    public function savePrjDeductionMapping($prj_id, $deductions){
        $data_array = [];
        foreach ($deductions as $d){
            $data_array[] = [
                'prj_id' => "$prj_id",
                'ded_id' => "$d",
            ];
        }
        $this->db->insert_batch('comm_projects_deductions', $data_array);
    }
    
    
    
    public function clearPrjDeductionMapping($prj_id){
        $where = ['prj_id' => $prj_id];
        $this->db->where($where)
                 ->delete('comm_projects_deductions');
    }



    public function getProjectAssignedDeductions($project_id){
        $where_array = array(
            'pd.prj_id' => $project_id
        );
        $result = $this->db->select('d.*')
            ->from('comm_deductions d')
            ->join('comm_projects_deductions pd', 'd.ded_id = pd.ded_id', 'INNER')
            ->where($where_array)
            ->get();
//        die("<pre>" . $this->db->last_query());
        $ret_val = $result->result_array();
        return $ret_val;
    }
    
    
    
    
}   //END class