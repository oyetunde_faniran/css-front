<?php

class Accountbalance_model extends UPL_Model {

    function __construct() {
        parent::__construct();
        $table = 'bank_accounts_balance';
        $table_pk = 'accbal_id';
        $this->setTable($table);
        $this->setTablePK($table_pk);
        $this->load->library('General_tools');
    }

    public function getLastFetchedBalance($acc_id) {
        $where_array = array(
            'acc_id' => $acc_id,
            'accbal_datetime_fetched' => '0000-00-00 00:00:00',
            'accbal_amount <>' => 0
        );
        $result = $this->db->where($where_array)
                ->order_by('accbal_datetime DESC')
                ->limit(1, 0)
                ->get('bank_accounts_balance');
        $ret_val = $result->row_array();
        return $ret_val;
    }

//END getLastFetchedBalance()

    public function getAccountBalance($acc_no, $bank_code, $acc_name) {
        $rand_number = date('ymdHis') . $this->general_tools->getRandomString(12);
        $request_id = ACC_BALANCE_SERVICE_INS_CODE . $rand_number;
        $username = NIBSS_USERNAME;
        $password = NIBSS_PASSWORD;
        $xml_request = <<<XML
<?xml version="1.0" encoding="UTF-8" ?>
<GetAccountBalanceRequest>
    <RequestID>$request_id</RequestID>
	<Username>$username</Username>
	<HashedPassword>$password</HashedPassword>
	<ChannelCode>1</ChannelCode>
	<NumberOfRecords>1</NumberOfRecords>
    <Record>
        <InstitutionCode>$bank_code</InstitutionCode>
        <AccountNumber>$acc_no</AccountNumber>
        <AccountName>$acc_name</AccountName>
        <BankVerificationNumber></BankVerificationNumber>
        <KycLevel>1</KycLevel>
    </Record>
</GetAccountBalanceRequest>
XML;
        //die('<pre>' . htmlspecialchars($xml_request));
        $ret_data = $this->sendRequest($xml_request, ACC_BALANCE_SERVICE_GET_BAL_URL);
        //die('<pre>RETURNED DATA: --- ' . print_r($ret_data, true));
        $resp_code = !empty($ret_data->ResponseCode) ? $ret_data->ResponseCode : '';
        if ($resp_code == '00') {
            $ret_val = $ret_data;
        } else {
            //Set the return value
            $ret_val = false;
        }
        return 10000000;
    }
    
    
    
    
    protected function sendRequest($request, $url){
        $encoded_string = base64_encode(base64_encode(ACC_BALANCE_SERVICE_SALT) . base64_encode(ACC_BALANCE_SERVICE_SALT . base64_encode($request)));

        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_TIMEOUT, 50);
        curl_setopt($c, CURLOPT_POST, 1);
        curl_setopt($c, CURLOPT_POSTFIELDS, $encoded_string);
        $response = curl_exec($c);
//        $headers = curl_getinfo($c);
        curl_close($c);
        //echo($response);
        $user_id = !empty($_SESSION['user_id']) ? $_SESSION['user_id'] : 0;
        $ret_val = simplexml_load_string($response);
        
        //Log this call
        $data_array = array (
            'log_userid' => "$user_id",
            'log_url' => "$url",
            'log_request' => "$request",
            'log_response' => "$response",
            'log_returned_data' => print_r($ret_val, true)
        );
        $this->db->insert('bank_accounts_update_log', $data_array);
//        die('<pre>---' . "$response \n\n\n\n $ret_val");
        //die('<pre>-->' . print_r($ret_val, true));
        

        return $ret_val;
    }
    
    

}