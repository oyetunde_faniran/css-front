<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Commission_model
 *
 * @author oyetunde.faniran
 */
class Commission_model extends UPL_Model {


    function __construct() {
        parent::__construct();
    }




    public function saveCommissionPayment($sch_id, $prj_id, $type, $trans, $accno, $bankcode, $accno_debit, $bankcode_debit, $nibss_data){
//        'create_response' => $create_response,
//                'update_response' => $update_response,
//                'process_response' => $process_response,
        $dis_date = date('Y-m-d H:i:s');
        $data_array = array(
            'sch_id' => $sch_id,
            'prj_id' => $prj_id,
            'schcomm_type' => "$type",
            'schcomm_amount' => $type == 1 ? $trans['sch_commission'] : $trans['sch_commission_successful_only'],
            'schcomm_bankcode_debit' => $bankcode_debit,
            'schcomm_accno_debit' => $accno_debit,
            'schcomm_bankcode' => $bankcode,
            'schcomm_accno' => $accno,
            'schcomm_status' => (!empty($nibss_data['process_response']['status']) && $nibss_data['process_response']['status'] == 16) ? '0' : '2',
            'schcomm_create_request' => $nibss_data['create_response']['request'],
            'schcomm_create_response' => $nibss_data['create_response']['response'],
            'schcomm_create_response_code' => $nibss_data['create_response']['status'],
            'schcomm_update_request' => $nibss_data['update_response']['request'],
            'schcomm_update_response' => $nibss_data['update_response']['response'],
            'schcomm_update_response_code' => $nibss_data['update_response']['status'],
            'schcomm_process_request' => $nibss_data['process_response']['request'],
            'schcomm_process_response' => $nibss_data['process_response']['response'],
            'schcomm_process_response_code' => $nibss_data['process_response']['status'],
            'schcomm_dateadded' => $dis_date
        );
        $this->db->insert('schedules_commission', $data_array);
    }



    public function saveCustomCommission($prj_id, $amount, $accno, $bankcode, $bankdeposit_id = 0){
        $dis_date = date('Y-m-d H:i:s');
        $data_array = array(
            'bdeposit_id' => "$bankdeposit_id",
            'prj_id' => "$prj_id",
            'schcomm_type' => "3",
            'schcomm_amount' => "$amount",
            'schcomm_bankcode' => "$bankcode",
            'schcomm_accno' => "$accno",
            'schcomm_status' => "0",
            'schcomm_dateadded' => "$dis_date"
        );
        $this->db->insert('schedules_commission', $data_array);
    }




    public function getCommissionPayments_old($where = array(), $single = false){
        if(!empty($where)){
            $this->db->where($where);
        }
        $result = $this->db->from('schedules_commission sc')
            ->join('schedules s', 'sc.sch_id = s.sch_id', 'LEFT')
            ->join('api_clients c', 's.client_id = c.client_id', 'LEFT')
            ->join('banks b', 'sc.schcomm_bankcode = b.bank_code', 'LEFT')
            ->get();
//        die('<pre>' . $this->db->last_query());
        $ret_val = $single ? $result->row_array() : $result->result_array();
        return $ret_val;
    }



    public function getCommissionPayments($where = array(), $single = false, $aggregate = false){
        if(!empty($where)){
            $this->db->where($where);
        }
        if($aggregate){
            $this->db->select('*, SUM(schcomm_amount) schcomm_amount, COUNT(sc.schcomm_id) total_count, GROUP_CONCAT(sc.schcomm_id) "comm_ids"');
        }
        $result = $this->db->from('schedules_commission sc')
            ->join('schedules s', 'sc.sch_id = s.sch_id', 'LEFT')
            ->join('comm_projects p', 'p.prj_id = sc.prj_id', 'INNER')
            ->join('api_clients c', 'p.client_id = c.client_id', 'INNER')
            ->join('banks b', 'sc.schcomm_bankcode = b.bank_code', 'LEFT')
            ->order_by('c.client_name, sc.prj_id, sc.schcomm_split_sched_id, sc.schcomm_status, sc.schcomm_split_status, sc.schcomm_dateadded')
            ->get();
//        die('<!--<pre>' . $this->db->last_query());
        $ret_val = $single ? $result->row_array() : $result->result_array();
        return $ret_val;
    }


    public function getSplitConfig($prj_id){
        $where_array = array('f.prj_id' => $prj_id);
        $result = $this->db->from('comm_sharing_formula f')
            ->join('comm_beneficiaries ben', 'f.ben_id = ben.ben_id', 'INNER')
            ->join('banks b', 'ben.bank_id = b.bank_id', 'INNER')
            ->join('comm_projects p', 'p.prj_id = f.prj_id', 'INNER')
            ->join('api_clients c', 'p.client_id = c.client_id', 'INNER')
            ->where($where_array)
            ->get();
        $ret_val = $result->result_array();
//        die('<pre>' . $this->db->last_query());
        return $ret_val;
    }




    public function updateCommission($comm_id, $data_array, $comm_id_list){
        if(!empty($comm_id_list)){
            $where_array = " schcomm_id IN (" . implode(',', $comm_id_list) . ') ';
        } else {
            $where_array = array(
                'schcomm_id' => $comm_id
            );
        }
        $this->db->where($where_array)
            ->update('schedules_commission', $data_array);
//        die('<pre>' . $this->db->last_query());
    }



    public function commTrends($days_count = 30){
        $where_array = array(
            'c.schcomm_dateadded >=' => "DATE_SUB(NOW(), INTERVAL $days_count DAY)"
        );
        $result = $this->db->select("DATE_FORMAT(c.schcomm_dateadded, '%M %d, %Y') 'date', SUM(c.schcomm_amount) trans_value", false)
            ->from('schedules_commission c')
            ->where($where_array, false, false)
            ->group_by('DATE(c.schcomm_dateadded)')
            ->get();
//        die('<pre>' . $this->db->last_query());
        $ret_val = $result->result_array();
        return $ret_val;
    }




    public function getBeneficiaryCommissionPayments($where = array()){
        if(!empty($where)){
            $this->db->where($where);
        }
        $result = $this->db->from('schedules_commission_split spl')
            ->join('schedules_commission c', 'c.schcomm_id = spl.schcomm_id', 'INNER')
            ->join('banks b', 'spl.split_bankcode = b.bank_code', 'INNER')
            ->join('comm_projects p', 'c.prj_id = p.prj_id', 'INNER')
            ->get();
//        die('<!--<pre>' . $this->db->last_query());
        $ret_val = $result->result_array();
        return $ret_val;
    }




    public function deleteCommission($id){
        $where_array = array(
            'schcomm_id' => "$id"
        );
        $this->db->where($where_array)
            ->where_in('schcomm_split_status', ['1', '2'])
            ->delete('schedules_commission');
        $ret_val = $this->db->affected_rows(0);
        return $ret_val;
    }




}