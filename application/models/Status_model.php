<?php

/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 11/26/2016
 * Time: 6:58 AM
 */
class Status_model extends UPL_Model
{
    function __construct()
    {
        parent::__construct();
        $table = 'statuses';
        $table_pk = 'status_id';

        $this->setTable($table);
        $this->setTablePK($table_pk);
    }

    /**
     * Retrieve all statuses.
     * Filter optionally by whether it is enabled or not.
     *
     * @param null $enabled
     * @return type
     */
    public function all($enabled = null)
    {
        $where = !is_null($enabled) ? "status_enabled = {$enabled}" : "";

        return $this->loadMultiple(0, 0, $where);
    }

    /**
     * Retrieve single record by Primary Key.
     *
     * @param $id
     * @return null
     */
    public function find($id)
    {
        $result = $this->db->get_where($this->table, array($this->pk_column => $id));

        return $result && $result->num_rows() > 0 ? $result->row_array() : null;
    }

    /**
     * Retrieve single record by description.
     *
     * @param $description
     * @return null
     */
    public function findByDescription($description)
    {
        $result = $this->db->get_where($this->table, array('status_description' => $description));

        return $result && $result->num_rows() > 0 ? $result->row_array() : null;
    }
}