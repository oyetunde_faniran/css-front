<?php

class Contractor_category_model extends UPL_Model {

    function __construct() {
        parent::__construct();
        $table = 'contractor_categories';
        $table_pk = 'contractor_category_id';
        $this->setTable($table);
        $this->setTablePK($table_pk);
    }


}

//END class()