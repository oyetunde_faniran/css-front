<?php

class Ips_model extends UPL_Model
{

    function __construct()
    {
        parent::__construct();
        $table = 'api_clients_ips';
        $table_pk = 'clientip_id';
        $this->setTable($table);
        $this->setTablePK($table_pk);
    }    //END __construct()


    public function getClientIps($client_id){
        $where = array(
            'client_id' => $client_id
        );
        $result = $this->db->select('clientip_ip ip')
                           ->where($where)
                           ->get($this->table);
        $ret_val = $result->result_array();
//        die('<pre>' . $this->db->last_query());
        return $ret_val;
    }
    
    
    
    public function clearClientIPs($client_id){
        $where = array(
            'client_id' => $client_id
        );
        $this->db->where($where)
                 ->delete($this->table);
        return true;
    }


}    //END class()