<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Clients
 *
 * @author oyetunde.faniran
 */
class Projects_model extends UPL_Model {
    
    
    function __construct() {
        parent::__construct();
        $table = 'comm_projects';
        $table_pk = 'prj_id';
        $this->setTable($table);
        $this->setTablePK($table_pk);
    }
    
    
    
    public function getProjects($enabled_only = true){
        if($enabled_only){
            $where_array = [
                'prj_enabled' => "1",
                'prj_default' => "1"
            ];
            $this->db->where($where_array);
        }
        $result = $this->db->from('comm_projects p')
                           ->join('api_clients c', 'c.client_id = p.client_id', 'LEFT')
                           ->order_by('prj_name')
                           ->get();
        $ret_val = $result->result_array();
        return $ret_val;
    }   //END getClients()
    
    
    
    
    public function getProject($id, $enabled_only = true){
        $where_array = array(
            'prj_id' => $id
        );
        if($enabled_only){
            $where_array['prj_enabled'] = "1";
        }
        $result = $this->db->from('comm_projects p')
                           ->join('api_clients c', 'c.client_id = p.client_id', 'LEFT')
                           ->where($where_array)
                           ->get();
        $ret_val = $result->row_array();
        return $ret_val;
    }
    
    
    
    
    public function getDefaultClientProject($client_id){
        $where_array = array(
            'client_id' => $client_id,
            'prj_enabled' => "1",
            'prj_default' => "1"
        );
        $result = $this->db->where($where_array)
                           ->get('comm_projects');
//        die('<pre>' . $this->db->last_query());
        $ret_val = $result->row_array();
        return $ret_val;
    }
    
    
    
    
}   //END class