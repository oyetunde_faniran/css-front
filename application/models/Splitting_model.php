<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Splitting_model
 *
 * @author oyetunde.faniran
 */
class Splitting_model extends UPL_Model {
    
    
    
    function __construct() {
        parent::__construct();
        $table = 'comm_sharing_formula';
        $table_pk = 'form_id';
        $this->setTable($table);
        $this->setTablePK($table_pk);
    }
    
    
    
    
    public function saveSplit($comm_id, $accno_debit, $bankcode_debit, $ben, $nibss_data, $comm_id_list = []){
//        die("<pre>$comm_id, $accno_debit, $bankcode_debit, " . print_r($ben, true) . ", " . print_r($nibss_data, true));

        //If the commission ID list is empty, then put the single ID provided into the array
        if(empty($comm_id_list)){
            $comm_id_list = [$comm_id];
        }

        foreach ($comm_id_list as $id){
            $dis_date = date('Y-m-d H:i:s');

            $data_array_big = array();

            //These are common to each row to be inserted
            $data_array = array(
                'schcomm_id' => $id,
                'split_bankcode_debit' => $bankcode_debit,
                'split_accno_debit' => $accno_debit,
                'split_status' => (!empty($nibss_data['process_response']['status']) && $nibss_data['process_response']['status'] == 16) ? '0' : '2',
                'split_create_request' => !empty($nibss_data['create_response']['request']) ? $nibss_data['create_response']['request'] : '',
                'split_create_response' => !empty($nibss_data['create_response']['response']) ? $nibss_data['create_response']['response'] : '',
                'split_create_response_code' => !empty($nibss_data['create_response']['status']) ? $nibss_data['create_response']['status'] : '',
                'split_update_request' => !empty($nibss_data['update_response']['request']) ? $nibss_data['update_response']['request'] : '',
                'split_update_response' => !empty($nibss_data['update_response']['response']) ? $nibss_data['update_response']['response'] : '',
                'split_update_response_code' => !empty($nibss_data['update_response']['status']) ? $nibss_data['update_response']['status'] : '',
                'split_process_request' => !empty($nibss_data['process_response']['request']) ? $nibss_data['process_response']['request'] : '',
                'split_process_response' => !empty($nibss_data['process_response']['response']) ? $nibss_data['process_response']['response'] : '',
                'split_process_response_code' => !empty($nibss_data['process_response']['status']) ? $nibss_data['process_response']['status'] : '',
                'split_dateadded' => $dis_date
            );

//        $sno = 0;
            foreach ($ben as $b){
//            echo (++$sno . '<hr />');
                //Data specific to each row of the split data
                $data_array['split_amount'] = $b['amount'];
                $data_array['split_bankcode'] = $b['bankcode'];
                $data_array['split_accno'] = $b['accno'];
                $data_array['split_accname'] = $b['accname'];
                $data_array['split_share'] = $b['share'];
                $data_array['ben_id'] = $b['ben_id'];
                $data_array_big[] = $data_array;
            }
//        die('123<pre>' . print_r($data_array_big, true));

            $this->db->insert_batch('schedules_commission_split', $data_array_big);
        }
    }
    
    
    
    
    public function getSplitDetails($comm_id){
        $where = array(
            'schcomm_id' => $comm_id,
            'split_create_response_code' => '16',
            'split_update_response_code' => '16',
            'split_process_response_code' => '16',
        );
        $result = $this->db->where($where)
                           ->get('schedules_commission_split');
        $ret_val = $result->result_array();
        return $ret_val;
    }
    
    
    
    
    
    
    
}
