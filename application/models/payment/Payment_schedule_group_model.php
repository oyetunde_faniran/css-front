<?php

/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 11/27/2016
 * Time: 5:58 AM
 */
class Payment_schedule_group_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Retrieve all payment schedule groups.
     * Filter optionally by the status of the group.
     * Compute total amount and total beneficiaries in all schedules in each group.
     * Optionally include all commission beneficiaries.
     *
     * @param null $status
     * @param bool|false $include_commission_accounts
     * @return null
     */
    public function all($status = null, $include_commission_accounts = false)
    {
        if ($status) {
            $this->db->where(array('psg_status' => $status));
        }

        $q = $include_commission_accounts ? "" : " AND psb.psb_commission_account = FALSE";

        $result = $this->db
            ->select("psg.psg_id AS schedule_group_id,
                        psg.psg_created_at AS created_at,
                        SUM(ps2.total_amount) AS total_amount,
                        SUM(ps2.total_beneficiaries) AS total_beneficiaries,
                        GROUP_CONCAT( CONCAT_WS(':',ps2.status_description,ps2.total_amount,ps2.total_beneficiaries) ) AS summary,
                        pfg.narration,
                        pfg.num_debit_accounts,
                        pfg.num_credit_accounts,
                        pt.pt_name AS payment_type_name,
                        CONCAT(u.user_firstname, ' ', u.user_surname) AS initiator,
                        CONCAT_WS( '|', pff.source_account_bank_name, pff.source_account_no/*, pff.source_account_name*/) AS debit_account,
                        CONCAT_WS( '|', pfc.beneficiary_bank_name, pfc.beneficiary_account_no/*, pfc.beneficiary_account_name*/) AS credit_account", false)
            ->from("payment_schedule_groups psg")
            ->join($this->get_table_view('ps2',$q), "psg.psg_id = ps2.payment_schedule_group", "INNER")
            ->join("payment_file_groups pfg", "psg.payment_file_group = pfg.id", "INNER")
            ->join("payment_types pt", "pfg.payment_type = pt.pt_id", "INNER")
            ->join("upl_users u", "u.user_id = psg.psg_created_by")
            ->join("(
                        SELECT
                            pfg.id AS payment_file_group_id,
                            pf.source_account AS source_account_id,
                            ba.acc_name AS source_account_name,
                            ba.acc_number AS source_account_no,
                            b.bank_name AS source_account_bank_name
                        FROM payment_file_groups pfg
                        JOIN payment_files pf ON pfg.`id` = pf.`payment_file_group`
                        JOIN bank_accounts ba ON pf.source_account = ba.acc_id
                        JOIN banks b ON ba.bank_id = b.bank_id
                        WHERE pfg.num_debit_accounts = 1
                        GROUP BY pf.`payment_file_group`
                     ) pff", "psg.payment_file_group = pff.payment_file_group_id", "LEFT")
            ->join("(
                        SELECT
                            pfg.id AS payment_file_group_id,
                            pfb.pfb_account_name AS beneficiary_account_name,
                            pfb.pfb_account_no AS beneficiary_account_no,
                            b.bank_name AS beneficiary_bank_name
                        FROM payment_file_groups pfg
                        JOIN payment_files pf ON pfg.`id` = pf.`payment_file_group`
                        JOIN payment_file_beneficiaries pfb ON pf.`pf_id` = pfb.`payment_file`
                        JOIN banks b ON pfb.pfb_bank = b.bank_id
                        WHERE pfg.num_credit_accounts = 1
                        GROUP BY pf.`payment_file_group`
                     ) pfc", "psg.payment_file_group = pfc.payment_file_group_id", "LEFT")
            ->group_by("psg.psg_id")
            ->order_by('psg.psg_id', 'desc')
            ->get();
        //die('<pre>' . $this->db->last_query());
        $ret_val = $result->num_rows() > 0 ? $result->result_array() : null;

        return $ret_val;
    }


    /**
     * Retrieve payment schedule group.
     * Optionally include commission beneficiaries.
     *
     * @param $id
     * @param bool|false $include_commission_accounts
     * @return null
     */
    public function find($id, $include_commission_accounts = false)
    {
        $q = $include_commission_accounts ? "" : " AND psb.psb_commission_account = FALSE";

        $result = $this->db
            ->select("psg.psg_id AS schedule_group_id,
                        psg.psg_created_at AS created_at,
                        SUM(ps2.total_amount) AS total_amount,
                        SUM(ps2.total_beneficiaries) AS total_beneficiaries,
                        GROUP_CONCAT( CONCAT_WS(':',ps2.status_description,ps2.total_amount,ps2.total_beneficiaries) ) AS summary,
                        pfg.narration,
                        pfg.num_debit_accounts,
                        pfg.num_credit_accounts,
                        pt.pt_name AS payment_type_name,
                        CONCAT(u.user_firstname, ' ', u.user_surname) AS initiator", false)
            ->from("payment_schedule_groups psg")
            ->join($this->get_table_view('ps2',$q), "psg.psg_id = ps2.payment_schedule_group", "INNER")
            ->join("payment_file_groups pfg", "psg.payment_file_group = pfg.id", "INNER")
            ->join("payment_types pt", "pfg.payment_type = pt.pt_id", "INNER")
            ->join("upl_users u", "u.user_id = psg.psg_created_by")
            ->where("psg.psg_id", $id)
            ->group_by("psg.psg_id")
            ->get();
        //die($this->db->last_query());
        $ret_val = $result->num_rows() > 0 ? $result->row_array() : null;

        return $ret_val;
    }


    /** This Private method will simulate a table view for joining purpose
     * @param $view
     * @param $q
     * @return string
     */
    private function get_table_view($view, $q){

        switch($view){
            case "ps2":
                return "(
                        SELECT
                            ps.ps_id,
                            ps.payment_schedule_group,
                            psb.psb_payment_status,
                            COUNT(psb.`psb_id`) AS total_beneficiaries,
                            SUM(psb.`psb_amount`) AS total_amount,
                            s.status_description
                        FROM payment_schedules ps
                        JOIN payment_schedule_beneficiaries psb ON ps.`ps_id` = psb.`payment_schedule` $q
                        JOIN statuses s ON psb.psb_payment_status = s.`status_id`
                        GROUP BY ps.payment_schedule_group, psb.`psb_payment_status`
                    ) $view";
                break;


        }

    }

    /**
     * Save payment schedule group.
     *
     * @param $data
     * @return null
     */
    public function save($data)
    {
        return empty($data['id']) ? $this->create($data) : $this->update($data);
    }

    /**
     * Store new payment schedule group.
     *
     * @param $data
     * @return null
     */
    public function create($data)
    {
        $_data = array(
            'payment_file_group' => $data['payment_file_group'],
            'psg_status' => $data['status'],
            'psg_created_by' => $data['created_by'],
            'psg_created_at' => date('Y-m-d H:i:s')
        );

        $response = $this->db->insert('payment_schedule_groups', $_data);

        return $response ? $this->db->insert_id() : null;
    }

    /**
     * Update payment schedule group.
     *
     * @param $data
     * @return mixed
     */
    public function update($data)
    {
        $sql = "UPDATE payment_schedule_groups SET psg_status = ? WHERE psg_id = ?";

        $values = array($data['status'], $data['id']);

        return $this->db->query($sql, $values);
    }
}