<?php

/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 11/27/2016
 * Time: 5:58 AM
 */
class Payment_schedule_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Retrieve all payment schedules.
     * Optionally, filter by payment schedule group and status.
     *
     * @param null $id
     * @param null $status
     * @return null
     */
    public function all($id = null, $status = null)
    {
        if ($id) {
            if (is_array($id))
                $this->db->where_in("ps.payment_schedule_group", $id);
            else
                $this->db->where("ps.payment_schedule_group", $id);
        }

        if ($status)
            $this->db->where('ps.ps_payment_status', $status);

        $result = $this->db
            ->select("ps.ps_id AS payment_schedule_id,
                      ps.payment_schedule_group,
                      ps.payment_file,
                      ps.ps_ref AS schedule_ref,
                      ps.ps_payment_status AS payment_status,
                      ps.ps_payment_response_code,
                      ps.ps_payment_response_description,
                      s.status_description AS payment_status_description,
                      pf.source_account AS source_account_id,
                      ba.acc_name AS source_account_name,
                      ba.acc_number AS source_account_no,
                      b.bank_name AS source_account_bank_name,
                      b.bank_code AS source_account_bank_code,
                      pfg.narration", false)
            ->from("payment_schedules ps")
            ->join("payment_files pf", "ps.payment_file = pf.pf_id", "INNER")
            ->join("payment_file_groups pfg", "pf.payment_file_group = pfg.id", "INNER")
            ->join('statuses s', "ps.ps_payment_status = s.status_id", "LEFT")
            ->join("bank_accounts ba", "pf.source_account = ba.acc_id", "LEFT")
            ->join("banks b", "ba.bank_id = b.bank_id", "LEFT")
            ->order_by('ps.payment_schedule_group')
            ->order_by('ps.ps_id')
            ->get();

        $ret_val = $result->num_rows() > 0 ? $result->result_array() : null;

        return $ret_val;
    }

    /**
     * Retrieve payment schedule by ID.
     *
     * @param $id
     * @return null
     */
    public function find($id)
    {
        $result = $this->db
            ->select("ps.ps_id AS payment_schedule_id,
                      ps.payment_schedule_group,
                      ps.payment_file,
                      ps.ps_ref AS schedule_ref,
                      ps.ps_payment_status AS payment_status,
                      ps.ps_payment_response_code,
                      ps.ps_payment_response_description,
                      s.status_description AS payment_status_description,
                      pf.source_account AS source_account_id,
                      ba.acc_name AS source_account_name,
                      ba.acc_number AS source_account_no,
                      b.bank_name AS source_account_bank_name,
                      b.bank_code AS source_account_bank_code,
                      pfg.narration", false)
            ->from("payment_schedules ps")
            ->join("payment_files pf", "ps.payment_file = pf.pf_id", "INNER")
            ->join("payment_file_groups pfg", "pf.payment_file_group = pfg.id", "INNER")
            ->join('statuses s', "ps.ps_payment_status = s.status_id", "LEFT")
            ->join("bank_accounts ba", "pf.source_account = ba.acc_id", "LEFT")
            ->join("banks b", "ba.bank_id = b.bank_id", "LEFT")
            ->where('ps.ps_id', $id)
            ->get();

        $ret_val = $result->num_rows() > 0 ? $result->row_array() : null;

        return $ret_val;
    }

    /**
     * Retrieve payment schedule by schedule ref.
     *
     * @param $ref
     * @return null
     */
    public function findByScheduleRef($ref)
    {
        $result = $this->db
            ->select("ps.ps_id AS payment_schedule_id,
                      ps.payment_schedule_group,
                      ps.payment_file,
                      ps.ps_ref AS schedule_ref,
                      ps.ps_payment_status AS payment_status,
                      ps.ps_payment_response_code,
                      ps.ps_payment_response_description,
                      s.status_description AS payment_status_description,
                      pf.source_account AS source_account_id,
                      ba.acc_name AS source_account_name,
                      ba.acc_number AS source_account_no,
                      b.bank_name AS source_account_bank_name,
                      b.bank_code AS source_account_bank_code,
                      pfg.narration", false)
            ->from("payment_schedules ps")
            ->join("payment_files pf", "ps.payment_file = pf.pf_id", "INNER")
            ->join("payment_file_groups pfg", "pf.payment_file_group = pfg.id", "INNER")
            ->join('statuses s', "ps.ps_payment_status = s.status_id", "LEFT")
            ->join("bank_accounts ba", "pf.source_account = ba.acc_id", "LEFT")
            ->join("banks b", "ba.bank_id = b.bank_id", "LEFT")
            ->where('ps.ps_ref', $ref)
            ->get();

        $ret_val = $result->num_rows() > 0 ? $result->row_array() : null;

        return $ret_val;
    }

    /**
     * Save payment schedule.
     *
     * @param $data
     * @return null
     */
    public function save($data)
    {
        return empty($data['id']) ? $this->create($data) : $this->update($data);
    }

    /**
     * Store new payment schedule.
     *
     * @param $data
     * @return null
     */
    public function create($data)
    {
        $_data = array(
            'payment_schedule_group' => $data['payment_schedule_group'],
            'payment_file' => $data['payment_file'],
            'source_account' => $data['source_account'],
            'ps_ref' => $data['schedule_ref'],
            'ps_payment_status' => $data['status'],
            'ps_created_at' => date('Y-m-d H:i:s')
        );

        $response = $this->db->insert('payment_schedules', $_data);

        return $response ? $this->db->insert_id() : null;
    }

    /**
     * Update payment schedule by its reference.
     *
     * @param $data
     * @return mixed
     */
    public function updateByRef($data)
    {
        $sql = "UPDATE payment_schedules
                SET ps_payment_status = ?, ps_payment_response_code = ?, ps_payment_response_description = ?
                WHERE ps_ref = ?";

        $values = array($data['status'], $data['response_code'], $data['response_description'], $data['schedule_ref']);

        return $this->db->query($sql, $values);
    }

    /**
     * Update payment schedule.
     *
     * @param $data
     * @return mixed
     */
    protected function update($data)
    {
        $_data = array();

        if (!empty($data['status']))
            $_data['ps_payment_status'] = $data['status'];

        if (!empty($data['response_code']))
            $_data['ps_payment_response_code'] = $data['response_code'];

        if (!empty($data['response_description']))
            $_data['ps_payment_response_description'] = $data['response_description'];

        return $this->db->update('payment_schedules', $_data, array('ps_id' => $data['id']));
    }
}