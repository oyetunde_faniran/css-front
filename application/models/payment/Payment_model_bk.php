<?php

/**
 * Contains methods for presenting the main payments of the payroll
 *
 * @author Segun Ojo
 */
class Payment_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Account_model', 'acc');
        $this->load->model('Bank_model', 'bank');
    }

    /**
     * Retrieve all payment file groups that meet certain criteria
     * Load relationships for each group found.
     *
     * @param null $criteria
     * @param null $relationships
     * @return array|null
     */
    public function paymentFileGroups($criteria = null, $relationships = null)
    {
        $where_array = array();

        if (!empty($criteria['created_by'])) {
            $where_array['pfg.created_by'] = $criteria['created_by'];
        }

        if (isset($criteria['status']) && $criteria['status'] != '') {
            $where_array['pfg.status'] = $criteria['status'];
        }

        if (!empty($where_array)) {
            $this->db->where($where_array);
        }

        $result = $this->db
            ->select("pfg.*, CONCAT(u.user_firstname, ' ', u.user_surname) AS initiator", false)
            ->from("payment_file_groups pfg")
            ->join("upl_users u", "u.user_id = pfg.created_by")
            ->order_by('pfg.id', 'DESC')
            ->get();

        $retVal = $result->num_rows() > 0 ? $result->result_array() : null;

        if ($retVal && !empty($relationships)) {
            if (array_key_exists('payment_files', $relationships)) {
                $retVal = $this->loadPaymentFiles($retVal, $relationships['payment_files']);
            }
        }

        return $retVal;
    }

    /**
     * Retrieve a Payment File Group.
     *
     * @param $id
     * @param null $relationships
     * @return null
     */
    public function getPaymentFileGroup($id, $relationships = null)
    {
        $result = $this->db
            ->select("pfg.*,
                        CONCAT(u.user_firstname, ' ', u.user_surname) AS initiator,
                        (SELECT CONCAT(u.user_firstname, ' ', u.user_surname) FROM payment_file pf
                            JOIN schedule_status ss ON pf.paymentFileId = ss.paymentFileId AND ss.scheduleStatus = '" . PAYMENT_FORWARDED . "'
                            JOIN upl_users AS u ON ss.modifiedBy = u.user_id
                            WHERE pf.payment_file_group_id = $id GROUP BY pf.payment_file_group_id) AS forwarder,
                        (SELECT CONCAT(u.user_firstname, ' ', u.user_surname) FROM payment_file pf
                            JOIN schedule_status ss ON pf.paymentFileId = ss.paymentFileId AND ss.scheduleStatus = '" . PAYMENT_APPROVED . "'
                            JOIN upl_users AS u ON ss.modifiedBy = u.user_id
                            WHERE pf.payment_file_group_id = $id GROUP BY pf.payment_file_group_id) AS approver,
                        (SELECT CONCAT(u.user_firstname, ' ', u.user_surname, ':', ss.statusReason) FROM payment_file pf
                            JOIN schedule_status ss ON pf.paymentFileId = ss.paymentFileId AND ss.scheduleStatus = '" . PAYMENT_REJECTED . "'
                            JOIN upl_users AS u ON ss.modifiedBy = u.user_id
                            WHERE pf.payment_file_group_id = $id GROUP BY pf.payment_file_group_id) AS rejecter,
                        (SELECT CONCAT(u.user_firstname, ' ', u.user_surname) FROM payment_file pf
                            JOIN schedule_status ss ON pf.paymentFileId = ss.paymentFileId AND ss.scheduleStatus = '" . PAYMENT_AUTHORIZED . "'
                            JOIN upl_users AS u ON ss.modifiedBy = u.user_id
                            WHERE pf.payment_file_group_id = $id GROUP BY pf.payment_file_group_id) AS authorizer",
                false)
            ->from("payment_file_groups pfg")
            ->join("upl_users u", "u.user_id = pfg.created_by")
            ->where(array("id" => $id))
            ->get();

        $retVal = $result->num_rows() > 0 ? $result->row_array() : null;

        if ($retVal && !empty($relationships)) {
            if (array_key_exists('payment_files', $relationships)) {
                //get the payment files in a group
                $retValWithRel = $this->loadPaymentFiles($retVal, $relationships['payment_files']);
                $retVal = $retValWithRel[0];
            }
        }

        return $retVal;
    }

    /**
     * Get the details of the payment schedule
     *
     * @param $paymentFileId
     * @return array of the payment details of the supplied paymentFileId
     */
    public function getPaymentFileRecord($paymentFileId, $relationships)
    {
        $merchantId = $_SESSION['mymerchantId'];
        $where_array = array('merchantId' => "$merchantId", 'paymentFileId' => "$paymentFileId");
        $query = $this->db->select("*", false)
            ->from('payment_file')
            ->where($where_array)
            ->order_by("paymentFileId", "desc")
            ->get();


        $retVal = $query->num_rows() > 0 ? $query->row_array() : array();

        if (!empty($retVal) && !empty($relationships)) {
            //get all the payment details in each file
            if (array_key_exists('payment_details', $relationships)) {
                $retVal = $this->loadPaymentDetails($retVal, $relationships['payment_details']);
            }

            if (array_key_exists('source_account', $relationships)) {
                $retVal = $this->loadSourceAccount($retVal, $relationships['source_account']);
            }

            if (array_key_exists('transaction_batch', $relationships)) {
                $retVal = $this->loadPaymentFileTransactionBatch($retVal, $relationships['transaction_batch']);
            }

            $retVal = $retVal[0];
        }

        return $retVal;
    }

    /**
     * Get all payment files / salary uploaded by a merchant.
     *
     * @param string $status status of the file
     * @param int $uploaded_by user that uploaded the file
     * @return array
     */
    public function getPaymentFiles($status = '', $uploaded_by = 0, $paymentFileId = 0, $payment_file_group_id = 0)
    {
        $merchantId = $_SESSION['mymerchantId'];
        $where_array = array('p.merchantId' => "$merchantId");

        if ($uploaded_by != 0) {
            $where_array['p.createdBy'] = $uploaded_by;
        }

        if ($paymentFileId != 0) {
            $where_array['p.paymentFileId'] = $paymentFileId;
        }

        if ($status != '') {
            $where_array['p.paymentStatus'] = $status;
        }

        if ($payment_file_group_id != 0) {
            $where_array['p.payment_file_group_id'] = $payment_file_group_id;
        }

        $query = $this->db->select('p.*, u.user_surname, u.user_firstname')
            ->from('payment_file p')
            ->join('upl_users u', 'u.user_id = p.createdBy', 'INNER')
            ->where($where_array)
            ->order_by("p.paymentFileId", "desc")
            ->get();

        $retVal = array();

        if ($query->num_rows() > 0) {
            $retVal = $query->result_array();
        }

        return $retVal;
    }

    /**
     * Retrieve payment files that are either forwarded or approved.
     *
     * @param null $relationships
     * @return array
     */
    public function getPaymentFileGroupsForApproval($relationships = null)
    {
        $approved_status = PAYMENT_APPROVED;
        $forwarded_status = PAYMENT_FORWARDED;
        $where = "(p.status='$approved_status' OR p.status='$forwarded_status')";

        $query = $this->db->select("p.*, CONCAT(u.user_surname, ' ',  u.user_firstname) initiator", false)
            ->from('payment_file_groups p')
            ->join('upl_users u', 'u.user_id = p.updated_by', 'INNER')
            ->where($where)
            ->order_by("p.updated_at", "desc")
            ->get();

        $retVal = array();

        if ($query->num_rows() > 0) {
            $retVal = $query->result_array();

            if ($retVal && !empty($relationships)) {
                if (array_key_exists('payment_files', $relationships)) {
                    $retVal = $this->loadPaymentFiles($retVal, $relationships['payment_files']);
                }
            }
        }

        return $retVal;
    }

    /**
     * @param string $status
     * @return array of all payment files uploaded by user
     */
    public function getMyPaymentFiles($status = '')
    {
        $merchantId = $_SESSION['mymerchantId'];
        $userId = $_SESSION['myuserId'];
        if ($status == '') {
            $where_array = array('merchantId' => "$merchantId", 'paymentStatus' => "$status", 'createdBy' => "$userId");
        }
        $where_array = array('merchantId' => "$merchantId", 'createdBy' => "$userId");
        $query = $this->db->select('*')
            ->from('payment_file')
            ->where($where_array)
            ->order_by("paymentFileId", "desc")
            ->get();

        $retVal = array();
        if ($query->num_rows() > 0) {
            $retVal = $query->result_array();
        }
        return $retVal;

    }

    public function requeryTransaction($BatchDetailsNumber)
    {

        $client = new SoapClient("https://orion.interswitchng.com/transactionthrottler/transactionthrottlerservice.svc?wsdl");
        //$client = new SoapClient("http://localhost/staging/interswitch.wsdl");
        $TerminalId = "3UPP0001";
        $SponsorCode = "UPP";
        $TransactionCode = "12341";

        $dstring = <<<XML
<?xml version="1.0" encoding="utf-8" ?>
<RequestDetails>
<TerminalId>$TerminalId</TerminalId>
<BatchNumber>$BatchDetailsNumber</BatchNumber>
</RequestDetails>
XML;
//echo $dstring;
//echo "<br>";
        $params2 = array('xmlParams' => $dstring);
        $objectresult2 = $client->CheckTransactionStatus($params2);
        echo "<!--";
        var_dump($objectresult2);
        echo "-->";
        $result2 = $objectresult2->CheckTransactionStatusResult;
        //echo "<br>$result2<br>";
        $xml2 = new SimpleXMLElement($result2);
        //echo $xml2;
        $Number = $xml2->Transactions['Number'];
        $k = 0;
        foreach ($xml2->Transactions->Transaction as $drecord) {
            $k++;
            $ReferenceNumber = $drecord['ReferenceNumber'];
            $Status = $drecord['Status'];
            $Attempts = $drecord['Attempts'];
            $FinalResponseCode = $drecord['FinalResponseCode'];
            $Amount = $drecord['Amount'];
//convert to naira
            $Amount = $Amount / 100;

            @$famount = $Amount;
            $famount = number_format($famount, "2");
//echo $Status;
//echo "<br>";
            if ($Status == "2") {
                $update = "UPDATE `transaction` SET `statusDescription` = 'Paid',`paymentStatus` = '00' WHERE `batchNo` = '$BatchDetailsNumber' AND  transNo = '$ReferenceNumber'";
//echo $update;
                $results = mysql_query($update);
            } elseif ($Status == "3") {
                $update = "UPDATE `transaction` SET `statusDescription` = 'Failed',`paymentStatus` = '02' WHERE `batchNo` = '$BatchDetailsNumber' AND transNo = '$ReferenceNumber'";
                $results = mysql_query($update);
            } elseif ($Status == "1") {

                $update = "UPDATE `transaction` SET `statusDescription` = 'Pending',`paymentStatus` = '01' WHERE `batchNo` = '$BatchDetailsNumber' AND transNo = '$ReferenceNumber'";
                $results = mysql_query($update);
            } elseif ($Status == "0") {
                $update = "UPDATE `transaction` SET `statusDescription` = 'Failed',`paymentStatus` = '02' WHERE `batchNo` = '$BatchDetailsNumber' AND transNo = '$ReferenceNumber'";
                $results = mysql_query($update)
                or die(mysql_error());
            } else {
                $update = "UPDATE `transaction` SET `statusDescription` = 'Pending',`paymentStatus` = '01' WHERE `batchNo` = '$BatchDetailsNumber' AND transNo = '$ReferenceNumber'";
                $results = mysql_query($update);
            }
        }

    }

    public function getBatchDetails($batchNo)
    {
        $whereArray = array('t.batchNo' => "$batchNo");

        $query = $this->db->select("p.*, b.bankName,f.paymentFileDescription,f.paymentFileId,f.paymentStatus,
            (SELECT t.paymentStatus FROM transaction t WHERE t.paymentDetailsId = p.paymentDetailsId AND t.sentStatus = '1' AND t.paymentStatus = '01') 'pendingStatus',
            (SELECT t.paymentStatus FROM transaction t WHERE t.paymentDetailsId = p.paymentDetailsId AND t.sentStatus = '1' AND t.paymentStatus = '00') 'paidStatus',
            (SELECT t.statusDescription FROM transaction t WHERE t.paymentDetailsId = p.paymentDetailsId AND t.sentStatus = '1' AND t.paymentStatus = '02' ORDER BY transactionId DESC LIMIT 1) 'failedStatus'
            ", false)
            ->from('payment_details p')
            ->join('payment_file f', "f.paymentFileId = p.paymentFileId", 'INNER')
            ->join('transaction t', "t.paymentDetailsId = p.paymentDetailsId")
            ->join('banks b', "p.beneficiaryBankId = b.bankId", 'LEFT')
            ->where($whereArray)
            ->get();
        // echo $this->db->last_query();
        $retVal = array();
        if ($query->num_rows() > 0) {
            $retVal = $query->result_array();
        }

        //die('<pre>'.print_r($retVal,1));
        return $retVal;
    }

    public function getPaymentFileDetails($paymentFileId)
    {
        $whereArray = array('p.paymentFileId' => "$paymentFileId");

        $query = $this->db->select("p.*, b.bankName, f.paymentFileDescription, f.paymentFileId, f.paymentStatus, f.paymentFileType, f.sourceAccountId,
            (SELECT CONCAT_WS('-',k.bankName, a.acc_number, a.acc_name) FROM banks k, bank_accounts a WHERE a.acc_id = f.sourceAccountId AND k.bank_id = a.bank_id) 'sourceBank',
            (SELECT t.paymentStatus FROM transaction t WHERE t.paymentDetailsId = p.paymentDetailsId AND t.sentStatus = '1' AND t.paymentStatus = '01') 'pendingStatus',
            (SELECT t.paymentStatus FROM transaction t WHERE t.paymentDetailsId = p.paymentDetailsId AND t.sentStatus = '1' AND t.paymentStatus = '00') 'paidStatus',
            (SELECT t.statusDescription FROM transaction t WHERE t.paymentDetailsId = p.paymentDetailsId AND t.sentStatus = '1' AND t.paymentStatus = '02' ORDER BY transactionId DESC LIMIT 1) 'failedStatus'
            ", false)
            ->from('payment_details p')
            ->join('payment_file f', "f.paymentFileId = p.paymentFileId", 'INNER')
            ->join('banks b', "p.beneficiaryBankId = b.bankId", 'LEFT')
            ->where($whereArray)
            ->order_by('p.beneficiarySurname')
            ->get();

        $retVal = array();

        if ($query->num_rows() > 0) {
            $retVal = $query->result_array();
        }

        return $retVal;
    }

    /**
     * Compute the total amount to be paid to all the beneficiaries in batch.
     *
     * @param $paymentFileGroupId
     * @return array
     */
    public function getPaymentFileGroupSum($paymentFileGroupId)
    {
        $retVal = array();

        $query = $this->db->select_sum('amount')
            ->from("payment_details pd")
            ->join("payment_file pf", "pf.paymentFileId = pd.paymentFileId")
            ->where(array('pf.payment_file_group_id' => "$paymentFileGroupId"))
            ->get();

        if ($query->num_rows() > 0) {
            $retVal = $query->row_array();
        }

        return $retVal;
    }

    public function getPaymentFileSum($paymentFileId)
    {
        $whereArray = array('paymentFileId' => "$paymentFileId");
        $this->db->select_sum('amount');
        $this->db->where($whereArray);
        $query = $this->db->get('payment_details');
        $retVal = array();

        if ($query->num_rows() > 0) {
            $retVal = $query->row_array();
        }

        return $retVal;
    }

    public function getUsersAccountMandate()
    {
        $retVal = array();

        $userId = $_SESSION['myuserId'];
        $whereArray = array('a.userId' => "$userId", 'a.mandateStatus' => '1');

        $result = $this->db->select('a.mandateLimit, a.merchantAccountId,  m.accountNo, b.bankName', false)
            ->from('merchant_account_mandate a')
            ->join('merchant_account m', "m.merchantAccountId = a.merchantAccountId", 'LEFT')
            ->join('banks b', "b.bankId = m.bankId", 'LEFT')
            ->where($whereArray)
            ->get();

        if ($result->num_rows() > 0) {
            $retVal = $result->result_array();
        }

        return $retVal;
    }

    public function getAccountMandateLimit($sourceAccount, $userId)
    {
        $retVal = 0;

        $whereArray = array('a.userId' => "$userId", 'a.merchantAccountId' => "$sourceAccount");

        $result = $this->db->select('a.mandateLimit', false)
            ->from(' merchant_account_mandate a')
            ->where($whereArray)
            ->get();

        if ($result->num_rows() > 0) {
            $row = $result->row_array();
            $retVal = $row['mandateLimit'];
        }

        return $retVal;
    }

    public function getMandate($paymentFileId)
    {
        $whereArray = array('p.paymentFileId' => "$paymentFileId");
        $result = $this->db->select("p.*, m.accountNo, b.bankName, u.user_surname, u.user_firstname")
            ->from('payment_authorize p')
            ->join('merchant_account m', "m.merchantAccountId = p.merchantAccountId", "LEFT")
            ->join('banks b', "b.bankId = m.bankId", 'LEFT')
            ->join('upl_users u', "u.user_id = p.userId")
            ->where($whereArray)
            ->get();

        $retVal = array();
        if ($result->num_rows() > 0) {
            $retVal = $result->result_array();
        }

        return $retVal;
    }

    public function doMandate($paymentFileId, $sourceAccount)
    {
        $retVal = true;

        $userId = $_SESSION['myuserId'];
        $insertArray = array(
            'paymentFileId' => "$paymentFileId",
            'merchantAccountId' => "$sourceAccount",
            'userId' => "$userId",
            'authorized' => "1"
        );

        $this->db->replace('payment_authorize', $insertArray);

        return $retVal;
    }

    public function confirmSignatories($paymentFileId, $sourceAccount)
    {
        $retVal = false;

        $where = array('merchantAccountId' => $sourceAccount);
        $query = $this->db->select('minimumSignatory')
            ->from('merchant_account')
            ->where($where)
            ->get();

        $minimumSignatory = 0;
        if ($query->num_rows() > 0) {
            $sigArray = $query->row_array();
            $minimumSignatory = $sigArray['minimumSignatory'];
        }

        $whereArray = array('paymentFileId' => "$paymentFileId", 'merchantAccountId' => "$sourceAccount");
        $this->db->where($whereArray);
        $this->db->from('payment_authorize');
        $numSignatories = $this->db->count_all_results();

        if ($numSignatories >= $minimumSignatory) {
            $retVal = true;
        }

        return $retVal;
    }

    public function authotrizePaymentFile($paymentFileId)
    {
        try {
            $retVal = true;
            $this->db->trans_begin();
            $now = date("Y-m-d H:i:s");
            $set = array('paymentStatus' => PAYMENT_AUTHORIZED, 'modifiedBy' => $_SESSION['myuserId'], 'modifiedDate' => "$now");
            $where = array('paymentFileId' => $paymentFileId);
            $this->db->update("payment_file", $set, $where);
            //insert into schedule status
            $insertArray = array('paymentFileId' => "$paymentFileId", 'scheduleStatus' => PAYMENT_AUTHORIZED,
                'statusReason' => "", 'statusDate' => "$now", 'modifiedBy' => $_SESSION['myuserId']);
            $this->db->insert('schedule_status', $insertArray);
            $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $retVal = false;
        }

        return $retVal;
    }

    /**
     * Flag the payment group in tbl:payment_file_groups as REJECTED;
     * Flag all the the payment schedules in the group in tbl:payment_file as REJECTED;
     * log this update in tbl:schedule_status
     *
     * @param $paymentFileGroupId
     * @param $reason
     * @return bool
     */
    public function rejectPaymentFile($paymentFileGroupId, $reason)
    {
        try {
            $retVal = true;
            $this->db->trans_begin();

            $this->db->update("payment_file_groups", array('status' => PAYMENT_REJECTED, 'updated_by' => $_SESSION['myuserId']), array('id' => $paymentFileGroupId));

            $now = date("Y-m-d H:i:s");
            $set = array('paymentStatus' => PAYMENT_REJECTED, 'modifiedBy' => $_SESSION['myuserId'], 'modifiedDate' => "$now");
            $where = array('payment_file_group_id' => $paymentFileGroupId);
            $this->db->update("payment_file", $set, $where);

            //insert into schedule status
            $payment_files = $this->getPaymentFiles('', 0, 0, $paymentFileGroupId);
            $insert_data = array();
            foreach ($payment_files as $file) {
                $insert_data[] = array(
                    'paymentFileId' => "$file[paymentFileId]",
                    'scheduleStatus' => PAYMENT_REJECTED,
                    'statusReason' => "$reason",
                    'statusDate' => "$now",
                    'modifiedBy' => $_SESSION['myuserId']
                );
            }
            $this->db->insert_batch('schedule_status', $insert_data);

            $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $retVal = false;
        }

        return $retVal;
    }

    /**
     * Flag the payment group in tbl:payment_file_groups as APPROVED;
     * Flag all the the payment schedules in the group in tbl:payment_file as APPROVED;
     * log this update in tbl:schedule_status
     *
     * @param $paymentFileGroupId
     * @return bool
     */
    public function approvePaymentFile($paymentFileGroupId)
    {
        try {
            $retVal = true;
            $this->db->trans_begin();

            $this->db->update("payment_file_groups", array('status' => PAYMENT_APPROVED, 'updated_by' => $_SESSION['myuserId']), array('id' => $paymentFileGroupId));

            $now = date("Y-m-d H:i:s");
            $set = array('paymentStatus' => PAYMENT_APPROVED, 'modifiedBy' => $_SESSION['myuserId'], 'modifiedDate' => "$now");
            $where = array('payment_file_group_id' => $paymentFileGroupId);
            $this->db->update("payment_file", $set, $where);

            //insert into schedule status
            $payment_files = $this->getPaymentFiles('', 0, 0, $paymentFileGroupId);
            $insert_data = array();
            foreach ($payment_files as $file) {
                $insert_data[] = array(
                    'paymentFileId' => "$file[paymentFileId]",
                    'scheduleStatus' => PAYMENT_APPROVED,
                    'statusReason' => "",
                    'statusDate' => "$now",
                    'modifiedBy' => $_SESSION['myuserId']
                );
            }
            $this->db->insert_batch('schedule_status', $insert_data);

            $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $retVal = false;
        }

        return $retVal;
    }

    /*
     * Store a payment file group
     *
     * @param data array $data
     * @return int $payment_file_group_id or null
     */
    public function savePaymentFileGroup($data)
    {
        $response = $this->db->insert('payment_file_groups', $data);
        return $response ? $this->db->insert_id() : null;
    }

    /**
     * Save the payment file group data in tbl:payment_file_groups;
     * Save the payment file data in tbl:payment_file
     * Save the beneficiary(ies)' details in tbl:payment_details
     *
     * @param $payment_file_group_data
     * @param $payment_data including the payment details data
     * @return bool|null
     */
    public function savePayment($payment_file_group_data, $payment_data)
    {
        try {
            $now = date("Y-m-d H:i:s");

            //Begin a database transaction
            $this->db->trans_begin();

            $id = $this->savePaymentFileGroup($payment_file_group_data);

            foreach ($payment_data as $data) {
                $payment_file_data = array(
                    "payment_file_group_id" => "$id",
                    'paymentFileDescription' => "$data[narration]",
                    'paymentFileType' => "$data[file_type]",
                    'sourceAccountId' => "$data[source_account_id]",
                    'paymentStatus' => "0",
                    'merchantId' => $_SESSION['mymerchantId'],
                    'createdBy' => $_SESSION['myuserId'],
                    'createdDate' => "$now",
                    'modifiedBy' => $_SESSION['myuserId'],
                    'modifiedDate' => "$now"
                );
                $this->db->insert('payment_file', $payment_file_data);

                $payment_file_id = $this->db->insert_id();

                $payment_details_data = array();

                foreach ($data['beneficiaries'] as $beneficiary) {
                    $payment_details_data[] = array(
                        'beneficiarySurname' => "$beneficiary[surname]",
                        'beneficiaryFirstname' => "$beneficiary[firstname]",
                        'beneficiaryOthernames' => "$beneficiary[othernames]",
                        'beneficiaryBankId' => "$beneficiary[bankId]",
                        'beneficiaryAccountNo' => "$beneficiary[account_no]",
                        'paymentFileId' => "$payment_file_id",
                        'amount' => "$beneficiary[amount]",
                        'createdBy' => $_SESSION['myuserId'],
                        'createdDate' => $now,
                        'modifiedBy' => $_SESSION['myuserId']
                    );
                }

                $this->db->insert_batch('payment_details', $payment_details_data);
            }

            $retVal = $id;

            $this->db->trans_commit();
        } catch (Exception $ex) {
            //Transaction failed, so rollback
            $this->db->trans_rollback();
            $retVal = false;
        }

        return $retVal;
    }

    /**
     * Generate transaction records for the beneficiaries in the payment schedule;
     * Generate a token and send to the client
     *
     * @param $payment_file_group_id
     * @return bool|string
     */
    public function saveSchedule($payment_file_group_id)
    {
        $userId = $_SESSION['myuserId'];
        $now = date("Y-m-d H:i:s");

        try {
            //Begin a database transaction
            $relationships = array(
                'payment_files' => array(
                    "payment_details" => "",
                )
            );

            $payment_file_group = $this->getPaymentFileGroup($payment_file_group_id, $relationships);

            $this->db->trans_begin();

            $retVal = array('payment_file_group_id' => $payment_file_group_id, 'batchNo' => array());

            //generate a batch for each payment file in the group
            foreach ($payment_file_group['payment_files'] as $file) {
                $retVal['batchNo'][] = $batchNo = strtoupper($this->basic_functions->create_random_name(12));
                $schedule_id = $this->basic_functions->create_random_number_string(12);

                $batch_data = array(
                    'batchNo' => "$batchNo",
                    'schedule_id' => "$schedule_id",
                    'paymentFileId' => $file['paymentFileId'],
                    'sourceAccId' => "$file[sourceAccountId]",
                    'status' => '1',
                    'processedBy' => "$userId",
                    'processedDate' => "$now"
                );
                $this->db->insert("transaction_batches", $batch_data);
                $batch_id = $this->db->insert_id();

                //save the transaction beneficiaries
                $insert_data = array();
                foreach ($file['beneficiaries'] as $beneficiary) {
                    $transNo = strtoupper($this->basic_functions->create_random_name(10));
                    $insertArray = array(
                        'batchId' => $batch_id,
                        'batchNo' => "$batchNo",
                        'transNo' => "$transNo",
                        'paymentDetailsId' => "$beneficiary[paymentDetailsId]",
                        'transaction_amount' => $beneficiary['amount'],
                        'sentStatus' => '0',
                        'paymentStatus' => "01",
                        'statusDescription' => "Pending",
                        'processedBy' => "$userId",
                        'processedDate' => "$now",
                        'acc_id' => "$file[sourceAccountId]"
                    );
                    $insert_data[] = $insertArray;
                }
                $this->db->insert_batch("transaction", $insert_data);
            }

            $this->db->trans_commit();
        } catch (Exception $ex) {
            //Transaction failed, so rollback
            $this->db->trans_rollback();
            $retVal = false;
        }

        return $retVal;
    }

    public function checkBalance($batchNo)
    {
        //$client_id = 'IKIADAD11343B9F4BEB648C6AA891D07C65F1B4F1A30';
        // $secret_key= '/lYQkCvub/3DpIYWgExiq/CHvvSVpOr0gfIYRNernyw=';
        $http_verb = 'GET';
        $content_type = 'application/json';
        $u_url = 'https://sandbox.interswitchng.com/api/v1/cards/balances';

        $percent_encoded_url = urlencode($u_url);
        $timestamp = time();
        $nonce = substr(str_shuffle(MD5(microtime())), 0, 10);

        $value1 = $http_verb . '&' . $percent_encoded_url . '&' . $timestamp . '&' . $nonce . '&' . ISW_CLIENTID . '&' . ISW_SECRETKEY;

        $auth = base64_encode($client_id);

        $signature = base64_encode(sha1($value1, true));

        $sourceAccountArray = $this->getBatchSourceAccount($batchNo);
        $sourceCardPan = $this->basic_functions->card_decrypt($sourceAccountArray->cardPAN);
        $sourcePinBlock = $this->basic_functions->card_decrypt($sourceAccountArray->cardPinBlock);
        $cardExpiryDate = $sourceAccountArray->cardExpiryDate;
        $expiryArray = explode("-", $cardExpiryDate);
        $yymm = substr($expiryArray['0'], 2, 2) . "" . $expiryArray[1];
        $auth_data = $this->getAuthData($sourcePinBlock, $sourceCardPan, $yymm);
        $header = array(
            "Content-Type: $content_type",
            "Authorization: InterswitchAuth $auth",
            "Signature: $signature",
            "Nonce: $nonce",
            "Timestamp: $timestamp",
            "SignatureMethod: SHA1",
            "TERMINAL_ID: 3UPP0001",
            "PRODUCT_CODE: AUTOPAY",
            "AUTH_DATA: $auth_data"
        );
    }
    //
//generateINFile
    public function generateXML()
    {
        $retVal = array(false, "Error Connecting to Payment Gateway");
        $merchantName = $_SESSION['mymerchantName'];
        $batchNo = $_SESSION['myBatchNo'];
        $TerminalId = "3UPP0001";

        $sourceAccountArray = $this->getBatchSourceAccount($batchNo);
        $bankCode = $sourceAccountArray->bankNibssCode;
        $sourceAccountNo = $sourceAccountArray->accountNo; //"0101111";//
        $sourceCardPan = $this->basic_functions->card_decrypt($sourceAccountArray->cardPAN);
        //$sourceAccountArray->cardPinBlock = "1111";
        if (!empty($sourceAccountArray->cardPinBlock)) {
            $sourcePin = $this->basic_functions->card_decrypt($sourceAccountArray->cardPinBlock);
            $cardExpiryDate = $sourceAccountArray->cardExpiryDate;
            $expiryArray = explode("-", $cardExpiryDate);
            $yymm = substr($expiryArray['0'], 2, 2) . "" . $expiryArray[1];
            $sourceAccountType = "20";

            $securejson = $this->getSecureData($sourcePin, $sourceCardPan, $yymm);

            $secureArray = json_decode($securejson, true);
            // $secureData ="994d693b5a324e47cfb5eccffe08aa28b8fe5664bd489f2ede43b7fd9921813ffbb9025eb7641ee4c5bd6c71881895d799f4d69a35e48614d639e9daf5690a4cc64c537659281547aa80ebaffe54e9b2ea2517da5f5cef2c9204528d896cc99915c4e5d0b63893a4958b6ee04876663dc4960d7b91152768524a69ad650e867799c687e4f7c732a3bee816dbdc629e5787448707573d51ab2c3e90e8bf6b19863cb4dba059f6835af7aa68dc34517643303feae8e88076d9be25a29fab07d3d43a648b160e8c9b086856b0baba67df44849d1fd36d267c8680e64b55c4e0ceebab8bd2ed201e2792eb704b8d93b95bbb15ac9a3be19fea28b021e3df68035f86";//
            //  $encrypted_pin = "44330e51f43a79cd";//
            $secureData = $secureArray['Item1'];
            $encrypted_pin = $secureArray['Item2'];
            $mac_input = "";
            $payArray = $this->getTransactionDetails($batchNo);
            $description = $payArray[0]['paymentFileDescription'];
            $file_content_line_1 = "$batchNo, $description, false, $TerminalId\n";

            $totalAmount = 0;
            $allAccount = "";
            $file_content = "";
            foreach ($payArray AS $u) {
                $description = $u['paymentFileDescription'];
                $beneficiary = $u['fullname'];
                $amount = $u['amount'];
                $TransactionAmount = $amount * 100;
                $totalAmount += $TransactionAmount;
                $TransactionToAccountNumber = $u['beneficiaryAccountNo'];
                $allAccount .= $TransactionToAccountNumber;
                $IS_PrepaidLoad = 'false';
                $Currency_Code = "NGN";
                $Mobile_Number = "0";
                $TransactionReferenceNumber = $u['transNo'];
                $sentTransactionReferenceNumber = $description . "_" . $TransactionReferenceNumber;
                $TransactionToAccountType = '10';
                $testEmail = "segun.ojo@upperlink.ng";
                $TransactionBankName = $u['bankName'];
                $TransactionTransactionType = "50"; //50 means Payment to Account
                $TransactionBankCBNCode = $u['bankNibssCode'];

                $file_content .= "$sentTransactionReferenceNumber,$TransactionAmount,$description,$TransactionToAccountNumber,$testEmail, $TransactionBankCBNCode,$TransactionToAccountNumber, $TransactionToAccountType, $IS_PrepaidLoad,$Currency_Code, $beneficiary,$Mobile_Number \n";

            }
            $password = "$TerminalId$allAccount$totalAmount$allAccount";
            $mac = hash('sha512', $password);
            $file_content_line_2 = "$secureData,$sourceAccountNo, $sourceAccountType, $bankCode, $encrypted_pin, $mac\n";
            $real_file_content = "$file_content_line_1$file_content_line_2$file_content";
            $small_filename = $this->basic_functions->create_random_name(10);
            $filename = strtoupper($small_filename);
            $update_array = array('filename' => "$filename");
            $where_array = array('batchNo' => "$batchNo");
            $this->db->update("transaction", $update_array, $where_array);


            $this->load->helper('file');

            if (!write_file("myfiles/IN/$filename.csv", $real_file_content)) {
                $retVal = array(false, "Error creating $filename.csv");
            } else {
                $this->load->library('Sftplib');
                $sftp = new Net_SFTP(SFTP_HOST);
                if ($sftp->login(SFTP_USERNAME, SFTP_PASSWORD)) {
                    $local_file = "myfiles/IN/$filename.csv";
                    $remote_file = "/IN/$filename.csv";
                    if (file_exists($local_file)) {
                        $upload = $sftp->put($remote_file, $local_file, NET_SFTP_LOCAL_FILE);
                        if (!$upload) {
                            $retVal = array(false, "Transaction Logging Failed");
                        } else {
                            $retVal = array(true, "Payment Successfully Logged");
                            $query3333 = "UPDATE `transaction` SET `sentStatus` = '1' WHERE  `batchNo` = '$batchNo'";
                            $this->db->query($query3333);
                        }
                    } else {
                        $retVal = array(false, "$local_file does not exist!!");
                    }
                } else {
                    $retVal = array(true, "Unable to connect to Payment Gateway");
                }

            }
        } else {
            $retVal = array(false, "Please change Pin first");
        }

        return $retVal;
    }

    private function getPinBlockBase64($PINblock)
    {
        echo $PINblock;
        $TransactionID = $this->basic_functions->create_random_name(12);
        $Salt = "fP3[fI9*pR8}sZ4[fG0@kZ2{tM6)oN7]";
        $Token = base64_encode(base64_encode($Salt) . base64_encode("$Salt" . base64_encode($PINblock)));
        $data = "$TransactionID|$Salt";
        $MAC = hash('sha512', $data);

        $url = 'http://isw.upl.com.ng/ISWFinal/Default.aspx'; //http://isw.upl.com.ng/ISWFinal/Default.aspx
        $fields = array('T1' => urlencode($TransactionID), 'T2' => urlencode($Token), 'T3' => urlencode($MAC));

        $data = http_build_query($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $res = curl_exec($ch);

        $str1 = base64_decode(base64_decode($res));
        $str2 = str_replace($Salt, "", $str1);
        $EncryptPINblock = base64_decode($str2);

        return $EncryptPINblock;
    }

    private function getSecureData($pin, $pan, $expiry)
    {

        $TransactionID = $this->basic_functions->create_random_name(12);

        $Salt = "fP3[fI9*pR8}sZ4[fG0@kZ2{tM6)oN7]";

        $T1 = $TransactionID;
        $T2 = base64_encode(base64_encode($Salt) . base64_encode("$Salt" . base64_encode($pin)));
        $T3 = base64_encode(base64_encode($Salt) . base64_encode("$Salt" . base64_encode($pan)));
        $T4 = base64_encode(base64_encode($Salt) . base64_encode("$Salt" . base64_encode($expiry)));

        $data = "$TransactionID|$Salt";
        $MAC = hash('sha512', $data);
        $T5 = $MAC;

        $url = 'http://isw.upl.com.ng/ISWSecureDataFinal/Default.aspx';
        $fields = array('T1' => urlencode($T1), 'T2' => urlencode($T2), 'T3' => urlencode($T3), 'T4' => urlencode($T4), 'T5' => urlencode($T5));

        $data = http_build_query($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($ch);

//echo "$res<br><br>";
//$Finalres = base64_encode(base64_encode("$Salt" . base64_encode($EncryptPINblock)));

        $str1 = base64_decode(base64_decode($res));
        $str2 = str_replace($Salt, "", $str1);
        $finalstring = base64_decode($str2);
        return $finalstring;
    }

    private function getAuthData($pin, $pan, $expiry)
    {

        $TransactionID = $this->basic_functions->create_random_name(12);

        $Salt = "fP3[fI9*pR8}sZ4[fG0@kZ2{tM6)oN7]";

        $T1 = $TransactionID;
        $T2 = base64_encode(base64_encode($Salt) . base64_encode("$Salt" . base64_encode($pin)));
        $T3 = base64_encode(base64_encode($Salt) . base64_encode("$Salt" . base64_encode($pan)));
        $T4 = base64_encode(base64_encode($Salt) . base64_encode("$Salt" . base64_encode($expiry)));

        $data = "$TransactionID|$Salt";
        $MAC = hash('sha512', $data);
        $T5 = $MAC;

        $url = 'http://isw.upl.com.ng/ISWAuthData/Default.aspx';
        $fields = array('T1' => urlencode($T1), 'T2' => urlencode($T2), 'T3' => urlencode($T3), 'T4' => urlencode($T4), 'T5' => urlencode($T5));

        $data = http_build_query($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($ch);

//echo "$res<br><br>";
//$Finalres = base64_encode(base64_encode("$Salt" . base64_encode($EncryptPINblock)));

        $str1 = base64_decode(base64_decode($res));
        $str2 = str_replace($Salt, "", $str1);
        $finalstring = base64_decode($str2);
        return $finalstring;
    }

    public function generateXMLLIVE()
    {
        $retVal = array(false, "Error Connecting to Payment Gateway");
        $merchantName = $_SESSION['mymerchantName'];
        $batchNo = $_SESSION['myBatchNo'];
        $sourceAccountArray = $this->getBatchSourceAccount($batchNo);
        $sourceBankcode = $sourceAccountArray->bankNibssCode;
        $sourceAccountNo = $sourceAccountArray->accountNo;

        $CardPAN = $this->basic_functions->card_decrypt($sourceAccountArray->cardPAN);
        $SourceCardPIN = $this->basic_functions->card_decrypt($sourceAccountArray->cardPinBlock);
        $pinBlock = $this->GetPinBlock($SourceCardPIN, $CardPAN);
        $CardPinBlock = $this->getPinBlockBase64($pinBlock);
        $cardExpiryDate = $sourceAccountArray->cardExpiryDate;
        $CardTerminalId = $this->basic_functions->card_decrypt($sourceAccountArray->cardTerminalId);
        $dateArray = explode("-", $cardExpiryDate);
        $CardExpiryDay = $dateArray[2];
        $CardExpiryMonth = $dateArray[1];
        $CardExpiryYear = $dateArray[0];
        $CardSequenceNumber = 1;
        $payArray = $this->getTransactionDetails($batchNo);
        $Count = count($payArray);


        $CardTerminalId = "3IGW0BRC";
        try {
            //$rrr = $client = new SoapClient("http://localhost/staging/interswitch.wsdl");
            $rrr = $client = new SoapClient("https://orion.interswitchng.com/transactionthrottler/transactionthrottlerservice.svc?wsdl");
            $TerminalId = "3UPP0001";
            $SponsorCode = "UPP";
            $TransactionCode = "12341";

            $string1 = <<<XML
<?xml version="1.0" encoding="utf-8" ?>
<RequestDetails>
<TerminalId>$TerminalId</TerminalId>
<SponsorCode>$SponsorCode</SponsorCode>
<TransactionCode>$TransactionCode</TransactionCode>
<BatchDetails Number="$batchNo">
<SourceAccountDetails CurrencyCode="566" BankCBNCode="$sourceBankcode">
<Number>$sourceAccountNo</Number>
<Type>20</Type>
<CardPAN>$CardPAN</CardPAN>
<CardPinBlock>$CardPinBlock</CardPinBlock>
<CardExpiryDay>$CardExpiryDay</CardExpiryDay>
<CardExpiryMonth>$CardExpiryMonth</CardExpiryMonth>
<CardExpiryYear>$CardExpiryYear</CardExpiryYear>
<CardSequenceNumber>$CardSequenceNumber</CardSequenceNumber>
<CardTerminalId>$CardTerminalId</CardTerminalId>
</SourceAccountDetails>
<Transactions Count="$Count">
XML;
            $serialNumber = 0;
            $string2 = "";

            //$jsonData = json_encode($payArray);
            //log for report
            //$this->logTransactionForReport($jsonData);
            //die();
            foreach ($payArray AS $u) {
                $serialNumber++;
                $beneficiary = $u['fullname'];
                $amount = $u['amount'];
                $TransactionSurcharge = 10500;
                $TransactionAmount = ($amount * 100) - $TransactionSurcharge;
                $TransactionToAccountNumber = $u['beneficiaryAccountNo'];


                $description = $u['paymentFileDescription'];
                $TransactionReferenceNumber = $u['transNo'];
                $TransactionToAccountType = '10';
                $TransactionBankName = $u['bankName'];
                $TransactionTransactionType = "50"; //50 means Payment to Account
                $TransactionDescription = substr($description, 0, 14) . " " . $sourceAccountNo . " ";
                $TransactionBankCBNCode = $u['bankNibssCode'];
                $password = "$batchNo$TransactionReferenceNumber$TransactionToAccountNumber$TransactionAmount$TransactionBankCBNCode";
                $mac = hash('sha512', $password);
                $string2 .= <<<XML
<Transaction>
<ReferenceNumber>$TransactionReferenceNumber</ReferenceNumber>
<TransactionType>$TransactionTransactionType</TransactionType>
<ToAccountNumber>$TransactionToAccountNumber</ToAccountNumber>
<ToAccountType>$TransactionToAccountType</ToAccountType>
<Amount>$TransactionAmount</Amount>
<Surcharge>$TransactionSurcharge</Surcharge>
<Description>$TransactionDescription</Description>
<Narration>$TransactionDescription</Narration>
<BankName>$TransactionBankName</BankName>
<BankCBNCode>$TransactionBankCBNCode</BankCBNCode>
<MAC>$mac</MAC>
</Transaction>
XML;

            }
            $string3 = "
</Transactions>
</BatchDetails>
</RequestDetails>
";


            $string = "$string1$string2$string3";

            $params = array('xmlParams' => $string);
            //print_r($params); //die();

            $query3333 = "UPDATE `transaction` SET `sentStatus` = '1' WHERE  `batchNo` = '$batchNo'";
            $this->db->query($query3333);

            $objectresult = $client->ProcessTransactions($params);

            $result = $objectresult->ProcessTransactionsResult;

            $xml = new SimpleXMLElement($result);

            $ResponseCode = (string)$xml->ResponseCode;

            if ($ResponseCode == "90000") {
                //update to still processing
                $today = date("Y-m-d H:i:s");
                $tommorrow = "SELECT DATE_ADD('$today',INTERVAL 1 DAY) AS dday";
                $resulttt = $this->db->query($tommorrow);
                $rowday = $resulttt->row();
                $dday = $rowday->dday;
                $queryupdate = "UPDATE `transaction` SET `statusDescription` = 'Pending',`paymentStatus` = '01' WHERE `batchNo` = '$batchNo'";
                $this->db->query($queryupdate);
                $retVal = array(true, "Payment File Completely Received");
            } else {
                $whereUpdate = array('batchNo' => "$batchNo");
                $setArray = array('paymentStatus' => '02', 'statusDescription' => "Error Logging Transaction");
                $this->db->update("transaction", $setArray, $whereUpdate);
                $retVal = array(false, "Error Logging Transaction");
            }

        } catch (Exception $ex) {
            $whereUpdate = array('batchNo' => "$batchNo");
            $setArray = array('paymentStatus' => '02', 'statusDescription' => "Cannot Connect");
            $this->db->update("transaction", $setArray, $whereUpdate);
            $retVal = array(false, "Cannot Connect to Payment Gateway");
        }

        return $retVal;
    }

    public function generateXMLTest()
    {
        $retVal = array(false, "Error Connecting to Payment Gateway");

        $merchantName = $_SESSION['mymerchantName'];
        $batchNo = $_SESSION['myBatchNo'];
        $surcharge = 10;
        $sourceAccountArray = $this->getBatchSourceAccount($batchNo);

        $sourceBankcode = $sourceAccountArray->bankNibssCode; //"001";//

        $sourceAccountNo = $sourceAccountArray->accountNo; //"0101111";//
        $CardPAN = $this->basic_functions->card_decrypt($sourceAccountArray->cardPAN);
        $CardPinBlock = $this->basic_functions->card_decrypt($sourceAccountArray->cardPinBlock);
        $cardExpiryDate = $sourceAccountArray->cardExpiryDate;
        $CardTerminalId = $this->basic_functions->card_decrypt($sourceAccountArray->cardTerminalId);
        //echo $cardExpiryDate;
        $dateArray = explode("-", $cardExpiryDate);
        $CardExpiryDay = $dateArray[2];
        $CardExpiryMonth = $dateArray[1];
        $CardExpiryYear = $dateArray[0];
        $CardSequenceNumber = 1;
        $payArray = $this->getTransactionDetails($batchNo);
        $Count = count($payArray);
        $payer = $merchantName;
        //echo "here";
        // die();
        $sourceAccountNo = "0124578963";
        $sourceBankcode = "057";
        $CardPAN = "6280511000000095";//$rowb['CardPAN'];
        $CardPinBlock = "GKIN+bWijeWb5gR09wPVeA==";//base64_encode($this->GetPinBlock("0000",$CardPAN));//$rowb['CardPinBlock'];
        $CardExpiryDay = "01";;//$rowb['CardExpiryDay'];
        $CardExpiryMonth = "12";//$rowb['CardExpiryMonth'];
        $CardExpiryYear = "2026";//$rowb['CardExpiryYear'];
        $CardTerminalId = "3IAP0044";
        try {
            $rrr = $client = new SoapClient("http://localhost/staging/interswitch.wsdl");
            $TerminalId = "3UPP0001";
            $SponsorCode = "UPP";
            $TransactionCode = "12341";

            $string1 = <<<XML
<?xml version="1.0" encoding="utf-8" ?>
<RequestDetails>
<TerminalId>$TerminalId</TerminalId>
<SponsorCode>$SponsorCode</SponsorCode>
<TransactionCode>$TransactionCode</TransactionCode>
<BatchDetails Number="$batchNo">
<SourceAccountDetails CurrencyCode="566" BankCBNCode="$sourceBankcode">
<Number>$sourceAccountNo</Number>
<Type>20</Type>
<CardPAN>$CardPAN</CardPAN>
<CardPinBlock>$CardPinBlock</CardPinBlock>
<CardExpiryDay>$CardExpiryDay</CardExpiryDay>
<CardExpiryMonth>$CardExpiryMonth</CardExpiryMonth>
<CardExpiryYear>$CardExpiryYear</CardExpiryYear>
<CardSequenceNumber>$CardSequenceNumber</CardSequenceNumber>
<CardTerminalId>$CardTerminalId</CardTerminalId>
</SourceAccountDetails>
<Transactions Count="$Count">
XML;
            $serialNumber = 0;
            $string2 = "";

            //$jsonData = json_encode($payArray);
            //log for report
            //$this->logTransactionForReport($jsonData);


            //die();
            foreach ($payArray AS $u) {
                $serialNumber++;
                $beneficiary = $u['fullname'];
                $amount = $u['amount'];
                $TransactionAmount = $amount * 100;
                $TransactionToAccountNumber = $u['beneficiaryAccountNo'];
                $sortCode = $u['bankNibssCode'] . "150000";
                $TransactionSurcharge = 10500;
                $description = $u['paymentFileDescription'];
                $TransactionReferenceNumber = $u['transNo'];
                $TransactionToAccountType = '10';
                $TransactionBankName = $u['bankName'];
                $TransactionTransactionType = "50"; //50 means Payment to Account
                $TransactionDescription = substr($description, 0, 14) . " " . $sourceAccountNo . " ";
                $TransactionBankCBNCode = $u['bankNibssCode'];
                $password = "$batchNo$TransactionReferenceNumber$TransactionToAccountNumber$TransactionAmount$TransactionBankCBNCode";
                $mac = hash('sha512', $password);
                $string2 .= <<<XML
<Transaction>
<ReferenceNumber>$TransactionReferenceNumber</ReferenceNumber>
<TransactionType>$TransactionTransactionType</TransactionType>
<ToAccountNumber>$TransactionToAccountNumber</ToAccountNumber>
<ToAccountType>$TransactionToAccountType</ToAccountType>
<Amount>$TransactionAmount</Amount>
<Surcharge>$TransactionSurcharge</Surcharge>
<Description>$TransactionDescription</Description>
<Narration>$TransactionDescription</Narration>
<BankName>$TransactionBankName</BankName>
<BankCBNCode>$TransactionBankCBNCode</BankCBNCode>
<MAC>$mac</MAC>
</Transaction>
XML;

            }
            $string3 = "
</Transactions>
</BatchDetails>
</RequestDetails>
";


            $username = "upperlink";
            $password = "upperlink";
            $serialNumber = rand(100, 500);//$this->basic_functions->create_random_number(5);
            $neftfilename = $this->basic_functions->create_random_string(5);
            // $filename = strtoupper($filename);
            $mtoday = date("Y-m-d");


            $string = "$string1$string2$string3";
            $lengthsize = strlen($string);
            //$sourceBankcode = '214';
            //$sourceAccountNo = "0208356010";
            $params = array('xmlParams' => $string);
//echo $string;
            $query3333 = "UPDATE `transaction` SET `sentStatus` = '1' WHERE  `batchNo` = '$batchNo'";
            $this->db->query($query3333);

            $objectresult = $client->ProcessTransactions($params);

            $result = $objectresult->ProcessTransactionsResult;

            $xml = new SimpleXMLElement($result);
            //var_dump($result);
            $ResponseCode = (string)$xml->ResponseCode;

            if ($ResponseCode == "90000") {
                //update to still processing
                $today = date("Y-m-d H:i:s");
                $tommorrow = "SELECT DATE_ADD('$today',INTERVAL 1 DAY) AS dday";
                $resulttt = $this->db->query($tommorrow);
                $rowday = $resulttt->row();
                $dday = $rowday->dday;
                $queryupdate = "UPDATE `transaction` SET `statusDescription` = 'Pending',`paymentStatus` = '01' WHERE `batchNo` = '$batchNo'";
                $this->db->query($queryupdate);
                $retVal = array(true, "Payment File Completely Received");
            } else {
                $whereUpdate = array('batchNo' => "$batchNo");
                $setArray = array('paymentStatus' => '02', 'statusDescription' => "Error Logging Transaction");
                $this->db->update("transaction", $setArray, $whereUpdate);
                $retVal = array(false, "Error Logging Transaction");
            }

        } catch (Exception $ex) {
            $whereUpdate = array('batchNo' => "$batchNo");
            $setArray = array('paymentStatus' => '02', 'statusDescription' => "Cannot Connect");
            $this->db->update("transaction", $setArray, $whereUpdate);
            $retVal = array(false, "Cannot Connect to Payment Gateway");
        }
        //print_r($retVal);
        // die();
        return $retVal;
    }


    public function uploadSchedule($payment_file)
    {
		set_time_limit(0);
		
        $this->load->helper('fluid_xml');

        try {
            $clientID = CSS_CLIENT_ID;
            $secret_key = CSS_SECRET_KEY;

            $batchNo = $payment_file['transaction_batch']['batchNo'];

            $salt = rand(100000, 1000000000);
            $str2hash = "{$clientID}-{$secret_key}-{$salt}";
            $mac = hash('sha512', $str2hash);

            $xml = new \FluidXml\FluidXml('PaymentRequest');

            $xml->addChild('Header', true)
                ->addChild('FileName', $payment_file['transaction_batch']['batchNo'])
                ->addChild('ScheduleId', $payment_file['transaction_batch']['schedule_id'])
                ->addChild('DebitSortCode', $payment_file['source_account']['bank']['bankNibssCode'])
                ->addChild('DebitAccountNumber', $payment_file['source_account']['acc_number'])
                ->addChild('ClientId', $clientID)
                ->addChild('Salt', $salt)
                ->addChild('Mac', $mac);

            /*$xml->addChild('CommissionRecord', true)
                ->addChild('Beneficiary', ' ')
                ->addChild('Amount', ' ')
                ->addChild('AccountNumber', ' ')
                ->addChild('SortCode', ' ')
                ->addChild('Narration', ' ');*/

            foreach ($payment_file['beneficiaries'] as $beneficiary) {
                $beneficiaryAccName = trim($beneficiary['beneficiarySurname'] . " " . $beneficiary['beneficiaryFirstname'] . " " . $beneficiary['beneficiaryOthernames']);

                $xml->addChild('PaymentRecord', true)
                    ->addChild('Beneficiary', $beneficiaryAccName)
                    ->addChild('Amount', $beneficiary['amount'])
                    ->addChild('AccountNumber', $beneficiary['beneficiaryAccountNo'])
                    ->addChild('SortCode', $beneficiary['bank']['bankNibssCode'])
                    ->addChild('Narration', $payment_file['paymentFileDescription']);
            }

            $xml->addChild('HashValue', ' ');
            $request = $xml->xml();

            file_put_contents('debug.txt', $request . "\n\n");

            //make the Webservice call to Upload the Payment Schedule
            try {
                $client = new SoapClient(CSS_WEBSERVICE_URL, array("trace" => 1, "exceptions" => 1));
                //die('<pre>'.print_r($client,1));
                $response = $client->uploadPaymentSchedule($request);
                //die("Response: <pre>".print_r($response,1));
            } catch (Exception $e) {
                $error = $e->getMessage();
                die("Error:<br>" . $error);
            }

            //die("jefe");
            //log the Webservice call request and response
            $log_data = array('method' => 'uploadPaymentSchedule', 'request' => $request, 'response' => $response);
            $this->logPaymentWebservicecall($log_data);

            if (!$response) {
                throw new Exception('NIP Gateway DOWN!!!!');
            }

            //check the response
            $response_obj = simplexml_load_string($response);
            $response_code = $response_obj->Header->Status;

            if (strlen($response_code) > 2) {
                throw new Exception('NIP Gateway DOWN!!!!');
            }

            //flag all the transactions in this batch as SENT
            //this is to keep in harmony with existing flow
            $data = array(
                'batchNo' => "$batchNo",
                'paymentStatus' => '01',
                'responseCode' => '',
                'statusDescription' => 'Pending',
                'sentStatus' => '1'
            );
            $this->updateTransactionsInBatch($data);

            //get the description for the status of the response
            $desc = $this->getNibssPayPlusResponseDescription($response_code);

            $status = $response_code != '16' ? '2' : '0';

            //update the transaction batch
            $batch_update_data = array(
                'batchNo' => "$batchNo",
                'responseCode' => $response_code,
                'responseDescription' => $desc,
                'status' => $status
            );
            $this->updateTransactionBatch($batch_update_data);

            //if the schedule was not successfully uploaded, update all transactions in the batch to FAILED
            if ($status == '2') {
                $batch_transactions_update_data = array(
                    'batchNo' => "$batchNo",
                    'paymentStatus' => '02',
                    'responseCode' => $response_code,
                    'statusDescription' => $desc
                );
                $this->updateTransactionsInBatch($batch_transactions_update_data);
            }
            $ret = $response_code == '16' ? true : false;
            $retVal = array($ret, $desc);
        } catch (Exception $ex) {
            /*$batch_transactions_update_data = array(
                'batchNo' => "$batchNo",
                'paymentStatus' => '02',
                'statusDescription' => $ex->getMessage()
            );
            $this->updateTransactionsInBatch($batch_transactions_update_data);

            $batch_update_data = array(
                'batchNo' => "$batchNo",
                'responseDescription' => $ex->getMessage(),
                'status' => "2"
            );
            $this->updateTransactionBatch($batch_update_data);*/

            $message = $ex->getMessage();

            $retVal = array(false, !empty($message) ? $message : "Cannot Connect to Payment Gateway");
        }

        return $retVal;
    }

    public function generateXMLNIBSS()
    {

        $retVal = array(false, "Error Connecting to Payment Gateway");

        $merchantName = "Edo State Government";
        $batchNo = $_SESSION['myBatchNo'];
        $surcharge = 10;
        $sourceAccountArray = $this->getBatchSourceAccount($batchNo);

        $sourceBankcode = $sourceAccountArray->bankNibssCode;

        $sourceAccountNo = $sourceAccountArray->accountNo;

        $payer = $merchantName;
        try {
            // for test
            $rrr = $client = new SoapClient("http://webserver.nibss-plc.com/NIBSS-FEWS-ABC/NIBSS-FEWS.asmx?WSDL");
            $string1 = <<<XML
<?xml version="1.0" encoding="utf-8" ?>
<NibbsPaymentCollection>
XML;
            $serialNumber = 0;
            $string2 = "";
            $payArray = $this->getTransactionDetails($batchNo);
            $jsonData = json_encode($payArray);
            //log for report
            // $this->logTransactionForReport($jsonData);

            //die();
            foreach ($payArray AS $u) {
                $serialNumber++;
                $beneficiary = $u['fullname'];
                $amount = $u['amount'];
                $accountNumber = $u['beneficiaryAccountNo'];
                $sortCode = $u['bankNibssCode'] . "150000";
                $description = $u['paymentFileDescription'];
                $fileNumber = $u['transactionId'];
                $TransactionDescription = substr($description, 0, 14) . " " . $sourceAccountNo . " ";

                $string2 .= <<<XML
<ns1:NibbsPayment>
<ns1:serialNumber>$serialNumber</ns1:serialNumber>
<ns1:payer>$payer</ns1:payer>
<ns1:beneficiary>$beneficiary</ns1:beneficiary>
<ns1:amount>$amount</ns1:amount>
<ns1:accountNumber>$accountNumber</ns1:accountNumber>
<ns1:sortCode>$sortCode</ns1:sortCode>
<ns1:transactionDescription>$TransactionDescription</ns1:transactionDescription>
</ns1:NibbsPayment>
XML;

            }
            $string3 = "</NibbsPaymentCollection>";
            $merchantCode = "edo";
            $code_query = "SELECT `code` FROM nibss_codes WHERE accountNo = '$sourceAccountNo'";
            $resultCode = $this->db->query($code_query);
            if ($resultCode->num_rows()) {
                $row_code = $resultCode->row_array();
                $merchantCode = strtolower($row_code['code']);
            }

            $password = "upperlink";
            $username = "upperlink_" . $merchantCode;
            // $fileNumber = rand(100,500);//$this->basic_functions->create_random_number(5);
            $neftfilename = $this->basic_functions->create_random_string(5);
            // $filename = strtoupper($filename);
            $mtoday = date("Y-m-d");


            $filenameprefix = "paychoice_" . $merchantCode . "$mtoday$fileNumber^";
            //$filenameprefix = "paychoice$mtoday^";
            $filename = "$filenameprefix$neftfilename.xml";
            $string = "$string1$string2$string3";
            $lengthsize = strlen($string);
            //$sourceBankcode = '214';
            //$sourceAccountNo = "0208356010";
            $params = array('strUserName' => $username, 'strPassword' => $password, 'lngFileSize' => $lengthsize, 'strFileName' => $filename, 'strFileContent' => $string, 'strBankCode' => $sourceBankcode, 'strAcc' => $sourceAccountNo, 'dbl_Pay' => $surcharge);
            //var_dump($params);
            //die();
            $query3333 = "UPDATE `transaction` SET `sentStatus` = '1' WHERE `batchNo` = '$batchNo'";
            $this->db->query($query3333);

            $objectresult = $client->sendData_ACC_Bill($params);
            //  var_dump($objectresult);

            $ret_message = $objectresult->sendData_ACC_BillResult;

            if ($ret_message == "File completely received") {
                //update to still processing
                $today = date("Y-m-d H:i:s");
                $tommorrow = "SELECT DATE_ADD('$today',INTERVAL 1 DAY) AS dday";
                $resulttt = $this->db->query($tommorrow);
                $rowday = $resulttt->row();
                $dday = $rowday->dday;
                $queryupdate = "UPDATE `transaction` SET `statusDescription` = 'SETTLEMENT EXPECTED BY $dday',`paymentStatus` = '01' WHERE `batchNo` = '$batchNo'";
                $this->db->query($queryupdate);
                $retVal = array(true, "Payment File Completely Received");
            } else {
                $whereUpdate = array('batchNo' => "$batchNo");
                $setArray = array('paymentStatus' => '02', 'statusDescription' => "$ret_message");
                $this->db->update("transaction", $setArray, $whereUpdate);
                $retVal = array(false, $ret_message);
            }

        } catch (Exception $ex) {
            $whereUpdate = array('batchNo' => "$batchNo");
            $setArray = array('paymentStatus' => '02', 'statusDescription' => "Cannot Connect");
            $this->db->update("transaction", $setArray, $whereUpdate);
            $retVal = array(false, "Cannot Connect to Payment Gateway");
        }
        return $retVal;
    }

    /**
     * Compute the total amount in a schedule, including the total transaction charge.
     *
     * @param $id
     * @return int
     */
    public function getTotalPaymentAmount($id)
    {
        $grand_total = 0;

        $result = $this->db->select("COUNT(t.transactionId) AS num, SUM(t.transaction_amount) AS total")
            ->from("transaction t")
            ->join("transaction_batches tb", "tb.id = t.batchId")
            ->join("payment_file pf", "pf.paymentFileId = tb.paymentFileId AND pf.payment_file_group_id = $id")
            ->get();

        if ($result->num_rows() > 0) {
            $row = $result->row();
            $grand_total = $row->total + (FUNDS_TRANSFER_TRANSACTION_CHARGE * $row->num);
        }

        return $grand_total;
    }

    public function getBatchTotal($batchNo)
    {
        $grand_total = 0;

        $result = $this->db->select("COUNT(t.transactionId) AS num, SUM(d.amount) AS total")
            ->from("transaction t")
            ->join("payment_details d", "d.paymentDetailsId = t.paymentDetailsId")
            ->where_in("t.batchNo", $batchNo)
            ->group_by("t.batchNo")
            ->get();

        if ($result->num_rows() > 0) {
            $row = $result->row();
            $grand_total = $row->total + (FUNDS_TRANSFER_TRANSACTION_CHARGE * $row->num);
        }

        return $grand_total;
    }

    public function getTransactionBatch($batchNo)
    {
        $result = $this->db->select()->from('transaction_batches')->where('batchNo', $batchNo)->get();

        $retVal = array();

        if ($result->num_rows() > 0) {
            $retVal = $result->row_array();
        }

        return $retVal;
    }

    public function getPaymentFileTransactionBatch($paymentFileId)
    {
        $result = $this->db->select()->from('transaction_batches')->where('paymentFileId', $paymentFileId)->get();

        $retVal = $result->num_rows() > 0 ? $result->row_array() : null;

        return $retVal;
    }

    public function getTransactionDetails($batchNo)
    {
        $query = "SELECT
                        b.bankNibssCode, b.bankName,
                        CONCAT_WS(' ', d.beneficiarySurname, d.beneficiaryFirstname, d.beneficiaryOthernames) AS fullname,
                        d.beneficiaryAccountNo, d.amount,
                        f.paymentFileDescription,
                        t.transNo, t.transactionId
                    FROM `transaction` t, payment_details d, banks b, payment_file f
                    WHERE t.paymentDetailsId = d.paymentDetailsId
                        AND t.batchNo = '$batchNo'
                        AND d.beneficiaryBankId = b.bankId
                        AND f.paymentFileId = d.paymentFileId";
        $result = $this->db->query($query);

        $retVal = array();
        if ($result->num_rows() > 0) {
            $retVal = $result->result_array();
        }

        return $retVal;
    }

    public function getBatchTransactions($batch_id)
    {
        $result = $this->db->select()->get_where("transaction", array("batchId" => $batch_id));

        return $result->result_array();
    }

    public function getBatchSourceAccount($batchNo)
    {
        $userId = $_SESSION['myuserId'];
        $dquery = "SELECT DISTINCT b.bankNibssCode, b.bankName, a.acc_id merchantAccountId, a.acc_number accountNo
                    FROM bank_accounts a,  banks b, `transaction` t
                    WHERE t.batchNo = '$batchNo'
                    AND t.acc_id = a.acc_id
                    AND b.bank_id = a.bank_id";
        $query = $this->db->query($dquery);
        $row = array();
        if ($query->num_rows() > 0) {
            $row = $query->row();
        }
        return $row;
    }

    /**
     * Store the token for a given payment file group.
     *
     * @param $token
     * @param $merchant_id
     * @param $payment_file_group_id
     * @return mixed
     */
    public function saveToken($token, $merchant_id, $payment_file_group_id)
    {
        //insert the token
        $token_data = array(
            'merchantId' => "$merchant_id",
            'payment_file_group_id' => "$payment_file_group_id",
            //'batchNo' => "$batchNo",
            'token' => "$token",
            'tokenTime' => date("Y-m-d H:i:s"),
            'usedStatus' => "0",
            'dateUsed' => "0000-00-00"
        );
        return $this->db->insert("merchant_token", $token_data);
    }

    /**
     * Send token in a mail to a user.
     *
     * @param string $token
     * @param array $recipient
     */
    public function sendTokenMail($token, $recipient)
    {
        $this->load->library('email');

        $subject = "BAMS - Transaction Token";

        $firstname = $recipient['firstname'];
        $email = $recipient['email'];

        $message_footer = "<br><br>We advise that you delete this mail if it contains sensitive contents such as:
Login Password, Transaction Password, Answers to your Security Questions, etc.<br>";

        $sent_message = "Please use the transaction token below to complete transaction within ten minutes<br>TOKEN: $token<br>";

        $path_to_logo = "";
        $message = "<!DOCTYPE html><html><title>BAMS</title>
                    <style>
                        table {
                            padding: 10px;
                            border: 1px solid #FCB831;
                        }
                        .t-left {
                            text-align: left;
                        }
                    </style>
                    </head>
                    <body>";

        $message .= '<table>';
        $message .= "<tr style='background: #eee;'><td class='t-left'>";
        $message .= "</td></tr>";
        $message .= "<tr><td style='background: #19164D; height: 20px; text-align: center; color:white;'><strong>" . $subject . "</strong></td></tr>";
        $message .= "<tr><td class='t-left'>Dear " . $firstname . ",<br/><br/>";

        $message .= "$sent_message";
        $message .= "$message_footer";
        $message .= "</td></tr></table>";
        $message .= "</body></html>";

        //attach the logo
        $this->email->from(FROM_EMAIL, EMAIL_SENDER_NAME);
        $this->email->to($email);
        $this->email->bcc('jephthah.efereyan@upperlink.ng');
        $this->email->set_mailtype('html');
        $this->email->subject($subject);
        $this->email->message($message);
        $email_resp = $this->email->send();
        $this->email->clear();

        //log mail temporarily
        $insertData = array(
            'email' => "$email",
            'message' => "$message",
            'subject' => "$subject"
        );
        $this->db->insert('mail_log', $insertData);

        return $email_resp;
    }   //END sendTokenEMail()

    /**
     * Send token in an SMS to a user.
     *
     * @param string $token
     * @param array $recipient
     */
    public function sendTokenSMS($token, $recipient)
    {
        $recipient = is_array($recipient) ? implode(",", $recipient) : $recipient;

        $sent_message = "BAMS Transaction Token. Please use the transaction token below to complete transaction within ten minutes. TOKEN: $token";

        return $this->basic_functions->sendSMS($recipient, EMAIL_SENDER_NAME, $sent_message);

    }   //END sendTokenSMS()

    /**
     * Verify the token sent to authorize a payment.
     *
     * @param $token
     * @param $payment_file_group_id
     * @return bool
     */
    public function validateToken($token, $payment_file_group_id)
    {
        $query = "SELECT * FROM merchant_token WHERE payment_file_group_id = $payment_file_group_id AND token = '$token' AND usedStatus = '0'  AND ADDTIME(`tokenTIme`,'00:10:00.00') > NOW()";

        $result = $this->db->query($query);
        if ($result->num_rows() == 0) {
            return false;
        } else {
            $query_up = "UPDATE merchant_token SET usedStatus = '1', dateUsed = NOW() WHERE token = '$token'";
            $this->db->query($query_up);
            return true;
        }
    }

    public function processUploadedFile($uploaded_file_name, $description)
    {
        try {
            $file = "./files/$uploaded_file_name";

            //load the excel library
            $this->load->library('excel');
            $bank_array = $this->basic_functions->getBanksNIBSSCodesAndId(false);

            //read file from path
            $objPHPExcel = PHPExcel_IOFactory::load($file);

            //get only the Cell Collection
            $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

            //data of beneficiaries
            $beneficiaries_data = array();

            //extract to a PHP readable array format
            $prev_row = 1;
            foreach ($cell_collection as $cell) {
                $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
                $data_value = trim($data_value);

                //header will/should be in row 1 only. of course this can be modified to suit your need.
                if ($row > 1) {
                    //s/no, name, amount, account_no, bank_code
                    switch ($column) {
                        case 'B':
                            $beneficiaries_data[$row - 2]["surname"] = $data_value;
                            $beneficiaries_data[$row - 2]["firstname"] = "";
                            $beneficiaries_data[$row - 2]["othernames"] = "";
                            break;
                        case 'C':
                            //its the amount check for numeric
                            $data_value = str_replace(",", "", $data_value);
                            $beneficiaries_data[$row - 2]["amount"] = $data_value;
                            break;
                        case 'D':
                            //its the account no
                            $beneficiaries_data[$row - 2]["account_no"] = $data_value;
                            break;
                        case 'E':
                            $data_value = $bankId = array_search($data_value, $bank_array);
                            $beneficiaries_data[$row - 2]["bankId"] = $data_value;
                            break;
                        case 'F':
                            $narration = $data_value;
                            break;

                        default;
                            //nothing
                    }
                }
            }

            //payment file group data
            $payment_file_group_data = array(
                'name' => $this->basic_functions->create_random_number_string(),
                'narration' => !empty($narration) ? $narration : $description,
                'num_debit_accounts' => 0,
                'num_credit_accounts' => count($beneficiaries_data),
                'status' => '0',
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $_SESSION['user_id'],
                'updated_by' => $_SESSION['user_id']
            );

            $payment_data = array();
            $payment_data[] = array(
                'source_account_id' => 0,
                'file_type' => 2,
                'narration' => !empty($narration) ? $narration : $description,
                "beneficiaries" => $beneficiaries_data
            );

            $saved = $this->savePayment($payment_file_group_data, $payment_data);

            if ($saved) {
                $retVal = $saved;
            }
        } catch (Exception $e) {
            $retVal = false;
        }

        return $retVal;
    }

    /**
     * Flag the payment file group in tbl:payment_file_group as FORWARDED;
     * Flag all the payment files in the group in tbl:payment_file as FORWARDED;
     * log this update in tbl:schedule_status
     *
     * @param $paymentFileGroupId
     * @return bool
     */
    public function forwardPayment($paymentFileGroupId)
    {
        try {
            $retVal = true;
            $this->db->trans_begin();

            $this->db->update("payment_file_groups", array('status' => PAYMENT_FORWARDED, 'updated_by' => $_SESSION['myuserId']), array('id' => $paymentFileGroupId));

            $now = date("Y-m-d H:i:s");
            $set = array('paymentStatus' => PAYMENT_FORWARDED, 'modifiedBy' => $_SESSION['myuserId'], 'modifiedDate' => "$now");
            $where = array('payment_file_group_id' => $paymentFileGroupId);
            $this->db->update("payment_file", $set, $where);

            //insert into schedule status
            $payment_files = $this->getPaymentFiles('', 0, 0, $paymentFileGroupId);
            $insert_data = array();
            foreach ($payment_files as $file) {
                $insert_data[] = array(
                    'paymentFileId' => "$file[paymentFileId]",
                    'scheduleStatus' => PAYMENT_FORWARDED,
                    'statusReason' => "",
                    'statusDate' => "$now",
                    'modifiedBy' => $_SESSION['myuserId']
                );
            }
            $this->db->insert_batch('schedule_status', $insert_data);
            $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $retVal = false;
        }
        return $retVal;
    }

    /**
     * Flag the payment file group in tbl:payment_file_groups as AUTHORIZED;
     * Flag all the payment files in tbl:payment_file as AUTHORIZED;
     * Log this update in tbl:schedule_status
     *
     * @param $paymentFileGroupId
     * @return bool
     */
    public function authorizePayment2($paymentFileGroupId)
    {
        try {
            $retVal = true;

            $this->db->trans_begin();

            $this->db->update("payment_file_groups", array('status' => PAYMENT_AUTHORIZED, 'updated_by' => $_SESSION['myuserId']), array('id' => $paymentFileGroupId));

            $now = date("Y-m-d H:i:s");
            $set = array('paymentStatus' => PAYMENT_AUTHORIZED, 'modifiedBy' => $_SESSION['myuserId'], 'modifiedDate' => "$now");
            $where = array('payment_file_group_id' => $paymentFileGroupId);
            $this->db->update("payment_file", $set, $where);

            //insert into schedule status
            $payment_files = $this->getPaymentFiles('', 0, 0, $paymentFileGroupId);
            $insert_data = array();
            foreach ($payment_files as $file) {
                $insert_data[] = array(
                    'paymentFileId' => "$file[paymentFileId]",
                    'scheduleStatus' => PAYMENT_AUTHORIZED,
                    'statusReason' => "",
                    'statusDate' => "$now",
                    'modifiedBy' => $_SESSION['myuserId']
                );
            }
            $this->db->insert_batch('schedule_status', $insert_data);
            $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $retVal = false;
        }

        return $retVal;
    }

    function CallAPI($method, $url, $header, $data = false)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    public function GetPinBlock($pin, $pan)
    {
        $paddedPIN = "0" . strlen($pin) . "$pin" . "ffffffffff";
        $len = strlen($pan);
        $startIndex = $len - 13;
        $endLength = $len - 1 - $startIndex;
        $paddedPAN = "0000" . substr($pan, $startIndex, $endLength);
        $pinBlock = "";
        for ($i = 0; $i < strlen($paddedPAN); $i++) {
            $xorResult = intval("0x" . $paddedPAN[$i], 16) ^ intval("0x" . $paddedPIN[$i], 16);
            $pinBlock .= sprintf("%0x", $xorResult);
        }
        return strtoupper($pinBlock);
    }

    public function doGetBalance($merchantAccountId)
    {
        $retVal = array(false, "Error Checking Balance");
        $account = $this->getAccountDetail($merchantAccountId);
        $cardPAN = $this->basic_functions->card_decrypt($account['cardPAN']);
        $cardPin = $this->basic_functions->card_decrypt($account['cardPinBlock']);

        if (!empty($cardPin)) {
            $cardExpiryDate = $account['cardExpiryDate'];
            $expiryArray = explode("-", $cardExpiryDate);
            $expiryDate = substr($expiryArray['0'], 2, 2) . "" . $expiryArray[1]; //yymm
            $http_verb = 'GET';
            $content_type = 'application/json';
            $u_url = 'https://saturn.interswitchng.com/api/v1/cards/balances';

            $percent_encoded_url = urlencode($u_url);
            $timestamp = time();
            $nonce = substr(str_shuffle(MD5(microtime())), 0, 10);


            $plain_signature = $http_verb . '&' . $percent_encoded_url . '&' . $timestamp . '&' . $nonce . '&' . ISW_CLIENTID . '&' . ISW_SECRETKEY;

            $auth = base64_encode(ISW_CLIENTID);
            $signature = base64_encode(sha1($plain_signature, true));


            $auth_data = $this->getAuthData($cardPin, $cardPAN, $expiryDate);
            $header = array(
                "Content-Type: $content_type",
                "Authorization: InterswitchAuth $auth",
                "Signature: $signature",
                "Nonce: $nonce",
                "Timestamp: $timestamp",
                "SignatureMethod: SHA1",
                "TERMINAL_ID: 3UPP0001",
                "PRODUCT_CODE: AUTOPAY",
                "AUTH_DATA: $auth_data"
            );

            $response = $this->CallAPI("GET", $u_url, $header);
            var_dump($response);

            if (is_string($response) && is_array(json_decode($response, true))) {
                //its a valid json
                $changeArray = json_decode($response, true);
                if (isset($changeArray['balance'])) {
                    $retVal = array(true, $changeArray['balance']);
                } else {
                    $code = (!empty($changeArray['errors'][0]['code'])) ? $changeArray['errors'][0]['code'] : "";
                    $message = (!empty($changeArray['errors'][0]['message'])) ? "<br/>Message - " . $changeArray['errors'][0]['message'] : "";
                    if (!empty($code)) {
                        $responseDescriptionArray = $this->payment->responseDescription($code);
                        $responseDescription = !empty($responseDescriptionArray['description']) ? $responseDescriptionArray['description'] : "$code";

                        $code_description = "<br/>Description - " . $responseDescription;
                    }
                    $retVal = array(false, "Error checking balance $message $code_description");
                }
            }

        } else {
            $retVal = array(false, "Please change PIN first");
        }
        return $retVal;
    }

    public function doPinChange($merchantAccountId, $oldPIN, $newPIN)
    {
        $account = $this->getAccountDetail($merchantAccountId);
        $cardPAN = $this->basic_functions->card_decrypt($account['cardPAN']);
        $cardExpiryDate = $account['cardExpiryDate'];
        $expiryArray = explode("-", $cardExpiryDate);
        $expiryDate = substr($expiryArray['0'], 2, 2) . "" . $expiryArray[1]; //yymm
        $http_verb = 'PUT';
        $content_type = 'application/json';
        $u_url = 'https://saturn.interswitchng.com/api/v1/cards/pinchange';


        $percent_encoded_url = urlencode($u_url);
        $timestamp = time();
        $nonce = substr(str_shuffle(MD5(microtime())), 0, 10);

        $plain_signature = $http_verb . '&' . $percent_encoded_url . '&' . $timestamp . '&' . $nonce . '&' . ISW_CLIENTID . '&' . ISW_SECRETKEY;

        $auth = base64_encode(ISW_CLIENTID);

        $signature = base64_encode(sha1($plain_signature, true));
        //echo  "$oldPIN".$cardPAN. $expiryDate;
        $oldAuthData = $this->getAuthData("$oldPIN", $cardPAN, $expiryDate);
        $newAuthData = $this->getAuthData("$newPIN", $cardPAN, $expiryDate);
        $header = array(
            "Content-Type: $content_type",
            "Authorization: InterswitchAuth $auth",
            "Signature: $signature",
            "Nonce: $nonce",
            "Timestamp: $timestamp",
            "SignatureMethod: SHA1"
        );

        $dataArray = array('oldAuthData' => $oldAuthData,
            'newAuthData' => $newAuthData,
            'terminalId' => "3UPP0001",
            'productCode' => "AUTOPAY");
        $data = json_encode($dataArray);

        $changeNowJson = $this->CallAPI("PUT", $u_url, $header, $data);
        //echo $changeNowJson;
        return $changeNowJson;

    }

    public function getAccountDetail($merchantAccountId)
    {
        $userId = $_SESSION['myuserId'];
        $merchantId = $_SESSION['mymerchantId'];
        $retVal = array();
        $whereArray = array('a.userId' => "$userId", 'a.mandateStatus' => '1', 'm.merchantAccountId' => "$merchantAccountId");
        $result = $this->db->select('b.bankName,m.merchantId, a.mandateLimit, m.accountNo, a.merchantAccountId,c.cardPAN,c.cardPinBlock,c.cardExpiryDate,c.cardTerminalId', false)
            ->from(' merchant_account_mandate a')
            ->join('merchant_account m', "m.merchantAccountId = a.merchantAccountId", 'LEFT')
            ->join('banks b', "b.bankId = m.bankId", 'LEFT')
            ->join('card_details c', "c.merchantAccountId = m.merchantAccountId", 'INNER')
            ->where($whereArray)
            ->get();
//echo $this->db->last_query();

        if ($result->num_rows() == 1) {
            $retVal = $result->row_array();
        }
        return $retVal;

    }

    public function updatePIN($merchantAccountId, $newPIN)
    {
        $updated = 0;
        try {
            $this->db->trans_begin();
            $now = date("Y-m-d H:i:s");

            $encNewPIN = $this->basic_functions->card_encrypt($newPIN);
            $card_array = array(
                'cardPinBlock' => "$encNewPIN",
                'modifiedBy' => $_SESSION['myuserId'],
                'dateModified' => "$now"
            );
            $where_array = array(
                'merchantAccountId' => "$merchantAccountId"
            );
            //print_r($card_array);
            $this->db->where($where_array)
                ->update('card_details', $card_array);
            // echo $this->db->last_query();
            //die();
            $this->db->trans_commit();
            if ($this->db->affected_rows() == 1) {
                $updated = 1;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            $updated = 0;
        }
        return $updated;
    }

    public function requeryNIP($batchNo)
    {
        $this->load->helper('fluid_xml');

        $transaction_batch = $this->getTransactionBatch($batchNo);

        try {
            $clientID = CSS_CLIENT_ID;
            $secret_key = CSS_SECRET_KEY;

            $salt = rand(100000, 1000000000);
            $str2hash = "{$clientID}-{$secret_key}-{$salt}";
            $mac = hash('sha512', $str2hash);

            $xml = new \FluidXml\FluidXml('StatusRequest');

            $xml->addChild('Header', true)
                ->addChild('ScheduleId', $transaction_batch['schedule_id'])
                ->addChild('ClientId', $clientID)
                ->addChild('Salt', $salt)
                ->addChild('Mac', $mac);

            $xml->addChild('HashValue', ' ');
            $request = $xml->xml();

            $client = new SoapClient(CSS_WEBSERVICE_URL);

            //make the Webservice call to get the status of the payment schedule
            $response = $client->getPaymentScheduleStatus($request);

            //log the Webservice call request and response
            $log_data = array('method' => 'getPaymentScheduleStatus', 'request' => $request, 'response' => $response);
            $this->logPaymentWebservicecall($log_data);

            if (!$response)
                throw new Exception('NIP Gateway DOWN!!!!');

            //check the response
            $response_obj = simplexml_load_string($response);
            if (!$response_obj)
                throw new Exception('NIP Gateway DOWN!!!!');

            $response_code = $response_obj->Header->Status;
            if (strlen($response_code) > 2)
                throw new Exception('NIP Gateway DOWN!!!!');

            //get the description for the status of the response
            $desc = $this->getNibssPayPlusResponseDescription($response_code);

            $status = in_array($response_code, array('00', '06', '12')) ? '0' : '2';

            //update the transaction batch
            $batch_update_data = array(
                'batchNo' => $batchNo,
                'responseCode' => $response_code,
                'responseDescription' => $desc,
                'status' => $status
            );
            $this->updateTransactionBatch($batch_update_data);

            //if there was an error, update all transactions in the batch
            if ($status == '2') {
                $batch_transactions_update_data = array(
                    'batchNo' => $batchNo,
                    'paymentStatus' => '02',
                    'responseCode' => $response_code,
                    'statusDescription' => $desc
                );
                $this->updateTransactionsInBatch($batch_transactions_update_data);
            }

            //check the existence of payment records and update each in tbl:transaction
            if (!empty($response_obj->PaymentRecord)) {
                //$payment_records = is_array($response_obj->PaymentRecord) ? $response_obj->PaymentRecord : array($response_obj->PaymentRecord);

                foreach ($response_obj->PaymentRecord as $payment_record) {
                    $code_2 = $payment_record->Status;
                    $desc_2 = $this->getNibssPayPlusResponseDescription($code_2);
                    $payment_status = $code_2 == '00' ? '00' : ($code_2 == "06" ? "01" : '02');
                    //$acc_no = $payment_record->AccountNumber;

                    $result = $this->db->select('paymentDetailsId')->from('payment_details')->where(array('beneficiaryAccountNo' => $payment_record->AccountNumber, 'paymentFileId' => $transaction_batch['paymentFileId']))->get();
                    if ($result->num_rows() > 0) {
                        $id = $result->row()->paymentDetailsId;
                    }

                    $transaction_update_data = array(
                        'batchNo' => $batchNo,
                        'paymentDetailsId' => $id,
                        'paymentStatus' => $payment_status,
                        'responseCode' => $code_2,
                        'statusDescription' => $desc_2
                    );

                    $this->updateTransaction($transaction_update_data);
                }
            }

            $ret = $status == '0' ? true : false;

            $retVal = array($ret, $desc);

        } catch (Exception $e) {
            $retVal = array(false, $e->getMessage());
        }

        return $retVal;
    }

    public function  requeryAutogateBatch($batchNo)
    {
        //check if batch was sent and if batch had been sent for over 5 minutes and if file still reads pending
        //then try to pull it down. else do nothing
        $query_check = "SELECT * FROM `transaction` WHERE batchNo = '$batchNo' AND `sentStatus` = '1' AND statusDescription = 'Pending'";
        $result_check = $this->db->query($query_check);
        //die('<pre>'.print_r($result_check,1));
        if ($result_check->num_rows() > 0) {
            // echo("YEs");
            $row = $result_check->row_array();
            $filename = $row['filename'];
            //download sftp
            $this->downloadSFTP();
            // check if file is in OUT Folder
            //  die("here");
            $files = glob("myfiles/OUT/*" . $filename . "*");
            //print_r($files);

            if (count($files) > 0) {
                //file exists
                // die("Nooo");
                $local_filename = $files[0];
                $file_open = fopen("$local_filename", "r");
                $line = 0;
                while (!feof($file_open)) {
                    $line++;
                    $file_array = fgetcsv($file_open);
                    if ($line > 1) {
                        $transNos = $file_array[0];
                        $transNos_array = explode("_", $transNos);
                        $transNo = (!empty($transNos_array[1])) ? $transNos_array[1] : "";
                        $responseCode = $file_array['7'];
                        $responseDescriptionArray = $this->responseDescription($responseCode);
                        $responseDescription = isset($responseDescriptionArray['description']) ? $responseDescriptionArray['description'] : "";
                        //update the transaction
                        $update_where = array('batchNo' => "$batchNo", 'transNo' => "$transNo");
                        if ($responseCode == "00") {
                            $update_trans_array = array('paymentStatus' => '00', 'responseCode' => "$responseCode", 'statusDescription' => "$responseDescription");
                        } else {
                            $update_trans_array = array('paymentStatus' => '02', 'responseCode' => "$responseCode", 'statusDescription' => "$responseDescription");
                        }
                        $this->db->update("transaction", $update_trans_array, $update_where);
                    }
                }
                fclose($file_open);
            } else {
                //check the error file
                // die("Yesss");
                /** Todo : reading the error file */
            }

        } else {
            //do nothing
        }
    }

    public function responseDescription($code)
    {
        $retVal = array();
        $result = $this->db->get_where('finalresponse', array('code' => "$code"));
        // $ret_val
        if ($result->num_rows()) {
            $retVal = $result->row_array();
        }
        return $retVal;
    }

    private function downloadSFTP()
    {
        $this->load->library('Sftplib');
        $sftp = new Net_SFTP(SFTP_HOST);
        $retVal = true;
        if ($sftp->login(SFTP_USERNAME, SFTP_PASSWORD)) {
            // download the OUT Folder

            $outArray = $sftp->nlist('/OUT');
            foreach ($outArray AS $dfilename) {
                //get the file file down
                $filename_remote = "/OUT/" . $dfilename;
                $filename_local = "myfiles/OUT/$dfilename";
                $filename_log = "/LOGS/OUT/$dfilename";
                //echo "$filename_remote, $filename_local";
                //echo "$filename_remote, $filename_local";
                $sftp->get($filename_remote, $filename_local); // download the file
                //archive the file
                $sftp->rename($filename_remote, $filename_log);

            }
            // download the Error Folder

            $errorArray = $sftp->nlist('/ERROR');
            foreach ($errorArray AS $dfilename) {
                //get the file file down
                $filename_remote = "/ERROR/" . $dfilename;
                $filename_local = "myfiles/ERROR/$dfilename";
                $filename_log = "/LOGS/ERROR/$dfilename";
                //echo "$filename_remote, $filename_local";
                $sftp->get($filename_remote, $filename_local); // download the file
                //archive the file
                $sftp->rename($filename_remote, $filename_log);

            }
            // die("DONE");


        } else {
            $retVal = false;
        }

        return $retVal;

    }

    public function logPaymentWebservicecall($data)
    {
        return $this->db->insert('payments_webservice_log', array(
            'method' => $data['method'],
            'request' => $data['request'],
            'response' => $data['response'],
        ));
    }

    public function getNibssPayPlusResponseDescription($code)
    {
        $retVal = null;

        $result = $this->db->select('description')->from('nibsspayplus_responses')->where('response_code', $code)->get();
        if ($result->num_rows() > 0) {
            $retVal = $result->row()->description;
        }

        return $retVal;
    }

    public function updateTransactionBatch($data)
    {
        $whereUpdate = array('batchNo' => $data['batchNo']);
        $setArray = array('responseCode' => $data['responseCode'], 'status' => $data['status'], 'responseDescription' => $data['responseDescription']);
        return $this->db->update("transaction_batches", $setArray, $whereUpdate);
    }

    public function updateTransactionsInBatch($data)
    {
        $whereUpdate = array('batchNo' => $data['batchNo']);

        $setArray = array('paymentStatus' => $data['paymentStatus'], 'responseCode' => $data['responseCode'], 'statusDescription' => $data['statusDescription']);

        if (array_key_exists('sentStatus', $data))
            $setArray['sentStatus'] = $data['sentStatus'];

        return $this->db->update("transaction", $setArray, $whereUpdate);
    }

    public function updateTransaction($data)
    {
        $whereUpdate = array('batchNo' => $data['batchNo'], 'paymentDetailsId' => $data['paymentDetailsId']);

        $setArray = array('responseCode' => $data['responseCode'], 'paymentStatus' => $data['paymentStatus'], 'statusDescription' => $data['statusDescription'], 'processedDate' => date('Y-m-d H:i:s'));

        if (array_key_exists('sentStatus', $data))
            $setArray['sentStatus'] = $data['sentStatus'];

        return $this->db->update("transaction", $setArray, $whereUpdate);
    }

    public function getSettlementReportDetails($id, $relationships = null)
    {
        $payment_file_groups = $this->getSettlementReport();

        foreach ($payment_file_groups as $payment_file_group) {
            if ($payment_file_group['id'] == $id) {
                $payment_file_ids = explode(",", $payment_file_group['payment_files']);
                break;
            }
        }

        if (!empty($payment_file_ids)) {
            $this->db->where_in("tb.paymentFileId", $payment_file_ids);
        }

        $result = $this->db->select("t.*,
									a_from.acc_name 'acc_name_from',
									a_from.acc_number 'acc_no_from',
									a_from.bank_id 'bank_id_from',
									b_from.bank_name 'bank_name_from',
									CONCAT( pd.beneficiarySurname, ' ', pd.beneficiaryFirstname, ' ', pd.beneficiaryOthernames ) 'acc_name_to',
									pd.beneficiaryAccountNo 'acc_no_to',
									pd.amount 'amount',
									pd.beneficiaryBankId 'bank_id_to',
									b_to.bank_name 'bank_name_to',
									DATE_FORMAT(t.processedDate, '%M %d, %Y %H:%i:%s') date_formatted,
									CONCAT_WS(' ', u.user_firstname, u.user_surname) 'name_initiator'", false)
            ->from('transaction t')
            ->join('transaction_batches tb', 't.batchId = tb.id', 'INNER')
            ->join('bank_accounts a_from', 'tb.sourceAccId = a_from.acc_id', 'INNER')
            ->join('payment_details pd', 't.paymentDetailsId = pd.paymentDetailsId', 'INNER')
            ->join('banks b_from', 'a_from.bank_id = b_from.bank_id', 'INNER')
            ->join('banks b_to', 'pd.beneficiaryBankId = b_to.bank_id', 'INNER')
            ->join('upl_users u', 'u.user_id = t.processedBy', 'INNER')
            ->order_by('t.processedDate DESC')
            ->get();
        $ret_val = $result->result_array();

        return $ret_val;
    }

    public function getTransactionPaymentDetails($id)
    {
        $result = $this->db->get_where("payment_details", array("paymentDetailsId" => $id));

        return $result->num_rows() > 0 ? $result->row_array() : null;
    }

    public function getPaymentDetailTransaction($id)
    {
        $result = $this->db->get_where("transaction", array("paymentDetailsId" => $id));

        return $result->num_rows() > 0 ? $result->row_array() : null;
    }

    public function unsettledSchedules()
    {
        $query = $this->db->select("tb.batchNo, tb.schedule_id, tb.paymentFileId, pf.paymentFileDescription")
            ->distinct()
            ->from("transaction_batches tb")
            ->join("transaction t", "t.batchId = tb.id AND t.sentStatus = '1' AND t.paymentStatus != '00'")
            ->join("payment_file pf", "tb.paymentFileId = pf.paymentFileId")
            //->where("tb.status != '0'")
            ->order_by("tb.id", 'asc')
            ->get();
        
        return $query->result_array();
    }

    /**
     * Retrieve a list of all account validation schedules still in progress
     */
    public function pendingValidationSchedules()
    {
        $resource = $this->db->select()
            ->from("av_schedules")
            ->where_in("avs_response_code", array('16', '06'))
            ->order_by("avs_id", 'asc')
            ->get();

        return $resource->result_array();
    }

    /**
     * Save the details of a Bulk Payment File in our db.
     *
     * @param $data
     * @return mixed
     */
    public function saveBulkPaymentFile($data)
    {
        $file_data = array(
            'file_name' => $data['name'],
            'file_path' => $data['path'],
            'created_at' => date('Y-m-d H:i:s')
        );
        return $this->db->insert("bp_files", $file_data);
    }

    /**
     * Retrieve the logged details of a Bulk Payment File by Name.
     *
     * @param $name
     * @return null
     */
    public function getBulkPaymentFileByName($name)
    {
        $resource = $this->db->get_where("bp_files", array('file_name' => $name));

        return $resource->num_rows() > 0 ? $resource->row_array() : null;
    }

    /**
     * Retrieve a list of all bulk payment files.
     *
     * @return null
     */
    public function bulkPaymentFiles()
    {
        $resource = $this->db
            ->select("bpf.*, bpd.total AS total_accounts, bpd1.total_valid AS total_valid_accounts, bpd2.total_swapped AS total_swapped_accounts, bpd3.total_force_allowed AS total_forced_accounts")
            ->from('bp_files AS bpf')
            ->join("(SELECT `bp_details`.bpf_id, COUNT(*) AS total FROM bp_details GROUP BY bpf_id) AS bpd", "bpd.bpf_id = bpf.file_id", 'left')
            ->join("(SELECT `bp_details`.bpf_id, COUNT(*) AS total_valid FROM bp_details WHERE bp_details.`bpd_av_response_code` = '00' GROUP BY bpf_id) AS bpd1", "bpd1.bpf_id = bpf.file_id", 'left')
            ->join("(SELECT `bp_details`.bpf_id, COUNT(*) AS total_swapped FROM bp_details WHERE bp_details.`bpd_av_status` = '4' GROUP BY bpf_id) AS bpd2", "bpd2.bpf_id = bpf.file_id", 'left')
            ->join("(SELECT `bp_details`.bpf_id, COUNT(*) AS total_force_allowed FROM bp_details WHERE bp_details.`bpd_av_status` = '5' GROUP BY bpf_id) AS bpd3", "bpd3.bpf_id = bpf.file_id", 'left')
            ->order_by('bpf.file_id', 'desc')
            ->get();

        //die($this->db->last_query());
        $ret_val = $resource->num_rows() > 0 ? $resource->result_array() : null;

        return $ret_val;
    }

    /**
     * Retrieve a bulk payment file by its ID.
     *
     * @param $file_id
     * @return null
     */
    public function bulkPaymentFile($file_id)
    {
        $resource = $this->db
            ->select("bpf.*, COUNT(bpd.bpd_id) AS total_accounts")
            ->from('bp_files AS bpf')
            ->join("bp_details AS bpd", "bpf.file_id = bpd.bpf_id", 'left')
            ->group_by("bpf.file_id")
            ->where('bpf.file_id', $file_id)
            ->get();

        $ret_val = $resource->num_rows() > 0 ? $resource->row_array() : null;

        return $ret_val;
    }

    /**
     * Retrieve a bulk payment file by its name.
     *
     * @param $file_name
     * @return null
     */
    public function bulkPaymentFileByName($file_name)
    {
        $resource = $this->db
            ->select("bpf.*, COUNT(bpd.bpd_id) AS total_accounts")
            ->from('bp_files AS bpf')
            ->join("bp_details AS bpd", "bpf.file_id = bpd.bpf_id", 'left')
            ->group_by("bpf.file_id")
            ->where('bpf.file_name', $file_name)
            ->get();

        $ret_val = $resource->num_rows() > 0 ? $resource->row_array() : null;

        return $ret_val;
    }

    public function bulkPaymentFiles2()
    {
        $resource = $this->db
            ->select("avs.*, COUNT(ava.ava_id) AS total_validated")
            ->from('av_schedules AS avs')
            ->join("av_accounts AS ava", "avs.avs_id = ava.avs_id AND ava.ava_status = '0'", 'left')
            ->group_by("avs.avs_id")
            ->order_by('avs.avs_id', 'desc')
            ->get();

        $ret_val = $resource->num_rows() > 0 ? $resource->result_array() : null;

        return $ret_val;
    }

    public function bulkPaymentAccounts2($schedule_id)
    {
        $resource = $this->db
            ->select("ava.*")
            ->from('av_accounts AS ava')
            ->join("av_schedules AS avs", "avs.avs_id = ava.avs_id AND avs.avs_schedule_id = {$schedule_id}")
            ->get();

        $ret_val = $resource->num_rows() > 0 ? $resource->result_array() : null;

        return $ret_val;
    }

    /**
     * Retrieve a list of all accounts in a bulk payment file.
     *
     * @param $file_id
     * @param $page
     * @return null
     */
    public function bulkPaymentAccounts($file_id, $page = 0 )
    {
        if (!empty($page)) {
            $per_page = 1000;
            $offset = ($page - 1) * $per_page;

            $this->db->limit($per_page, $offset);
        }

        $resource = $this->db
            ->select("bpd.*")
            ->from('bp_details AS bpd')
            ->where("bpd.bpf_id", $file_id)
            ->order_by('bpd.bpd_id')
            ->get();

        $ret_val = $resource->num_rows() > 0 ? $resource->result_array() : null;

        return $ret_val;
    }

    protected function loadPaymentFiles($groups, $relationships = null)
    {
        $retVal = !isset($groups['0']) ? array(0 => $groups) : $groups;

        foreach ($retVal as $_key => $file_group) {
            //get the payment files in a group
            $retVal[$_key]['payment_files'] = $this->getPaymentFiles("", "", 0, $file_group['id']);

            if (!empty($retVal[$_key]['payment_files']) && !empty($relationships)) {
                //get all the payment details in each file
                if (array_key_exists('payment_details', $relationships)) {
                    $retVal[$_key]['payment_files'] = $this->loadPaymentDetails($retVal[$_key]['payment_files'], $relationships['payment_details']);
                }

                if (array_key_exists('source_account', $relationships)) {
                    $retVal[$_key]['payment_files'] = $this->loadSourceAccount($retVal[$_key]['payment_files'], $relationships['source_account']);
                }

                if (array_key_exists('transaction_batch', $relationships)) {
                    $retVal[$_key]['payment_files'] = $this->loadPaymentFileTransactionBatch($retVal[$_key]['payment_files'], $relationships['transaction_batch']);
                }
            }
        }

        return $retVal;
    }

    protected function loadPaymentDetails($files, $relationships = null)
    {
        $retVal = !isset($files['0']) ? array(0 => $files) : $files;

        foreach ($retVal as $key => $file) {
            $retVal[$key]['beneficiaries'] = $this->getPaymentFileDetails($file['paymentFileId']);

            if (!empty($retVal[$key]['beneficiaries']) && !empty($relationships)) {
                //get the bank details of the account
                if (array_key_exists('bank', $relationships)) {
                    foreach ($retVal[$key]['beneficiaries'] as $_key => $beneficiary) {
                        $retVal[$key]['beneficiaries'][$_key] = $this->loadBankDetails($beneficiary);
                    }
                }

                //get the payment details of the
                if (array_key_exists('transaction', $relationships)) {
                    foreach ($retVal[$key]['beneficiaries'] as $_key => $beneficiary) {
                        $retVal[$key]['beneficiaries'][$_key] = $this->loadPaymentDetailTransaction($beneficiary);
                    }
                }
            }
        }

        return $retVal;
    }

    protected function loadSourceAccount($files, $relationships = null)
    {
        $retVal = !isset($files['0']) ? array(0 => $files) : $files;

        foreach ($retVal as $key => $file) {
            $retVal[$key]['source_account'] = $this->acc->getAccount($file['sourceAccountId']);

            if (!empty($retVal[$key]['source_account']) && !empty($relationships)) {
                //get the bank details of the account
                if (array_key_exists('bank', $relationships)) {
                    $retVal[$key]['source_account'] = $this->loadBankDetails($retVal[$key]['source_account']);
                }
            }
        }

        return $retVal;
    }

    protected function loadPaymentFileTransactionBatch($files, $relationships = null)
    {
        $retVal = !isset($files['0']) ? array(0 => $files) : $files;

        foreach ($retVal as $key => $file) {
            $retVal[$key]['transaction_batch'] = $this->getPaymentFileTransactionBatch($file['paymentFileId']);

            if (!empty($retVal[$key]['transaction_batch']) && !empty($relationships)) {
                //get the bank details of the account
                if (array_key_exists('transactions', $relationships)) {
                    $retVal[$key]['transaction_batch'] = $this->loadBatchTransactions($retVal[$key]['transaction_batch'], $relationships['transactions']);
                }
            }
        }

        return $retVal;
    }

    protected function loadBatchTransactions($batch, $relationships = null)
    {
        $batch['transactions'] = $this->getBatchTransactions($batch['id']);

        if (!empty($batch['transactions']) && !empty($relationships)) {
            //get the bank details of the account
            if (array_key_exists('payment_detail', $relationships)) {
                foreach ($batch['transactions'] as $key => $transaction) {
                    $batch['transactions'][$key] = $this->loadTransactionPaymentDetails($transaction);
                }
            }
        }

        return $batch;
    }

    protected function loadPaymentDetailTransaction($detail)
    {
        $detail['transaction'] = $this->getPaymentDetailTransaction($detail['paymentDetailsId']);

        return $detail;
    }

    protected function loadBankDetails($account, $relationships = null)
    {
        if (isset($account['bank_id'])) {
            $bank_id = $account['bank_id'];
        } elseif (isset($account['beneficiaryBankId'])) {
            $bank_id = $account['beneficiaryBankId'];
        }

        $account['bank'] = $this->bank->getBank($bank_id);

        return $account;
    }

}
