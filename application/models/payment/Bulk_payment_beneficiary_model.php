<?php

/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 11/28/2016
 * Time: 7:40 AM
 */
class Bulk_payment_beneficiary_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Retrieve list of all accounts in a bulk payment file.
     *
     * @param $file_id
     * @param int $limit
     * @param int $offset
     * @return null
     */
    public function all($file_id, $limit = 0, $offset = 0)
    {
        if (!empty($limit) && !empty($offset)) {
            $this->db->limit($limit, $offset);
        } elseif (!empty($limit)) {
            $this->db->limit($limit);
        }

        $resource = $this->db
            ->select("bpb.bpb_id AS beneficiary_id,
                        bpb.bulk_payment_file,
                        bpb.bpb_unique_id AS beneficiary_unique_id,
                        bpb.bpb_account_name AS beneficiary_account_name,
                        bpb.bpb_account_no AS beneficiary_account_no,
                        bpb.bpb_bank_code AS beneficiary_bank_code,
                        bpb.bpb_amount AS beneficiary_amount,
                        bpb.bpb_cv_status AS beneficiary_content_validation_status,
                        bpb.bpb_cv_error_description AS beneficiary_content_validation_error_description,
                        bpb.bpb_av_status AS beneficiary_account_validation_status,
                        bpb.bpb_av_response_code AS beneficiary_account_validation_response_code,
                        bpb.bpb_av_response_description AS beneficiary_account_validation_response_description,
                        bpb.bpb_av_error_reason AS beneficiary_account_validation_error_reason,
                        bpb.bpb_correct_account_name AS beneficiary_correct_account_name,
                        bpb.payment_beneficiary,
                        bpb.bpb_payment_status AS beneficiary_payment_status,
                        bpb.bpb_payment_error_reason AS beneficiary_payment_failure_reason,
                        s.status_description AS beneficiary_account_validation_status_description,
                        s2.status_description AS beneficiary_payment_status_description,
                        s3.status_description AS beneficiary_content_validation_status_description", false)
            ->from('bulk_payment_beneficiaries bpb')
            ->join("statuses s", "bpb.bpb_av_status = s.status_id", "LEFT")
            ->join("statuses s2", "bpb.bpb_payment_status = s2.status_id", "LEFT")
            ->join("statuses s3", "bpb.bpb_cv_status = s3.status_id", "LEFT")
            ->where("bpb.bulk_payment_file", $file_id)
            ->order_by('bpb.bpb_id')
            ->get();
        //die($this->db->last_query());
        $ret_val = $resource->num_rows() > 0 ? $resource->result_array() : null;

        return $ret_val;
    }

    /**
     * Delete all beneficiaries in a file.
     *
     * @param $file_id
     * @return mixed
     */
    public function deleteAllInFile($file_id)
    {
        $this->db->delete("bulk_payment_beneficiaries", array('bulk_payment_file' => $file_id));

        return $this->db->affected_rows();
    }
}