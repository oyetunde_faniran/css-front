<?php

/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 11/27/2016
 * Time: 8:13 AM
 */
class Payment_schedule_beneficiary_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Retrieve all payment schedule beneficiaries, in 1 or more payment schedules.
     * Optionally include all commission beneficiaries.
     *
     * @param $id
     * @param bool|false $include_commission_accounts
     * @return null
     */
    public function all($id, $include_commission_accounts = false)
    {
        if (is_array($id))
            $this->db->where_in("psb.payment_schedule", $id);
        else
            $this->db->where("psb.payment_schedule", $id);

        if (!$include_commission_accounts)
            $this->db->where("psb.psb_commission_account = FALSE");

        $result = $this->db
            ->select("psb.psb_id AS beneficiary_id,
                      psb.payment_schedule,
                      psb.psb_account_name AS beneficiary_account_name,
                      psb.psb_account_no AS beneficiary_account_no,
                      psb.psb_bank_code AS beneficiary_bank_code,
                      psb.psb_amount AS beneficiary_amount,
                      psb.psb_commission_account AS beneficiary_is_commission,
                      psb.psb_payment_response_code AS beneficiary_payment_response_code,
                      psb.psb_payment_response_description AS beneficiary_payment_response_description,
                      s.status_description AS beneficiary_payment_status,
                      b.bank_name AS beneficiary_bank_name,
                      b.bank_id AS beneficiary_bank_id", false)
            ->from("payment_schedule_beneficiaries psb")
            ->join("payment_file_beneficiaries pfb", "psb.payment_beneficiary = pfb.pfb_id", "INNER")
            ->join("statuses s", "psb.psb_payment_status = s.status_id", "LEFT")
            ->join("banks b", "pfb.pfb_bank = b.bank_id", "LEFT")
            ->order_by('psb.payment_schedule', 'desc')
            ->order_by('beneficiary_account_name')
            ->get();
        //die($this->db->last_query());
        $ret_val = $result->num_rows() > 0 ? $result->result_array() : null;

        return $ret_val;
    }

    /**
     * Retrieve payment schedule by ID.
     *
     * @param $id
     * @return null
     */
    public function find($id)
    {
        $result = $this->db
            ->select("psb.psb_id AS beneficiary_id,
                      psb.payment_schedule,
                      psb.psb_account_name AS beneficiary_account_name,
                      psb.psb_account_no AS beneficiary_account_no,
                      psb.psb_bank_code AS beneficiary_bank_code,
                      psb.psb_amount AS beneficiary_amount,
                      psb.psb_commission_account AS beneficiary_is_commission,
                      psb.psb_payment_response_code AS beneficiary_payment_response_code,
                      psb.psb_payment_response_description AS beneficiary_payment_response_description,
                      s.status_description AS beneficiary_payment_status,
                      b.bank_name AS beneficiary_bank_name,
                      b.bank_id AS beneficiary_bank_id", false)
            ->from("payment_schedule_beneficiaries psb", false)
            ->from("payment_schedules ps")
            ->join("payment_files pf", "ps.payment_file = pf.pf_id", "INNER")
            ->join("bank_accounts ba", "pf.source_account = ba.acc_id", "LEFT")
            ->join("banks b", "ba.bank_id = b.bank_id", "LEFT")
            ->where('ps.ps_id', $id)
            ->get();

        $ret_val = $result->num_rows() > 0 ? $result->row_array() : null;

        return $ret_val;
    }

    /**
     * Save single payment schedule beneficiary.
     *
     * @param $data
     * @return null
     */
    public function save($data)
    {
        $_data = array(
            'payment_schedule' => $data['payment_schedule'],
            'payment_beneficiary' => $data['payment_beneficiary'],
            'psb_account_name' => $data['account_name'],
            'psb_account_no' => $data['account_no'],
            'psb_bank_code' => $data['bank_code'],
            'psb_amount' => $data['amount'],
            'psb_commission_account' => !empty($data['commission_account']) ? true : false,
            'psb_status' => $data['status'],
            'psb_created_at' => date("Y-m-d H:i:s")
        );

        $response = $this->db->insert('payment_schedule_beneficiaries', $_data);

        return $response ? $this->db->insert_id() : null;
    }

    /**
     * Save many payment schedule beneficiaries at once.
     *
     * @param $data
     * @return null
     */
    public function saveMany($data)
    {
        $_data = array();

        foreach ($data as $piece) {
            $_data[] = array(
                'payment_schedule' => $piece['payment_schedule'],
                'payment_beneficiary' => $piece['payment_beneficiary'],
                'psb_account_name' => $piece['account_name'],
                'psb_account_no' => $piece['account_no'],
                'psb_bank_code' => $piece['bank_code'],
                'psb_amount' => $piece['amount'],
                'psb_commission_account' => !empty($piece['commission_account']) ? true : false,
                'psb_payment_status' => $piece['payment_status'],
                'psb_created_at' => date("Y-m-d H:i:s")
            );
        }

        $response = $this->db->insert_batch('payment_schedule_beneficiaries', $_data);

        if ($response) {
            $result = $this->db->select("CONCAT_WS(':', psb_id, payment_beneficiary) AS summary")->from('payment_schedule_beneficiaries')->where('payment_schedule', $piece['payment_schedule'])->get();
            $ret_val = $result && $result->num_rows() > 0 ? $result->result_array() : null;
        } else {
            $ret_val = null;
        }

        return $ret_val;
    }

    /**
     * Update all beneficiaries in a schedule.
     *
     * @param $data
     * @return mixed
     */
    public function updateAllInSchedule($data)
    {
        $sql = "UPDATE payment_schedule_beneficiaries
                SET psb_payment_status = ?, psb_payment_response_code = ?, psb_payment_response_description = ?
                WHERE payment_schedule = ?";

        $values = array($data['status'], $data['response_code'], $data['response_description'], $data['payment_schedule_id']);

        return $this->db->query($sql, $values);
    }

    /**
     * Update single beneficiary in a schedule.
     *
     * @param $data
     * @return mixed
     */
    public function updateOneInSchedule($data)
    {
        $sql = "UPDATE payment_schedule_beneficiaries
                SET psb_payment_status = ?, psb_payment_response_code = ?, psb_payment_response_description = ?
                WHERE payment_schedule = ? AND psb_account_no = ? AND psb_bank_code = ?";

        $values = array($data['status'], $data['response_code'], $data['response_description'], $data['payment_schedule_id'], $data['beneficiary_account_no'], $data['beneficiary_bank_code']);

        $this->db->query($sql, $values);

        return $this->db->affected_rows();
    }
}