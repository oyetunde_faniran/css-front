<?php

/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 11/28/2016
 * Time: 2:17 AM
 */
class Bulk_payment_file_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('status_model', 'status');
        $this->load->model('payment/bulk_payment_beneficiary_model', 'bulk_payment_beneficiary');
    }

    /**
     * Retrieve all bulk payment files.
     *
     * @param null $content_validation_status
     * @param null $account_validation_status
     * @return null
     */
    public function all($content_validation_status = null, $account_validation_status = null)
    {
        $filters = array();
        if ($content_validation_status) {
            $filters['bpf_cv_status'] = $content_validation_status;
        }

        if ($account_validation_status) {
            $filters['bpf_av_status'] = $account_validation_status;
        }

        if (!empty($filters))
            $this->db->where($filters);

        $result = $this->db
            ->select("bpf.bpf_id AS file_id,
                        bpf.bpf_name AS file_name,
                        bpf.bpf_path AS file_path,
                        bpf.bpf_cv_status AS content_validation_status,
                        bpf.bpf_cv_total AS total_records,
                        bpf.bpf_av_status AS account_validation_status,
                        bpf.payment_file_group,
                        bpf.bpf_created_at AS created_at,
                        s.status_description AS content_validation_status_description,
                        s2.status_description AS account_validation_status_description,
                        cvv.cv_summary AS content_validation_summary,
                        avv.av_summary AS account_validation_summary
                        ", false)
            ->from('bulk_payment_files AS bpf')
            ->join("statuses s", "bpf.bpf_cv_status = s.status_id", "LEFT")
            ->join("statuses s2", "bpf.bpf_av_status = s2.status_id", "LEFT")
            ->join("(
                        SELECT cv.bulk_payment_file, GROUP_CONCAT( CONCAT_WS( ':', cv.status_description, cv.total) ORDER BY cv.bpb_cv_status) AS cv_summary
                        FROM (
                            SELECT bulk_payment_file, bpb_cv_status, COUNT(*) AS total, s.status_description
                            FROM bulk_payment_beneficiaries bpb
                            JOIN statuses s ON bpb.bpb_cv_status = s.status_id
                            GROUP BY bulk_payment_file, bpb_cv_status
                        ) AS cv
                        GROUP BY cv.bulk_payment_file
                    ) cvv", "bpf.bpf_id = cvv.bulk_payment_file", 'LEFT')
            ->join("(
                        SELECT av.bulk_payment_file, GROUP_CONCAT( CONCAT_WS( ':', av.status_description, av.total) ORDER BY av.bpb_av_status) AS av_summary
                        FROM (
                            SELECT  bulk_payment_file, bpb_av_status, COUNT(*) AS total, s.status_description
                            FROM bulk_payment_beneficiaries bpb
                            JOIN statuses s ON bpb.bpb_av_status = s.status_id
                            GROUP BY bulk_payment_file, bpb_av_status
                        ) AS av
                        GROUP BY av.bulk_payment_file
                     ) avv", "bpf.bpf_id = avv.bulk_payment_file", 'LEFT')
            ->order_by('bpf.bpf_id', 'desc')
            ->get();

        //die($this->db->last_query());
        $ret_val = $result->num_rows() > 0 ? $result->result_array() : null;

        return $ret_val;
    }

    /**
     * Retrieve a bulk payment file by its ID.
     *
     * @param $id
     * @return null
     */
    public function find($id)
    {
        $result = $this->db
            ->select("bpf.bpf_id AS file_id,
                        bpf.bpf_name AS file_name,
                        bpf.bpf_path AS file_path,
                        bpf.bpf_cv_status AS content_validation_status,
                        bpf.bpf_cv_total AS total_records,
                        bpf.bpf_av_status AS account_validation_status,
                        bpf.payment_file_group,
                        bpf.bpf_created_at AS created_at,
                        s.status_description AS content_validation_status_description,
                        s2.status_description AS account_validation_status_description,
                        cvv.cv_summary AS content_validation_summary,
                        avv.av_summary AS account_validation_summary
                        ", false)
            ->from('bulk_payment_files AS bpf')
            ->join("statuses s", "bpf.bpf_cv_status = s.status_id", "LEFT")
            ->join("statuses s2", "bpf.bpf_av_status = s2.status_id", "LEFT")
            ->join("(
                        SELECT cv.bulk_payment_file, GROUP_CONCAT( CONCAT_WS( ':', cv.status_description, cv.total) ORDER BY cv.bpb_cv_status) AS cv_summary
                        FROM (
                            SELECT bulk_payment_file, bpb_cv_status, COUNT(*) AS total, s.status_description
                            FROM bulk_payment_beneficiaries bpb
                            JOIN statuses s ON bpb.bpb_cv_status = s.status_id
                            GROUP BY bulk_payment_file, bpb_cv_status
                        ) AS cv
                        GROUP BY cv.bulk_payment_file
                    ) cvv", "bpf.bpf_id = cvv.bulk_payment_file", 'LEFT')
            ->join("(
                        SELECT av.bulk_payment_file, GROUP_CONCAT( CONCAT_WS( ':', av.status_description, av.total) ORDER BY av.bpb_av_status) AS av_summary
                        FROM (
                            SELECT  bulk_payment_file, bpb_av_status, COUNT(*) AS total, s.status_description
                            FROM bulk_payment_beneficiaries bpb
                            JOIN statuses s ON bpb.bpb_av_status = s.status_id
                            GROUP BY bulk_payment_file, bpb_av_status
                        ) AS av
                        GROUP BY av.bulk_payment_file
                     ) avv", "bpf.bpf_id = avv.bulk_payment_file", 'LEFT')
            ->where("bpf.bpf_id", $id)
            ->get();

        $ret_val = $result->num_rows() > 0 ? $result->row_array() : null;

        return $ret_val;
    }

    /**
     * Retrieve bulk payment file by its name.
     *
     * @param $name
     * @return null
     */
    public function findByName($name)
    {
        $result = $this->db
            ->select("bpf.bpf_id AS file_id,
                        bpf.bpf_name AS file_name,
                        bpf.bpf_path AS file_path,
                        bpf.bpf_cv_status AS content_validation_status,
                        bpf.bpf_cv_total AS total_records,
                        bpf.bpf_av_status AS account_validation_status,
                        bpf.payment_file_group,
                        bpf.bpf_created_at AS created_at,
                        s.status_description AS content_validation_status_description,
                        s2.status_description AS account_validation_status_description,
                        cvv.cv_summary AS content_validation_summary,
                        avv.av_summary AS account_validation_summary
                        ", false)
            ->from('bulk_payment_files AS bpf')
            ->join("statuses s", "bpf.bpf_cv_status = s.status_id", "LEFT")
            ->join("statuses s2", "bpf.bpf_av_status = s2.status_id", "LEFT")
            ->join("(
                        SELECT cv.bulk_payment_file, GROUP_CONCAT( CONCAT_WS( ':', cv.status_description, cv.total) ORDER BY cv.bpb_cv_status) AS cv_summary
                        FROM (
                            SELECT bulk_payment_file, bpb_cv_status, COUNT(*) AS total, s.status_description
                            FROM bulk_payment_beneficiaries bpb
                            JOIN statuses s ON bpb.bpb_cv_status = s.status_id
                            GROUP BY bulk_payment_file, bpb_cv_status
                        ) AS cv
                        GROUP BY cv.bulk_payment_file
                    ) cvv", "bpf.bpf_id = cvv.bulk_payment_file", 'LEFT')
            ->join("(
                        SELECT av.bulk_payment_file, GROUP_CONCAT( CONCAT_WS( ':', av.status_description, av.total) ORDER BY av.bpb_av_status) AS av_summary
                        FROM (
                            SELECT  bulk_payment_file, bpb_av_status, COUNT(*) AS total, s.status_description
                            FROM bulk_payment_beneficiaries bpb
                            JOIN statuses s ON bpb.bpb_av_status = s.status_id
                            GROUP BY bulk_payment_file, bpb_av_status
                        ) AS av
                        GROUP BY av.bulk_payment_file
                     ) avv", "bpf.bpf_id = avv.bulk_payment_file", 'LEFT')
            ->where("bpf.bpf_name", $name)
            ->get();

        $ret_val = $result->num_rows() > 0 ? $result->row_array() : null;

        return $ret_val;
    }

    /**
     * Save the details of a Bulk Payment File in our db.
     *
     * @param $data
     * @return mixed
     */
    public function save($data)
    {
        $file_data = array(
            'bpf_name' => $data['name'],
            'bpf_path' => $data['path'],
            'bpf_cv_status' => !empty($data['content_validation_status']) ? $data['content_validation_status'] : 1,
            'bpf_av_status' => !empty($data['account_validation_status']) ? $data['account_validation_status'] : 1,
            'bpf_created_at' => date('Y-m-d H:i:s'),
            'bpf_created_by' => $data['uploaded_by']
        );

        $response = $this->db->insert("bulk_payment_files", $file_data);

        return $response ? $this->db->insert_id() : null;
    }

    /**
     * Update the account validation status of a bulk payment file.
     *
     * @param $id
     * @param $status
     * @return mixed
     */
    public function updateAccountValidationStatus($id, $status)
    {
        return $this->db->query("UPDATE bulk_payment_files SET bpf_av_status = {$status} WHERE bpf_id = {$id}");
    }
    /**
     * Validate the content of bulk payment file.
     *
     * @param $file_id
     * @return bool|string
     */
    public function validateContent($file_id)
    {
        $file = $this->find($file_id);

        $info = pathinfo($file['file_path']);
        $ext = strtolower($info['extension']);
        try {
            if ($ext != "csv") {
                throw new Exception('Only CSV files are allowed!!!');
            }

            //determine the number of rows in the file
            $handle = fopen($file['file_path'], "r");
            $total_rows_in_file = 0;
            if ($handle) {
                while (!feof($handle)) {
                    $content = fgets($handle);
                    if ($content) $total_rows_in_file++;
                }
            }
            fclose($handle);

            //get the list of statuses
            $statuses = $this->status->all();

            //flag the number of rows in the bulk payment file table and that content validation is in progress
            foreach ($statuses as $status) {
                if ($status['status_description'] == 'IN PROGRESS')
                    break;
            }

            $_data = array(
                'bpf_cv_total' => ($total_rows_in_file - 1),
                'bpf_cv_status' => $status['status_id']
            );
            $this->db->update("bulk_payment_files", $_data, array('bpf_id' => $file['file_id']));

            //load the bankcodes
            $bank_array = $this->basic_functions->getBanksNIBSSCodes(false);

            $db_cols = array('sno', 'unique_id', 'account_name', 'account_no', 'bank_code', 'amount', 'narration');

            $account_number_array = array();

            $num_rows_to_read_per_time = 20000;

            $i = 0;
            $handle = fopen($file['file_path'], "r");

            //validate the content in batches of $num_rows_to_read_per_time
            for ($iterations = 0; $iterations < $total_rows_in_file; $iterations += $num_rows_to_read_per_time) {

                $this->db->trans_begin();

                $file_content = array();
                $file_content_to_save_in_db = array();
                $errors = array();

                $counter_file = FCPATH . "/files/validation_counter.json";
                if (file_exists($counter_file)) {
                    $counter_file_data = (array)json_decode(file_get_contents($counter_file));
                    $key = basename($file['file_path']);
                    if (array_key_exists($key, $counter_file_data)) {
                        $row_start = $counter_file_data[$key];
                    } else {
                        $counter_file_data[$key] = $row_start = 0;
                        file_put_contents($counter_file, json_encode(array(basename($file['file_path']) => 0)));
                    }
                } else {
                    $row_start = 0;
                    file_put_contents($counter_file, json_encode(array(basename($file['file_path']) => 0)));
                }

                //read the content of the files into this array
                $file_data = array();

                //$i = 0;
                $loops = $row_start + $num_rows_to_read_per_time;
                //$handle = fopen($file['file_path'], "r");
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    $i++;
                    //if ($i > $loops || $i > $total_rows_in_file) break;
                    if ($i <= $row_start) continue;
                    $file_data[$i] = $data;
                    if ($i == $loops || $i == $total_rows_in_file) break;
                }

                //validate the content of these records
                foreach ($file_data as $i => $row_data) {
                    //remove the header row
                    if ($row_data[0] == 'S/NO') continue;

                    foreach ($row_data as $column => $value) {
                        $value = trim($value);
                        $value = empty($value) ? null : $value;

                        $file_content_to_save_in_db[$i]["bulk_payment_file"] = $file['file_id'];
                        $file_content[$i][$db_cols[$column]] = $value;

                        //header should be in row 1 only. of course this can be modified to suit your need.
                        if ($i == 1) {
                            //confirm the headers contain the right data
                            switch ($column) {
                                case 0:
                                    if (strtoupper($value) != "S/NO") {
                                        $errors[$i][] = "The title of Column A should be S/NO.";
                                    }
                                    break;
                                case 1:
                                    if (strtoupper($value) != "UNIQUE ID") {
                                        $errors[$i][] = "The title of Column F should be UNIQUE ID.";
                                    }
                                    break;
                                case 2:
                                    if (strtoupper($value) != "ACCOUNT NAME") {
                                        $errors[$i][] = "The title of Column B should be ACCOUNT NAME.";
                                    }
                                    break;
                                case 3:
                                    if (strtoupper($value) != "ACCOUNT NO") {
                                        $errors[$i][] = "The title of Column C should be ACCOUNT NO.";
                                    }
                                    break;
                                case 4:
                                    if (strtoupper($value) != "BANK CODE") {
                                        $errors[$i][] = "The title of Column D should be BANK CODE.";
                                    }
                                    break;
                                case 5:
                                    if (strtoupper($value) != "AMOUNT") {
                                        $errors[$i][] = "The title of Column E should be AMOUNT.";
                                    }
                                    break;
                                /*case 6:
                                    if (strtoupper($value) != "NARRATION") {
                                        $errors[$i][] = "The title of Column G should be NARRATION.";
                                    }
                                    break;*/
                            }
                        } else {
                            switch ($column) {
                                case 0:
                                    //The S/No field
                                    if (!is_numeric($value)) {
                                        $errors[$i][] = "S/NO should be a number.";
                                    }
                                    break;
                                case 1:
                                    //The Unique ID field
                                    $file_content_to_save_in_db[$i]["bpb_unique_id"] = $value;
                                    break;
                                case 2:
                                    //The Account Name field
                                    $file_content_to_save_in_db[$i]["bpb_account_name"] = $value;
                                    if (empty($value)) {
                                        $errors[$i][] = "ACCOUNT NAME cannot be empty.";
                                    }
                                    break;
                                case 3:
                                    //The Account No field
                                    $file_content_to_save_in_db[$i]["bpb_account_no"] = $value;
                                    $row_with_same_account_no = array_search($value, $account_number_array);
                                    if ($row_with_same_account_no) {
                                        $errors[$i][] = "Duplicate Account Number. See Row {$row_with_same_account_no}";
                                    }

                                    if (!is_null($value))
                                        $account_number_array[$i] = $value;

                                    if (!is_numeric($value) || strlen($value) != 10) {
                                        $errors[$i][] = "Invalid NUBAN.";
                                    }
                                    break;
                                case 4:
                                    //The Bank Code field
                                    $file_content_to_save_in_db[$i]["bpb_bank_code"] = $value;
                                    if (!is_numeric($value) || !in_array($value, $bank_array)) {
                                        $errors[$i][] = "Invalid Bank Code.";
                                    }
                                    break;
                                case 5:
                                    //The Amount field
                                    $value = str_replace(",", "", $value);
                                    $file_content_to_save_in_db[$i]["bpb_amount"] = $value;
                                    if (!is_numeric($value)) {
                                        $errors[$i][] = "Amount should be a number.";
                                    }
                                    break;
                                default;
                                    //nothing
                            }
                        }
                    }

                    if (empty($file_content[$i]['narration'])) {
                        $file_content[$i]['narration'] = null;
                    }

                    if (!empty($errors[$i])) {
                        foreach ($statuses as $status) {
                            if ($status['status_description'] == 'INVALID')
                                break;
                        }

                        $file_content_to_save_in_db[$i]["bpb_cv_status"] = $status['status_id'];
                        $file_content_to_save_in_db[$i]["bpb_cv_error_description"] = implode("|", $errors[$i]);
                    } else {
                        foreach ($statuses as $status) {
                            if ($status['status_description'] == 'VALID')
                                break;
                        }

                        $file_content_to_save_in_db[$i]["bpb_cv_status"] = $status['status_id'];
                        $file_content_to_save_in_db[$i]["bpb_cv_error_description"] = null;
                    }
                }

                //save the file content in the db table
                //echo ('<pre>' . print_r($file_content_to_save_in_db, 1));
                $resp = $this->db->insert_batch("bulk_payment_beneficiaries", $file_content_to_save_in_db);
                /*if (!$resp)
                    die('<pre>' . print_r($file_content_to_save_in_db, 1));*/

                $this->db->trans_commit();

                file_put_contents($counter_file, json_encode(array(basename($file['file_path']) => ($loops < $total_rows_in_file ? $loops : $total_rows_in_file))));
                file_put_contents(FCPATH . "/files/validation_debug.txt", date("Y-m-d H:i:s") . " " .  $iterations . " " . count($file_content_to_save_in_db) . "\n", FILE_APPEND);
            }

            fclose($handle);

            //flag the bulk payment file as Validation Complete
            foreach ($statuses as $status) {
                if ($status['status_description'] == 'COMPLETED')
                    break;
            }

            $_data = array('bpf_cv_status' => $status['status_id']);
            $this->db->update("bulk_payment_files", $_data, array('bpf_id' => $file['file_id']));


            $ret_val = true;
        } catch (Exception $e) {
            $ret_val = $e->getMessage();

            $this->db->trans_rollback();
        }

        return $ret_val;
    }

    /**
     * Delete bulk payment file plus all the beneficiaries in it.
     *
     * @param $file_id
     * @return mixed
     */
    public function delete($file_id)
    {
        $ret_val = array();

        //delete the uploaded file
        $file = $this->find($file_id);
        unlink($file['bpf_path']);

        //delete the file record from db
        $this->db->delete("bulk_payment_files", array("bpf_id" => $file_id));
        $ret_val['files'] = $this->db->affected_rows();

        //delete all beneficiary records from db
        $this->bulk_payment_beneficiary->deleteAllInFile($file_id);
        $ret_val['beneficiaries'] = $this->db->affected_rows();

        return $ret_val;
    }
}