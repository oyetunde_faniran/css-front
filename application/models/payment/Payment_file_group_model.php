<?php

/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 11/26/2016
 * Time: 10:05 AM
 */
class Payment_file_group_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Retrieve all payment file groups.
     * Compute the total amount and total number of beneficiaries in each group.
     * Optionally include commission beneficiaries.
     *
     * @param null $payment_type
     * @param null $status
     * @param null $created_by
     * @param bool|false $include_commission_accounts
     * @return null
     */
    public function all($payment_type = null, $status = null, $created_by = null, $include_commission_accounts = false)
    {
        $filters = array();

        if (!empty($payment_type)) {
            $filters['pfg.payment_type'] = $payment_type;
        }

        if (!empty($status)) {
            $filters['pfg.status'] = $status;
        }

        if (!empty($created_by)) {
            $filters['pfg.created_by'] = $created_by;
        }

        if (!empty($filters)) {
            $this->db->where($filters);
        }

        $q = $include_commission_accounts ? "" : " AND pfb.pfb_commission_account = FALSE";

        $result = $this->db
            ->select("pfg.*,
                    SUM(pf2.payment_file_amount) AS total_amount,
                    SUM(pf2.num_beneficiaries) AS total_beneficiaries,
                    CONCAT(u.user_firstname, ' ', u.user_surname) AS initiator,
                    pt.pt_name AS payment_type,
                    s.status_description,
                    pff.beneficiary_account_name,
                    h.history,
                    psg.*")
            ->from("payment_file_groups pfg")
            ->join("(
                        SELECT
                            pf.pf_id,
                            pf.payment_file_group,
                            SUM(pfb.pfb_amount) AS payment_file_amount,
                            COUNT(pfb.`pfb_id`) AS num_beneficiaries
                        FROM payment_files pf
                        JOIN payment_file_beneficiaries pfb ON pf.pf_id = pfb.payment_file $q
                        GROUP BY pf.pf_id
                     ) pf2", "pfg.id = pf2.payment_file_group", "INNER")
            ->join("upl_users u", "u.user_id = pfg.created_by", "INNER")
            ->join("payment_types pt", "pfg.payment_type = pt.pt_id", "INNER")
            ->join("statuses s", "pfg.status = s.status_id", "INNER")
            ->join("(
                        SELECT pfg.id AS payment_file_group_id, pfb.pfb_account_name AS beneficiary_account_name
                        FROM payment_file_groups pfg
                        JOIN payment_files pf ON pfg.`id` = pf.`payment_file_group`
                        JOIN payment_file_beneficiaries pfb ON pf.`pf_id` = pfb.`payment_file` $q
                        WHERE pfg.`payment_type` = 2
                     ) pff", "pfg.id = pff.payment_file_group_id", "LEFT")
            ->join("(
                        SELECT
                            pfh2.payment_file_group,
                            GROUP_CONCAT( CONCAT_WS(':', pfh2.status_description, pfh2.actor, pfh2.status_reason) ORDER BY pfh2.status_id ) AS history
                        FROM (
                            SELECT
                                pfh.payment_file_group,
                                pfh.pfh_status AS status_id,
                                pfh.pfh_status_reason AS status_reason,
                                CONCAT(u.user_firstname, ' ', u.user_surname) AS actor,
                                s.status_description
                            FROM payment_file_history pfh
                            JOIN upl_users AS u ON pfh_created_by = u.user_id
                            JOIN statuses s ON pfh.pfh_status = s.status_id
                            ORDER BY payment_file_group, status_id
                        ) pfh2
                        GROUP BY pfh2.payment_file_group
                    ) h", "pfg.id = h.payment_file_group", "LEFT")
            ->join("payment_schedule_groups psg", "pfg.id = psg.payment_file_group", "LEFT")
            ->group_by("pfg.id")
            ->order_by("pfg.id", "DESC")
            ->get();

        //die($this->db->last_query());
        $ret_val = $result->num_rows() > 0 ? $result->result_array() : null;

        return $ret_val;
    }

    /**
     * Retrieve payment file group by ID.
     * Compute the total amount and total number of beneficiaries in each group.
     * Optionally include commission beneficiaries.
     *
     * @param $id
     * @param bool|false $include_commission_accounts
     * @return null
     */
    public function find($id, $include_commission_accounts = false)
    {
        $q = $include_commission_accounts ? "" : " AND pfb.pfb_commission_account = FALSE";

        $result = $this->db
            ->select("pfg.*,
                    SUM(pf2.payment_file_amount) AS total_amount,
                    SUM(pf2.num_beneficiaries) AS total_beneficiaries,
                    CONCAT(u.user_firstname, ' ', u.user_surname) AS initiator,
                    pt.pt_name AS payment_type,
                    s.status_description,
                    pff.beneficiary_account_name,
                    h.history,
                    psg.*")
            ->from("payment_file_groups pfg")
            ->join("(
                        SELECT
                            pf.pf_id,
                            pf.payment_file_group,
                            SUM(pfb.pfb_amount) AS payment_file_amount,
                            COUNT(pfb.`pfb_id`) AS num_beneficiaries
                        FROM payment_files pf
                        JOIN payment_file_beneficiaries pfb ON pf.pf_id = pfb.payment_file $q
                        GROUP BY pf.pf_id
                     ) pf2", "pfg.id = pf2.payment_file_group", "INNER")
            ->join("upl_users u", "u.user_id = pfg.created_by", "INNER")
            ->join("payment_types pt", "pfg.payment_type = pt.pt_id", "INNER")
            ->join("statuses s", "pfg.status = s.status_id", "INNER")
            ->join("(
                        SELECT pfg.id AS payment_file_group_id, pfb.pfb_account_name AS beneficiary_account_name
                        FROM payment_file_groups pfg
                        JOIN payment_files pf ON pfg.`id` = pf.`payment_file_group`
                        JOIN payment_file_beneficiaries pfb ON pf.`pf_id` = pfb.`payment_file` $q
                        WHERE pfg.`payment_type` = 2
                     ) pff", "pfg.id = pff.payment_file_group_id", "LEFT")
            ->join("(
                        SELECT
                            pfh2.payment_file_group,
                            GROUP_CONCAT( CONCAT_WS(':', pfh2.status_description, pfh2.actor, pfh2.status_reason) ORDER BY pfh2.status_id ) AS history
                        FROM (
                            SELECT
                                pfh.payment_file_group,
                                pfh.pfh_status AS status_id,
                                pfh.pfh_status_reason AS status_reason,
                                CONCAT(u.user_firstname, ' ', u.user_surname) AS actor,
                                s.status_description
                            FROM payment_file_history pfh
                            JOIN upl_users AS u ON pfh_created_by = u.user_id
                            JOIN statuses s ON pfh.pfh_status = s.status_id
                            ORDER BY payment_file_group, status_id
                        ) pfh2
                        GROUP BY pfh2.payment_file_group
                    ) h", "pfg.id = h.payment_file_group", "LEFT")
            ->join("payment_schedule_groups psg", "pfg.id = psg.payment_file_group", "LEFT")
            ->where('pfg.id', $id)
            ->group_by("pfg.id")
            ->get();

        //die($this->db->last_query()); ->where('pfg.id', $id)
        $ret_val = $result->num_rows() > 0 ? $result->row_array() : null;

        return $ret_val;
    }

    /**
     * Save payment file group.
     *
     * @param $data
     * @return null
     */
    public function save($data)
    {
        return empty($data['id']) ? $this->create($data) : $this->update($data);
    }

    /**
     * Store new payment file group.
     *
     * @param $data
     * @return null
     */
    public function create($data)
    {
        $response = $this->db->insert('payment_file_groups', $data);

        return $response ? $this->db->insert_id() : null;
    }

    /**
     * Update payment file group.
     *
     * @param $data
     * @return mixed
     */
    protected function update($data)
    {
        $_data = array(
            'status' => $data['status'],
            'updated_by' => $data['updated_by']
        );

        return $this->db->update('payment_file_groups', $_data, array('id' => $data['id']));
    }
}