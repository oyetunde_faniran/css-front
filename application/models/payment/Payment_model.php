<?php

/**
 * Contains methods for presenting the main payments of the payroll
 *
 * @author Segun Ojo
 */
class Payment_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Account_model', 'acc');
        $this->load->model('Bank_model', 'bank');
        $this->load->model('status_model', 'status');

        $this->load->model('payment/payment_file_group_model', 'payment_file_group');
        $this->load->model('payment/payment_file_model', 'payment_file');
        $this->load->model('payment/payment_file_beneficiary_model', 'payment_file_beneficiary');
        $this->load->model('payment/payment_schedule_group_model', 'payment_schedule_group');
        $this->load->model('payment/payment_schedule_model', 'payment_schedule');
        $this->load->model('payment/payment_schedule_beneficiary_model', 'payment_schedule_beneficiary');
    }

    /**
     * Save the payment file group data in tbl:payment_file_groups;
     * Save the payment files data in tbl:payment_files
     * Save the beneficiary(ies)' details in tbl:payment_file_beneficiaries
     *
     * @param $payment_file_group_data
     * @param $payment_data including the beneficiaries' data
     * @return bool|null
     */
    public function savePayment($payment_file_group_data, $payment_data)
    {
        try {
            //Begin a database transaction
            $this->db->trans_begin();

            //save the payment file group
            $payment_file_group_id = $this->payment_file_group->save($payment_file_group_data);

            foreach ($payment_data as $data) {

                //save each payment file in the group
                $payment_file_data = array(
                    "payment_file_group" => $payment_file_group_id,
                    'source_account' => "{$data['source_account_id']}",
                    'merchant_id' => !empty($data['merchant_id']) ? $data['merchant_id'] : $_SESSION['mymerchantId'],
                    'created_by' => !empty($data['created_by']) ? $data['created_by'] : $_SESSION['myuserId']
                );
                $payment_file_id = $this->payment_file->save($payment_file_data);

                //save all beneficiaries in each payment file in the group
                $payment_beneficiaries_data = array();
                foreach ($data['beneficiaries'] as $beneficiary) {
                    $payment_beneficiaries_data[] = array(
                        'payment_file_id' => $payment_file_id,
                        'account_name' => "$beneficiary[account_name]",
                        'account_no' => "$beneficiary[account_no]",
                        'bank_id' => "$beneficiary[bank_id]",
                        'amount' => "$beneficiary[amount]",
                        'commission_account' => !empty($beneficiary['commission_account']) ? true : false,
                        'created_by' => !empty($data['created_by']) ? $data['created_by'] : $_SESSION['myuserId']
                    );
                }
                $this->payment_file_beneficiary->saveMany($payment_beneficiaries_data);
            }

            $retVal = $payment_file_group_id;

            $this->db->trans_commit();
        } catch (Exception $ex) {
            //Transaction failed, so rollback
            $this->db->trans_rollback();
            $retVal = false;
        }

        return $retVal;
    }

    /**
     * Store the token for a given payment file group.
     *
     * @param $token
     * @param $merchant_id
     * @param $payment_file_group_id
     * @return mixed
     */
    public function saveToken($token, $merchant_id, $payment_file_group_id)
    {
        //insert the token
        $token_data = array(
            'merchantId' => "$merchant_id",
            'payment_file_group_id' => "$payment_file_group_id",
            'token' => "$token",
            'tokenTime' => date("Y-m-d H:i:s"),
            'usedStatus' => "0",
            'dateUsed' => "0000-00-00"
        );

        return $this->db->insert("merchant_token", $token_data);
    }

    /**
     * Send token in a mail to a user.
     *
     * @param string $token
     * @param array $recipient
     */
    public function sendTokenMail($token, $recipient)
    {
        $this->load->library('email');

        $subject = "BAMS - Transaction Token";

        $firstname = $recipient['firstname'];
        $email = $recipient['email'];

        $message_footer = "<br><br>We advise that you delete this mail if it contains sensitive contents such as:
Login Password, Transaction Password, Answers to your Security Questions, etc.<br>";

        $sent_message = "Please use the transaction token below to complete transaction within ten minutes<br>TOKEN: $token<br>";

        $path_to_logo = "";
        $message = "<!DOCTYPE html><html><title>BAMS</title>
                    <style>
                        table {
                            padding: 10px;
                            border: 1px solid #FCB831;
                        }
                        .t-left {
                            text-align: left;
                        }
                    </style>
                    </head>
                    <body>";

        $message .= '<table>';
        $message .= "<tr style='background: #eee;'><td class='t-left'>";
        $message .= "</td></tr>";
        $message .= "<tr><td style='background: #19164D; height: 20px; text-align: center; color:white;'><strong>" . $subject . "</strong></td></tr>";
        $message .= "<tr><td class='t-left'>Dear " . $firstname . ",<br/><br/>";

        $message .= "$sent_message";
        $message .= "$message_footer";
        $message .= "</td></tr></table>";
        $message .= "</body></html>";

        //attach the logo
        $this->email->from(FROM_EMAIL, EMAIL_SENDER_NAME);
        $this->email->to($email);
        $this->email->bcc('jephthah.efereyan@upperlink.ng');
        $this->email->set_mailtype('html');
        $this->email->subject($subject);
        $this->email->message($message);
        $email_resp = $this->email->send();
        $this->email->clear();

        //log mail temporarily
        $insertData = array(
            'email' => "$email",
            'message' => "$message",
            'subject' => "$subject"
        );
        $this->db->insert('mail_log', $insertData);

        return $email_resp;
    }   //END sendTokenEMail()

    /**
     * Send token in an SMS to a user.
     *
     * @param string $token
     * @param array $recipient
     */
    public function sendTokenSMS($token, $recipient)
    {
        $recipient = is_array($recipient) ? implode(",", $recipient) : $recipient;

        $sent_message = "BAMS Transaction Token. Please use the transaction token below to complete transaction within ten minutes. TOKEN: $token";

        return $this->basic_functions->sendSMS($recipient, EMAIL_SENDER_NAME, $sent_message);

    }   //END sendTokenSMS()

    /**
     * Verify the token sent to authorize a payment.
     *
     * @param $token
     * @param $payment_file_group_id
     * @return bool
     */
    public function validateToken($token, $payment_file_group_id)
    {
        $query = "SELECT * FROM merchant_token WHERE payment_file_group_id = $payment_file_group_id AND token = '$token' AND usedStatus = '0'  AND ADDTIME(`tokenTIme`,'00:10:00.00') > NOW()";

        $result = $this->db->query($query);
        if ($result->num_rows() == 0) {
            return false;
        } else {
            $query_up = "UPDATE merchant_token SET usedStatus = '1', dateUsed = NOW() WHERE token = '$token'";
            $this->db->query($query_up);
            return true;
        }
    }

    public function requeryTransaction($BatchDetailsNumber)
    {

        $client = new SoapClient("https://orion.interswitchng.com/transactionthrottler/transactionthrottlerservice.svc?wsdl");
        //$client = new SoapClient("http://localhost/staging/interswitch.wsdl");
        $TerminalId = "3UPP0001";
        $SponsorCode = "UPP";
        $TransactionCode = "12341";

        $dstring = <<<XML
<?xml version="1.0" encoding="utf-8" ?>
<RequestDetails>
<TerminalId>$TerminalId</TerminalId>
<BatchNumber>$BatchDetailsNumber</BatchNumber>
</RequestDetails>
XML;
//echo $dstring;
//echo "<br>";
        $params2 = array('xmlParams' => $dstring);
        $objectresult2 = $client->CheckTransactionStatus($params2);
        echo "<!--";
        var_dump($objectresult2);
        echo "-->";
        $result2 = $objectresult2->CheckTransactionStatusResult;
        //echo "<br>$result2<br>";
        $xml2 = new SimpleXMLElement($result2);
        //echo $xml2;
        $Number = $xml2->Transactions['Number'];
        $k = 0;
        foreach ($xml2->Transactions->Transaction as $drecord) {
            $k++;
            $ReferenceNumber = $drecord['ReferenceNumber'];
            $Status = $drecord['Status'];
            $Attempts = $drecord['Attempts'];
            $FinalResponseCode = $drecord['FinalResponseCode'];
            $Amount = $drecord['Amount'];
//convert to naira
            $Amount = $Amount / 100;

            @$famount = $Amount;
            $famount = number_format($famount, "2");
//echo $Status;
//echo "<br>";
            if ($Status == "2") {
                $update = "UPDATE `transaction` SET `statusDescription` = 'Paid',`paymentStatus` = '00' WHERE `batchNo` = '$BatchDetailsNumber' AND  transNo = '$ReferenceNumber'";
//echo $update;
                $results = mysql_query($update);
            } elseif ($Status == "3") {
                $update = "UPDATE `transaction` SET `statusDescription` = 'Failed',`paymentStatus` = '02' WHERE `batchNo` = '$BatchDetailsNumber' AND transNo = '$ReferenceNumber'";
                $results = mysql_query($update);
            } elseif ($Status == "1") {

                $update = "UPDATE `transaction` SET `statusDescription` = 'Pending',`paymentStatus` = '01' WHERE `batchNo` = '$BatchDetailsNumber' AND transNo = '$ReferenceNumber'";
                $results = mysql_query($update);
            } elseif ($Status == "0") {
                $update = "UPDATE `transaction` SET `statusDescription` = 'Failed',`paymentStatus` = '02' WHERE `batchNo` = '$BatchDetailsNumber' AND transNo = '$ReferenceNumber'";
                $results = mysql_query($update)
                or die(mysql_error());
            } else {
                $update = "UPDATE `transaction` SET `statusDescription` = 'Pending',`paymentStatus` = '01' WHERE `batchNo` = '$BatchDetailsNumber' AND transNo = '$ReferenceNumber'";
                $results = mysql_query($update);
            }
        }

    }

    public function getUsersAccountMandate()
    {
        $retVal = array();

        $userId = $_SESSION['myuserId'];
        $whereArray = array('a.userId' => "$userId", 'a.mandateStatus' => '1');

        $result = $this->db->select('a.mandateLimit, a.merchantAccountId,  m.accountNo, b.bankName', false)
            ->from('merchant_account_mandate a')
            ->join('merchant_account m', "m.merchantAccountId = a.merchantAccountId", 'LEFT')
            ->join('banks b', "b.bankId = m.bankId", 'LEFT')
            ->where($whereArray)
            ->get();

        if ($result->num_rows() > 0) {
            $retVal = $result->result_array();
        }

        return $retVal;
    }

    public function getAccountMandateLimit($sourceAccount, $userId)
    {
        $retVal = 0;

        $whereArray = array('a.userId' => "$userId", 'a.merchantAccountId' => "$sourceAccount");

        $result = $this->db->select('a.mandateLimit', false)
            ->from(' merchant_account_mandate a')
            ->where($whereArray)
            ->get();

        if ($result->num_rows() > 0) {
            $row = $result->row_array();
            $retVal = $row['mandateLimit'];
        }

        return $retVal;
    }

    public function getMandate($paymentFileId)
    {
        $whereArray = array('p.paymentFileId' => "$paymentFileId");
        $result = $this->db->select("p.*, m.accountNo, b.bankName, u.user_surname, u.user_firstname")
            ->from('payment_authorize p')
            ->join('merchant_account m', "m.merchantAccountId = p.merchantAccountId", "LEFT")
            ->join('banks b', "b.bankId = m.bankId", 'LEFT')
            ->join('upl_users u', "u.user_id = p.userId")
            ->where($whereArray)
            ->get();

        $retVal = array();
        if ($result->num_rows() > 0) {
            $retVal = $result->result_array();
        }

        return $retVal;
    }

    public function doMandate($paymentFileId, $sourceAccount)
    {
        $retVal = true;

        $userId = $_SESSION['myuserId'];
        $insertArray = array(
            'paymentFileId' => "$paymentFileId",
            'merchantAccountId' => "$sourceAccount",
            'userId' => "$userId",
            'authorized' => "1"
        );

        $this->db->replace('payment_authorize', $insertArray);

        return $retVal;
    }

    public function confirmSignatories($paymentFileId, $sourceAccount)
    {
        $retVal = false;

        $where = array('merchantAccountId' => $sourceAccount);
        $query = $this->db->select('minimumSignatory')
            ->from('merchant_account')
            ->where($where)
            ->get();

        $minimumSignatory = 0;
        if ($query->num_rows() > 0) {
            $sigArray = $query->row_array();
            $minimumSignatory = $sigArray['minimumSignatory'];
        }

        $whereArray = array('paymentFileId' => "$paymentFileId", 'merchantAccountId' => "$sourceAccount");
        $this->db->where($whereArray);
        $this->db->from('payment_authorize');
        $numSignatories = $this->db->count_all_results();

        if ($numSignatories >= $minimumSignatory) {
            $retVal = true;
        }

        return $retVal;
    }

    public function authotrizePaymentFile($paymentFileId)
    {
        try {
            $retVal = true;
            $this->db->trans_begin();
            $now = date("Y-m-d H:i:s");
            $set = array('paymentStatus' => PAYMENT_AUTHORIZED, 'modifiedBy' => $_SESSION['myuserId'], 'modifiedDate' => "$now");
            $where = array('paymentFileId' => $paymentFileId);
            $this->db->update("payment_file", $set, $where);
            //insert into schedule status
            $insertArray = array('paymentFileId' => "$paymentFileId", 'scheduleStatus' => PAYMENT_AUTHORIZED,
                'statusReason' => "", 'statusDate' => "$now", 'modifiedBy' => $_SESSION['myuserId']);
            $this->db->insert('schedule_status', $insertArray);
            $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $retVal = false;
        }

        return $retVal;
    }

    public function checkBalance($batchNo)
    {
        //$client_id = 'IKIADAD11343B9F4BEB648C6AA891D07C65F1B4F1A30';
        // $secret_key= '/lYQkCvub/3DpIYWgExiq/CHvvSVpOr0gfIYRNernyw=';
        $http_verb = 'GET';
        $content_type = 'application/json';
        $u_url = 'https://sandbox.interswitchng.com/api/v1/cards/balances';

        $percent_encoded_url = urlencode($u_url);
        $timestamp = time();
        $nonce = substr(str_shuffle(MD5(microtime())), 0, 10);

        $value1 = $http_verb . '&' . $percent_encoded_url . '&' . $timestamp . '&' . $nonce . '&' . ISW_CLIENTID . '&' . ISW_SECRETKEY;

        $auth = base64_encode($client_id);

        $signature = base64_encode(sha1($value1, true));

        $sourceAccountArray = $this->getBatchSourceAccount($batchNo);
        $sourceCardPan = $this->basic_functions->card_decrypt($sourceAccountArray->cardPAN);
        $sourcePinBlock = $this->basic_functions->card_decrypt($sourceAccountArray->cardPinBlock);
        $cardExpiryDate = $sourceAccountArray->cardExpiryDate;
        $expiryArray = explode("-", $cardExpiryDate);
        $yymm = substr($expiryArray['0'], 2, 2) . "" . $expiryArray[1];
        $auth_data = $this->getAuthData($sourcePinBlock, $sourceCardPan, $yymm);
        $header = array(
            "Content-Type: $content_type",
            "Authorization: InterswitchAuth $auth",
            "Signature: $signature",
            "Nonce: $nonce",
            "Timestamp: $timestamp",
            "SignatureMethod: SHA1",
            "TERMINAL_ID: 3UPP0001",
            "PRODUCT_CODE: AUTOPAY",
            "AUTH_DATA: $auth_data"
        );
    }
    //
//generateINFile
    public function generateXML()
    {
        $retVal = array(false, "Error Connecting to Payment Gateway");
        $merchantName = $_SESSION['mymerchantName'];
        $batchNo = $_SESSION['myBatchNo'];
        $TerminalId = "3UPP0001";

        $sourceAccountArray = $this->getBatchSourceAccount($batchNo);
        $bankCode = $sourceAccountArray->bankNibssCode;
        $sourceAccountNo = $sourceAccountArray->accountNo; //"0101111";//
        $sourceCardPan = $this->basic_functions->card_decrypt($sourceAccountArray->cardPAN);
        //$sourceAccountArray->cardPinBlock = "1111";
        if (!empty($sourceAccountArray->cardPinBlock)) {
            $sourcePin = $this->basic_functions->card_decrypt($sourceAccountArray->cardPinBlock);
            $cardExpiryDate = $sourceAccountArray->cardExpiryDate;
            $expiryArray = explode("-", $cardExpiryDate);
            $yymm = substr($expiryArray['0'], 2, 2) . "" . $expiryArray[1];
            $sourceAccountType = "20";

            $securejson = $this->getSecureData($sourcePin, $sourceCardPan, $yymm);

            $secureArray = json_decode($securejson, true);
            // $secureData ="994d693b5a324e47cfb5eccffe08aa28b8fe5664bd489f2ede43b7fd9921813ffbb9025eb7641ee4c5bd6c71881895d799f4d69a35e48614d639e9daf5690a4cc64c537659281547aa80ebaffe54e9b2ea2517da5f5cef2c9204528d896cc99915c4e5d0b63893a4958b6ee04876663dc4960d7b91152768524a69ad650e867799c687e4f7c732a3bee816dbdc629e5787448707573d51ab2c3e90e8bf6b19863cb4dba059f6835af7aa68dc34517643303feae8e88076d9be25a29fab07d3d43a648b160e8c9b086856b0baba67df44849d1fd36d267c8680e64b55c4e0ceebab8bd2ed201e2792eb704b8d93b95bbb15ac9a3be19fea28b021e3df68035f86";//
            //  $encrypted_pin = "44330e51f43a79cd";//
            $secureData = $secureArray['Item1'];
            $encrypted_pin = $secureArray['Item2'];
            $mac_input = "";
            $payArray = $this->getTransactionDetails($batchNo);
            $description = $payArray[0]['paymentFileDescription'];
            $file_content_line_1 = "$batchNo, $description, false, $TerminalId\n";

            $totalAmount = 0;
            $allAccount = "";
            $file_content = "";
            foreach ($payArray AS $u) {
                $description = $u['paymentFileDescription'];
                $beneficiary = $u['fullname'];
                $amount = $u['amount'];
                $TransactionAmount = $amount * 100;
                $totalAmount += $TransactionAmount;
                $TransactionToAccountNumber = $u['beneficiaryAccountNo'];
                $allAccount .= $TransactionToAccountNumber;
                $IS_PrepaidLoad = 'false';
                $Currency_Code = "NGN";
                $Mobile_Number = "0";
                $TransactionReferenceNumber = $u['transNo'];
                $sentTransactionReferenceNumber = $description . "_" . $TransactionReferenceNumber;
                $TransactionToAccountType = '10';
                $testEmail = "segun.ojo@upperlink.ng";
                $TransactionBankName = $u['bankName'];
                $TransactionTransactionType = "50"; //50 means Payment to Account
                $TransactionBankCBNCode = $u['bankNibssCode'];

                $file_content .= "$sentTransactionReferenceNumber,$TransactionAmount,$description,$TransactionToAccountNumber,$testEmail, $TransactionBankCBNCode,$TransactionToAccountNumber, $TransactionToAccountType, $IS_PrepaidLoad,$Currency_Code, $beneficiary,$Mobile_Number \n";

            }
            $password = "$TerminalId$allAccount$totalAmount$allAccount";
            $mac = hash('sha512', $password);
            $file_content_line_2 = "$secureData,$sourceAccountNo, $sourceAccountType, $bankCode, $encrypted_pin, $mac\n";
            $real_file_content = "$file_content_line_1$file_content_line_2$file_content";
            $small_filename = $this->basic_functions->create_random_name(10);
            $filename = strtoupper($small_filename);
            $update_array = array('filename' => "$filename");
            $where_array = array('batchNo' => "$batchNo");
            $this->db->update("transaction", $update_array, $where_array);


            $this->load->helper('file');

            if (!write_file("myfiles/IN/$filename.csv", $real_file_content)) {
                $retVal = array(false, "Error creating $filename.csv");
            } else {
                $this->load->library('Sftplib');
                $sftp = new Net_SFTP(SFTP_HOST);
                if ($sftp->login(SFTP_USERNAME, SFTP_PASSWORD)) {
                    $local_file = "myfiles/IN/$filename.csv";
                    $remote_file = "/IN/$filename.csv";
                    if (file_exists($local_file)) {
                        $upload = $sftp->put($remote_file, $local_file, NET_SFTP_LOCAL_FILE);
                        if (!$upload) {
                            $retVal = array(false, "Transaction Logging Failed");
                        } else {
                            $retVal = array(true, "Payment Successfully Logged");
                            $query3333 = "UPDATE `transaction` SET `sentStatus` = '1' WHERE  `batchNo` = '$batchNo'";
                            $this->db->query($query3333);
                        }
                    } else {
                        $retVal = array(false, "$local_file does not exist!!");
                    }
                } else {
                    $retVal = array(true, "Unable to connect to Payment Gateway");
                }

            }
        } else {
            $retVal = array(false, "Please change Pin first");
        }

        return $retVal;
    }

    private function getPinBlockBase64($PINblock)
    {
        echo $PINblock;
        $TransactionID = $this->basic_functions->create_random_name(12);
        $Salt = "fP3[fI9*pR8}sZ4[fG0@kZ2{tM6)oN7]";
        $Token = base64_encode(base64_encode($Salt) . base64_encode("$Salt" . base64_encode($PINblock)));
        $data = "$TransactionID|$Salt";
        $MAC = hash('sha512', $data);

        $url = 'http://isw.upl.com.ng/ISWFinal/Default.aspx'; //http://isw.upl.com.ng/ISWFinal/Default.aspx
        $fields = array('T1' => urlencode($TransactionID), 'T2' => urlencode($Token), 'T3' => urlencode($MAC));

        $data = http_build_query($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $res = curl_exec($ch);

        $str1 = base64_decode(base64_decode($res));
        $str2 = str_replace($Salt, "", $str1);
        $EncryptPINblock = base64_decode($str2);

        return $EncryptPINblock;
    }

    private function getSecureData($pin, $pan, $expiry)
    {

        $TransactionID = $this->basic_functions->create_random_name(12);

        $Salt = "fP3[fI9*pR8}sZ4[fG0@kZ2{tM6)oN7]";

        $T1 = $TransactionID;
        $T2 = base64_encode(base64_encode($Salt) . base64_encode("$Salt" . base64_encode($pin)));
        $T3 = base64_encode(base64_encode($Salt) . base64_encode("$Salt" . base64_encode($pan)));
        $T4 = base64_encode(base64_encode($Salt) . base64_encode("$Salt" . base64_encode($expiry)));

        $data = "$TransactionID|$Salt";
        $MAC = hash('sha512', $data);
        $T5 = $MAC;

        $url = 'http://isw.upl.com.ng/ISWSecureDataFinal/Default.aspx';
        $fields = array('T1' => urlencode($T1), 'T2' => urlencode($T2), 'T3' => urlencode($T3), 'T4' => urlencode($T4), 'T5' => urlencode($T5));

        $data = http_build_query($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($ch);

//echo "$res<br><br>";
//$Finalres = base64_encode(base64_encode("$Salt" . base64_encode($EncryptPINblock)));

        $str1 = base64_decode(base64_decode($res));
        $str2 = str_replace($Salt, "", $str1);
        $finalstring = base64_decode($str2);
        return $finalstring;
    }

    private function getAuthData($pin, $pan, $expiry)
    {

        $TransactionID = $this->basic_functions->create_random_name(12);

        $Salt = "fP3[fI9*pR8}sZ4[fG0@kZ2{tM6)oN7]";

        $T1 = $TransactionID;
        $T2 = base64_encode(base64_encode($Salt) . base64_encode("$Salt" . base64_encode($pin)));
        $T3 = base64_encode(base64_encode($Salt) . base64_encode("$Salt" . base64_encode($pan)));
        $T4 = base64_encode(base64_encode($Salt) . base64_encode("$Salt" . base64_encode($expiry)));

        $data = "$TransactionID|$Salt";
        $MAC = hash('sha512', $data);
        $T5 = $MAC;

        $url = 'http://isw.upl.com.ng/ISWAuthData/Default.aspx';
        $fields = array('T1' => urlencode($T1), 'T2' => urlencode($T2), 'T3' => urlencode($T3), 'T4' => urlencode($T4), 'T5' => urlencode($T5));

        $data = http_build_query($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($ch);

//echo "$res<br><br>";
//$Finalres = base64_encode(base64_encode("$Salt" . base64_encode($EncryptPINblock)));

        $str1 = base64_decode(base64_decode($res));
        $str2 = str_replace($Salt, "", $str1);
        $finalstring = base64_decode($str2);
        return $finalstring;
    }

    public function generateXMLLIVE()
    {
        $retVal = array(false, "Error Connecting to Payment Gateway");
        $merchantName = $_SESSION['mymerchantName'];
        $batchNo = $_SESSION['myBatchNo'];
        $sourceAccountArray = $this->getBatchSourceAccount($batchNo);
        $sourceBankcode = $sourceAccountArray->bankNibssCode;
        $sourceAccountNo = $sourceAccountArray->accountNo;

        $CardPAN = $this->basic_functions->card_decrypt($sourceAccountArray->cardPAN);
        $SourceCardPIN = $this->basic_functions->card_decrypt($sourceAccountArray->cardPinBlock);
        $pinBlock = $this->GetPinBlock($SourceCardPIN, $CardPAN);
        $CardPinBlock = $this->getPinBlockBase64($pinBlock);
        $cardExpiryDate = $sourceAccountArray->cardExpiryDate;
        $CardTerminalId = $this->basic_functions->card_decrypt($sourceAccountArray->cardTerminalId);
        $dateArray = explode("-", $cardExpiryDate);
        $CardExpiryDay = $dateArray[2];
        $CardExpiryMonth = $dateArray[1];
        $CardExpiryYear = $dateArray[0];
        $CardSequenceNumber = 1;
        $payArray = $this->getTransactionDetails($batchNo);
        $Count = count($payArray);


        $CardTerminalId = "3IGW0BRC";
        try {
            //$rrr = $client = new SoapClient("http://localhost/staging/interswitch.wsdl");
            $rrr = $client = new SoapClient("https://orion.interswitchng.com/transactionthrottler/transactionthrottlerservice.svc?wsdl");
            $TerminalId = "3UPP0001";
            $SponsorCode = "UPP";
            $TransactionCode = "12341";

            $string1 = <<<XML
<?xml version="1.0" encoding="utf-8" ?>
<RequestDetails>
<TerminalId>$TerminalId</TerminalId>
<SponsorCode>$SponsorCode</SponsorCode>
<TransactionCode>$TransactionCode</TransactionCode>
<BatchDetails Number="$batchNo">
<SourceAccountDetails CurrencyCode="566" BankCBNCode="$sourceBankcode">
<Number>$sourceAccountNo</Number>
<Type>20</Type>
<CardPAN>$CardPAN</CardPAN>
<CardPinBlock>$CardPinBlock</CardPinBlock>
<CardExpiryDay>$CardExpiryDay</CardExpiryDay>
<CardExpiryMonth>$CardExpiryMonth</CardExpiryMonth>
<CardExpiryYear>$CardExpiryYear</CardExpiryYear>
<CardSequenceNumber>$CardSequenceNumber</CardSequenceNumber>
<CardTerminalId>$CardTerminalId</CardTerminalId>
</SourceAccountDetails>
<Transactions Count="$Count">
XML;
            $serialNumber = 0;
            $string2 = "";

            //$jsonData = json_encode($payArray);
            //log for report
            //$this->logTransactionForReport($jsonData);
            //die();
            foreach ($payArray AS $u) {
                $serialNumber++;
                $beneficiary = $u['fullname'];
                $amount = $u['amount'];
                $TransactionSurcharge = 10500;
                $TransactionAmount = ($amount * 100) - $TransactionSurcharge;
                $TransactionToAccountNumber = $u['beneficiaryAccountNo'];


                $description = $u['paymentFileDescription'];
                $TransactionReferenceNumber = $u['transNo'];
                $TransactionToAccountType = '10';
                $TransactionBankName = $u['bankName'];
                $TransactionTransactionType = "50"; //50 means Payment to Account
                $TransactionDescription = substr($description, 0, 14) . " " . $sourceAccountNo . " ";
                $TransactionBankCBNCode = $u['bankNibssCode'];
                $password = "$batchNo$TransactionReferenceNumber$TransactionToAccountNumber$TransactionAmount$TransactionBankCBNCode";
                $mac = hash('sha512', $password);
                $string2 .= <<<XML
<Transaction>
<ReferenceNumber>$TransactionReferenceNumber</ReferenceNumber>
<TransactionType>$TransactionTransactionType</TransactionType>
<ToAccountNumber>$TransactionToAccountNumber</ToAccountNumber>
<ToAccountType>$TransactionToAccountType</ToAccountType>
<Amount>$TransactionAmount</Amount>
<Surcharge>$TransactionSurcharge</Surcharge>
<Description>$TransactionDescription</Description>
<Narration>$TransactionDescription</Narration>
<BankName>$TransactionBankName</BankName>
<BankCBNCode>$TransactionBankCBNCode</BankCBNCode>
<MAC>$mac</MAC>
</Transaction>
XML;

            }
            $string3 = "
</Transactions>
</BatchDetails>
</RequestDetails>
";


            $string = "$string1$string2$string3";

            $params = array('xmlParams' => $string);
            //print_r($params); //die();

            $query3333 = "UPDATE `transaction` SET `sentStatus` = '1' WHERE  `batchNo` = '$batchNo'";
            $this->db->query($query3333);

            $objectresult = $client->ProcessTransactions($params);

            $result = $objectresult->ProcessTransactionsResult;

            $xml = new SimpleXMLElement($result);

            $ResponseCode = (string)$xml->ResponseCode;

            if ($ResponseCode == "90000") {
                //update to still processing
                $today = date("Y-m-d H:i:s");
                $tommorrow = "SELECT DATE_ADD('$today',INTERVAL 1 DAY) AS dday";
                $resulttt = $this->db->query($tommorrow);
                $rowday = $resulttt->row();
                $dday = $rowday->dday;
                $queryupdate = "UPDATE `transaction` SET `statusDescription` = 'Pending',`paymentStatus` = '01' WHERE `batchNo` = '$batchNo'";
                $this->db->query($queryupdate);
                $retVal = array(true, "Payment File Completely Received");
            } else {
                $whereUpdate = array('batchNo' => "$batchNo");
                $setArray = array('paymentStatus' => '02', 'statusDescription' => "Error Logging Transaction");
                $this->db->update("transaction", $setArray, $whereUpdate);
                $retVal = array(false, "Error Logging Transaction");
            }

        } catch (Exception $ex) {
            $whereUpdate = array('batchNo' => "$batchNo");
            $setArray = array('paymentStatus' => '02', 'statusDescription' => "Cannot Connect");
            $this->db->update("transaction", $setArray, $whereUpdate);
            $retVal = array(false, "Cannot Connect to Payment Gateway");
        }

        return $retVal;
    }

    public function generateXMLTest()
    {
        $retVal = array(false, "Error Connecting to Payment Gateway");

        $merchantName = $_SESSION['mymerchantName'];
        $batchNo = $_SESSION['myBatchNo'];
        $surcharge = 10;
        $sourceAccountArray = $this->getBatchSourceAccount($batchNo);

        $sourceBankcode = $sourceAccountArray->bankNibssCode; //"001";//

        $sourceAccountNo = $sourceAccountArray->accountNo; //"0101111";//
        $CardPAN = $this->basic_functions->card_decrypt($sourceAccountArray->cardPAN);
        $CardPinBlock = $this->basic_functions->card_decrypt($sourceAccountArray->cardPinBlock);
        $cardExpiryDate = $sourceAccountArray->cardExpiryDate;
        $CardTerminalId = $this->basic_functions->card_decrypt($sourceAccountArray->cardTerminalId);
        //echo $cardExpiryDate;
        $dateArray = explode("-", $cardExpiryDate);
        $CardExpiryDay = $dateArray[2];
        $CardExpiryMonth = $dateArray[1];
        $CardExpiryYear = $dateArray[0];
        $CardSequenceNumber = 1;
        $payArray = $this->getTransactionDetails($batchNo);
        $Count = count($payArray);
        $payer = $merchantName;
        //echo "here";
        // die();
        $sourceAccountNo = "0124578963";
        $sourceBankcode = "057";
        $CardPAN = "6280511000000095";//$rowb['CardPAN'];
        $CardPinBlock = "GKIN+bWijeWb5gR09wPVeA==";//base64_encode($this->GetPinBlock("0000",$CardPAN));//$rowb['CardPinBlock'];
        $CardExpiryDay = "01";;//$rowb['CardExpiryDay'];
        $CardExpiryMonth = "12";//$rowb['CardExpiryMonth'];
        $CardExpiryYear = "2026";//$rowb['CardExpiryYear'];
        $CardTerminalId = "3IAP0044";
        try {
            $rrr = $client = new SoapClient("http://localhost/staging/interswitch.wsdl");
            $TerminalId = "3UPP0001";
            $SponsorCode = "UPP";
            $TransactionCode = "12341";

            $string1 = <<<XML
<?xml version="1.0" encoding="utf-8" ?>
<RequestDetails>
<TerminalId>$TerminalId</TerminalId>
<SponsorCode>$SponsorCode</SponsorCode>
<TransactionCode>$TransactionCode</TransactionCode>
<BatchDetails Number="$batchNo">
<SourceAccountDetails CurrencyCode="566" BankCBNCode="$sourceBankcode">
<Number>$sourceAccountNo</Number>
<Type>20</Type>
<CardPAN>$CardPAN</CardPAN>
<CardPinBlock>$CardPinBlock</CardPinBlock>
<CardExpiryDay>$CardExpiryDay</CardExpiryDay>
<CardExpiryMonth>$CardExpiryMonth</CardExpiryMonth>
<CardExpiryYear>$CardExpiryYear</CardExpiryYear>
<CardSequenceNumber>$CardSequenceNumber</CardSequenceNumber>
<CardTerminalId>$CardTerminalId</CardTerminalId>
</SourceAccountDetails>
<Transactions Count="$Count">
XML;
            $serialNumber = 0;
            $string2 = "";

            //$jsonData = json_encode($payArray);
            //log for report
            //$this->logTransactionForReport($jsonData);


            //die();
            foreach ($payArray AS $u) {
                $serialNumber++;
                $beneficiary = $u['fullname'];
                $amount = $u['amount'];
                $TransactionAmount = $amount * 100;
                $TransactionToAccountNumber = $u['beneficiaryAccountNo'];
                $sortCode = $u['bankNibssCode'] . "150000";
                $TransactionSurcharge = 10500;
                $description = $u['paymentFileDescription'];
                $TransactionReferenceNumber = $u['transNo'];
                $TransactionToAccountType = '10';
                $TransactionBankName = $u['bankName'];
                $TransactionTransactionType = "50"; //50 means Payment to Account
                $TransactionDescription = substr($description, 0, 14) . " " . $sourceAccountNo . " ";
                $TransactionBankCBNCode = $u['bankNibssCode'];
                $password = "$batchNo$TransactionReferenceNumber$TransactionToAccountNumber$TransactionAmount$TransactionBankCBNCode";
                $mac = hash('sha512', $password);
                $string2 .= <<<XML
<Transaction>
<ReferenceNumber>$TransactionReferenceNumber</ReferenceNumber>
<TransactionType>$TransactionTransactionType</TransactionType>
<ToAccountNumber>$TransactionToAccountNumber</ToAccountNumber>
<ToAccountType>$TransactionToAccountType</ToAccountType>
<Amount>$TransactionAmount</Amount>
<Surcharge>$TransactionSurcharge</Surcharge>
<Description>$TransactionDescription</Description>
<Narration>$TransactionDescription</Narration>
<BankName>$TransactionBankName</BankName>
<BankCBNCode>$TransactionBankCBNCode</BankCBNCode>
<MAC>$mac</MAC>
</Transaction>
XML;

            }
            $string3 = "
</Transactions>
</BatchDetails>
</RequestDetails>
";


            $username = "upperlink";
            $password = "upperlink";
            $serialNumber = rand(100, 500);//$this->basic_functions->create_random_number(5);
            $neftfilename = $this->basic_functions->create_random_string(5);
            // $filename = strtoupper($filename);
            $mtoday = date("Y-m-d");


            $string = "$string1$string2$string3";
            $lengthsize = strlen($string);
            //$sourceBankcode = '214';
            //$sourceAccountNo = "0208356010";
            $params = array('xmlParams' => $string);
//echo $string;
            $query3333 = "UPDATE `transaction` SET `sentStatus` = '1' WHERE  `batchNo` = '$batchNo'";
            $this->db->query($query3333);

            $objectresult = $client->ProcessTransactions($params);

            $result = $objectresult->ProcessTransactionsResult;

            $xml = new SimpleXMLElement($result);
            //var_dump($result);
            $ResponseCode = (string)$xml->ResponseCode;

            if ($ResponseCode == "90000") {
                //update to still processing
                $today = date("Y-m-d H:i:s");
                $tommorrow = "SELECT DATE_ADD('$today',INTERVAL 1 DAY) AS dday";
                $resulttt = $this->db->query($tommorrow);
                $rowday = $resulttt->row();
                $dday = $rowday->dday;
                $queryupdate = "UPDATE `transaction` SET `statusDescription` = 'Pending',`paymentStatus` = '01' WHERE `batchNo` = '$batchNo'";
                $this->db->query($queryupdate);
                $retVal = array(true, "Payment File Completely Received");
            } else {
                $whereUpdate = array('batchNo' => "$batchNo");
                $setArray = array('paymentStatus' => '02', 'statusDescription' => "Error Logging Transaction");
                $this->db->update("transaction", $setArray, $whereUpdate);
                $retVal = array(false, "Error Logging Transaction");
            }

        } catch (Exception $ex) {
            $whereUpdate = array('batchNo' => "$batchNo");
            $setArray = array('paymentStatus' => '02', 'statusDescription' => "Cannot Connect");
            $this->db->update("transaction", $setArray, $whereUpdate);
            $retVal = array(false, "Cannot Connect to Payment Gateway");
        }
        //print_r($retVal);
        // die();
        return $retVal;
    }


    public function uploadSchedule($payment_file)
    {
        set_time_limit(0);

        $this->load->helper('fluid_xml');

        try {
            $clientID = CSS_CLIENT_ID;
            $secret_key = CSS_SECRET_KEY;

            $batchNo = $payment_file['transaction_batch']['batchNo'];

            $salt = rand(100000, 1000000000);
            $str2hash = "{$clientID}-{$secret_key}-{$salt}";
            $mac = hash('sha512', $str2hash);

            $xml = new \FluidXml\FluidXml('PaymentRequest');

            $xml->addChild('Header', true)
                ->addChild('FileName', $payment_file['transaction_batch']['batchNo'])
                ->addChild('ScheduleId', $payment_file['transaction_batch']['schedule_id'])
                ->addChild('DebitSortCode', $payment_file['source_account']['bank']['bankNibssCode'])
                ->addChild('DebitAccountNumber', $payment_file['source_account']['acc_number'])
                ->addChild('ClientId', $clientID)
                ->addChild('Salt', $salt)
                ->addChild('Mac', $mac);

            /*$xml->addChild('CommissionRecord', true)
                ->addChild('Beneficiary', ' ')
                ->addChild('Amount', ' ')
                ->addChild('AccountNumber', ' ')
                ->addChild('SortCode', ' ')
                ->addChild('Narration', ' ');*/

            foreach ($payment_file['beneficiaries'] as $beneficiary) {
                $beneficiaryAccName = trim($beneficiary['beneficiarySurname'] . " " . $beneficiary['beneficiaryFirstname'] . " " . $beneficiary['beneficiaryOthernames']);

                $xml->addChild('PaymentRecord', true)
                    ->addChild('Beneficiary', $beneficiaryAccName)
                    ->addChild('Amount', $beneficiary['amount'])
                    ->addChild('AccountNumber', $beneficiary['beneficiaryAccountNo'])
                    ->addChild('SortCode', $beneficiary['bank']['bankNibssCode'])
                    ->addChild('Narration', $payment_file['paymentFileDescription']);
            }

            $xml->addChild('HashValue', ' ');
            $request = $xml->xml();

            file_put_contents('debug.txt', $request . "\n\n");

            //make the Webservice call to Upload the Payment Schedule
            try {
                $client = new SoapClient(CSS_WEBSERVICE_URL, array("trace" => 1, "exceptions" => 1));
                //die('<pre>'.print_r($client,1));
                $response = $client->uploadPaymentSchedule($request);
                //die("Response: <pre>".print_r($response,1));
            } catch (Exception $e) {
                $error = $e->getMessage();
                die("Error:<br>" . $error);
            }

            //die("jefe");
            //log the Webservice call request and response
            $log_data = array('method' => 'uploadPaymentSchedule', 'request' => $request, 'response' => $response);
            $this->logPaymentWebservicecall($log_data);

            if (!$response) {
                throw new Exception('NIP Gateway DOWN!!!!');
            }

            //check the response
            $response_obj = simplexml_load_string($response);
            $response_code = $response_obj->Header->Status;

            if (strlen($response_code) > 2) {
                throw new Exception('NIP Gateway DOWN!!!!');
            }

            //flag all the transactions in this batch as SENT
            //this is to keep in harmony with existing flow
            $data = array(
                'batchNo' => "$batchNo",
                'paymentStatus' => '01',
                'responseCode' => '',
                'statusDescription' => 'Pending',
                'sentStatus' => '1'
            );
            $this->updateTransactionsInBatch($data);

            //get the description for the status of the response
            $desc = $this->getNibssPayPlusResponseDescription($response_code);

            $status = $response_code != '16' ? '2' : '0';

            //update the transaction batch
            $batch_update_data = array(
                'batchNo' => "$batchNo",
                'responseCode' => $response_code,
                'responseDescription' => $desc,
                'status' => $status
            );
            $this->updateTransactionBatch($batch_update_data);

            //if the schedule was not successfully uploaded, update all transactions in the batch to FAILED
            if ($status == '2') {
                $batch_transactions_update_data = array(
                    'batchNo' => "$batchNo",
                    'paymentStatus' => '02',
                    'responseCode' => $response_code,
                    'statusDescription' => $desc
                );
                $this->updateTransactionsInBatch($batch_transactions_update_data);
            }
            $ret = $response_code == '16' ? true : false;
            $retVal = array($ret, $desc);
        } catch (Exception $ex) {
            /*$batch_transactions_update_data = array(
                'batchNo' => "$batchNo",
                'paymentStatus' => '02',
                'statusDescription' => $ex->getMessage()
            );
            $this->updateTransactionsInBatch($batch_transactions_update_data);

            $batch_update_data = array(
                'batchNo' => "$batchNo",
                'responseDescription' => $ex->getMessage(),
                'status' => "2"
            );
            $this->updateTransactionBatch($batch_update_data);*/

            $message = $ex->getMessage();

            $retVal = array(false, !empty($message) ? $message : "Cannot Connect to Payment Gateway");
        }

        return $retVal;
    }

    public function generateXMLNIBSS()
    {

        $retVal = array(false, "Error Connecting to Payment Gateway");

        $merchantName = "Edo State Government";
        $batchNo = $_SESSION['myBatchNo'];
        $surcharge = 10;
        $sourceAccountArray = $this->getBatchSourceAccount($batchNo);

        $sourceBankcode = $sourceAccountArray->bankNibssCode;

        $sourceAccountNo = $sourceAccountArray->accountNo;

        $payer = $merchantName;
        try {
            // for test
            $rrr = $client = new SoapClient("http://webserver.nibss-plc.com/NIBSS-FEWS-ABC/NIBSS-FEWS.asmx?WSDL");
            $string1 = <<<XML
<?xml version="1.0" encoding="utf-8" ?>
<NibbsPaymentCollection>
XML;
            $serialNumber = 0;
            $string2 = "";
            $payArray = $this->getTransactionDetails($batchNo);
            $jsonData = json_encode($payArray);
            //log for report
            // $this->logTransactionForReport($jsonData);

            //die();
            foreach ($payArray AS $u) {
                $serialNumber++;
                $beneficiary = $u['fullname'];
                $amount = $u['amount'];
                $accountNumber = $u['beneficiaryAccountNo'];
                $sortCode = $u['bankNibssCode'] . "150000";
                $description = $u['paymentFileDescription'];
                $fileNumber = $u['transactionId'];
                $TransactionDescription = substr($description, 0, 14) . " " . $sourceAccountNo . " ";

                $string2 .= <<<XML
<ns1:NibbsPayment>
<ns1:serialNumber>$serialNumber</ns1:serialNumber>
<ns1:payer>$payer</ns1:payer>
<ns1:beneficiary>$beneficiary</ns1:beneficiary>
<ns1:amount>$amount</ns1:amount>
<ns1:accountNumber>$accountNumber</ns1:accountNumber>
<ns1:sortCode>$sortCode</ns1:sortCode>
<ns1:transactionDescription>$TransactionDescription</ns1:transactionDescription>
</ns1:NibbsPayment>
XML;

            }
            $string3 = "</NibbsPaymentCollection>";
            $merchantCode = "edo";
            $code_query = "SELECT `code` FROM nibss_codes WHERE accountNo = '$sourceAccountNo'";
            $resultCode = $this->db->query($code_query);
            if ($resultCode->num_rows()) {
                $row_code = $resultCode->row_array();
                $merchantCode = strtolower($row_code['code']);
            }

            $password = "upperlink";
            $username = "upperlink_" . $merchantCode;
            // $fileNumber = rand(100,500);//$this->basic_functions->create_random_number(5);
            $neftfilename = $this->basic_functions->create_random_string(5);
            // $filename = strtoupper($filename);
            $mtoday = date("Y-m-d");


            $filenameprefix = "paychoice_" . $merchantCode . "$mtoday$fileNumber^";
            //$filenameprefix = "paychoice$mtoday^";
            $filename = "$filenameprefix$neftfilename.xml";
            $string = "$string1$string2$string3";
            $lengthsize = strlen($string);
            //$sourceBankcode = '214';
            //$sourceAccountNo = "0208356010";
            $params = array('strUserName' => $username, 'strPassword' => $password, 'lngFileSize' => $lengthsize, 'strFileName' => $filename, 'strFileContent' => $string, 'strBankCode' => $sourceBankcode, 'strAcc' => $sourceAccountNo, 'dbl_Pay' => $surcharge);
            //var_dump($params);
            //die();
            $query3333 = "UPDATE `transaction` SET `sentStatus` = '1' WHERE `batchNo` = '$batchNo'";
            $this->db->query($query3333);

            $objectresult = $client->sendData_ACC_Bill($params);
            //  var_dump($objectresult);

            $ret_message = $objectresult->sendData_ACC_BillResult;

            if ($ret_message == "File completely received") {
                //update to still processing
                $today = date("Y-m-d H:i:s");
                $tommorrow = "SELECT DATE_ADD('$today',INTERVAL 1 DAY) AS dday";
                $resulttt = $this->db->query($tommorrow);
                $rowday = $resulttt->row();
                $dday = $rowday->dday;
                $queryupdate = "UPDATE `transaction` SET `statusDescription` = 'SETTLEMENT EXPECTED BY $dday',`paymentStatus` = '01' WHERE `batchNo` = '$batchNo'";
                $this->db->query($queryupdate);
                $retVal = array(true, "Payment File Completely Received");
            } else {
                $whereUpdate = array('batchNo' => "$batchNo");
                $setArray = array('paymentStatus' => '02', 'statusDescription' => "$ret_message");
                $this->db->update("transaction", $setArray, $whereUpdate);
                $retVal = array(false, $ret_message);
            }

        } catch (Exception $ex) {
            $whereUpdate = array('batchNo' => "$batchNo");
            $setArray = array('paymentStatus' => '02', 'statusDescription' => "Cannot Connect");
            $this->db->update("transaction", $setArray, $whereUpdate);
            $retVal = array(false, "Cannot Connect to Payment Gateway");
        }
        return $retVal;
    }

    function CallAPI($method, $url, $header, $data = false)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    public function GetPinBlock($pin, $pan)
    {
        $paddedPIN = "0" . strlen($pin) . "$pin" . "ffffffffff";
        $len = strlen($pan);
        $startIndex = $len - 13;
        $endLength = $len - 1 - $startIndex;
        $paddedPAN = "0000" . substr($pan, $startIndex, $endLength);
        $pinBlock = "";
        for ($i = 0; $i < strlen($paddedPAN); $i++) {
            $xorResult = intval("0x" . $paddedPAN[$i], 16) ^ intval("0x" . $paddedPIN[$i], 16);
            $pinBlock .= sprintf("%0x", $xorResult);
        }
        return strtoupper($pinBlock);
    }

    public function doGetBalance($merchantAccountId)
    {
        $retVal = array(false, "Error Checking Balance");
        $account = $this->getAccountDetail($merchantAccountId);
        $cardPAN = $this->basic_functions->card_decrypt($account['cardPAN']);
        $cardPin = $this->basic_functions->card_decrypt($account['cardPinBlock']);

        if (!empty($cardPin)) {
            $cardExpiryDate = $account['cardExpiryDate'];
            $expiryArray = explode("-", $cardExpiryDate);
            $expiryDate = substr($expiryArray['0'], 2, 2) . "" . $expiryArray[1]; //yymm
            $http_verb = 'GET';
            $content_type = 'application/json';
            $u_url = 'https://saturn.interswitchng.com/api/v1/cards/balances';

            $percent_encoded_url = urlencode($u_url);
            $timestamp = time();
            $nonce = substr(str_shuffle(MD5(microtime())), 0, 10);


            $plain_signature = $http_verb . '&' . $percent_encoded_url . '&' . $timestamp . '&' . $nonce . '&' . ISW_CLIENTID . '&' . ISW_SECRETKEY;

            $auth = base64_encode(ISW_CLIENTID);
            $signature = base64_encode(sha1($plain_signature, true));


            $auth_data = $this->getAuthData($cardPin, $cardPAN, $expiryDate);
            $header = array(
                "Content-Type: $content_type",
                "Authorization: InterswitchAuth $auth",
                "Signature: $signature",
                "Nonce: $nonce",
                "Timestamp: $timestamp",
                "SignatureMethod: SHA1",
                "TERMINAL_ID: 3UPP0001",
                "PRODUCT_CODE: AUTOPAY",
                "AUTH_DATA: $auth_data"
            );

            $response = $this->CallAPI("GET", $u_url, $header);
            var_dump($response);

            if (is_string($response) && is_array(json_decode($response, true))) {
                //its a valid json
                $changeArray = json_decode($response, true);
                if (isset($changeArray['balance'])) {
                    $retVal = array(true, $changeArray['balance']);
                } else {
                    $code = (!empty($changeArray['errors'][0]['code'])) ? $changeArray['errors'][0]['code'] : "";
                    $message = (!empty($changeArray['errors'][0]['message'])) ? "<br/>Message - " . $changeArray['errors'][0]['message'] : "";
                    if (!empty($code)) {
                        $responseDescriptionArray = $this->payment->responseDescription($code);
                        $responseDescription = !empty($responseDescriptionArray['description']) ? $responseDescriptionArray['description'] : "$code";

                        $code_description = "<br/>Description - " . $responseDescription;
                    }
                    $retVal = array(false, "Error checking balance $message $code_description");
                }
            }

        } else {
            $retVal = array(false, "Please change PIN first");
        }
        return $retVal;
    }

    public function doPinChange($merchantAccountId, $oldPIN, $newPIN)
    {
        $account = $this->getAccountDetail($merchantAccountId);
        $cardPAN = $this->basic_functions->card_decrypt($account['cardPAN']);
        $cardExpiryDate = $account['cardExpiryDate'];
        $expiryArray = explode("-", $cardExpiryDate);
        $expiryDate = substr($expiryArray['0'], 2, 2) . "" . $expiryArray[1]; //yymm
        $http_verb = 'PUT';
        $content_type = 'application/json';
        $u_url = 'https://saturn.interswitchng.com/api/v1/cards/pinchange';


        $percent_encoded_url = urlencode($u_url);
        $timestamp = time();
        $nonce = substr(str_shuffle(MD5(microtime())), 0, 10);

        $plain_signature = $http_verb . '&' . $percent_encoded_url . '&' . $timestamp . '&' . $nonce . '&' . ISW_CLIENTID . '&' . ISW_SECRETKEY;

        $auth = base64_encode(ISW_CLIENTID);

        $signature = base64_encode(sha1($plain_signature, true));
        //echo  "$oldPIN".$cardPAN. $expiryDate;
        $oldAuthData = $this->getAuthData("$oldPIN", $cardPAN, $expiryDate);
        $newAuthData = $this->getAuthData("$newPIN", $cardPAN, $expiryDate);
        $header = array(
            "Content-Type: $content_type",
            "Authorization: InterswitchAuth $auth",
            "Signature: $signature",
            "Nonce: $nonce",
            "Timestamp: $timestamp",
            "SignatureMethod: SHA1"
        );

        $dataArray = array('oldAuthData' => $oldAuthData,
            'newAuthData' => $newAuthData,
            'terminalId' => "3UPP0001",
            'productCode' => "AUTOPAY");
        $data = json_encode($dataArray);

        $changeNowJson = $this->CallAPI("PUT", $u_url, $header, $data);
        //echo $changeNowJson;
        return $changeNowJson;

    }

    public function getAccountDetail($merchantAccountId)
    {
        $userId = $_SESSION['myuserId'];
        $merchantId = $_SESSION['mymerchantId'];
        $retVal = array();
        $whereArray = array('a.userId' => "$userId", 'a.mandateStatus' => '1', 'm.merchantAccountId' => "$merchantAccountId");
        $result = $this->db->select('b.bankName,m.merchantId, a.mandateLimit, m.accountNo, a.merchantAccountId,c.cardPAN,c.cardPinBlock,c.cardExpiryDate,c.cardTerminalId', false)
            ->from(' merchant_account_mandate a')
            ->join('merchant_account m', "m.merchantAccountId = a.merchantAccountId", 'LEFT')
            ->join('banks b', "b.bankId = m.bankId", 'LEFT')
            ->join('card_details c', "c.merchantAccountId = m.merchantAccountId", 'INNER')
            ->where($whereArray)
            ->get();
//echo $this->db->last_query();

        if ($result->num_rows() == 1) {
            $retVal = $result->row_array();
        }
        return $retVal;

    }

    public function updatePIN($merchantAccountId, $newPIN)
    {
        $updated = 0;
        try {
            $this->db->trans_begin();
            $now = date("Y-m-d H:i:s");

            $encNewPIN = $this->basic_functions->card_encrypt($newPIN);
            $card_array = array(
                'cardPinBlock' => "$encNewPIN",
                'modifiedBy' => $_SESSION['myuserId'],
                'dateModified' => "$now"
            );
            $where_array = array(
                'merchantAccountId' => "$merchantAccountId"
            );
            //print_r($card_array);
            $this->db->where($where_array)
                ->update('card_details', $card_array);
            // echo $this->db->last_query();
            //die();
            $this->db->trans_commit();
            if ($this->db->affected_rows() == 1) {
                $updated = 1;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            $updated = 0;
        }
        return $updated;
    }

    public function requeryNIP($batchNo)
    {
        $this->load->helper('fluid_xml');

        $transaction_batch = $this->getTransactionBatch($batchNo);

        try {
            $clientID = CSS_CLIENT_ID;
            $secret_key = CSS_SECRET_KEY;
            $client = new SoapClient(CSS_WEBSERVICE_URL);

            $salt = rand(100000, 1000000000);
            $str2hash = "{$clientID}-{$secret_key}-{$salt}";
            $mac = hash('sha512', $str2hash);

            $xml = new \FluidXml\FluidXml('StatusRequest');

            $xml->addChild('Header', true)
                ->addChild('ScheduleId', $transaction_batch['schedule_id'])
                ->addChild('ClientId', $clientID)
                ->addChild('Salt', $salt)
                ->addChild('Mac', $mac);

            $xml->addChild('HashValue', ' ');
            $request = $xml->xml();

            //make the Webservice call to get the status of the payment schedule
            $response = $client->getPaymentScheduleStatus($request);

            //log the Webservice call request and response
            $log_data = array('method' => 'getPaymentScheduleStatus', 'request' => $request, 'response' => $response);
            $this->logPaymentWebservicecall($log_data);

            if (!$response) {
                throw new Exception('NIP Gateway DOWN!!!!');
            }

            //check the response
            $response_obj = simplexml_load_string($response);
            $response_code = $response_obj->Header->Status;

            if (strlen($response_code) > 2) {
                throw new Exception('NIP Gateway DOWN!!!!');
            }

            //get the description for the status of the response
            $desc = $this->getNibssPayPlusResponseDescription($response_code);

            $status = in_array($response_code, array('00', '06', '12')) ? '0' : '2';

            //update the transaction batch
            $batch_update_data = array(
                'batchNo' => $batchNo,
                'responseCode' => $response_code,
                'responseDescription' => $desc,
                'status' => $status
            );
            $this->updateTransactionBatch($batch_update_data);

            //if there was an error, update all transactions in the batch
            if ($status == '2') {
                $batch_transactions_update_data = array(
                    'batchNo' => $batchNo,
                    'paymentStatus' => '02',
                    'responseCode' => $response_code,
                    'statusDescription' => $desc
                );
                $this->updateTransactionsInBatch($batch_transactions_update_data);
            }

            //check the existence of payment records and update each in tbl:transaction
            if (!empty($response_obj->PaymentRecord)) {
                //$payment_records = is_array($response_obj->PaymentRecord) ? $response_obj->PaymentRecord : array($response_obj->PaymentRecord);

                foreach ($response_obj->PaymentRecord as $payment_record) {
                    $code_2 = $payment_record->Status;
                    $desc_2 = $this->getNibssPayPlusResponseDescription($code_2);
                    $payment_status = $code_2 == '00' ? '00' : ($code_2 == "06" ? "01" : '02');
                    //$acc_no = $payment_record->AccountNumber;

                    $result = $this->db->select('paymentDetailsId')->from('payment_details')->where(array('beneficiaryAccountNo' => $payment_record->AccountNumber, 'paymentFileId' => $transaction_batch['paymentFileId']))->get();
                    if ($result->num_rows() > 0) {
                        $id = $result->row()->paymentDetailsId;
                    }

                    $transaction_update_data = array(
                        'batchNo' => $batchNo,
                        'paymentDetailsId' => $id,
                        'paymentStatus' => $payment_status,
                        'responseCode' => $code_2,
                        'statusDescription' => $desc_2
                    );

                    $this->updateTransaction($transaction_update_data);
                }
            }

            $ret = $status == '0' ? true : false;

            $retVal = array($ret, $desc);

        } catch (Exception $e) {
            $retVal = array(false, $e->getMessage());
        }

        return $retVal;
    }

    public function  requeryAutogateBatch($batchNo)
    {
        //check if batch was sent and if batch had been sent for over 5 minutes and if file still reads pending
        //then try to pull it down. else do nothing
        $query_check = "SELECT * FROM `transaction` WHERE batchNo = '$batchNo' AND `sentStatus` = '1' AND statusDescription = 'Pending'";
        $result_check = $this->db->query($query_check);
        //die('<pre>'.print_r($result_check,1));
        if ($result_check->num_rows() > 0) {
            // echo("YEs");
            $row = $result_check->row_array();
            $filename = $row['filename'];
            //download sftp
            $this->downloadSFTP();
            // check if file is in OUT Folder
            //  die("here");
            $files = glob("myfiles/OUT/*" . $filename . "*");
            //print_r($files);

            if (count($files) > 0) {
                //file exists
                // die("Nooo");
                $local_filename = $files[0];
                $file_open = fopen("$local_filename", "r");
                $line = 0;
                while (!feof($file_open)) {
                    $line++;
                    $file_array = fgetcsv($file_open);
                    if ($line > 1) {
                        $transNos = $file_array[0];
                        $transNos_array = explode("_", $transNos);
                        $transNo = (!empty($transNos_array[1])) ? $transNos_array[1] : "";
                        $responseCode = $file_array['7'];
                        $responseDescriptionArray = $this->responseDescription($responseCode);
                        $responseDescription = isset($responseDescriptionArray['description']) ? $responseDescriptionArray['description'] : "";
                        //update the transaction
                        $update_where = array('batchNo' => "$batchNo", 'transNo' => "$transNo");
                        if ($responseCode == "00") {
                            $update_trans_array = array('paymentStatus' => '00', 'responseCode' => "$responseCode", 'statusDescription' => "$responseDescription");
                        } else {
                            $update_trans_array = array('paymentStatus' => '02', 'responseCode' => "$responseCode", 'statusDescription' => "$responseDescription");
                        }
                        $this->db->update("transaction", $update_trans_array, $update_where);
                    }
                }
                fclose($file_open);
            } else {
                //check the error file
                // die("Yesss");
                /** Todo : reading the error file */
            }

        } else {
            //do nothing
        }
    }

    public function responseDescription($code)
    {
        $retVal = array();
        $result = $this->db->get_where('finalresponse', array('code' => "$code"));
        // $ret_val
        if ($result->num_rows()) {
            $retVal = $result->row_array();
        }
        return $retVal;
    }

    private function downloadSFTP()
    {
        $this->load->library('Sftplib');
        $sftp = new Net_SFTP(SFTP_HOST);
        $retVal = true;
        if ($sftp->login(SFTP_USERNAME, SFTP_PASSWORD)) {
            // download the OUT Folder

            $outArray = $sftp->nlist('/OUT');
            foreach ($outArray AS $dfilename) {
                //get the file file down
                $filename_remote = "/OUT/" . $dfilename;
                $filename_local = "myfiles/OUT/$dfilename";
                $filename_log = "/LOGS/OUT/$dfilename";
                //echo "$filename_remote, $filename_local";
                //echo "$filename_remote, $filename_local";
                $sftp->get($filename_remote, $filename_local); // download the file
                //archive the file
                $sftp->rename($filename_remote, $filename_log);

            }
            // download the Error Folder

            $errorArray = $sftp->nlist('/ERROR');
            foreach ($errorArray AS $dfilename) {
                //get the file file down
                $filename_remote = "/ERROR/" . $dfilename;
                $filename_local = "myfiles/ERROR/$dfilename";
                $filename_log = "/LOGS/ERROR/$dfilename";
                //echo "$filename_remote, $filename_local";
                $sftp->get($filename_remote, $filename_local); // download the file
                //archive the file
                $sftp->rename($filename_remote, $filename_log);

            }
            // die("DONE");


        } else {
            $retVal = false;
        }

        return $retVal;

    }

    public function logPaymentWebservicecall($data)
    {
        return $this->db->insert('payments_webservice_log', array(
            'method' => $data['method'],
            'request' => $data['request'],
            'response' => $data['response'],
        ));
    }

    public function getNibssPayPlusResponseDescription($code)
    {
        $retVal = null;

        $result = $this->db->select('description')->from('nibsspayplus_responses')->where('response_code', $code)->get();
        if ($result->num_rows() > 0) {
            $retVal = $result->row()->description;
        }

        return $retVal;
    }

    public function unsettledSchedules()
    {
        $query = $this->db->select("tb.batchNo, tb.schedule_id, tb.paymentFileId, pf.paymentFileDescription")
            ->distinct()
            ->from("transaction_batches tb")
            ->join("transaction t", "t.batchId = tb.id AND t.sentStatus = '1' AND t.paymentStatus != '00'")
            ->join("payment_file pf", "tb.paymentFileId = pf.paymentFileId")
            ->where("tb.status = '0'")
            ->order_by("tb.id", 'asc')
            ->get();

        return $query->result_array();
    }

    /**
     * Retrieve a list of all account validation schedules still in progress
     */
    public function pendingValidationSchedules()
    {
        $resource = $this->db->select()
            ->from("av_schedules")
            ->where_in("avs_response_code", array('16', '06'))
            ->order_by("avs_id", 'asc')
            ->get();

        return $resource->result_array();
    }
}
