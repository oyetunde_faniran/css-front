<?php

/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 11/26/2016
 * Time: 7:12 AM
 */
class Payment_type_model extends UPL_Model
{
    function __construct()
    {
        parent::__construct();
        $table = 'payment_types';
        $table_pk = 'pt_id';

        $this->setTable($table);
        $this->setTablePK($table_pk);
    }

    /**
     * Retrieve all records.
     *
     * @return type
     */
    public function all()
    {
        return $this->loadMultiple();
    }

    /**
     * Retrieve single record by Primary Key.
     *
     * @param $id
     * @return null
     */
    public function find($id)
    {
        $result = $this->db->get_where($this->table, array($this->pk_column => $id));

        return $result && $result->num_rows() > 0 ? $result->row_array() : null;
    }

    /**
     * Retrieve single record by name.
     *
     * @param $name
     * @return null
     */
    public function findByName($name)
    {
        $result = $this->db->get_where($this->table, array('pt_name' => $name));

        return $result && $result->num_rows() > 0 ? $result->row_array() : null;
    }

}