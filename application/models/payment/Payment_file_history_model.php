<?php

/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 11/27/2016
 * Time: 10:50 PM
 */
class Payment_file_history_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Save a payment file group history record.
     *
     * @param $data
     * @return null
     */
    public function save($data)
    {
        $_data = array(
            'payment_file_group' => $data['payment_file_group'],
            'pfh_status' => $data['status'],
            'pfh_status_reason' => !empty($data['status_reason']) ? $data['status_reason'] : null,
            'pfh_created_by' => $data['created_by'],
            'pfh_created_at' => date('Y-m-d H:i:s')
        );

        $response = $this->db->insert("payment_file_history", $_data);

        return $response ? $this->db->insert_id() : null;
    }
}