<?php

/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 11/27/2016
 * Time: 4:26 AM
 */
class Payment_file_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Retrieve all payment files in 1 or more payment file groups.
     *
     * @param $id
     * @return null
     */
    public function all($id)
    {
        if (is_array($id))
            $this->db->where_in("pf.payment_file_group", $id);
        else
            $this->db->where("pf.payment_file_group", $id);

        $result = $this->db
            ->select("pf.pf_id AS payment_file_id,
                        pf.payment_file_group,
                        pf.source_account AS source_account_id,
                        ba.acc_name AS source_account_name,
                        ba.acc_number AS source_account_no,
                        b.bank_name AS source_account_bank_name", false)
            ->from("payment_files pf")
            ->join("bank_accounts ba", "pf.source_account = ba.acc_id", "LEFT")
            ->join("banks b", "ba.bank_id = b.bank_id", "LEFT")
            ->get();

        $ret_val = $result->num_rows() > 0 ? $result->result_array() : null;

        return $ret_val;
    }

    /**
     * Retrieve payment file by ID.
     *
     * @param $id
     * @return null
     */
    public function find($id)
    {
        $result = $this->db
            ->select("pf.pf_id AS payment_file_id,
                        pf.payment_file_group,
                        pf.source_account AS source_account_id,
                        ba.acc_name AS source_account_name,
                        ba.acc_number AS source_account_no,
                        b.bank_name AS source_account_bank_name", false)
            ->from("payment_files pf")
            ->join("bank_accounts ba", "pf.source_account = ba.acc_id", "LEFT")
            ->join("banks b", "ba.bank_id = b.bank_id", "LEFT")
            ->where('pf.pf_id', $id)
            ->get();

        $ret_val = $result->num_rows() > 0 ? $result->row_array() : null;

        return $ret_val;
    }

    /**
     * Save payment file.
     *
     * @param $data
     * @return null
     */
    public function save($data)
    {
        $_data = array(
            'payment_file_group' => $data['payment_file_group'],
            'merchant_id' => $data['merchant_id'],
            'source_account' => $data['source_account'],
            'pf_created_by' => $data['created_by'],
            'pf_created_at' => date("Y-m-d H:i:s"),
            'pf_updated_by' => $data['created_by']
        );

        $response = $this->db->insert('payment_files', $_data);

        return $response ? $this->db->insert_id() : null;
    }

    /**
     * Update the payment status of a file.
     *
     * @param $file_id
     * @param $status_id
     * @return mixed
     */
    public function updatePaymentStatus($file_id, $status_id)
    {
        $sql = "UPDATE payment_files SET pf_payment_status = ? WHERE pf_id = ?";

        return $this->db->query($sql, array($status_id, $file_id));
    }
}