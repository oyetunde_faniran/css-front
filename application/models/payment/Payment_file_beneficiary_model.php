<?php

/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 11/27/2016
 * Time: 4:11 AM
 */
class Payment_file_beneficiary_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Retrieve all payment beneficiaries, optionally in 1 or more payment files.
     * Optionally include all commission beneficiaries.
     *
     * @param null $id
     * @param bool|false $include_commission_accounts
     * @return null
     */
    public function all($id = null, $include_commission_accounts = false)
    {
        if ($id) {
            if (is_array($id))
                $this->db->where_in("pfb.payment_file", $id);
            else
                $this->db->where("pfb.payment_file", $id);
        }

        if (!$include_commission_accounts)
            $this->db->where("pfb.pfb_commission_account = FALSE");

        $result = $this->db
            ->select("pfb.pfb_id AS beneficiary_id,
                      pfb.payment_file,
                      pfb.pfb_account_name AS beneficiary_account_name,
                      pfb.pfb_account_no AS beneficiary_account_no,
                      pfb.pfb_bank AS beneficiary_bank_id,
                      pfb.pfb_amount AS beneficiary_amount,
                      pfb.pfb_commission_account AS beneficiary_is_commission_account,
                      pfb.pfb_payment_description AS beneficiary_payment_description,
                      s.status_description AS beneficiary_payment_status,
                      b.bank_name AS beneficiary_bank_name,
                      b.bank_code AS beneficiary_bank_code", false)
            ->from("payment_file_beneficiaries pfb")
            ->join("banks b", "pfb.pfb_bank = b.bank_id", "INNER")
            ->join("statuses s", "pfb.pfb_payment_status = s.status_id", "LEFT")
            ->order_by('pfb.payment_file')
            //->order_by('beneficiary_is_commission_account')
            ->order_by('beneficiary_account_name')
            ->get();

        $ret_val = $result->num_rows() > 0 ? $result->result_array() : null;

        return $ret_val;
    }

    /**
     * Retrieve payment file beneficiary.
     *
     * @param $id
     * @return null
     */
    public function find($id)
    {
        $result = $this->db
            ->select("pfb.pfb_id AS beneficiary_id,
                      pfb.payment_file,
                      pfb.pfb_account_name AS beneficiary_account_name,
                      pfb.pfb_account_no AS beneficiary_account_no,
                      pfb.pfb_bank AS beneficiary_bank_id,
                      pfb.pfb_amount AS beneficiary_amount,
                      pfb.pfb_commission_account AS beneficiary_is_commission_account,
                      pfb.pfb_payment_description AS beneficiary_payment_description,
                      s.status_description AS beneficiary_payment_status,
                      b.bank_name AS beneficiary_bank_name,
                      b.bank_code AS beneficiary_bank_code", false)
            ->from("payment_file_beneficiaries pfb")
            ->join("banks b", "pfb.pfb_bank = b.bank_id", "INNER")
            ->join("statuses s", "pfb.pfb_payment_status = s.status_id", "LEFT")
            ->where('pfb.pfb_id', $id)
            ->get();

        $ret_val = $result->num_rows() > 0 ? $result->row_array() : null;

        return $ret_val;
    }

    /**
     * Save single payment file beneficiary.
     *
     * @param $data
     * @return null
     */
    public function save($data)
    {
        $_data = array(
            'payment_file' => $data['payment_file_id'],
            'pfb_account_name' => $data['account_name'],
            'pfb_account_no' => $data['account_no'],
            'pfb_bank' => $data['bank_id'],
            'pfb_amount' => $data['amount'],
            'pfb_commission_account' => !empty($data['commission_account']) ? true : false,
            'pfb_created_by' => $data['created_by'],
            'pfb_created_at' => date("Y-m-d H:i:s"),
            'pfb_updated_by' => $data['created_by']
        );

        $response = $this->db->insert('payment_file_beneficiaries', $_data);

        return $response ? $this->db->insert_id() : null;
    }

    /**
     * Save many payment file beneficiaries at once.
     *
     * @param $data
     * @return null
     */
    public function saveMany($data)
    {
        $_data = array();

        foreach ($data as $piece) {
            $_data[] = array(
                'payment_file' => $piece['payment_file_id'],
                'pfb_account_name' => $piece['account_name'],
                'pfb_account_no' => $piece['account_no'],
                'pfb_bank' => $piece['bank_id'],
                'pfb_amount' => $piece['amount'],
                'pfb_commission_account' => !empty($piece['commission_account']) ? true : false,
                'pfb_created_by' => $piece['created_by'],
                'pfb_created_at' => date("Y-m-d H:i:s"),
                'pfb_updated_by' => $piece['created_by']
            );
        }

        $response = $this->db->insert_batch('payment_file_beneficiaries', $_data);

        if ($response) {
            $result = $this->db->select("CONCAT_WS(':', pfb_id, pfb_account_name, pfb_account_no, pfb_bank)")->from('payment_file_beneficiaries')->where('payment_file', $piece['payment_file_id'])->get();
            $ret_val = $result && $result->num_rows() > 0 ? $result->result_array() : null;
        } else {
            $ret_val = null;
        }
        
        return $ret_val;
    }

    /**
     * Update the payment status of all beneficiaries in a payment file
     * with the status of the beneficiary record in a corresponding schedule.
     *
     * @param $payment_file_id
     */
    public function updatePaymentForAllInFile($payment_file_id)
    {
        $sql = "UPDATE payment_file_beneficiaries pfb
                JOIN payment_schedule_beneficiaries psb ON pfb.pfb_id = psb.payment_beneficiary
                SET
                  pfb.pfb_payment_status = psb.psb_payment_status,
                  pfb.pfb_payment_description = psb.psb_payment_response_description
                WHERE pfb.payment_file = ?";

        return $this->db->query($sql, array($payment_file_id));
    }
}