<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/* 
    Document   : Task Page
    Created on : 10-Feb-2016, 11:05:35
    Author     : Bankork
    Link       : olaoluwa.bankole@upperlink.ng
    Description: The national feeding helper
*/

/**
 *  Clear Success Alert
 * @param string $frmname - The form id to reset value
 * @param int $clearfrm - Status to clear form field (0=clear, 1=dont)
 */
if (!function_exists('clearSuccessAlert')) {
    function clearSuccessAlert($frmname,$clearfrm = 0) {
        return clearAlert($frmname,"1",$clearfrm);
    }
}

/**
 *  Clear Warning Alert
 * @param string $frmname - The form id to reset value
 * @param int $clearfrm - Status to clear form field (0=clear, 1=dont)
 */
if (!function_exists('clearWarningAlert')) {
    function clearWarningAlert($frmname,$clearfrm = 0) {
        return clearAlert($frmname,"2",$clearfrm);
    }
}

/**
 *  Clear Error Alert
 * @param string $frmname - The form id to reset value
 * @param int $clearfrm - Status to clear form field (0=clear, 1=dont)
 */
if (!function_exists('clearErrorAlert')) {
    function clearErrorAlert($frmname,$clearfrm = 0) {
        return clearAlert($frmname,"3",$clearfrm);
    }
}


/**
 *  Clear Alert
 * @param string $frmname - The form id to reset value
 * @param int $clearfrm - Status to clear form field (0=clear, 1=dont)
 * @param int $type - The alert type
 */
if (!function_exists('clearAlert')) {
    function clearAlert($frmname,$type,$clearfrm) {
        switch($type){
            case 1:     //Success
                $alert = 'alert-success';
		break;
            case 2:     //Warning
                $alert = 'alert-warning';
                break;
            default:     //Error
                $alert = 'alert-danger';
                break;
        }
        
        $result = <<<JS
                window.setTimeout(function () {
                    $(".$alert").fadeTo(2000, 500).slideUp(500, function () {
                        $(".$alert").alert('close');
                        if($clearfrm == 0){
                            $("#$frmname").find('input:text, input:password, input:file, input:email, select, textarea').val('');
                            $("#$frmname").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
                        }   
                    });
                }, 2000);
JS;
        return $result;
    }
}

    /**
     * Checks if a value is unique excluding the others with the ID specified in the check.
     * @param string $tbl The table to check for uniqueness
     * @param array $where_array  The array condition
     * @return boolean          Returns TRUE if unique, FALSE otherwise
     */
if (!function_exists('isUniqueValue')) {
    function isUniqueValue($tbl, $where_array){
        $CI =& get_instance();
        
        $CI->db->get_where($tbl, $where_array);
        if ($CI->db->affected_rows() > 0){
            $ret_val = false;
        } else {
            $ret_val = true;
        }
        return $ret_val;
    }   
}//END isUniqueValue()