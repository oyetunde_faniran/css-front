<?php

if(!function_exists('dd')){
    function dd($var){
        echo "<pre>";
            print_r($var);
        echo "</pre>";
            die;
    }

}

if(!function_exists('dump')){
    function dump($var){
        echo "<pre>";
        print_r($var);
        echo "</pre>";
    }

}

if(!function_exists("human_case")){
    function human_case($string,$filter=array()){
        $match = array("/_/");
        $replace = array(" ");
        if(!empty($filter)){
            foreach($filter as $k=>$v){
                $filter[$k] = "/$v/";
                $replace[] = "";
            }
            $match = array_merge($match,$filter);
        }
        return preg_replace($match,$replace,$string);
    }
}

if(!function_exists("inttoword")){
    function inttoword($int){
        $int = intval($int);
        $arr = array(0=>false,1=>"one",2=>"two",3=>"three",4=>"four",5=>"five",6=>"six",7=>"seven",8=>"eight",9=>"nine",10=>"ten",11=>"eleven",12=>"twelve",13=>"thirteen",14=>"fourteen",15=>"fifteen",16=>"sixteen",17=>"seventeen",18=>"eighteen",19=>"nineteen",20=>"twenty",30=>"thirty",40=>"fourty",50=>"fifty",60=>"sixty",70=>"seventy",80=>"eighty",90=>"ninety",100=>"hundred",1000=>"thousand",1000000=>"million",1000000000=>"billion",1000000000000=>"trillion",1000000000000000=>"zillion");
        return isset($arr[$int])?$arr[$int]:"unknown";
    }
}


if(!function_exists("numtoword")){
    function numtoword($num){
        $num = strstr($num,",")?$num:number_format($num);
        $num_a = explode(",",$num);
        $len = strlen(implode("",$num_a));
        $tw = "";
        foreach($num_a as $n){
            if(intval($n) == 0){
                $len = $len - 3;
            }
            else{
                $lint = strlen(intval($n));
                $len = $len - $lint - (strlen($n) - $lint);

                $i = 0;
                $ch = 0;
                if($len > 0){
                    while($i <= $len){
                        if($i == 0){
                            $ch = "1";
                        }
                        else{
                            $ch.= "0";
                        }
                        $i++;
                    }
                }
                $n = strval(intval($n));
                if(strlen($n) == 3){
                    $x = inttoword($n[2]);
                    if("$n[1]$n[2]" < "20"){
                        $xx = inttoword("$n[1]$n[2]");
                        $x = false;
                    }
                    else{
                        $xx = inttoword($n[1]."0");
                    }
                    $xxx = inttoword($n[0]);
                    if($x and $xx){
                        $w = "$xxx hundred and $xx $x";
                    }
                    elseif($xx){
                        $w = "$xxx hundred and $xx";
                    }
                    elseif($x){
                        $w = "$xxx hundred and $x";
                    }
                    else{
                        $w = "$xxx hundred";
                    }

                }
                elseif(strlen($n) == 2 and $n > 20){
                    $x = inttoword($n[1]);
                    $xx = inttoword($n[0]."0");
                    if($x){
                        $w = "$xx $x";
                    }
                    else{
                        $w = "$xx";
                    }
                }
                else{
                    $w = inttoword($n);
                }
                if($tw == ""){
                    $tw.=$w." ".inttoword($ch);
                }
                else{
                    $tw.=", ".$w." ".inttoword($ch);
                }
            }

        }
        return $tw;
    }
}


if(!function_exists("options")){
    function options(Array $src,$selected=null,$options_label="-choose-",$pairs=array(),$delimiter=" "){
        if($options_label)$opt = "<option value=''>$options_label</option>";
        else $opt = "";
        if(!empty($src)){
            foreach($src as $k=>$v){
                $select = "";
                if(!is_null($selected) and ($selected == $k or (is_array($selected) and in_array($k, $selected)))) {
                    $select = "selected";
                }
                $opt .= "<option $select value='$k'>";
                if(is_array($v)) {
                    if(!empty($pairs)){
                        $nv = array();
                        foreach ($v as $value) {
                            $nv[] = $pairs[$value];
                        }
                        $v = $nv;
                    }
                    $v = implode($delimiter,$v);
                }
                $opt .= "$v</option>";
            }
        }
        return $opt;
    }
}



if(!function_exists("required_forms")){
    function required_forms(){
        $CI = get_instance();
        //Get the list of $pitr_forms
        $CI->load->model('form_model','forms');

        return $CI->forms->lists('id','name');

    }
}


if(!function_exists("required_attachments")){
    function required_attachments(){
        $CI = get_instance();
        //Get the list of $pitr_forms
        $CI->load->model('attachment_model','attach');

        return $CI->attach->lists('id','name');
    }
}


if(!function_exists("getAppRequirements")){
    function getAppRequirements($jsonString){
        $obj = json_decode($jsonString);
        $req = array('forms'=>false,'attachments'=>false);
        if(isset($obj->req_forms)) $req['forms'] = $obj->req_forms;

        if(isset($obj->req_attaches)) $req['attachments'] = $obj->req_attaches;

        return $req;
    }
}

if(!function_exists("sweet_datetime")){
    function sweet_date($time = false,$pad=false){
        if($pad)$pad = ". g:i A";
        else $pad="";
        return date('dS \of F, Y'.$pad,is_int($time)?$time:strtotime($time));
    }
}

if(!function_exists("strdate")) {
    function strdate($time = false, $pad = false)
    {
        if ($pad) $pad = ". g:i A";
        else $pad = "";
        return date('l, F jS, Y' . $pad, is_int($time)?$time:strtotime($time));
    }
}

if(!function_exists("translate")){
    global $transformer;

    $transformer = array(
        'basic_salary'=>'amount',
        'pro_rated_gross_rent'=>'amount',
        'total_outgoing_expenses'=>'amount',
        'tax_withheld_on_rent'=>'amount',
        'income_from_abroad'=>'amount',
        'nstif_contributions'=>'amount',
        'pension_funds'=>'amount',
        'widows_orphans_pension'=>'amount',
        'sum_payable_on_death'=>'amount',
        'premium_amount_paid'=>'amount',
        'income_of_child'=>'amount',
        'maintenance_contribution'=>'amount',
        'gross_profit'=>'amount',
        'net_profit'=>'amount',
        'housing_allowance'=>'amount',
        'transport_allowance'=>'amount',
        'utility_allowance'=>'amount',
        'entertainment_allowance'=>'amount',
        'meal_subsidy_allowance'=>'amount',
        'acting_allowance'=>'amount',
        'hazard_allowance'=>'amount',
        'call_duty_allowance'=>'amount',
        'domestic_staff_allowance'=>'amount',
        'personal_assistant_allowance'=>'amount',
        'inducement_allowance'=>'amount',
        'special_assistant_allowance'=>'amount',
        'hardship_allowance'=>'amount',
        'bonuses'=>'amount',
        'commission'=>'amount',
        'gratuities'=>'amount',
        'others'=>'amount',
        'interest_paid'=>'amount',
        'tax_paid'=>'amount',
        'loan_security'=>'amount',


        'acquired_or_ceased_at'=>'date',
        'building_completion_date'=>'date',
        'acquisition_date'=>'date',
        'completion_or_purchase_date'=>'date',
        'date_of_first_use'=>'date',
        'date_of_birth'=>'date',
    );

    function translate($k,$v){
        global $transformer;

        if(!$v) return $v;

        if(isset($transformer[$k])){
            $method = "translate_".$transformer[$k];
            return $method($v);
        }

        elseif(strstr($k,'date')){
            return translate_date($v);
        }

        elseif(strstr($k,'profit') or strstr($k,'amount') or strstr($k,'cost') or strstr($k,'income')  or strstr($k,'rent') or  strstr($k,'tax')){
            return translate_amount($v);
        }

        return $v;
    }

    function translate_date($v){
        return date('m/d/Y',strtotime($v));
    }

    function translate_amount($v){
        $v = intval($v);
        return CUR_SYM1." ".number_format($v);
    }


}

if(!function_exists("form_value")){
    function form_value($field){
        global $fvData;
        return isset($fvData->$field)?translate($field,$fvData->$field):"";
    }
}

if(!function_exists("get_value")){
    function get_value($scope,$field){
        return isset($scope[$field])?translate($field,$scope[$field]):"";
    }
}

if(!function_exists("form_check")){
    function form_check($value,$field){
        global $fvData;
        return (isset($fvData->$field) and $value == $fvData->$field)?"checked":"";
    }
}

if(!function_exists("display")){
    function display($file,$attribute="",$image=null){
        if(is_null($image)){
            $info = pathinfo($file);
            $ext = strtolower($info['extension']);
            $image_ext = array('jpg','png','jpe','bmp','jpeg','gif');
            $image = (in_array($ext,$image_ext))?true:false;
        }

        $attribute = str_replace('"',"\\\"",str_replace("'",'"',$attribute));

        if($image){
            return "<img $attribute width=100% src=".base_url($file)." />";
        }
        else{
            return "<iframe $attribute height=500 width=100%  src=https://docs.google.com/gview?url=".base_url($file)."&embedded=true ></iframe>";
        }
    }
}


if(!function_exists("swal")){
    function swal($title,$file,$class="preview"){
        return "swal({customClass:\"$class\",allowOutsideClick: true,html: true,title:\"<small>$title</small>\",text: \"".display($file )."\"})";
    }
}


function strip_tags_content($text, $tags = '', $invert = FALSE) {

    preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags);
    $tags = array_unique($tags[1]);

    if(is_array($tags) AND count($tags) > 0) {
        if($invert == FALSE) {
            return preg_replace('@<(?!(?:'. implode('|', $tags) .')\b)(\w+)\b.*?>.*?</\1>@si', '', $text);
        }
        else {
            return preg_replace('@<('. implode('|', $tags) .')\b.*?>.*?</\1>@si', '', $text);
        }
    }
    elseif($invert == FALSE) {
        return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);
    }
    return $text;
}



