<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png'); ?>">
    <title>
        <?php
        $page_title = !empty($page_title) ? $page_title : (!empty($title) ? $title : '');
        $dis_page_title = !empty($page_title) ? "$page_title - " : '';
        echo $dis_page_title . DEFAULT_TITLE;
        ?>
    </title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/lib/stroke-7/style.css'); ?>"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url('assets/lib/jquery.nanoscroller/css/nanoscroller.css'); ?>"/>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url('assets/lib/theme-switcher/theme-switcher.min.css'); ?>"/>
    <link type="text/css" href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
</head>
<body class="am-splash-screen">
<div class="am-wrapper am-login">
    <div class="am-content">
        <div class="main-content">
            <div class="login-container">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <img src="<?php echo base_url('assets/img/logo.png'); ?>"
                             alt="logo" width="120px" class="logo-img"><br>
<!--                        <img src="<?php echo base_url('assets/img/logo-full-retina.png'); ?>"
                             alt="logo" width="150px" height="39px" class="logo-img">-->
                    </div>
                    <?php echo !empty($msg) ? $msg : ''; ?>
                    <div class="panel-body">
                        <?php echo form_open('sys-admin/login/login_page', array('method' => 'post', 'class' => 'form-horizontal')) ?>
                        <div class="login-form">
                            <div class="form-group">
                                <div class="input-group"><span class="input-group-addon"><i
                                            class="icon s7-user"></i></span>
                                    <input id="email" name="email" type="email" placeholder="E-Mail" autocomplete="off"
                                           class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group"><span class="input-group-addon"><i
                                            class="icon s7-lock"></i></span>
                                    <input name="password" id="password" type="password" placeholder="Password"
                                           class="form-control">
                                </div>
                            </div>
                            <div class="form-group login-submit">
                                <button data-dismiss="modal" type="submit" class="btn btn-primary btn-lg">Log me in
                                </button>
                            </div>
                            <div class="form-group footer row">
                                <div class="col-xs-6"><?php echo anchor('forgot_password', 'Forgot Password?'); ?></div>
                                <!--<div class="col-xs-6 remember">
                                  <label for="remember">Remember Me</label>
                                  <div class="am-checkbox">
                                    <input type="checkbox" id="remember">
                                    <label for="remember"></label>
                                  </div>
                                </div>-->
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url('assets/lib/jquery/jquery.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/lib/jquery.nanoscroller/javascripts/jquery.nanoscroller.min.js'); ?>"
        type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/main.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/lib/bootstrap/dist/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/lib/theme-switcher/theme-switcher.min.js'); ?>"
        type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        //initialize the javascript
        App.init();
    });

</script>
<script type="text/javascript">
    $(document).ready(function () {
        App.livePreview();
    });

</script>
<script type="text/javascript">
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '../../www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-68396117-1', 'auto');
    ga('send', 'pageview');


</script>
</body>
</html>