<?php
$client_dd['0'] = '--All Clients--';
foreach($clients as $c){
    $client_dd[$c['client_id']] = $c['client_name'] . ' (' . $c['client_code'] . ')';
}
?>
<div class="row" style="border-bottom: 1px solid #CCC; padding-bottom: 20px;">
    <div class="container">
        <div id="accordion3" class="panel-group accordion accordion-semi" style="margin-top: 20px;">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion3" href="#ac3-1" aria-expanded="false" class="collapsed"><i class="icon s7-angle-down"></i> Click here to filter</a></h4>
                </div>
                <div id="ac3-1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        <form method="post" role="form">
                            <div class="col-md-6">
                                <div class="form-group col-md-12">
                                    <label>FROM DATE</label>
                                    <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker col-md-12">
                                        <input size="16" type="text" value="<?php echo (!empty($fromdate) ? $fromdate : date('Y-m-d')); ?>" name="fromdate" id="fromdate" class="form-control" />
                                        <span class="input-group-addon btn btn-primary"><i class="icon-th s7-date"></i></span>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <label>TO DATE</label>
                                    <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker col-md-12">
                                        <input size="16" type="text" value="<?php echo (!empty($todate) ? $todate : date('Y-m-d')); ?>" name="todate" id="todate" class="form-control" />
                                        <span class="input-group-addon btn btn-primary"><i class="icon-th s7-date"></i></span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group col-md-12">
                                    <label>CLIENT</label>
                                    <?php echo form_dropdown('client', $client_dd, set_value('client'), 'class="form-control"'); ?>
                                    <!--                                                <select name="client" id="client" class="form-control">
                                                                                        <option value="0">--All--</option>
                                                                                    </select>-->
                                </div>
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-lg btn-space btn-primary">Filter</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
//    die('<pre>' . print_r($trans, true));
$display = '<!--<div class="row"><button type="button" class="btn btn-primary">Re-push Feedback For Selected Schedules</button></div>-->
            <div class="table-responsive">
                <table id="report-table" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>S/NO <input type="checkbox" name="checkall" id="checkall" value="0" /></th>
                            <th>CLIENT</th>
                            <th>SCHEDULE</th>
                            <th>TOTAL BENEFICIARIES</th>
                            <th>SUCCESSFUL BENEFICIARIES</th>
                            <th>FAILED BENEFICIARIES</th>
                            <th>PENDING BENEFICIARIES</th>
                            <th>TOTAL COMMISSION (&#8358;)</th>
                            <th>COMMISSION ON SUCCESSFUL BEN. (&#8358;)</th>
                            <th>SOURCE BANK CODE</th>
                            <th>SOURCE BANK ACC. NO</th>
                            <th>TOTAL AMOUNT (&#8358;)</th>
                            <th>STATUS</th>
                            <th>DATE INITIATED</th>
                            <th>LAST UPDATED</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>S/NO <input type="checkbox" name="checkall" id="checkall" value="0" /></th>
                            <th>CLIENT</th>
                            <th>SCHEDULE</th>
                            <th>TOTAL BENEFICIARIES</th>
                            <th>SUCCESSFUL BENEFICIARIES</th>
                            <th>FAILED BENEFICIARIES</th>
                            <th>PENDING BENEFICIARIES</th>
                            <th>TOTAL COMMISSION (&#8358;)</th>
                            <th>COMMISSION ON SUCCESSFUL BEN. (&#8358;)</th>
                            <th>SOURCE BANK CODE</th>
                            <th>SOURCE BANK ACC. NO</th>
                            <th>TOTAL AMOUNT (&#8358;)</th>
                            <th>STATUS</th>
                            <th>DATE INITIATED</th>
                            <th>LAST UPDATED</th>
                        </tr>
                    </tfoot>
                    <tbody>';
$sno = $total = $comm_total = $comm_successful = 0;
//    die('<pre>' . print_r($trans, true));
foreach($trans as $t){
    $dis_status = $t['sch_status'] == '0' ? '<span class="label label-success">Accepted by NIBSS</span>' : ($t['sch_status'] == '1' ? '<span class="label label-warning">Pending</span>' : '<span class="label label-danger">NOT Accepted by NIBSS</span>');
    if($t['sch_status'] == '0' && $t['sch_comm_status'] == '0' && $t['sch_commission'] > 0){
        $comm_process_btn_total = '<div class="comm-pay-btn-container-' . $t['sch_id'] . '"><button class="btn-danger comm-pay-btn" onclick="processComm(' . $t['sch_id'] . ', 1);">Collect Commission</button></div><div style="width: 100%; display: none;" id="loading-image-1-' . $t['sch_id'] . '" class="progress-bar progress-bar-danger progress-bar-striped active">Please wait...</div>';
        if($t['sch_commission_successful_only'] > 0){
            $comm_process_btn_successful = '<div class="comm-pay-btn-container-' . $t['sch_id'] . '"><button class="btn-danger comm-pay-btn" onclick="processComm(' . $t['sch_id'] . ', 2);">Collect Commission</button></div><div style="width: 100%; display: none;" id="loading-image-2-' . $t['sch_id'] . '" class="progress-bar progress-bar-danger progress-bar-striped active">Please wait...</div>';
        } else {
            $comm_process_btn_successful = '';
        }
    } elseif($t['sch_status'] == '0' && $t['sch_comm_status'] == '1') {
        $comm_process_btn_total = $comm_process_btn_successful = '<span class="label label-success">Commission processed</span>';
    } else {
        $comm_process_btn_total = $comm_process_btn_successful = '<span class="label label-warning">Unpayable commission</span>';
    }

    if($t['sch_status'] == '0' && ($t['sch_ben_count_successful_only'] > 0 || $t['sch_ben_count_failed_only'] > 0)){
        $push_button = '<input name="feedback_checked" type="checkbox" value="' . $t['sch_sched_id'] . '" id="checkbox-' . $t['sch_sched_id'] . '" />
                <a target="_blank" href="http://css.ng/v1prod/css_api_v2_response_repush/doRepushFromScheduleTable/' . $t['client_code'] . '/' . $t['sch_sched_id'] . '" class="btn btn-primary">Re-push Feedback</a>';
    } else {
        $push_button = '';
    }
    $pending_count_temp = $t['sch_ben_count'] - ($t['sch_ben_count_successful_only'] + $t['sch_ben_count_failed_only']);
    $pending_count = $pending_count_temp < 0 ? 0 : $pending_count_temp;
    $display .= '<tr>
                        <td>' . (++$sno) . '.' . $push_button .
        '
                        </td>
                        <td>' . $t['client_name'] . '<br />(' . $t['client_code'] . ')</td>
                        <td><a target="_blank" href="' . site_url('reports/trans_beneficiaries/' . $t['sch_id']) . '">' . $t['sch_sched_id'] . '</a></td>
                        <td>' . number_format($t['sch_ben_count'], 0) . '</td>
                        <td>' . number_format($t['sch_ben_count_successful_only'], 0) . '</td>
                        <td>' . number_format($t['sch_ben_count_failed_only'], 0) . '</td>
                        <td>' . number_format($pending_count, 0) . '</td>
                        <td>' . number_format($t['sch_commission'], 2) . '<br />' . $comm_process_btn_total . '</td>
                        <td>' . number_format($t['sch_commission_successful_only'], 2) . '<br />' . $comm_process_btn_successful . '</td>
                        <td>' . $t['bank_name'] . '<br />(' . $t['sch_source_bank_code'] . ')</td>
                        <td>' . $t['sch_source_accno'] . '</td>
                        <td>' . number_format($t['sch_total_amount'], 2) . '</td>
                        <td>' . $dis_status . '</td>
                        <td>' . $t['sch_dateadded'] . '</td>
                        <td>' . $t['sch_lastupdated'] . '</td>
                    </tr>';
}
$display .= '</tbody></table></div>';
echo $display;
?>


<script>
    function processComm(sch_id, proc_type){
        return 0;
        if(confirm('Are you sure you want to process the payment of commission for this schedule?')){
            dis_url = '<?php echo site_url('commission/processCommission') . '/'; ?>' + sch_id + '/' + proc_type;
            console.log(dis_url);
//            $('#close-button').fadeOut();
            $('.comm-pay-btn').fadeOut();
            $('#loading-image-' + proc_type + '-' + sch_id).fadeIn();
            $.ajax({
                type: 'GET',
                url: dis_url,
                data_type: 'json',
                success: function(data){
                    console.log("DONE! \n" + data);
                    $('.comm-pay-btn-container-' + sch_id).html('<span class="label label-success">Commission processed</span>');
                    $('.comm-pay-btn').fadeIn();
                    $('#loading-image-' + proc_type + '-' + sch_id).fadeOut();
//                    $('#loading-image').fadeOut();
//                    $('#msg-container').html(data.msg);
//                    $('#msg-container').fadeIn();
//                    if(data.status){
//    //                    dis_timer = setInterval(reloadClients, 2000);
//                        window.location = '<?php //echo site_url('client'); ?>';
//                    } else {
//                        $('#close-button').fadeIn();
//                        $('#save-button').fadeIn();
//                    }
                }
            });
        }
    }

</script>