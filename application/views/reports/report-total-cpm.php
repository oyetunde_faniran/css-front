<div class="row">
	<div class="col-md-4">
	  <div class="panel panel-primary">
		<div class="panel-heading">
		  <span class="title">Account Total</span>
		</div>
		<div class="panel-body">
		  <table class="table table-striped">
			<tr><td><?php echo anchor('reports/do_report/total/total', 'Latest total balance in all accounts'); ?></td></tr>
			<tr><td><?php echo anchor('reports/do_report/total/cph', 'Total change per hour'); ?></td></tr>
			<tr><td><?php echo anchor('reports/do_report/total/cpd', 'Total change per day'); ?></td></tr>
			<tr><td><?php echo anchor('reports/do_report/total/cpm', 'Total change per month'); ?></td></tr>
		  </table>
		</div>
	  </div>
	</div>


	<div class="col-md-4">
	  <div class="panel panel-alt1">
		<div class="panel-heading">
		  <span class="title">Per Account</span>
		</div>
		<div class="panel-body">
		  <table class="table table-striped">
			<tr><td><?php echo anchor('reports/do_report/account/total', 'Latest Balance'); ?></td></tr>
			<tr><td><?php echo anchor('reports/do_report/account/cph', 'Change per hour'); ?></td></tr>
			<tr><td><?php echo anchor('reports/do_report/account/cpd', 'Change per day'); ?></td></tr>
			<tr><td><?php echo anchor('reports/do_report/account/cpm', 'Change per month'); ?></td></tr>
		  </table>
		</div>
	  </div>
	</div>
	
	<div class="col-md-4">
	  <div class="panel panel-alt3">
		<div class="panel-heading">
		  <span class="title">Per Bank</span>
		</div>
		<div class="panel-body">
		  <table class="table table-striped">
			<tr><td><?php echo anchor('reports/do_report/bank/total', 'Latest Total Balance'); ?></td></tr>
			<tr><td><?php echo anchor('reports/do_report/bank/cph', 'Total change per hour'); ?></td></tr>
			<tr><td><?php echo anchor('reports/do_report/bank/cpd', 'Total change per day'); ?></td></tr>
			<tr><td><?php echo anchor('reports/do_report/bank/cpm', 'Total change per month'); ?></td></tr>
		  </table>
		</div>
	  </div>
	</div>
</div>