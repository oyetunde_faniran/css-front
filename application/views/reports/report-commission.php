<?php
$dis_sys = $this->system_authorization;
$client_dd['0'] = '--All Clients--';
foreach($clients as $c){
    $client_dd[$c['client_id']] = $c['client_name'] . ' (' . $c['client_code'] . ')';
}
?>

<div class="row" style="border-bottom: 1px solid #CCC; padding-bottom: 20px;">
    <div class="container">
        <div id="accordion3" class="panel-group accordion accordion-semi" style="margin-top: 20px;">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion3" href="#ac3-1" aria-expanded="false" class="collapsed"><i class="icon s7-angle-down"></i> Click here to filter</a></h4>
                </div>
                <div id="ac3-1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        <form method="post" role="form">
                            <div class="col-md-6">
                                <div class="form-group col-md-12">
                                    <label>FROM DATE</label>
                                    <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker col-md-12">
                                        <input size="16" type="text" value="<?php echo (!empty($fromdate) ? $fromdate : date('Y-m-d')); ?>" name="fromdate" id="fromdate" class="form-control" />
                                        <span class="input-group-addon btn btn-primary"><i class="icon-th s7-date"></i></span>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <label>TO DATE</label>
                                    <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker col-md-12">
                                        <input size="16" type="text" value="<?php echo (!empty($todate) ? $todate : date('Y-m-d')); ?>" name="todate" id="todate" class="form-control" />
                                        <span class="input-group-addon btn btn-primary"><i class="icon-th s7-date"></i></span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group col-md-12">
                                    <label>CLIENT</label>
                                    <?php echo form_dropdown('client', $client_dd, set_value('client'), 'class="form-control"'); ?>
                                    <!--                                                <select name="client" id="client" class="form-control">
                                                                                        <option value="0">--All--</option>
                                                                                    </select>-->
                                </div>
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-lg btn-space btn-primary">Filter</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php
//    $create_comm_btn = '<div style="margin-top: 10px;">
//                            <button class="btn btn-danger" data-toggle="modal" data-target="#comm-create-modal"><i class="icon s7-plus"></i> Create Custom Commission</button>
//                        </div>';
$create_comm_btn = '';

echo ($dis_sys->hasAccess('trans-commission-create-custom') ? $create_comm_btn : '');
//    die('<pre>' . print_r($trans, true));
$display = '<div class="table-responsive"><table id="report-table" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>S/NO</th>
                            <th>CLIENT</th>
                            <th>SCHEDULE</th>
                            <!--<th>TOTAL BENEFICIARIES</th>
                            <th>SUCCESSFUL BENEFICIARIES</th>
                            <th>COMMISSION ON SUCCESSFUL BEN. (&#8358;)</th>-->
                            <th>TOTAL COMMISSION (&#8358;)</th>
                            <th>SPLIT ID</th>
                            <th>ACCOUNT CREDITED</th>
                            <!--<th>TOTAL AMOUNT (&#8358;)</th>-->
                            <th>STATUS</th>
                            <th>DATE PROCESSED</th>
                            <!--<th>LAST UPDATED</th>-->
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>S/NO</th>
                            <th>CLIENT</th>
                            <th>SCHEDULE</th>
                            <!--<th>TOTAL BENEFICIARIES</th>
                            <th>SUCCESSFUL BENEFICIARIES</th>
                            <th>COMMISSION ON SUCCESSFUL BEN. (&#8358;)</th>-->
                            <th>TOTAL COMMISSION (&#8358;)</th>
                            <th>SPLIT ID</th>
                            <th>ACCOUNT CREDITED</th>
                            <!--<th>TOTAL AMOUNT (&#8358;)</th>-->
                            <th>STATUS</th>
                            <th>DATE PROCESSED</th>
                            <!--<th>LAST UPDATED</th>-->
                        </tr>
                    </tfoot>
                    <tbody>';
$sno = $total = $comm_total = $comm_successful = 0;
//die('<pre>' . print_r($trans, true));
foreach($trans as $t){
    $dis_status = $t['schcomm_status'] == '0' ? '<span class="label label-success">Accepted by NIBSS</span>' : ($t['schcomm_status'] == '1' ? '<span class="label label-warning">Comm. Debit In Progress</span>' : '<span class="label label-danger">NOT Accepted by NIBSS</span>');
    if($t['schcomm_split_status'] == 1 || $t['schcomm_split_status'] == 2){
        $split_process_btn = ($dis_sys->hasAccess('trans-commission-show-preview') && $dis_sys->hasAccess('trans-commission-split')) ? '<div class="comm-pay-btn-container-' . $t['schcomm_id'] . '"><button class="btn-info comm-pay-btn" onclick="initSplit(' . $t['schcomm_id'] . ', 2, 0);">Split Single</button> <button class="btn-danger comm-pay-btn" onclick="initSplit(' . $t['schcomm_id'] . ', 2, ' . $t['prj_id'] . ');">Split All</button></div><div style="width: 100%; display: none;" id="loading-image-2-' . $t['schcomm_id'] . '" class="progress-bar progress-bar-danger progress-bar-striped active">Please wait...</div>' : '<span class="label label-danger">Split Pending</span>';
//            $delete_btn = $dis_sys->hasAccess('trans-commission-delete-custom') ? '<div class="comm-delete-btn-container-' . $t['schcomm_id'] . '"><button class="btn-danger comm-delete-btn" onclick="deleteCommission(' . $t['schcomm_id'] . ');">Delete</button></div><div style="width: 100%; display: none;" id="loading-del-image-' . $t['schcomm_id'] . '" class="progress-bar progress-bar-danger progress-bar-striped active">Please wait...</div>' : '';
        $delete_btn = '';
    } else {
        $split_process_btn = '<span class="label label-success">Split processed</span> <button type="button" id="show-details-btn-' . $t['schcomm_id'] . '" onclick="showSplit(' . $t['schcomm_id'] . ', 2)" class="btn btn-info show-details-btn">Show Split</button><div style="width: 100%; display: none;" id="loading-image-display-only-2-' . $t['schcomm_id'] . '" class="progress-bar progress-bar-danger progress-bar-striped active">Please wait...</div>';
        $delete_btn = '';
    }
    if($t['schcomm_status'] != '0'){
        $split_process_btn = '';
    }
    $display .= '<tr>
                        <td>' . (++$sno) . '.' . $delete_btn . '</td>
                        <td>' . $t['client_name'] . '<br />(' . $t['client_code'] . ')</td>
                        <td>' . (!empty($t['sch_sched_id']) && $t['schcomm_type'] != '3' ? anchor('reports/trans_beneficiaries/' . $t['sch_id'], $t['sch_sched_id'], 'target="_blank"') : '<em>Custom Commission</em>') . '</td>
                        <td>' . number_format($t['schcomm_amount'], 2) . '<br />' . $split_process_btn . '</td>
                        <td>' . (!empty($t['schcomm_split_sched_id']) ? $t['schcomm_split_sched_id'] : '-') . '</td>
                        <td>' . $t['schcomm_accno'] . '<br />' . $t['bank_name'] . '<br />(' . $t['schcomm_bankcode'] . ')</td>
                        <td>' . $dis_status . '</td>
                        <td>' . $t['schcomm_dateadded'] . '</td>
                    </tr>';
}
$display .= '</tbody></table></div>';
echo $display;
?>




<div id="split-details-modal" tabindex="-1" role="dialog" class="modal fade colored-header">
    <div class="modal-dialog custom-width">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i
                            class="icon s7-close"></i></button>
                <h3 class="modal-title">Split Details</h3>
            </div>
            <div id="narration-container" style="padding:10px;">
                <label for="split_narration" style="font-weight: bold;">NARRATION:</label>
                <input type="text" name="split_narration" id="split_narration" class="form-control" />
            </div>
            <div id="msg-container-details"></div>
            <div class="modal-body form" style="overflow: auto; max-height: 400px;">
                <div id="split-details"></div>
            </div>
            <div class="modal-footer">
                <div id="loading-image-modal" style="width: 100%; display: none;" class="progress-bar progress-bar-danger progress-bar-striped active">Please, wait...</div>
                <button type="button" id="close-button-modal" data-dismiss="modal" class="btn btn-default md-close">Close</button>
                <button type="button" id="proceed-button-modal" class="btn btn-primary" onclick="processSplit();">Proceed</button>
            </div>
        </div>
    </div>
</div>



<div id="split-details-display-only-modal" tabindex="-1" role="dialog" class="modal fade colored-header">
    <div class="modal-dialog custom-width">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i
                            class="icon s7-close"></i></button>
                <h3 class="modal-title">Split Details</h3>
            </div>
            <div id="msg-container-details-display-only"></div>
            <div class="modal-body form" style="overflow: auto; max-height: 400px;">
                <div id="split-details-display-only"></div>
            </div>
            <div class="modal-footer">
                <div id="loading-image-display-only-modal" style="width: 100%; display: none;" class="progress-bar progress-bar-danger progress-bar-striped active">Please, wait...</div>
                <button type="button" id="close-button-display-only-modal" data-dismiss="modal" class="btn btn-default md-close">Close</button>
            </div>
        </div>
    </div>
</div>



<div id="comm-create-modal" tabindex="-1" role="dialog" class="modal fade colored-header">
    <div class="modal-dialog custom-width">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i
                            class="icon s7-close"></i></button>
                <h3 class="modal-title">Create Custom Commission</h3>
            </div>
            <div id="msg-container-comm-create"></div>
            <form method="post" action="<?php echo site_url('commission/create_custom'); ?>">
                <div class="modal-body form" style="overflow: auto; max-height: 400px;">
                    <div class="form-group">
                        <label id="project">PROJECT:</label>
                        <?php
                        $prj_dd = array(
                            '0' => '--Select Project--'
                        );
                        foreach($prjs as $p){
                            $prj_dd[$p['prj_id']] = $p['prj_name'];
                        }
                        echo form_dropdown('project', $prj_dd, 0, 'id="project" class="form-control"');
                        ?>
                    </div>
                    <div class="form-group">
                        <label id="clientname">AMOUNT (&#8358;):</label>
                        <input type="number" required="required" name="amount" id="amount" placeholder="Custom commission amount here" class="form-control" />
                    </div>

                </div>
                <div class="modal-footer">
                    <div id="loading-image-comm-create-modal" style="width: 100%; display: none;" class="progress-bar progress-bar-danger progress-bar-striped active">Please, wait...</div>
                    <button type="button" id="close-button-comm-create-modal" data-dismiss="modal" class="btn btn-default md-close">Close</button>
                    <button type="button" id="create-button-comm-create-modal" class="btn btn-primary" onclick="createCommission(this.form);">Create Commission</button>
                </div>
            </form>
        </div>
    </div>
</div>




<script>
    dis_sch_id = 0
    dis_proc_type = 0;
    dis_prj_id = 0;
    function initSplit(sch_id, proc_type, prj_id){
        // console.log(sch_id + ' --- ' + proc_type + ' --- ' + prj_id);
//        alert('Work on-going!');

//        if(confirm('Are you sure you want to initiate the splitting of the selected commission?')){
        $('#proceed-button-modal').fadeIn();
        $('#close-button-modal').fadeIn();
        $('#split_narration').val('');
        dis_url = '<?php echo site_url('commission/initSplit') . '/'; ?>' + sch_id + (prj_id != 0 ? '/' + prj_id : '');
        console.log(dis_url);
        //return 0;
        dis_sch_id = sch_id;
        dis_proc_type = proc_type;
        dis_prj_id = prj_id;
//            return 0;
//            $('#close-button').fadeOut();
        $('.comm-pay-btn').fadeOut();
        $('.comm-delete-btn').fadeOut();
        $('#loading-image-' + proc_type + '-' + sch_id).fadeIn();
        $.ajax({
            type: 'GET',
            url: dis_url,
            data_type: 'json',
            success: function(data){
                $('.comm-pay-btn').fadeIn();
                $('.comm-delete-btn').fadeIn();
                $('#loading-image-' + proc_type + '-' + sch_id).fadeOut();
                if(data.status){

                    dis_display = '';
                    dis_len = data.beneficiaries.length;
                    d = data.beneficiaries;

                    dis_display = dis_display + '<table class="table table-striped table-bordered">';
                    dis_display = dis_display + '<thead><tr>';
                    dis_display = dis_display + '<th>S/NO</th>';
                    dis_display = dis_display + '<th>NAME</th>';
                    dis_display = dis_display + '<th>ACCOUNT NUMBER</th>';
                    dis_display = dis_display + '<th>BANK CODE</th>';
                    dis_display = dis_display + '<th>AMOUNT (&#8358;)<br />[ LESS &#8358;<?php echo  NIBSS_TRANS_CHARGE; ?> ]</th>';
                    dis_display = dis_display + '<th>SHARE (%)</th>';
                    dis_display = dis_display + '</tr></thead><tbody>';

                    for(k = 0; k < dis_len; k++){
                        dis_display = dis_display + '<tr><td>' + ( k + 1) + '. </td>';
                        dis_display = dis_display + '<td>' + d[k].accname + '</td>';
                        dis_display = dis_display + '<td>' + d[k].accno + '</td>';
                        dis_display = dis_display + '<td>' + d[k].bankcode + '</td>';
                        dis_display = dis_display + '<td>' + d[k].amount_formatted + '</td>';
                        dis_display = dis_display + '<td>' + d[k].share + '</td></tr>';
                    }
                    dis_display = dis_display + '</tbody></table>';

                    dis_display = dis_display + '<h4><strong>Total Amount:</strong> &#8358;' + data.total_amount + '</h4>';
                    dis_display = dis_display + '<h4><strong>Total Charge:</strong> &#8358;' + data.total_charges + '</h4>';
                    dis_display = dis_display + '<h4><strong>Total Amount Less Charges:</strong> &#8358;' + data.total_amount_less_charges + '</h4>';
                    dis_display = dis_display + '<h4><strong>Total Count:</strong> ' + data.total_count + '</h4>';

                    $('#split-details').html(dis_display);
                    $('#split-details-modal').modal('show');
                } else {
                    $('#proceed-button-modal').fadeIn();
                    alert(data.msg);
                }
            }
        });
    }



    function processSplit(){
        sch_id = dis_sch_id;
        proc_type = dis_proc_type;
        narration = $('#split_narration').val();
        if(narration == ''){
//            alert('Please, enter a valid narration');
//            return 0;
        }

        if(confirm('Are you sure you want to trigger the splitting of the selected commission?')){
            dis_url = '<?php echo site_url('commission/processSplit') . '/'; ?>' + sch_id + (dis_prj_id != 0 ? '/' + dis_prj_id : '');
            console.log(dis_url);
            // return 0;
//            $('#close-button').fadeOut();
            $('.comm-pay-btn').fadeOut();
            $('.comm-delete-btn').fadeOut();
            $('#loading-image-' + proc_type + '-' + sch_id).fadeIn();
            $('#loading-image-modal').fadeIn();
            $('#proceed-button-modal').fadeOut();
            $('#close-button-modal').fadeOut();
            $.ajax({
                type: 'POST',
                url: dis_url,
                data_type: 'json',
                data: 'narration=' + narration,
                success: function(data){
//                    console.log("DONE! \n" + data.msg);
                    if(data.status){
                        $('.comm-pay-btn-container-' + sch_id).html('<span class="label label-success">Split processed</span> <button type="button" id="show-details-btn-' + sch_id + '" onclick="showSplit(' + sch_id + ', 2)" class="btn btn-info show-details-btn">Show Split</button><div style="width: 100%; display: none;" id="loading-image-display-only-2-' + sch_id + '" class="progress-bar progress-bar-danger progress-bar-striped active">Please wait...</div>');
                        $('#split-details-modal').modal('hide');
                    } else {
                        document.getElementById('split_narration').focus();
                        $('#proceed-button-modal').fadeIn();
                        $('#close-button-modal').fadeIn();
                        alert(data.msg);
                    }
                    $('#loading-image-' + proc_type + '-' + sch_id).fadeOut();
                    $('#loading-image-modal').fadeOut();
                    $('.comm-pay-btn').fadeIn();
                    $('.comm-delete-btn').fadeIn();
                }
            });
        }
    }


    function showSplit(sch_id, proc_type){
        dis_url = '<?php echo site_url('commission/showSplit') . '/'; ?>' + sch_id;
        $('.comm-pay-btn').fadeOut();
        $('.comm-delete-btn').fadeOut();
        $('.show-details-btn').fadeOut();
        $('#loading-image-display-only-2-' + sch_id).fadeIn();
        console.log(dis_url);
        $.ajax({
            type: 'GET',
            url: dis_url,
            data_type: 'json',
            success: function(data){
//                    console.log("DONE! \n" + JSON.stringify(data));
//                    $('.comm-pay-btn-container-' + sch_id).html('<span class="label label-success">Commission processed</span>');
                $('.comm-pay-btn').fadeIn();
                $('.comm-delete-btn').fadeIn();
                $('.show-details-btn').fadeIn();
                $('#loading-image-display-only-2-' + sch_id).fadeOut();

                dis_display = '';
                dis_len = data.beneficiaries.length;
                d = data.beneficiaries;

                dis_display = dis_display + '<table class="table table-striped table-bordered">';
                dis_display = dis_display + '<thead><tr>';
                dis_display = dis_display + '<th>S/NO</th>';
                dis_display = dis_display + '<th>NAME</th>';
                dis_display = dis_display + '<th>ACCOUNT NUMBER</th>';
                dis_display = dis_display + '<th>BANK CODE</th>';
                dis_display = dis_display + '<th>AMOUNT (&#8358;)<br />[ LESS &#8358;<?php echo  NIBSS_TRANS_CHARGE; ?> ]</th>';
                dis_display = dis_display + '<th>SHARE (%)</th>';
                dis_display = dis_display + '</tr></thead><tbody>';

                for(k = 0; k < dis_len; k++){
                    dis_display = dis_display + '<tr><td>' + ( k + 1) + '. </td>';
                    dis_display = dis_display + '<td>' + d[k].accname + '</td>';
                    dis_display = dis_display + '<td>' + d[k].accno + '</td>';
                    dis_display = dis_display + '<td>' + d[k].bankcode + '</td>';
                    dis_display = dis_display + '<td>' + d[k].amount_formatted + '</td>';
                    dis_display = dis_display + '<td>' + d[k].share + '</td></tr>';
                }
                dis_display = dis_display + '</tbody></table>';

                $('#split-details-display-only').html(dis_display);
                $('#split-details-display-only-modal').modal('show');
            }
        });
    }


    function createCommission(dis_form){
        if(confirm('Are you sure?')){
            dis_form.submit();
        }
    }



    function deleteCommission(id){
        if(confirm('Are you sure you want to delete this commission?')){
            dis_url = '<?php echo site_url('commission/deleteCommission') . '/'; ?>' + id;
//            console.log(dis_url);
//            $('#close-button').fadeOut();
            $('.comm-delete-btn').fadeOut();
            $('.comm-pay-btn').fadeOut();
            $('#loading-del-image-' + id).fadeIn();
            $.ajax({
                type: 'GET',
                url: dis_url,
                data_type: 'json',
//                data: 'narration=' + narration,
                success: function(data){
//                    console.log("DONE! \n" + data.msg);
                    if(data.status){
                        alert('Deleted successfully!');
                        location.reload();
                    } else {
                        $('.comm-delete-btn').fadeIn();
                        $('.comm-pay-btn').fadeIn();
                        alert(data.msg);
                    }
                    $('#loading-del-image-' + id).fadeOut();
                }
            });
        }
    }




</script>