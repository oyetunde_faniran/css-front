<?php
    $client_dd['0'] = '--All Clients--';
    foreach($clients as $c){
        $client_dd[$c['client_id']] = $c['client_name'] . ' (' . $c['client_code'] . ')';
    }
?>

<div class="row" style="border-bottom: 1px solid #CCC; padding-bottom: 20px;">
        <div class="container">
            <div id="accordion3" class="panel-group accordion accordion-semi" style="margin-top: 20px;">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion3" href="#ac3-1" aria-expanded="false" class="collapsed"><i class="icon s7-angle-down"></i> Click here to filter</a></h4>
                            </div>
                            <div id="ac3-1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <form method="post" role="form">
                                        <div class="col-md-6">
                                            <div class="form-group col-md-12">
                                                <label>FROM DATE</label>
                                                <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker col-md-12">
                                                    <input size="16" type="text" value="<?php echo (!empty($fromdate) ? $fromdate : date('Y-m-d')); ?>" name="fromdate" id="fromdate" class="form-control" />
                                                    <span class="input-group-addon btn btn-primary"><i class="icon-th s7-date"></i></span>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>TO DATE</label>
                                                <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker col-md-12">
                                                    <input size="16" type="text" value="<?php echo (!empty($todate) ? $todate : date('Y-m-d')); ?>" name="todate" id="todate" class="form-control" />
                                                    <span class="input-group-addon btn btn-primary"><i class="icon-th s7-date"></i></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
<!--                                            <div class="form-group col-md-12">
                                                <label>CLIENT</label>
                                                <?php echo form_dropdown('client', $client_dd, set_value('client'), 'class="form-control"'); ?>
                                            </div>-->
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-lg btn-space btn-primary">Filter</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
    </div>

<?php
//    die('<pre>' . print_r($trans, true));
    $display = '<div class="table-responsive"><table id="report-table" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>S/NO</th>
                            <th>CLIENT</th>
                            <!--<th>SCHEDULE</th>
                            <th>DESCRIPTION</th>-->
                            <th>TOTAL COMMISSION (&#8358;)</th>
                            <th>YOUR SHARE (&#8358;)</th>
                            <th>YOUR PERCENTAGE SHARE (% / &#8358;)</th>
                            <th>BANK CREDITED</th>
                            <th>BANK ACC. NO CREDITED</th>
                            <th>STATUS</th>
                            <th>DATE PROCESSED</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>S/NO</th>
                            <th>CLIENT</th>
                            <!--<th>SCHEDULE</th>
                            <th>DESCRIPTION</th>-->
                            <th>TOTAL COMMISSION (&#8358;)</th>
                            <th>YOUR SHARE (&#8358;)</th>
                            <th>YOUR PERCENTAGE SHARE (% / &#8358;)</th>
                            <th>BANK CREDITED</th>
                            <th>BANK ACC. NO CREDITED</th>
                            <th>STATUS</th>
                            <th>DATE PROCESSED</th>
                        </tr>
                    </tfoot>
                    <tbody>';
    $sno = $total = $comm_total = $comm_successful = 0;
//    die('<pre>' . print_r($trans, true));
    foreach($trans as $t){
        $dis_status = $t['split_process_response_code'] == '16' ? '<span class="label label-success">Processed Successfully</span>' : '<span class="label label-danger">NOT Processed Successfully</span>';
        if($t['schcomm_split_status']){
            $split_process_btn = '<div class="comm-pay-btn-container-' . $t['schcomm_id'] . '"><button class="btn-danger comm-pay-btn" onclick="initSplit(' . $t['schcomm_id'] . ', 2);">Split Commission</button></div><div style="width: 100%; display: none;" id="loading-image-2-' . $t['schcomm_id'] . '" class="progress-bar progress-bar-danger progress-bar-striped active">Please wait...</div>';
        } else {
            $split_process_btn = '<span class="label label-success">Split processed</span> <button type="button" id="show-details-btn-' . $t['schcomm_id'] . '" onclick="showSplit(' . $t['schcomm_id'] . ', 2)" class="btn btn-info show-details-btn">Show Split</button><div style="width: 100%; display: none;" id="loading-image-display-only-2-' . $t['schcomm_id'] . '" class="progress-bar progress-bar-danger progress-bar-striped active">Please wait...</div>';
        }
        $display .= '<tr>
                        <td>' . (++$sno) . '.</td>
                        <td>' . $t['prj_name'] . '</td>
                        <!--<td>' . '--' . '</td>
                        <td>' . '--' . '</td>-->
                        <td>' . number_format($t['schcomm_amount'], 2) . '</td>
                        <td>' . number_format($t['split_amount'], 2) . '</td>
                        <td>' . number_format($t['split_share'], 2) . '</td>
                        <td>' . $t['bank_name'] . '</td>
                        <td>' . $t['split_accno'] . '</td>
                        <td>' . $dis_status . '</td>
                        <td>' . $t['split_dateadded'] . '</td>
                    </tr>';
    }
    $display .= '</tbody></table></div>';
    echo $display;
?>




<div id="split-details-display-only-modal" tabindex="-1" role="dialog" class="modal fade colored-header">
    <div class="modal-dialog custom-width">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i
                        class="icon s7-close"></i></button>
                <h3 class="modal-title">Split Details</h3>
            </div>
            <div id="msg-container-details-display-only"></div>
            <div class="modal-body form" style="overflow: auto; max-height: 400px;">
                <div id="split-details-display-only"></div>
            </div>
            <div class="modal-footer">
                <div id="loading-image-display-only-modal" style="width: 100%; display: none;" class="progress-bar progress-bar-danger progress-bar-striped active">Please, wait...</div>
                <button type="button" id="close-button-display-only-modal" data-dismiss="modal" class="btn btn-default md-close">Close</button>
            </div>
        </div>
    </div>
</div>








<script>
    dis_sch_id = 0
    dis_proc_type = 0;
    function initSplit(sch_id, proc_type){
//        alert('Work on-going!');
        
//        if(confirm('Are you sure you want to initiate the splitting of the selected commission?')){
        if(true){
            dis_url = '<?php echo site_url('commission/initSplit') . '/'; ?>' + sch_id;
            console.log(dis_url);
            dis_sch_id = sch_id;
            dis_proc_type = proc_type;
//            return 0;
//            $('#close-button').fadeOut();
            $('.comm-pay-btn').fadeOut();
            $('#loading-image-' + proc_type + '-' + sch_id).fadeIn();
            $.ajax({
                type: 'GET',
                url: dis_url,
                data_type: 'json',
                success: function(data){
//                    console.log("DONE! \n" + JSON.stringify(data));
//                    $('.comm-pay-btn-container-' + sch_id).html('<span class="label label-success">Commission processed</span>');
                    $('.comm-pay-btn').fadeIn();
                    $('#loading-image-' + proc_type + '-' + sch_id).fadeOut();
                    
                    dis_display = '';
                    dis_len = data.beneficiaries.length;
                    d = data.beneficiaries;
                    
                    dis_display = dis_display + '<table class="table table-striped table-bordered">';
                    dis_display = dis_display + '<thead><tr>';
                    dis_display = dis_display + '<th>S/NO</th>';
                    dis_display = dis_display + '<th>NAME</th>';
                    dis_display = dis_display + '<th>ACCOUNT NUMBER</th>';
                    dis_display = dis_display + '<th>BANK CODE</th>';
                    dis_display = dis_display + '<th>AMOUNT (&#8358;)</th>';
                    dis_display = dis_display + '<th>SHARE (%)</th>';
                    dis_display = dis_display + '</tr></thead><tbody>';
                    
                    for(k = 0; k < dis_len; k++){
                        dis_display = dis_display + '<tr><td>' + ( k + 1) + '. </td>';
                        dis_display = dis_display + '<td>' + d[k].accname + '</td>';
                        dis_display = dis_display + '<td>' + d[k].accno + '</td>';
                        dis_display = dis_display + '<td>' + d[k].bankcode + '</td>';
                        dis_display = dis_display + '<td>' + d[k].amount_formatted + '</td>';
                        dis_display = dis_display + '<td>' + d[k].share + '</td></tr>';
                    }
                    dis_display = dis_display + '</tbody></table>';
                    
                    $('#split-details').html(dis_display);
                    $('#split-details-modal').modal('show');
                }
            });
        }
    }
    
    
    
    function processSplit(){
        sch_id = dis_sch_id;
        proc_type = dis_proc_type;
        
        if(confirm('Are you sure you want to trigger the splitting of the selected commission?')){
            dis_url = '<?php echo site_url('commission/processSplit') . '/'; ?>' + sch_id;
            console.log(dis_url);
//            $('#close-button').fadeOut();
            $('.comm-pay-btn').fadeOut();
            $('#loading-image-' + proc_type + '-' + sch_id).fadeIn();
            $('#loading-image-modal').fadeIn();
            $('#proceed-button-modal').fadeOut();
            $('#close-button-modal').fadeOut();
            $.ajax({
                type: 'GET',
                url: dis_url,
                data_type: 'json',
                success: function(data){
                    console.log("DONE! \n" + data);
                    $('.comm-pay-btn-container-' + sch_id).html('<span class="label label-success">Split processed</span> <button type="button" id="show-details-btn-' + sch_id + '" onclick="showSplit(' + sch_id + ', 2)" class="btn btn-info show-details-btn">Show Split</button><div style="width: 100%; display: none;" id="loading-image-display-only-2-' + sch_id + '" class="progress-bar progress-bar-danger progress-bar-striped active">Please wait...</div>');
                    $('.comm-pay-btn').fadeIn();
                    $('#loading-image-' + proc_type + '-' + sch_id).fadeOut();
                    $('#loading-image-modal').fadeOut();
                    $('#split-details-modal').modal('hide');
                }
            });
        }
    }
    
    
    function showSplit(sch_id, proc_type){
        dis_url = '<?php echo site_url('commission/showSplit') . '/'; ?>' + sch_id;
        $('.comm-pay-btn').fadeOut();
        $('.show-details-btn').fadeOut();
        $('#loading-image-display-only-2-' + sch_id).fadeIn();
        console.log(dis_url);
        $.ajax({
            type: 'GET',
            url: dis_url,
            data_type: 'json',
            success: function(data){
//                    console.log("DONE! \n" + JSON.stringify(data));
//                    $('.comm-pay-btn-container-' + sch_id).html('<span class="label label-success">Commission processed</span>');
                $('.comm-pay-btn').fadeIn();
                $('.show-details-btn').fadeIn();
                $('#loading-image-display-only-2-' + sch_id).fadeOut();

                dis_display = '';
                dis_len = data.beneficiaries.length;
                d = data.beneficiaries;

                dis_display = dis_display + '<table class="table table-striped table-bordered">';
                dis_display = dis_display + '<thead><tr>';
                dis_display = dis_display + '<th>S/NO</th>';
                dis_display = dis_display + '<th>NAME</th>';
                dis_display = dis_display + '<th>ACCOUNT NUMBER</th>';
                dis_display = dis_display + '<th>BANK CODE</th>';
                dis_display = dis_display + '<th>AMOUNT (&#8358;)</th>';
                dis_display = dis_display + '<th>SHARE (%)</th>';
                dis_display = dis_display + '</tr></thead><tbody>';

                for(k = 0; k < dis_len; k++){
                    dis_display = dis_display + '<tr><td>' + ( k + 1) + '. </td>';
                    dis_display = dis_display + '<td>' + d[k].accname + '</td>';
                    dis_display = dis_display + '<td>' + d[k].accno + '</td>';
                    dis_display = dis_display + '<td>' + d[k].bankcode + '</td>';
                    dis_display = dis_display + '<td>' + d[k].amount_formatted + '</td>';
                    dis_display = dis_display + '<td>' + d[k].share + '</td></tr>';
                }
                dis_display = dis_display + '</tbody></table>';

                $('#split-details-display-only').html(dis_display);
                $('#split-details-display-only-modal').modal('show');
            }
        });
    }
    
    
    function createCommission(dis_form){
        if(confirm('Are you sure?')){
            dis_form.submit();
        }
    }
    
</script>