<?php
	$panel_title = '<p>Account Balances in <span class="label label-default">' . $report_data['account_count'] . ' accounts</span> as at <span class="label label-default">' . $report_data['latest_date'] . '</span></p>';
	$panel_title = '...';//'Latest balance as at <span class="label label-default">' . $report_data['latest_date'] . '</span>';
?>

<div class="row">
	<div class="col-md-12">
	  <div class="panel panel-alt1">
		<div class="panel-heading">
		  <span class="title">ACCOUNT DETAILS</span>
		</div>
		<div class="panel-body">
			<div>
<?php
    echo '<table class="table table-striped table-hover">
			<tr>
				<td class="col-md-1"><strong>BANK:</strong></td>
				<td><span class="label label-success" style="font-size: 16px;">' . $bank_name . '</span></td>
			</tr>
			<tr>
				<td><strong>ACCOUNT:</strong></td>
				<td><span class="label label-info" style="font-size: 16px;">' . $account_name . '</span></td>
			</tr>
			<tr>
				<td><strong>TIME FRAME:</strong></td>
				<td><span class="label label-danger" style="font-size: 16px;">' . $timeframe . '</span></td>
			</tr>
		  </table>';
?>
			</div>
		</div>
	  </div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
	  <div class="panel panel-alt3">
		<div class="panel-heading">
		  <span class="title">LINE GRAPH</span>
		</div>
		<div class="panel-body">
			<div id="all-accounts-bar-chart"></div>
		</div>
	  </div>
	</div>
</div>



<div class="row">
	<div class="col-md-12">
	  <div class="panel panel-primary">
		<div class="panel-heading">
		  <span class="title">DATA TABLE</span>
		</div>
		<div class="panel-body">
			<table class="table table-condensed table-hover table-bordered table-striped" id="report-table">
				<thead>
					<tr>
						<td>S/NO</td>
						<td>DATE/TIME</td>
						<td>BALANCE (&#8358;)</td>
						<td>BALANCE CHANGE (&#8358;)</td>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td>S/NO</td>
						<td>DATE/TIME</td>
						<td>BALANCE (&#8358;)</td>
						<td>BALANCE CHANGE (&#8358;)</td>
					</tr>
				</tfoot>
				<tbody>
<?php
	$display = '';
	$sno = 0;
	$old_value = 0;
	$first_run = true;
	foreach ($report_data['data'] as $acc){
		$change = !$first_run ? ($acc['accbal_amount'] - $old_value) : '--';
		$change_style = $change > 0 ? 'label-info' : ($change < 0 ? 'label-danger' : 'label-warning');
		$display .= '<tr>
						<td>' . (++$sno) . '.</td>
						<td>' . $acc['formatted_date'] . '</td>
						<td><strong>' . number_format($acc['accbal_amount'], 2) . '</strong></td>
						<td><span class="label ' . $change_style . '" style="font-size: 1em;">' . number_format($change, 2) . '</span></td>
					</tr>';
		$old_value = $acc['accbal_amount'];
		$first_run = false;
	}
	/* $display .= '<tr>
					<td><h3>TOTAL: </h3></td>
					<td>' . number_format(1, 2) . '</td>
				 </tr>'; */
	echo $display;
?>
				</tbody>
			</table>
<?php
	//echo '<h2>&#8358;' . number_format($report_data['total'], 2) . '</h2>';
	//echo '<h4>' . $report_data['amt_in_words'] . '</h4>';
	//echo('<pre>-->***' . print_r($report_data, true));
	echo anchor('reports', '&laquo; Back to Reports Home', 'class="btn btn-danger"');
?>
		</div>
	  </div>
	</div>
</div>