<?php
	$panel_title = '<p>Account Balances in <span class="label label-default">' . $report_data['account_count'] . ' accounts</span> as at <span class="label label-default">' . $report_data['latest_date'] . '</span></p>';
	$panel_title = 'Latest balance as at <span class="label label-default">' . $report_data['latest_date'] . '</span>';
?>
<div class="row">
	<div class="col-md-12">
	  <div class="panel panel-alt2">
		<div class="panel-heading">
		  <span class="title">Account Categories</span>
		</div>
		<div class="panel-body">
            <div class="spacer xs-mb-15">
              <div class="btn-group btn-group-justified">
<?php
    $acc_cat_display = '<a href="' . site_url('reports/do_report/account/total/') . '" class="btn btn-primary ' . ($cur_cat == 0 ? ' btn-alt2 ' : '') . '">All Account Categories</a>';
    foreach ($acc_cats as $dis_acc_cat){
        $cur_cat_class = $dis_acc_cat['accat_id'] == $cur_cat ? ' btn-alt2 ' : '';
        $acc_cat_display .= '<a href="' . site_url('reports/do_report/account/total/' . $dis_acc_cat['accat_id']) . '" class="btn btn-primary ' . $cur_cat_class . '">' . $dis_acc_cat['accat_name'] . '</a>';
    }
    echo $acc_cat_display;
?>
              </div>
            </div>
		</div>
	  </div>
    </div>
</div>
<div class="row">
	<div class="col-md-5">
	  <div class="panel panel-alt1">
		<div class="panel-heading">
		  <span class="title"><?php echo $panel_title ?></span>
		</div>
		<div class="panel-body">
			<div id="all-accounts-pie-chart"></div>
		</div>
	  </div>
	</div>
	
	<div class="col-md-7">
	  <div class="panel panel-alt3">
		<div class="panel-heading">
		  <span class="title"><?php echo $panel_title ?></span>
		</div>
		<div class="panel-body">
			<div id="all-accounts-bar-chart"></div>
		</div>
	  </div>
	</div>
</div>



<div class="row">
	<div class="col-md-12">
	  <div class="panel panel-primary">
		<div class="panel-heading">
		  <span class="title"><?php echo $panel_title ?></span>
		</div>
		<div class="panel-body">
<?php
//    echo '<div style="margin: 30px 0px;">' . anchor('account_balance_updater', 'Get Current Balance', 'id="get-balance-btn" class="btn btn-info" onclick="$(\'#get-balance-btn\').fadeOut();$(\'#big-loader-div\').fadeIn();"') . '</div>';
?>
			<table class="table table-condensed table-hover table-bordered table-striped" id="report-table">
				<thead>
					<tr>
						<td>S/NO</td>
						<td>BANK NAME</td>
						<td>ACCOUNT NAME</td>
						<td>ACCOUNT NUMBER</td>
						<td>BALANCE (&#8358;)</td>
						<td>ACTIONS</td>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td>S/NO</td>
						<td>BANK NAME</td>
						<td>ACCOUNT NAME</td>
						<td>ACCOUNT NUMBER</td>
						<td>BALANCE (&#8358;)</td>
						<td>ACTIONS</td>
					</tr>
				</tfoot>
<?php
	$display = '';
	$total = $sno = 0;
	foreach ($report_data['data'] as $acc){
		$cph_button = '<span class="col-md-3">
							<form action="' . site_url('reports') . '" method="post">
								<input type="hidden" name="account" value="' . $acc['acc_id'] . '" />
								<input type="hidden" name="showtotal" value="0" />
								<input type="hidden" name="aggregate_type" value="1" />
								<button class ="btn btn-primary" type="submit">Change Per Hour</button>
							</form>
						</span>';
		$cpd_button = '<span class="col-md-3">
							<form action="' . site_url('reports') . '" method="post">
								<input type="hidden" name="account" value="' . $acc['acc_id'] . '" />
								<input type="hidden" name="showtotal" value="0" />
								<input type="hidden" name="aggregate_type" value="2" />
								<button class ="btn btn-info" type="submit">Change Per Day</button>
							</form>
						</span>';
		$cpm_button = '<span class="col-md-3">
							<form action="' . site_url('reports') . '" method="post">
								<input type="hidden" name="account" value="' . $acc['acc_id'] . '" />
								<input type="hidden" name="showtotal" value="0" />
								<input type="hidden" name="aggregate_type" value="3" />
								<button class ="btn btn-warning" type="submit">Change Per Month</button>
							</form>
						</span>';
        
        $latestbal_warning = $acc['accbal_datetime_fetched'] != '0000-00-00 00:00:00' ? "<br /><div class='label label-danger'>As at {$acc['accbal_datetime_fetched']}</div>" : '';
        if($acc['accbal_datetime_fetched'] != '0000-00-00 00:00:00'){
            $latestbal_warning = "<br /><div class='label label-danger'>As at {$acc['accbal_datetime_fetched']}</div>";
        } else {
            $latestbal_warning = $acc['accbal_balance_nevergotten'] == 1 ? "<br /><div class='label label-warning'>Balance never gotten</div>" : '';
        }
		$display .= '<tr>
						<td>' . (++$sno) . '.</td>
						<td>' . $acc['bank_name'] . '</td>
						<td>' . $acc['acc_name'] . '</td>
						<td>' . $acc['acc_number'] . '</td>
						<td><strong>' . number_format($acc['accbal_amount'], 2) . $latestbal_warning . '</strong></td>
						<td>' . $cph_button . $cpd_button . $cpm_button . '</td>
					</tr>';
		/* $display .= '<tr>
						<td>' . (++$sno) . '.</td>
						<td>' . $acc['bank_name'] . '</td>
						<td>' . $acc['acc_name'] . '</td>
						<td>' . $acc['acc_number'] . '</td>
						<td><strong>' . number_format($acc['accbal_amount'], 2) . '</strong></td>
						<td>' .
							anchor('#', 'Change Per Hour', 'class ="btn btn-primary"') . ' ' .
							anchor('#', 'Change Per Day', 'class ="btn btn-info"') . ' ' .
							anchor('#', 'Change Per Month', 'class ="btn btn-warning"') .
						'</td>
					</tr>'; */
		$total += $acc['accbal_amount'];
	}
	
	$total_cph_button = '<span class="col-md-3">
							<form action="' . site_url('reports') . '" method="post">
								<input type="hidden" name="showtotal" value="0" />
								<input type="hidden" name="aggregate_type" value="1" />
								<button class ="btn btn-primary" type="submit">Change Per Hour</button>
							</form>
						</span>';
	$total_cpd_button = '<span class="col-md-3">
							<form action="' . site_url('reports') . '" method="post">
								<input type="hidden" name="showtotal" value="0" />
								<input type="hidden" name="aggregate_type" value="2" />
								<button class ="btn btn-info" type="submit">Change Per Day</button>
							</form>
						</span>';
	$total_cpm_button = '<span class="col-md-3">
							<form action="' . site_url('reports') . '" method="post">
								<input type="hidden" name="showtotal" value="0" />
								<input type="hidden" name="aggregate_type" value="3" />
								<button class ="btn btn-warning" type="submit">Change Per Month</button>
							</form>
						</span>';
	
	$display .= '<tbody>
					<tr>
						<td colspan="4"><h3>TOTAL: </h3></td>
						<td><strong>' . number_format($total, 2) . '</strong></td>
						<td>' . $total_cph_button . $total_cpd_button . $total_cpm_button . '</td>
					 </tr>
				 </tbody>';
	echo $display;
?>
			</table>
<?php
	//echo '<h2>&#8358;' . number_format($report_data['total'], 2) . '</h2>';
	//echo '<h4>' . $report_data['amt_in_words'] . '</h4>';
	//echo('<pre>-->***' . print_r($report_data, true));
	echo anchor('reports', '&laquo; Back to Reports Home', 'class="btn btn-danger"');
?>
		</div>
	  </div>
	</div>
</div>
<div id="big-loader-div" style="top: 0px; left: 0px; display: none; position: absolute; width: 100%; height: 100%; background: rgba(100, 100, 100, 0.7); text-align: center; padding: 100px;">
    <?php echo img('assets/img/ajax-loader.gif'); ?>
</div>