<?php
$status_dd['-1'] = '--Select Status--';
foreach($npp_responses as $crow){
    $status_dd[$crow['id']] = $crow['description'];
}

if(!empty($msg)){
    echo $msg;
}

if($trans['sch_status'] == '0' && ($trans['sch_ben_count_successful_only'] > 0 || $trans['sch_ben_count_failed_only'] > 0)){
    $push_button = '<a target="_blank" href="http://css.ng/v1prod/css_api_v2_response_repush/doRepushFromScheduleTable/' . $trans['client_code'] . '/' . $trans['sch_sched_id'] . '" class="btn btn-warning">Re-push Feedback</a>';
} else {
    $push_button = '';
}

//    die('<pre>' . print_r($trans, true));
    $display = '
<div style="width: 50%;">
    <table class="table table-bordered table-condensed table-striped">
        <tr>
            <td><strong>CLIENT:</strong></td>
            <td>' . $trans['client_name'] . '(' . $trans['client_code'] . ')' . '</td>
        </tr>
        <tr>
            <td><strong>SCHEDULE ID (Client):</strong></td>
            <td>' . $trans['sch_sched_id'] . '</td>
        </tr>
        <tr>
            <td><strong>SCHEDULE ID (NIBSS):</strong></td>
            <td>' . $trans['sch_sched_id_nibss'] . '</td>
        </tr>
        <tr>
            <td><strong>TOTAL AMOUNT:</strong></td>
            <td> &#8358;' . number_format($trans['sch_total_amount'] , 2). '</td>
        </tr>
        <tr>
            <td><strong>TOTAL BENEFICIARIES:</strong></td>
            <td>' . number_format($trans['sch_ben_count'], 0). '</td>
        </tr>
        <tr>
            <td><strong>COMPLETED BENEFICIARIES:</strong></td>
            <td>' . number_format($trans['sch_ben_count_successful_only'] + $trans['sch_ben_count_failed_only'] , 0). '</td>
        </tr>
        <tr>
            <td><strong>IN PROGRESS:</strong></td>
            <td>' . number_format($trans['sch_ben_count'] - ($trans['sch_ben_count_successful_only'] + $trans['sch_ben_count_failed_only']) , 0). '</td>
        </tr>
        <tr>
            <td><strong>DATE CREATED:</strong></td>
            <td>' . $trans['sch_dateadded'] . '</td>
        </tr>
        <tr>
            <td><strong>LAST UPDATED:</strong></td>
            <td>' . $trans['sch_lastupdated'] . '</td>
        </tr>
    </table>
</div>
<form action="" method="post" id="beneficiary-form">
                <div class="row" style="border-bottom: 1px solid #CCC;">
                    <div class="container" style="padding: 20px;">
                        <button type="button" id="checkall" onclick="doCheckAll(this.form);">Check / Uncheck All</button>' .
                        form_dropdown('status', $status_dd, set_value(0)) .
                        '<button type="submit" name="submit_btn" value="submit_btn" class="btn btn-success">Set Status</button>
                        ' . $push_button . '
                    </div>
                </div>
            <div class="table-responsive">
                <table id="report-table" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>S/NO</th>
                            <th>BENEFICIARY NAME</th>
                            <th>ACCOUNT NUMBER</th>
                            <th>BANK CODE</th>
                            <th>AMOUNT(&#8358;)</th>
                            <th>STATUS CODE</th>
                            <th>STATUS</th>
                            <th>REASON</th>
                            <th>NARRATION</th>
                            <th>SCHEDULE SERIAL NO</th>
                            <th>LAST UPDATED</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>S/NO</th>
                            <th>BENEFICIARY NAME</th>
                            <th>ACCOUNT NUMBER</th>
                            <th>BANK CODE</th>
                            <th>AMOUNT(&#8358;)</th>
                            <th>STATUS CODE</th>
                            <th>STATUS</th>
                            <th>REASON</th>
                            <th>NARRATION</th>
                            <th>SCHEDULE SERIAL NO</th>
                            <th>LAST UPDATED</th>
                        </tr>
                    </tfoot>
                    <tbody>';
    $sno = $total = $comm_total = $comm_successful = 0;
    foreach($bens as $row){
        switch ($row['ben_statuscode']){
            case '': if($trans['sch_status'] == '0'){
                         $dis_status = '<span class="label label-warning">In Progress</span>';
                     } else {
                        $dis_status = '<span class="label label-info">Pending Process</span>';
                     }
                    break;
            case '06': $dis_status = '<span class="label label-warning">In Progress</span>'; break;
            case '00': $dis_status = '<span class="label label-success">Paid</span>'; break;
            default: $dis_status = '<span class="label label-danger">Payment Failed</span>'; break;
        }
        if($trans['sch_status'] == '0' && ($row['ben_statuscode'] == '06' || $row['ben_statuscode'] == '')){
            $checkbox = '<input name="ben_checked[]" type="checkbox" value="' . $row['ben_id'] . '" id="checkbox-' . $row['ben_id'] . '" />';
        } else {
            $checkbox = '';
        }
        $display .= '<tr>
                        <td>' . (++$sno) . '. ' . $checkbox . '</td>
                        <td><label for="checkbox-' . $row['ben_id'] . '">' . $row['ben_name'] . '</label></td>
                        <td><label for="checkbox-' . $row['ben_id'] . '">' . $row['ben_accno'] . '</label></td>
                        <td>' . $row['ben_bankcode'] . '</td>
                        <td>' . number_format($row['ben_amount'],2) . '</td>
                        <td>' . $row['ben_statuscode'] . '</td>
                        <td>' . $dis_status . '</td>
                        <td>' . $row['ben_reason'] . '</td>
                        <td>' . $row['ben_narration'] . '</td>
                        <td>' . $row['ben_serialno'] . '</td>
                        <td>' . $row['ben_lastupdated'] . '</td>
                    </tr>';
    }
    $display .= '</tbody></table></div>';
    echo $display;
?>


<script>
    function processComm(sch_id, proc_type){
        if(confirm('Are you sure you want to process the payment of commission for this schedule?')){
            dis_url = '<?php echo site_url('commission/processCommission') . '/'; ?>' + sch_id + '/' + proc_type;
            console.log(dis_url);
//            $('#close-button').fadeOut();
            $('.comm-pay-btn').fadeOut();
            $('#loading-image-' + proc_type + '-' + sch_id).fadeIn();
            $.ajax({
                type: 'GET',
                url: dis_url,
                data_type: 'json',
                success: function(data){
                    console.log("DONE! \n" + data);
                    $('.comm-pay-btn-container-' + sch_id).html('<span class="label label-success">Commission processed</span>');
                    $('.comm-pay-btn').fadeIn();
                    $('#loading-image-' + proc_type + '-' + sch_id).fadeOut();
//                    $('#loading-image').fadeOut();
//                    $('#msg-container').html(data.msg);
//                    $('#msg-container').fadeIn();
//                    if(data.status){
//    //                    dis_timer = setInterval(reloadClients, 2000);
//                        window.location = '<?php //echo site_url('client'); ?>';
//                    } else {
//                        $('#close-button').fadeIn();
//                        $('#save-button').fadeIn();
//                    }
                }
            });
        }
    }


    boxchecked = false;
    function doCheckAll(dis_form){
        boxchecked = !boxchecked;
        //ben_checked
        checkboxes = document.getElementsByName('ben_checked[]');
        for(var i = 0, n = checkboxes.length; i < n; i++) {
            checkboxes[i].checked = boxchecked;
        }
    }
    
</script>