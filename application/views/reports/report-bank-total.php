<?php
	//die('<pre>-->' . print_r($report_data, true));
	$panel_title = '<p>Account Balances in <span class="label label-default">' . $report_data['account_count'] . ' accounts</span> as at <span class="label label-default">' . $report_data['latest_date'] . '</span></p>';
	$panel_title = 'Latest balance as at <span class="label label-default">' . $report_data['latest_date'] . '</span>';
?>
<div class="row">
	<div class="col-md-5">
	  <div class="panel panel-alt1">
		<div class="panel-heading">
		  <span class="title"><?php echo $panel_title ?></span>
		</div>
		<div class="panel-body">
			<div id="all-accounts-pie-chart"></div>
		</div>
	  </div>
	</div>
	
	<div class="col-md-7">
	  <div class="panel panel-alt3">
		<div class="panel-heading">
		  <span class="title"><?php echo $panel_title ?></span>
		</div>
		<div class="panel-body">
			<div id="all-accounts-bar-chart"></div>
		</div>
	  </div>
	</div>
</div>



<div class="row">
	<div class="col-md-12">
	  <div class="panel panel-primary">
		<div class="panel-heading">
		  <span class="title"><?php echo $panel_title ?></span>
		</div>
		<div class="panel-body">
			<table class="table table-condensed table-hover table-bordered table-striped" id="report-table">
				<thead>
					<tr>
						<td>S/NO</td>
						<td>BANK NAME</td>
						<td>NUMBER OF ACCOUNT(S)</td>
						<td>BALANCE (&#8358;)</td>
						<td>ACTIONS</td>
					</tr>
				</thead>
<?php
	$display = '';
	$sno = 0;
	foreach ($report_data['data'] as $acc){
		$display .= '<tr>
						<td>' . (++$sno) . '.</td>
						<td>' . $acc['bank_name'] . '</td>
						<td>' . $acc['acc_count'] . ' <!--[ ' . anchor('#', 'View Account(s)') . ' ]--></td>
						<td><strong>' . number_format($acc['bank_amount'], 2) . '</strong></td>
						<td>' .
							anchor('#', 'Change Per Hour', 'class ="btn btn-primary"') . ' ' .
							anchor('#', 'Change Per Day', 'class ="btn btn-info"') . ' ' .
							anchor('#', 'Change Per Month', 'class ="btn btn-warning"') .
						'</td>
					</tr>';
	}
	echo $display;
?>
			</table>
<?php
	//echo '<h2>&#8358;' . number_format($report_data['total'], 2) . '</h2>';
	//echo '<h4>' . $report_data['amt_in_words'] . '</h4>';
	//echo('<pre>-->***' . print_r($report_data, true));
	echo anchor('reports', '&laquo; Back to Reports Home', 'class="btn btn-danger"');
?>
		</div>
	  </div>
	</div>
</div>