<?php
	//Form the bank dropdown options
	$bank_dd[0] = '--All Banks--';
	foreach ($banks as $b){
		$bank_dd[$b['bank_id']] = $b['bank_name'];
	}
	
	//Form the account dropdown options
	$acc_dd[0] = '--All Accounts--';
	$bank_name = '';
	$inner_array = array();
	foreach ($accs as $acc){
		//If the bank name is changing and it's not the first one, add it to the final array
		if ($bank_name != '' && $bank_name != $acc['bank_name']){
			$acc_dd[$bank_name] = $inner_array;
			$inner_array = array();
		}
		
		$inner_array[$acc['acc_id']] = $acc['acc_name'];
		
		//Update the name of the current bank
		$bank_name = $acc['bank_name'];
	}
	if (!empty($inner_array)){
		$acc_dd[$bank_name] = $inner_array;
	}
    
    //Form the account categories dropdown options
    $acc_cat_dd[0] = '--All Account Categories--';
    foreach ($acc_cats as $a){
        $acc_cat_dd[$a['accat_id']] = $a['accat_name'];
    }
    
?>


<form role="form" action="" method="post">
	<div class="row">
		<div class="col-md-12">
		  <div class="panel panel-alt4">
			<div class="panel-heading">
			  <span class="title">Generate Reports</span>
			</div>
			<div class="panel-body">
				<div class="col-md-6">
					<div class="form-group">
					  <label>Bank</label>
					  <?php echo form_dropdown('bank', $bank_dd, null, 'class="form-control"'); ?>
					</div>
					<div class="form-group">
					  <label>Account</label>
					  <?php echo form_dropdown('account', $acc_dd, null, 'class="form-control"'); ?>
					</div>
                    <div class="form-group">
					  <label>Account Category</label>
					  <?php echo form_dropdown('account_cat', $acc_cat_dd, null, 'class="form-control"'); ?>
					</div>
					<div class="form-group">
						<label>From (Date/Time)</label>
						<div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker col-md-12">
							<input size="16" type="text" value="" name="fromdate" class="form-control" />
							<span class="input-group-addon btn btn-primary"><i class="icon-th s7-date"></i></span>
						</div>
					</div>
					<div class="form-group">
					  <label>To (Date/Time)</label>
						<div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker col-md-12">
						  <input size="16" type="text" value="" name="todate" class="form-control"><span class="input-group-addon btn btn-primary"><i class="icon-th s7-date"></i></span>
						</div>
					</div>
				</div>
			  <div class="col-md-6">
				<!--<div class="form-group">
				  <label for="showtotal">Show Totals instead of balance change trends</label>
					<div class="switch-button switch-button-lg">
						<input type="checkbox" checked="" name="showtotal" value="1" id="showtotal">
						<span><label for="swt4"></label></span>
					</div>
				</div>-->
				
				<div class="form-group">
					<div class="am-radio">
						<input type="radio" name="showtotal" checked="checked" value="1" id="totals">
						<label for="totals">Show Totals</label>
					</div>
					<div class="am-radio">
						<input type="radio" name="showtotal" value="0" id="balance-change">
						<label for="balance-change">Show Balance Change Trends</label>
					</div>
				</div>
				
				<div>
					<label>For balance change trends, show per</label>
					<div class="am-radio">
						<input type="radio" name="aggregate_type" checked="checked" value="1" id="rad1">
						<label for="rad1">Hour</label>
					</div>
					<div class="am-radio">
						<input type="radio" name="aggregate_type" value="2" id="rad2">
						<label for="rad2">Day</label>
					</div>
					<div class="am-radio">
						<input type="radio" name="aggregate_type" value="3" id="rad3">
						<label for="rad3">Month</label>
					</div>
					<div class="am-radio">
						<input type="radio" name="aggregate_type" value="4" id="rad4">
						<label for="rad4">Year</label>
					</div>
				</div>
				<div class="form-group spacer text-left" style="margin-top: 20px;">
				  <button type="submit" class="btn btn-space btn-primary">Generate Report</button>
				</div>
			  </div>
			</div>
		  </div>
		</div>
	<!--<div class="col-md-4">
	  <div class="panel panel-primary">
		<div class="panel-heading">
		  <span class="title">All Accounts</span>
		</div>
		<div class="panel-body">
		  <table class="table table-hover table-striped">
			<tr><td><?php echo anchor('reports/do_report/total/total', 'Latest total balance in all accounts'); ?></td></tr>
			<tr><td><?php echo anchor('reports/do_report/total/cph', 'Total change per hour'); ?></td></tr>
			<tr><td><?php echo anchor('reports/do_report/total/cpd', 'Total change per day'); ?></td></tr>
			<tr><td><?php echo anchor('reports/do_report/total/cpm', 'Total change per month'); ?></td></tr>
		  </table>
		</div>
	  </div>
	</div>


	<div class="col-md-4">
	  <div class="panel panel-alt1">
		<div class="panel-heading">
		  <span class="title">Per Account</span>
		</div>
		<div class="panel-body">
		  <table class="table table-hover table-striped">
			<tr><td><?php echo anchor('reports/do_report/account/total', 'Latest Balance'); ?></td></tr>
			<tr><td><?php echo anchor('reports/do_report/account/cph', 'Change per hour'); ?></td></tr>
			<tr><td><?php echo anchor('reports/do_report/account/cpd', 'Change per day'); ?></td></tr>
			<tr><td><?php echo anchor('reports/do_report/account/cpm', 'Change per month'); ?></td></tr>
		  </table>
		</div>
	  </div>
	</div>
	
	<div class="col-md-4">
	  <div class="panel panel-alt3">
		<div class="panel-heading">
		  <span class="title">Per Bank</span>
		</div>
		<div class="panel-body">
		  <table class="table table-hover table-striped">
			<tr><td><?php echo anchor('reports/do_report/bank/total', 'Latest Total Balance'); ?></td></tr>
			<tr><td><?php echo anchor('reports/do_report/bank/cph', 'Total change per hour'); ?></td></tr>
			<tr><td><?php echo anchor('reports/do_report/bank/cpd', 'Total change per day'); ?></td></tr>
			<tr><td><?php echo anchor('reports/do_report/bank/cpm', 'Total change per month'); ?></td></tr>
		  </table>
		</div>
	  </div>
	</div>-->
	</div>
</form>