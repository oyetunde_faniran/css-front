<div class="row">
	<div class="col-md-8">
	  <div class="panel panel-primary">
		<div class="panel-heading">
		  <span class="title"><?php echo '<p>Total in all <span class="label label-default">' . $report_data['account_count'] . ' accounts</span> as at <span class="label label-default">' . $report_data['latest_date'] . '</span></p>'; ?></span>
		</div>
		<div class="panel-body">
<?php
	echo '<h2>&#8358;' . number_format($report_data['total'], 2) . '</h2>';
	echo '<h4>' . $report_data['amt_in_words'] . '</h4>';
	echo anchor('reports', '&laquo; Back to Reports Home', 'class="btn btn-danger"');
?>
		</div>
	  </div>
	</div>
</div>