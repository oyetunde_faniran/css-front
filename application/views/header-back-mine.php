<?php /*die('<pre>' . print_r($_SESSION, true));*/ $dis_sys = $this->system_authorization; ?><!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png'); ?>">
    <title>
<?php
    $page_title = !empty($page_title) ? $page_title : (!empty($title) ? $title : '');
    $dis_page_title = !empty($page_title) ? "$page_title - " : '';
    echo $dis_page_title . DEFAULT_TITLE;
?>
	</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/lib/stroke-7/style.css'); ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/lib/jquery.nanoscroller/css/nanoscroller.css'); ?>"/>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/lib/theme-switcher/theme-switcher.min.css'); ?>"/>-->
    <link type="text/css" href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">  </head>
  <body>
    <div class="am-wrapper am-fixed-sidebar">
      <nav class="navbar navbar-default navbar-fixed-top am-top-header">
        <div class="container-fluid">
          <div class="navbar-header">
            <div class="page-title"><span>[ BAMS ]</span></div><a href="#" class="am-toggle-left-sidebar navbar-toggle collapsed"><span class="icon-bar"><span></span><span></span><span></span></span></a><a href="<?php echo site_url(); ?>" class="navbar-brand"></a>
          </div>
<!--          <a href="#" class="am-toggle-right-sidebar"><span class="icon s7-menu2"></span></a>-->
          <a href="#" data-toggle="collapse" data-target="#am-navbar-collapse" class="am-toggle-top-header-menu collapsed"><span class="icon s7-angle-down"></span></a>
          <div id="am-navbar-collapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right am-user-nav">
              <li class="dropdown">
				<a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle">
					<img src="<?php echo base_url('assets/img/avatar.png'); ?>">
					<span class="user-name"><?php echo $_SESSION['firstname']; ?></span>
					<span class="angle-down s7-angle-down"></span>
				</a>
                <ul role="menu" class="dropdown-menu">
                  <li><a href="<?php echo site_url('sys-admin/user/update_profile'); ?>"> <span class="icon s7-user"></span>My profile</a></li>
                  <li><a href="<?php echo site_url('logout'); ?>"> <span class="icon s7-power"></span>Sign Out</a></li>
                </ul>
              </li>
            </ul>
            <ul class="nav navbar-nav am-nav-right">
              <li><a href="<?php echo site_url(); ?>">Home</a></li>
              <li><a href="<?php echo site_url('sys-admin/user/update_profile'); ?>">My Profile</a></li>
              <li><a href="<?php echo site_url('logout'); ?>">Log Out</a></li>
            </ul>
          </div>
        </div>
      </nav>
      <div class="am-left-sidebar">
        <div class="content">
          <div class="am-logo"></div>
          <ul class="sidebar-elements">
            <li class="parent"><a href="<?php echo site_url(); ?>"><i class="icon s7-monitor"></i><span>Dashboard</span></a></li>
            <li class="parent"><a href="#"><i class="icon s7-note2"></i><span>Accounts</span></a>
              <ul class="sub-menu">
                <?php if ($dis_sys->hasAccess('accounts-list')){ ?><li><a href="<?php echo site_url('reports/do_report/account/total'); ?>">View Accounts</a></li><?php } ?>
                <?php if ($dis_sys->hasAccess('accounts-categories')){ ?><li><a href="<?php echo site_url('account/categorize'); ?>">Categorize Accounts</a></li><?php } ?>
                <?php if ($dis_sys->hasAccess('accounts-categories')){ ?><li><a href="<?php echo site_url('contractor_category/add'); ?>">Add Category</a></li><?php } ?>
                <?php if ($dis_sys->hasAccess('accounts-categories')){ ?><li><a href="<?php echo site_url('contractor_category/view'); ?>">View Categories</a></li><?php } ?>
              </ul>
            </li>
            <?php if ($dis_sys->hasAccess('accounts-balance-reports')){ ?><li class="parent"><a href="<?php echo site_url('reports'); ?>"><i class="icon s7-graph"></i><span>Reports</span></a></li><?php } ?>
            <li class="parent"><a href="#"><i class="icon s7-shuffle"></i><span>Accounts Transfer</span></a>
              <ul class="sub-menu">
                <li><a href="<?php echo site_url('account_transfer/do_transfer'); ?>">Transfer Funds</a></li>
                <li><a href="<?php echo site_url('account_transfer/reports'); ?>">Transfer Reports</a></li>
              </ul>
            </li>
<!--			<li class="parent"><a href="http://localhost/paychoice_fcmb" target="_blank"><i class="icon s7-expand2"></i><span>Vendor Payment</span></a></li>
            			<li class="parent">
                            <form method="post" action="http://localhost/paychoice_fcmb/sys-admin/login/login_page" target="_blank" id="vendor-payment-form">
                                <input type="hidden" name="username" value="serah@serah.comm"/>
                                <input type="hidden" name="password" value="password2$"/>
                                <input type="hidden" name="mcode" value="0033"/>
                            </form>
                            <a href="#" onclick="document.getElementById('vendor-payment-form').submit();"><i class="icon s7-expand2"></i><span>Vendor Payment</span></a>
                        </li>-->
 <li class="parent"><a href="#" ><i class="icon s7-shuffle"></i><span>Vendor Payment</span></a>
            <ul class="sub-menu">
                      <li><a href="<?php echo site_url('vendors/vendor/add'); ?>">New Vendor</a></li>
                      <li><a href="<?php echo site_url('vendors/vendor/adminList'); ?>">View Vendors</a></li>
                      <li><a href="<?php echo site_url('vendors/vendor/payList'); ?>">Pay Vendors</a></li>
                      <li><a href="<?php echo site_url('payments/schedule/forwardList'); ?>">Forward Payments</a></li>
                      
                      <li><a href="<?php echo site_url('payments/schedule/listPayment'); ?>">Authorized Payments</a></li>
                  </ul>
</li>
            <li class="parent"><a href="#"><i class="icon s7-settings"></i><span>Settings</span></a>
              <ul class="sub-menu">
                <?php if ($dis_sys->hasAccess('upl-user-add')){ ?><li><a href="<?php echo site_url('sys-admin/user/add'); ?>">Add User</a></li><?php } ?>
				<?php if ($dis_sys->hasAccess('upl-user-view')){ ?><li><a href="<?php echo site_url('sys-admin/user/view'); ?>">View Users</a></li><?php } ?>
				<?php if ($dis_sys->hasAccess('upl-usergroup-add')){ ?><li><a href="<?php echo site_url('sys-admin/usergroup/add'); ?>">Add User Group</a></li><?php } ?>
				<?php if ($dis_sys->hasAccess('upl-usergroup-view')){ ?><li><a href="<?php echo site_url('sys-admin/usergroup/view'); ?>">View User Groups</a></li><?php } ?>
				<!--<li><a href="#">Notification Settings</a></li>-->
              </ul>
            </li>
          </ul>
          <!--Sidebar bottom content-->
        </div>
      </div>
	  <div class="<?php echo (empty($skip_main_container_class) ? 'am-content' : ''); ?>" style="padding: 10px;">
<?php
	if (empty($skip_main_container_class)){
		echo '<div class="page-head" style="margin-bottom: 10px;">
				<h2>' . $page_title . '</h2>
			</div>';
	}
?>