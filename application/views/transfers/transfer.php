<style>
    table#dr_acc_table td, table#dr_acc_table th {
        padding: 0 10px
    }

    table#dr_acc_table .num {
        text-align: right
    }
</style>
<?php
//Form the Debit account list
$dr_acc_dd = "<table id='dr_acc_table'>";
$bank_name = '';
foreach ($accs as $acc) {
    //If the bank name is changing and it's not the first one, add it to the final array
    if ($bank_name != $acc['bank_name']) {
        $dr_acc_dd .= '<tr><th colspan="5">' . $acc['bank_name'] . '</th></tr>';
        $dr_acc_dd .= "<tr><th></th><th>Account Name</th><th class='num'>Account Number</th><th class='num'>Balance (&#8358;)</th><th>Amount to Transfer (&#8358;)</th></tr>";
    }

    $dr_acc_dd .= '<tr><td>';
    if ($acc['accbal_amount'] > 0) {
        $dr_acc_dd .= '<input type="checkbox" name="fromaccount[]" value="' . $acc['acc_id'] . '" id="acc' . $acc['acc_id'] . '" onclick="toggleEnableAmountBox(' . $acc['acc_id'] . ')"';
        $dr_acc_dd .= set_checkbox('fromaccount[]', $acc['acc_id']);
        $dr_acc_dd .= '>';
    }

    $dr_acc_dd .= '</td>';
    $dr_acc_dd .= '<td><label for="acc' . $acc['acc_id'] . '">' . $acc['acc_name'] . '</label></td>';
    $dr_acc_dd .= '<td class="num">' . $acc['acc_number'] . '</td>';
    $dr_acc_dd .= '<td class="num">' . number_format($acc['accbal_amount'], 2) . '</td>';
    $dr_acc_dd .= '<td>';

    if ($acc['accbal_amount'] > 0) {
        $dr_acc_dd .= '<input type="number" step=".01" min="1" name="amount[' . $acc['acc_id'] . ']" id="amount' . $acc['acc_id'] . '"';
        $dr_acc_dd .= 'value="' . set_value("amount[{$acc['acc_id']}]", '') . '"';
        $dr_acc_dd .= strpos(set_checkbox('fromaccount[]', $acc['acc_id']), 'checked') !== false ? '' : 'disabled';
        $dr_acc_dd .= '>';
    } else {
        $dr_acc_dd .= '--';
    }
    $dr_acc_dd .= '</td></tr>';


    //Update the name of the current bank
    $bank_name = $acc['bank_name'];
}

$dr_acc_dd .= "</table>";

//Form the Credit account dropdown option
$acc_dd[''] = '--Select Account--';
$bank_name = '';
$inner_array = array();
foreach ($accs as $acc) {
    //If the bank name is changing and it's not the first one, add it to the final array
    if ($bank_name != '' && $bank_name != $acc['bank_name']) {
        $acc_dd[$bank_name] = $inner_array;
        $inner_array = array();
    }

    $inner_array[$acc['acc_id']] = $acc['acc_name'] . ' - ' . $acc['acc_number'] . ' (&#8358; ' . number_format($acc['accbal_amount'], 2) . ')';

    if (empty($selected)) {
        $selected = strpos(set_select('toaccount', $acc['acc_id']), 'selected') !== false ? $acc['acc_id'] : null;
    }

    //Update the name of the current bank
    $bank_name = $acc['bank_name'];
}
if (!empty($inner_array)) {
    $acc_dd[$bank_name] = $inner_array;
}
$msg = !empty($msg) ? $msg : '';
echo $msg;

?>

<div class="panel panel-default col-md-12">
    <div class="panel-body">
        <form role="form" action="#" method="post">
            <div class="form-group" style="height: 300px; overflow: auto">
                <label>From Account</label>
                <?php echo $dr_acc_dd; ?>
            </div>
            <div class="form-group">
                <label>To Account</label>
                <?php echo form_dropdown('toaccount', $acc_dd, $selected, 'class="form-control"'); ?>
            </div>
            <div class="form-group">
                <label>Transfer Narration</label>
                <input type="text" name="narration" id="narration" placeholder="Enter transfer narration here"
                       class="form-control" oldautocomplete="remove" autocomplete="off"
                       value="<?php echo set_value('narration', ''); ?>">
            </div>
            <div class="spacer text-right">
                <button type="submit" class="btn btn-space btn-primary">Transfer Funds</button>
            </div>
        </form>
    </div>
</div>

<script>
    function toggleEnableAmountBox(accID) {
        var amountBox = $('#amount' + accID);
        if (amountBox.attr('disabled') == undefined) {
            amountBox.attr('disabled', 'disabled');
        } else {
            amountBox.removeAttr('disabled').val(amountBox.val() || 1).focus();
        }
    }
</script>