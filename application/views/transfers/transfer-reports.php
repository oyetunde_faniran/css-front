<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-body">
                <table class="table table-condensed table-hover table-bordered table-striped" id="report-table">
                    <thead>
                    <tr>
                        <td>S/NO</td>
                        <td>AMOUNT (&#8358;)</td>
                        <td>BANK NAME (FROM)</td>
                        <td>ACCOUNT NAME (FROM)</td>
                        <td>ACCOUNT NUMBER (FROM)</td>
                        <td>BANK NAME (TO)</td>
                        <td>ACCOUNT NAME (TO)</td>
                        <td>ACCOUNT NUMBER (TO)</td>
                        <td>DATE TRANSFERRED</td>
                        <td>TRANSFERRED BY</td>
                        <td>STATUS</td>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <td>S/NO</td>
                        <td>AMOUNT (&#8358;)</td>
                        <td>BANK NAME (FROM)</td>
                        <td>ACCOUNT NAME (FROM)</td>
                        <td>ACCOUNT NUMBER (FROM)</td>
                        <td>BANK NAME (TO)</td>
                        <td>ACCOUNT NAME (TO)</td>
                        <td>ACCOUNT NUMBER (TO)</td>
                        <td>DATE TRANSFERRED</td>
                        <td>TRANSFERRED BY</td>
                        <td>STATUS</td>
                    </tr>
                    </tfoot>
                    <tbody>
                    <?php
                    $display = '';
                    $total = $sno = 0;
                    foreach ($transfers as $trans) {
                        $display .= '<tr>
						<td>' . (++$sno) . '.</td>
						<td>' . number_format($trans['transreq_amount']) . '</td>
						<td>' . $trans['bank_name_from'] . '</td>
						<td>' . $trans['acc_name_from'] . '</td>
						<td>' . $trans['acc_no_from'] . '</td>
						<td>' . $trans['bank_name_to'] . '</td>
						<td>' . $trans['acc_name_to'] . '</td>
						<td>' . $trans['acc_no_to'] . '</td>
						<td>' . $trans['date_formatted'] . '</td>
						<td>' . $trans['name_initiator'] . '</td>
						<td>' . ($trans['transreq_responsecode'] == '00' ? '<span class="label label-info">Successful</span>' : '<span class="label label-danger">Failed</span>') . '</td>
					</tr>';
                        $total += $trans['transreq_amount'];
                    }

                    /* $display .= '<tr>
                                    <td>&nbsp;</td>
                                    <td><h3>' . number_format($total, 2) . '</h3></td>
                                    <td colspan="9"></td>
                                 </tr>'; */
                    echo $display;
                    ?>
                    </tbody>
                </table>
                <?php
                //echo '<h2>&#8358;' . number_format($report_data['total'], 2) . '</h2>';
                //echo '<h4>' . $report_data['amt_in_words'] . '</h4>';
                //echo('<pre>-->***' . print_r($report_data, true));
                echo anchor('reports', '&laquo; Back to Reports Home', 'class="btn btn-danger"');
                ?>
            </div>
        </div>
    </div>
</div>