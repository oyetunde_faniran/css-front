<div class="panel panel-default col-md-12">
    <div class="panel-body">
        <form role="form" action="" method="post">
            <table class="table table-condensed table-hover table-bordered table-striped">
                <tr>
                    <td><strong>From Accounts</strong></td>
                    <td>
                        <table class="table table-condensed">
                            <tr>
                                <th>Account Name</th>
                                <th>Account Number</th>
                                <th>Bank Name</th>
                                <th style="text-align: right">Amount (&#8358;)</th>
                            </tr>
                            <?php
                            $display = array();
                            $total_amount = 0;
                            foreach ($fromaccountDetails as $key => $fromaccountDetail) {
                                $total_amount += $amount[$fromaccountDetail['acc_id']];
                                echo '<tr>';
                                echo '<td>' . $fromaccountDetail['acc_name'] . '<input type="hidden" value="' . $fromaccountDetail['acc_id'] . '" name="fromaccount[]"></td>';
                                echo '<td>' . $fromaccountDetail['acc_number'] . '<input type="hidden" value="' . $amount[$fromaccountDetail['acc_id']] . '" name="amount[' . $fromaccountDetail['acc_id'] . ']"></td>';
                                echo '<td>' . $fromaccountDetail['bank_name'] . '</td>';
                                echo '<td style="text-align: right">' . number_format($amount[$fromaccountDetail['acc_id']], 2) . '</td>';
                                echo '</tr>';
                            }
                            ?>
                            <tr><th colspan="3">TOTAL</th><th style="text-align: right"><?php echo number_format($total_amount, 2) ?></th></tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td><strong>To Account</strong></td>
                    <td>
                        <?php echo $toaccountDetails['acc_name'] . "-" . $toaccountDetails['acc_number'] ?>
                        <input type='hidden' value="<?php echo $toaccountDetails['acc_id'] ?>" name="toaccount"></td>
                </tr>
                <tr>
                    <td><strong>To Bank</strong></td>
                    <td>
                        <?php echo $toaccountDetails['bank_name'] ?>
                </tr>
                <tr>
                    <td><strong>Narration</strong></td>
                    <td><input type="hidden" name="narration" id="narration"
                               value="<?php echo $narration ?>"> <?php echo $narration ?></td>
                </tr>
            </table>
            <div class="spacer text-right">
                <input type="hidden" name="confirmed" value="1">
                <button type="submit" name="Confirm Transfer" class="btn btn-space btn-primary">Confirm Transfer
                </button>
            </div>
        </form>
    </div>
</div>
