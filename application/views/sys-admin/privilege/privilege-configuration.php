<?php
    //die ('<pre>' . print_r($privileges, true) . '</pre>');
    $display = '';
    try {
        if(!$show_privileges){
            throw new Exception('Unknown user group selected. Please, ' . anchor('sys-admin/usergroup/view', 'click here to select a user group') . ' for which privileges would be configured.');
        }

        if (empty($tasks)){
            throw new Exception('No task created yet. Please, ' . anchor('sys-admin/task/add', 'click here to create a task') . '.');
        }

        //Show success/error messages if any
        $display .= !empty($msg) ? $msg : '';

        $priv_array = !empty($privileges) ? $privileges : '';

        $display .= form_open();
        $display .= '<p><button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save Privilege Configuration</button></p>';
        $display .= '<table cellspacing="0" class="' . UPL_STYLE_TABLE . ' table table-striped">
                        <thead>
                            <tr class="' . UPL_STYLE_TABLE_HEADER . '">
                                <th>S/NO</th>
                                <th>DESCRIPTION</th>
                            </tr>
                        </thead>
                        <tbody>';
        $s_no = 0;
        $use_style1 = true;
        $row_style1 = UPL_STYLE_TABLE_ROW1;
        $row_style2 = UPL_STYLE_TABLE_ROW2;
        $old_module = 0;
        foreach ($tasks as $t){
            //Show the module header if needed
            if ($old_module != $t['module_id']){
                $display .= '<tr class="' . UPL_STYLE_TABLE_SUB_HEADER . '">
                                <td colspan="8">
                                    ' . form_checkbox('module[]', $t['task_id'], false, 'id="checkAll_' . $t['module_id'] . '" onclick="checkAll(' . $t['module_id'] . ')"') . '
                                    <span class="label label-success">MODULE: ' . stripslashes($t['module']) . '</span>
                                </td>
                             </tr>';
            }
            $row_style = $use_style1 ? $row_style1 : $row_style2;
            $use_style1 = !$use_style1;
            $display .= '<tr class="' . $row_style . '">
                            <td>' . form_checkbox('privilege[]_' . $t['module_id'], $t['task_id'], isset($priv_array[$t['task_id']]), 'id="check_' . $t['module_id'] . '"') . ' ' . (++$s_no) . '.</td>
                            <td>' . stripslashes($t['task_description']) . '</td>
                         </tr>';
            $old_module = $t['module_id'];
        }   //END foreach()
        $display .= '</tbody></table>';
        $display .= '<p><button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save Privilege Configuration</button></p>';
        $display .= form_close();
        $display .= '<p>' . anchor("sys-admin/usergroup/view", '&laquo; Back to List of User Groups') . '</p>';
    } catch (Exception $ex) {
        $display = $ex->getMessage();
    }

    echo $display;
?>
<script type="text/javascript">
    function checkAll(moduleId){
        var moduleSelect = document.getElementById('checkAll_'+moduleId);
        var arrayCheckboxes = document.getElementsByName('privilege[]_'+moduleId);
        //alert(arrayCheckboxes.length);

        var i = 0;
        var checked = moduleSelect.checked;
        while(i < arrayCheckboxes.length){
            if (arrayCheckboxes[i].id == 'check_' + moduleId){
                arrayCheckboxes[i].checked = checked;
            }
            i++;
        }
    }
</script>
