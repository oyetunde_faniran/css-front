<?php
    $form_display = '';

    try {
        //Confirm that the user was found and the details were successfully loaded
        if (empty($task_deets)){
            throw new Exception('Unknown task selected.');
        }

        //Format the user group array for drop-down
        $modules_array = array('0' => '--Select Module--');
        if (!empty($modules)){
            foreach ($modules as $m){
                $modules_array[$m['module_id']] = stripslashes($m['module_name']);
            }
        }

        //Format the task array for drop-down (if any)
        $task_array = array('0' => '--No Parent--');
        if (!empty($tasks)){
            foreach ($tasks as $t){
                $task_array[$t['task_id']] = stripslashes($t['task_description']);
            }
        }

        //Show success/error messages if any
        $form_display .= !empty($msg) ? $msg : '';

        //Show the actual form
        $form_display .= form_open()
                        . '<table border="0">
                                <tr>
                                    <td><label for="module">Module:</label></td>
                                    <td>' . form_dropdown('module', $modules_array, $task_deets['module_id'], 'id="module"') . '</td>
                                </tr>
                                <tr>
                                    <td><label for="parent">Parent Task:</label></td>
                                    <td>' . form_dropdown('parent', $task_array, $task_deets['task_parent_id'], 'id="parent"') . '</td>
                                </tr>
                                <tr>
                                    <td><label for="code">Task Code:</label></td>
                                    <td>' . form_input('code', $task_deets['task_code'], 'id="code" size="30"') . '</td>
                                </tr>
                                <tr>
                                    <td><label for="url">URL (<em>Controller/Method</em>):</label></td>
                                    <td>' . form_input('url', $task_deets['task_url'], 'id="url" size="20"') . '</td>
                                </tr>
                                <tr>
                                    <td><label for="description">Description:</label></td>
                                    <td>' . form_input('description', $task_deets['task_description'], 'id="description" size="50"') . '</td>
                                </tr>
                                <tr>
                                    <td><label>Activate/Deactivate:</label></td>
                                    <td>
                                        ' . form_checkbox('active', 1, ($task_deets['task_enabled'] == '1'), 'id="active"') . '
                                        <label for="active">Activate this task</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label>Show in Nav. Bar:</label></td>
                                    <td>
                                        ' . form_checkbox('navbarshow', 1, ($task_deets['task_show_in_nav_bar'] == '1'), 'id="navbarshow"') . '
                                        <label for="navbarshow">Show in the navigation bar</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col1">&nbsp;</td>
                                    <td class="col2">
                                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Save Changes</button>
                                    </td>
                                </tr>
                           </table>'
                        . form_close();

    } catch (Exception $ex) {
        $form_display = $ex->getMessage();
    }

    $form_display .= '<p>' . anchor("sys-admin/task/view", '&laquo; Back to List of Tasks') . '</p>';
    echo $form_display;