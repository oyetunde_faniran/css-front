<?php
    $display = '<div style="margin:10px 0px;">' . anchor('sys-admin/task/add', '<button class="btn btn-info"><i class="fa fa-plus-circle"></i> Add New Task</button>') . '</div>';

    if (!empty($tasks)){
        $display .= '<table cellspacing="0" class="' . UPL_STYLE_TABLE . '">
                        <thead>
                            <tr class="' . UPL_STYLE_TABLE_HEADER . '">
                                <td>S/NO</td>
                                <td>DESCRIPTION</td>
                                <td>CODE</td>
                                <td>PARENT</td>
                                <td>URL</td>
                                <td>ACTIVE</td>
                                <td>SHOW IN NAV. BAR</td>
                                <td><em>Edit</em></td>
                            </tr>
                        </thead>
                        <tbody>';
        $s_no = 0;
        $use_style1 = true;
        $row_style1 = UPL_STYLE_TABLE_ROW1;
        $row_style2 = UPL_STYLE_TABLE_ROW2;
        $old_module = 0;
        foreach ($tasks as $t){
            //Show the module header if needed
            if ($old_module != $t['module_id']){
                $display .= '<tr class="' . UPL_STYLE_TABLE_SUB_HEADER . '">
                                <td colspan="8">MODULE: ' . stripslashes($t['module']) . '</td>
                             </tr>';
            }
            $row_style = $use_style1 ? $row_style1 : $row_style2;
            $use_style1 = !$use_style1;
            $display .= '<tr class="' . $row_style . '">
                            <td>' . (++$s_no) . '.</td>
                            <td>' . stripslashes($t['task_description']) . '</td>
                            <td>' . stripslashes($t['task_code']) . '</td>
                            <td>' . stripslashes($t['parent']) . '</td>
                            <td>' . stripslashes($t['task_url']) . '</td>
                            <td>' . ($t['task_enabled'] == 1 ? 'Yes' : 'No') . '</td>
                            <td>' . ($t['task_show_in_nav_bar'] == 1 ? 'Yes' : 'No') . '</td>
                            <td>' . anchor('sys-admin/task/edit/' . $t['task_id'], '<button class="btn btn-primary"><i class="fa fa-edit"></i></button>') . '</td>
                         </tr>';
            $old_module = $t['module_id'];
        }   //END foreach()
        $display .= '</tbody></table>';
    } else {
        $display = 'No task created yet. Please, ' . anchor('sys-admin/task/add', 'click here to create a task') . '.';
    }
    echo $display;