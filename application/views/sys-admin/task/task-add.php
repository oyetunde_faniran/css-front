<?php
    $form_display = '';

    //Format the user group array for drop-down
    $modules_array = array('0' => '--Select Module--');
    if (!empty($modules)){
        foreach ($modules as $m){
            $modules_array[$m['module_id']] = stripslashes($m['module_name']);
        }
    }

    //Format the task array for drop-down (if any)
    $task_array = array('0' => '--No Parent--');
    if (!empty($tasks)){
        foreach ($tasks as $t){
            $task_array[$t['task_id']] = stripslashes($t['task_description']);
        }
    }

    //Show success/error messages if any
    $form_display .= !empty($msg) ? $msg : '';

    //Show the actual form
    $form_display .= form_open()
                    . '<table border="0">
                            <tr>
                                <td><label for="module">Module:</label></td>
                                <td>' . form_dropdown('module', $modules_array, set_value('module'), 'id="module"') . '</td>
                            </tr>
                            <tr>
                                <td><label for="parent">Parent Task:</label></td>
                                <td>' . form_dropdown('parent', $task_array, set_value('parent'), 'id="parent"') . '</td>
                            </tr>
                            <tr>
                                <td><label for="code">Task Code:</label></td>
                                <td>' . form_input('code', set_value('code'), 'id="code" size="30"') . '</td>
                            </tr>
                            <tr>
                                <td><label for="url">URL (<em>Controller/Method</em>):</label></td>
                                <td>' . form_input('url', set_value('url'), 'id="url" size="20"') . '</td>
                            </tr>
                            <tr>
                                <td><label for="description">Description:</label></td>
                                <td>' . form_input('description', set_value('description'), 'id="description" size="50"') . '</td>
                            </tr>
                            <tr>
                                <td><label>Activate/Deactivate:</label></td>
                                <td>
                                    ' . form_checkbox('active', 1, true, 'id="active"') . '
                                    <label for="active">Activate this task</label>
                                </td>
                            </tr>
                            <tr>
                                <td><label>Show in Nav. Bar:</label></td>
                                <td>
                                    ' . form_checkbox('navbarshow', 1, set_value('navbarshow'), 'id="navbarshow"') . '
                                    <label for="navbarshow">Show in the navigation bar</label>
                                </td>
                            </tr>
                            <tr>
                                <td class="col1">&nbsp;</td>
                                <td class="col2">
                                    <button type="submit" class="btn btn-info"><i class="fa fa-plus-circle"></i> Add New Task</button>
                                </td>
                            </tr>
                       </table>'
                    . form_close();
    $form_display .= '<p>' . anchor("sys-admin/task/view", '&laquo; Back to List of Tasks') . '</p>';
    echo $form_display;