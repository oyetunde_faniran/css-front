<?php
    //Show success/error messages if any
    $form_display = !empty($msg) ? $msg : '';

    //Show the actual form
    $form_display .= form_open()
                    . '<table border="0">
                            <tr>
                                <td><label for="email">E-Mail:</label></td>
                                <td>' . form_input('email', set_value('email'), 'id="email" size="30"') . '</td>
                            </tr>
                            <tr>
                                <td><label for="password">Password:</label></td>
                                <td>' . form_password('password', '', 'id="password" size="30"') . '</td>
                            </tr>
                            <tr>
                                <td class="col1">&nbsp;</td>
                                <td class="col2">' . form_input(array('name' => 'submit_button', 'type' => 'submit', 'value' => 'Log In', 'class' => UPL_BUTTON_STYLE)) . '</td>
                            </tr>
                            <tr>
                                <td class="col1">&nbsp;</td>
                                <td class="col2">' . anchor('sys-admin/login/forgot_password', 'Forgot Password?', 'class="navbar-link"') . '</td>
                            </tr>
                       </table>'
                    . form_close();
    echo $form_display;