<?php
    //Show success/error messages if any
    $form_display = !empty($msg) ? $msg : '';

    //Generate token to be used to identify this instance of CAPTCHA
    $token = rand (10000, 1000000);

    //Show the actual form
    $form_display .= form_open()
                    . '<table border="0">
                            <tr>
                                <td><label for="email">E-Mail:</label></td>
                                <td>' . form_input('email', set_value('email'), 'id="email" size="30"') . '</td>
                            </tr>
                            <tr>
                                <td valign="top" align="left">
                                    <label for="Text Below"><strong>Text Below:</strong></label>
                                </td>
                                <td>
                                    ' . form_input('captcha_text', '', 'id="captcha-text" size="6" maxlength="6"') . '<br />
                                    ' . form_hidden('captcha_token', $token) . '
                                    ' . img('ic.php?zq=' . $token . '&c=w') . '
                                </td>
                            </tr>
                            <tr>
                                <td class="col1">&nbsp;</td>
                                <td class="col2">' . form_input(array('name' => 'submit_button', 'type' => 'submit', 'value' => 'Continue', 'class' => UPL_BUTTON_STYLE)) . '</td>
                            </tr>
                       </table>'
                    . form_close();
    echo $form_display;