<?php
    $form_display = '';

    try {
        //Confirm that the user was found and the details were successfully loaded
        if (empty($user_deets)){
            throw new Exception('Unable to fetch your details. Please, log out and log in again.');
        }
        
        if($_SESSION['first_timer']){
            $submit_btn = '<input type="checkbox" name="ga_confirmation" id ="ga_confirmation" onclick="enableSubmitBtn();" />
                            <label for="ga_confirmation">I have set up my Google Authenticator app successfully and I understand that I must use it to be able to log in subsequently.</label><br />
                            <button id="submit_btn" disabled="disabled" type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save Changes</button>';
            $ga_code_display = '<tr>
                                    <td><label><i class="fa fa-asterisk"> </i>GA Secret Key:</label></td>
                                    <td>
                                        <strong style="font-size: 2em;">' . $user_deets['user_ga_secretkey'] . '</strong>
                                        <em style="color: #F00; padding: 3px;">(Enter this secret key into your Google Authenticator app to set it up or simply scan the QR code below)</em><br /><br />
                                        ' . img($ga_qrcode_url) . '
                                    </td>
                                </tr>';
        } else {
            $submit_btn = '<button id="submit_btn" type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save Changes</button>';
            $ga_code_display = '';
        }
        
        

        //Show success/error messages if any
        $form_display .= !empty($msg) ? $msg : '';

        //Show the actual form
        $form_display .= form_open()
                        . '<table border="0" class="table table-striped">
                                <tr>
                                    <td><label for="firstname">First Name:</label></td>
                                    <td>' . form_input('firstname', stripslashes($user_deets['user_firstname']), 'id="firstname" size="30"') . '</td>
                                </tr>
                                <tr>
                                    <td><label for="surname">Surname:</label></td>
                                    <td>' . form_input('surname', stripslashes($user_deets['user_surname']), 'id="surname" size="30"') . '</td>
                                </tr>
                                <tr>
                                    <td><label for="phone">Phone:</label></td>
                                    <td>' . form_input('phone', $user_deets['user_phone'], 'id="phone" size="20"') . '</td>
                                </tr>
                                <tr>
                                    <td><label for="email">E-Mail:</label></td>
                                    <td>' . form_input('email', $user_deets['userlog_email'], 'id="email" size="50"') . '</td>
                                </tr>
                                <tr>
                                    <td><label>New Password:</label></td>
                                    <td>
                                        ' . form_password('newpassword', '', 'id="newpassword" size="20"') . '
                                        <em>(Leave blank if you do not wish to change your password)</em>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label>Confirm New Password:</label></td>
                                    <td>
                                        ' . form_password('cnewpassword', '', 'id="cnewpassword" size="20"') . '
                                        <em>(Leave blank if you do not wish to change your password)</em>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label><i class="fa fa-asterisk"> </i>Current Password:</label></td>
                                    <td>
                                        ' . form_password('password', '', 'id="password" size="20"') . '
                                        <em class="red" style="color: #FFF; padding: 3px;">(Required to make changes to your profile)</em>
                                    </td>
                                </tr>
                                ' . $ga_code_display . '
                                <tr>
                                    <td class="col1">&nbsp;</td>
                                    <td class="col2">
                                        ' . $submit_btn . '
                                    </td>
                                </tr>
                           </table>'
                        . form_close();

    } catch (Exception $ex) {
        $form_display = $ex->getMessage();
    }

    $form_display .= $this->system_authorization->hasAccess('upl-user-view') ? '<p>' . anchor("sys-admin/user/view", '&laquo; Back to List of Users') . '</p>' : '';
    echo $form_display;
?>
<script>
    function enableSubmitBtn(){
        sb = document.getElementById('submit_btn');
        sb.disabled = !sb.disabled;
    }
</script>