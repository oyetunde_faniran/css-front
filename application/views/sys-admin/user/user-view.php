<?php
$display = '<div style="margin:10px 0px;">' . anchor('sys-admin/user/add', '<button class="btn btn-primary"><i class="fa fa-plus-circle"></i> Add New User</button>') . '</div>';
if (!empty($users)) {
    $display .= '<table class="table table-striped table-hover table-fw-widget dataTable no-footer" role="grid" style="margin-bottom:0;">
                        <thead>
                            <tr>
                                <th>S/NO</th>
                                <!--<th>LGA</th>-->
                                <th>FIRST NAME</th>
                                <th>SURNAME</th>
                                <th>PHONE</th>
                                <th>E-MAIL</th>
                                <th>USER GROUP</th>
                                <th>DATE REGISTERED</th>
                                <th>LAST LOG-IN DATE</th>
                                <th>ACTIVE</th>
                                <th><em>Edit</em></th>
                            </tr>
                        </thead>
                        <tbody>';
    $s_no = 0;
    $use_style1 = true;
    $row_style1 = UPL_STYLE_TABLE_ROW1;
    $row_style2 = UPL_STYLE_TABLE_ROW2;
    foreach ($users as $u) {
        $omit = array('System Administrator', 'Super Administrator', 'Authoriser');
        if ($_SESSION['usergroup'] != 'Super Administrator' && in_array($u['usergroup_name'], $omit)) {
            continue;
        }

        $row_style = $use_style1 ? $row_style1 : $row_style2;
        $use_style1 = !$use_style1;
        $display .= '<tr class="' . $row_style . '">
                            <td>' . (++$s_no) . '.</td>
                            <!--<td>' . (!empty($u['lga_name']) ? $u['lga_name'] : 'All') . '</td>-->
                            <td>' . stripslashes($u['user_firstname']) . '</td>
                            <td>' . stripslashes($u['user_surname']) . '</td>
                            <td>' . stripslashes($u['user_phone']) . '</td>
                            <td>' . stripslashes($u['userlog_email']) . '</td>
                            <td>' . (isset($u['usergroup_name']) ? stripslashes($u['usergroup_name']) : '-') . '</td>
                            <td>' . $u['datecreated'] . '</td>
                            <td>' . $u['lastlogin'] . '</td>
                            <td>' . ($u['user_active'] == 1 ? 'Yes' : 'No') . '</td>
                            <td>' . anchor('sys-admin/user/edit/' . $u['user_id'], '<i class="fa fa-edit"></i> Edit', 'class="btn btn-primary"') . '</td>
                         </tr>';
    }
    $display .= '<tfoot>
                            <tr>
                                <th>S/NO</th>
                                <!--<th>LGA</th>-->
                                <th>FIRST NAME</th>
                                <th>SURNAME</th>
                                <th>PHONE</th>
                                <th>E-MAIL</th>
                                <th>USER GROUP</th>
                                <th>DATE REGISTERED</th>
                                <th>LAST LOG-IN DATE</th>
                                <th>ACTIVE</th>
                                <th><em>Edit</em></th>
                            </tr>
                        </tfoot>';
    $display .= '</tbody></table>';
} else {
    $display = 'No user created yet. Please, ' . anchor('sys-admin/user/add', 'click here to create a user.') . '.';
}
echo $display;