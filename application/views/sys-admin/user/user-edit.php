<?php
$form_display = '';

try {
    //Confirm that the user was found and the details were successfully loaded
    if (empty($user_deets)) {
        throw new Exception('Unknown user selected.');
    }

    //Format the user group array for drop-down
    $group_array = array('0' => '--Select User Group--');
    if (!empty($groups)) {
        foreach ($groups as $g) {
            $omit = array('System Administrator', 'Super Administrator', 'Authoriser');
            if ($_SESSION['usergroup'] != 'Super Administrator' && in_array($g['usergroup_name'], $omit)) {
                continue;
            }
            
            $group_array[$g['usergroup_id']] = stripslashes($g['usergroup_name']);
        }
    }

    //Format the LGAs array for drop-down
    /* $lga_dd = array(
        '-1' => '--Select LGA--',
        '0' => '***All LGAs'
    );
    foreach ($lgas as $l){
        $lga_dd[$l['lga_id']] = $l['lga_name'];
    } */

    //Show success/error messages if any
    $form_display .= !empty($msg) ? $msg : '';

    //Show the actual form
    $form_display .= form_open()
        . '<table border="0" class="table table-striped">
                                <tr>
                                    <td><label for="firstname">First Name:</label></td>
                                    <td>' . form_input('firstname', stripslashes($user_deets['user_firstname']), 'id="firstname" size="30"') . '</td>
                                </tr>
                                <tr>
                                    <td><label for="surname">Surname:</label></td>
                                    <td>' . form_input('surname', stripslashes($user_deets['user_surname']), 'id="surname" size="30"') . '</td>
                                </tr>
                                <tr>
                                    <td><label for="phone">Phone:</label></td>
                                    <td>' . form_input('phone', $user_deets['user_phone'], 'id="phone" size="20"') . '</td>
                                </tr>
                                <tr>
                                    <td><label for="email">E-Mail:</label></td>
                                    <td>' . form_input('email', $user_deets['userlog_email'], 'id="email" size="50"') . '</td>
                                </tr>
                                <tr>
                                    <td><label for="usergroup">User Group:</label></td>
                                    <td>' . form_dropdown('usergroup', $group_array, $user_deets['usergroup_id'], 'id="usergroup"') . '</td>
                                </tr>
                                <tr>
                                    <td><label>Password:</label></td>
                                    <td>
                                        ' . form_checkbox('password_reset', 1, false, 'id="password-reset"') . '
                                        <label for="password-reset">Reset the password (<em>The system would generate a new password and send it to the email address of the user.</em>)</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label>Activate/Deactivate:</label></td>
                                    <td>
                                        ' . form_checkbox('active', 1, ($user_deets['user_active'] == '1'), 'id="active"') . '
                                        <label for="active">Activate this user</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col1">&nbsp;</td>
                                    <td class="col2">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save Changes</button>
                                    </td>
                                </tr>
                           </table>'
        . form_close();

} catch (Exception $ex) {
    $form_display = $ex->getMessage();
}

$form_display .= '<p>' . anchor("sys-admin/user/view", '&laquo; Back to List of Users') . '</p>';
echo $form_display;