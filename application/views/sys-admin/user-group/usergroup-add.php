<?php
    $form_display = '';

    //Show success/error messages if any
    $form_display .= !empty($msg) ? $msg : '';

    //Show the actual form
    $form_display .= form_open()
                    . '<table border="0">
                            <tr>
                                <td><label>User Group:</label></td>
                                <td>' . form_input('user_group', set_value('user_group'), 'size="30"') . '</td>
                            </tr>
                            <tr>
                                <td><label>Description:</label></td>
                                <td>' . form_textarea('user_group_desc', set_value('user_group_desc')) . '</td>
                            </tr>
                            <tr>
                                <td class="col1">&nbsp;</td>
                                <td class="col2">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Add User Group</button>
                                </td>
                            </tr>
                       </table>'
                    . form_close();
    $form_display .= '<p>' . anchor("sys-admin/usergroup/view", '&laquo; Back to List of User Groups') . '</p>';
    echo $form_display;