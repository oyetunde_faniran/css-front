<?php
    //echo '<pre>' . print_r($groups, true) . '</pre>';
    $display = '<div style="margin:10px 0px;">' . anchor('sys-admin/usergroup/add', '<button class="btn btn-primary"><i class="fa fa-plus-circle"></i> Add New User Group</button>') . '</div>';
    if (!empty($groups)){
        $display .= '<table cellspacing="0" class="table table-striped">
                        <thead>
                            <tr>
                                <th>S/NO</th>
                                <th>USER GROUP</th>
                                <th>DESCRIPTION</th>
                                <th>ACTIVE</th>
                                <th><em>Edit</em></th>
                                <th><em>Privileges</em></th>
                            </tr>
                        </thead>
                        <tbody>';
        $s_no = 0;
        $use_style1 = true;
        $row_style1 = UPL_STYLE_TABLE_ROW1;
        $row_style2 = UPL_STYLE_TABLE_ROW2;
        foreach ($groups as $g){
            $row_style = $use_style1 ? $row_style1 : $row_style2;
            $use_style1 = !$use_style1;
            $display .= '<tr class="' . $row_style . '">
                            <td>' . (++$s_no) . '.</td>
                            <td>' . stripslashes($g['usergroup_name']) . '</td>
                            <td>' . stripslashes($g['usergroup_description']) . '</td>
                            <td>' . ($g['usergroup_enabled'] == 1 ? 'Yes' : 'No') . '</td>
                            <td>' . anchor('sys-admin/usergroup/edit/' . $g['usergroup_id'], '<i class="fa fa-edit"></i> Edit', 'class="btn btn-primary"') . '</td>
                            <td>' . anchor('sys-admin/privilege/configure/' . $g['usergroup_id'], '<i class="fa fa-gear"></i> Privileges', 'class="btn btn-primary"') . '</td>
                         </tr>';
        }
        $display .= '</tbody></table>';
    } else {
        $display = 'No user group created yet. Please, ' . anchor('sys-admin/usergroup/add', 'click here to create a user group') . '.';
    }
    echo $display;