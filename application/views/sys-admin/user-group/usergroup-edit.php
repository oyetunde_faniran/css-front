<?php
    $form_display = '';

    try {
        //Confirm that the user group was found and the details were successfully loaded
        if (empty($group_deets)){
            throw new Exception('Unknown user group selected.');
        }

        //Show success/error messages if any
        $form_display .= !empty($msg) ? $msg : '';

        //Show the actual form
        $form_display .= form_open()
                        . '<table border="0">
                                <tr>
                                    <td><label>User Group:</label></td>
                                    <td>' . form_input('user_group', $group_deets['usergroup_name'], 'size="30"') . '</td>
                                </tr>
                                <tr>
                                    <td><label>Description:</label></td>
                                    <td>' . form_textarea('user_group_desc', $group_deets['usergroup_description']) . '</td>
                                </tr>
                                <tr>
                                    <td><label>Active:</label></td>
                                    <td>' . form_checkbox('active', 1, ($group_deets['usergroup_enabled'] == 1)) . '</td>
                                </tr>
                                <tr>
                                    <td class="col1">&nbsp;</td>
                                    <td class="col2">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save Changes</button>
                                    </td>
                                </tr>
                           </table>'
                        . form_close();

    } catch (Exception $ex) {
        $form_display = $ex->getMessage();
    }

    $form_display .= '<p>' . anchor("sys-admin/usergroup/view", '&laquo; Back to List of User Groups') . '</p>';
    echo $form_display;