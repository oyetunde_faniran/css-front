<?php
    $dis_sys = $this->system_authorization;
?>

<div class="row" style="border-bottom: 1px solid #CCC; padding-bottom: 20px;">
        <div class="container">
            <div id="accordion3" class="panel-group accordion accordion-semi" style="margin-top: 20px;">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion3" href="#ac3-1" aria-expanded="false" class="collapsed"><i class="icon s7-angle-down"></i> Click here to filter</a></h4>
                    </div>
                    <div id="ac3-1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <form method="post" role="form">
                                <div class="col-md-6">
                                    <div class="form-group col-md-12">
                                        <label>FROM DATE</label>
                                        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker col-md-12">
                                            <input size="16" type="text" value="<?php echo (!empty($fromdate) ? $fromdate : date('Y-m-d')); ?>" name="fromdate" id="fromdate" class="form-control" />
                                            <span class="input-group-addon btn btn-primary"><i class="icon-th s7-date"></i></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group col-md-12">
                                        <label>TO DATE</label>
                                        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker col-md-12">
                                            <input size="16" type="text" value="<?php echo (!empty($todate) ? $todate : date('Y-m-d')); ?>" name="todate" id="todate" class="form-control" />
                                            <span class="input-group-addon btn btn-primary"><i class="icon-th s7-date"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-lg btn-space btn-primary">Filter</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



<?php
    $create_btn = '<div style="margin-top: 10px;">' . anchor('bankaccount/upload', 'Upload New Entry', 'class="btn btn-danger"') . '</div>';
    
    echo ($dis_sys->hasAccess('bankacc-entries-upload') ? $create_btn : '');
//    die('<pre>' . print_r($trans, true));
    $display = '<div class="table-responsive"><table id="report-table" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>S/NO</th>
                            <th>TOTAL AMOUNT (&#8358;)</th>
                            <th>ACCOUNT NAME</th>
                            <th>ACCOUNT NUMBER</th>
                            <th>BANK</th>
                            <th>DEPOSIT DATE</th>
                            <th>STATUS</th>
                            <th>UPLOADED BY</th>
                            <th>APPROVED BY</th>
                            <th>TIME UPLOADED</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>S/NO</th>
                            <th>TOTAL AMOUNT (&#8358;)</th>
                            <th>ACCOUNT NAME</th>
                            <th>ACCOUNT NUMBER</th>
                            <th>BANK</th>
                            <th>DEPOSIT DATE</th>
                            <th>STATUS</th>
                            <th>UPLOADED BY</th>
                            <th>APPROVED BY</th>
                            <th>TIME UPLOADED</th>
                        </tr>
                    </tfoot>
                    <tbody>';
    $sno = $total = 0;

    foreach($trans as $t){
        switch($t['bdeposit_status']){
            case 2: $current_status = 'Pending Approval'; break;
            case 3: $current_status = 'Disapproved'; break;
            case 4: $current_status = 'Approved'; break;
            case 5: $current_status = 'Commission Generated'; break;
            case 1:
            default:
                $current_status = 'Assignment in Progress'; break;
        }
        $display .= '<tr>
                        <td>' . (++$sno) . '. <div class="btn-group btn-space">
                                <!--<button type="button" class="btn btn-primary">Actions</button>-->
                                <button type="button" data-toggle="dropdown" class="btn btn-primary btn-shade1 dropdown-toggle" aria-expanded="false"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a target="_blank" href="' . base_url(BANKDEPOSIT_UPLOAD_DIR . $t['bdeposit_filename']) . '">Download File</a></li>
                                    ' . ($dis_sys->hasAccess('report-commission') ? '<li><a target="_blank" href="' . site_url("reports/comms/{$t['bdeposit_id']}/{$t['datelogged']}") . '">View Uploaded Commission</a></li>' : '') . 
                                    ($dis_sys->hasAccess('bankacc-assign2project') ? '<li><a target="_blank" href="' . site_url("bankaccount/assign2project/{$t['bdeposit_id']}") . '">Assign Project to Records</a></li>' : '') .
                                    ($dis_sys->hasAccess('bankacc-assign2project-approve') ? '<li><a target="_blank" href="' . site_url("bankaccount/assign2project/{$t['bdeposit_id']}") . '">Approve / Disapprove</a></li>' : '') .
                                '</ul>
                            </div></td>
                        <td>' . number_format($t['bdeposit_amount'], 2) . '</td>
                        <td>' . $t['bdeposit_accname'] . '</td>
                        <td>' . $t['bdeposit_accno'] . '</td>
                        <td>' . $t['bank_name'] . '</td>
                        <td>' . $t['depositdate'] . '</td>
                        <td>' . '<span class="label label-success">' . $current_status . '</span>' . '</td>
                        <td>' . $t['username'] . '</td>
                        <td>' . (!empty($t['approver']) ? $t['approver']  : '--') . '</td>
                        <td>' . $t['timelogged'] . '</td>
                    </tr>';
        $total += $t['bdeposit_amount'];
    }
    $display .= '</tbody></table></div>';
    echo $display;
?>




<script>
    dis_sch_id = 0
    dis_proc_type = 0;
    function initSplit(sch_id, proc_type){
//        alert('Work on-going!');
        
//        if(confirm('Are you sure you want to initiate the splitting of the selected commission?')){
        if(true){
            dis_url = '<?php echo site_url('commission/initSplit') . '/'; ?>' + sch_id;
            console.log(dis_url);
            dis_sch_id = sch_id;
            dis_proc_type = proc_type;
//            return 0;
//            $('#close-button').fadeOut();
            $('.comm-pay-btn').fadeOut();
            $('.comm-delete-btn').fadeOut();
            $('#loading-image-' + proc_type + '-' + sch_id).fadeIn();
            $.ajax({
                type: 'GET',
                url: dis_url,
                data_type: 'json',
                success: function(data){
//                    console.log("DONE! \n" + JSON.stringify(data));
//                    $('.comm-pay-btn-container-' + sch_id).html('<span class="label label-success">Commission processed</span>');
                    $('.comm-pay-btn').fadeIn();
                    $('.comm-delete-btn').fadeIn();
                    $('#loading-image-' + proc_type + '-' + sch_id).fadeOut();
                    
                    dis_display = '';
                    dis_len = data.beneficiaries.length;
                    d = data.beneficiaries;
                    
                    dis_display = dis_display + '<table class="table table-striped table-bordered">';
                    dis_display = dis_display + '<thead><tr>';
                    dis_display = dis_display + '<th>S/NO</th>';
                    dis_display = dis_display + '<th>NAME</th>';
                    dis_display = dis_display + '<th>ACCOUNT NUMBER</th>';
                    dis_display = dis_display + '<th>BANK CODE</th>';
                    dis_display = dis_display + '<th>AMOUNT (&#8358;)</th>';
                    dis_display = dis_display + '<th>SHARE (%)</th>';
                    dis_display = dis_display + '</tr></thead><tbody>';
                    
                    for(k = 0; k < dis_len; k++){
                        dis_display = dis_display + '<tr><td>' + ( k + 1) + '. </td>';
                        dis_display = dis_display + '<td>' + d[k].accname + '</td>';
                        dis_display = dis_display + '<td>' + d[k].accno + '</td>';
                        dis_display = dis_display + '<td>' + d[k].bankcode + '</td>';
                        dis_display = dis_display + '<td>' + d[k].amount_formatted + '</td>';
                        dis_display = dis_display + '<td>' + d[k].share + '</td></tr>';
                    }
                    dis_display = dis_display + '</tbody></table>';
                    
                    $('#split-details').html(dis_display);
                    $('#split-details-modal').modal('show');
                }
            });
        }
    }
    
    
    
    function processSplit(){
        sch_id = dis_sch_id;
        proc_type = dis_proc_type;
        narration = $('#split_narration').val();
        if(narration == ''){
//            alert('Please, enter a valid narration');
//            return 0;
        }
        
        if(confirm('Are you sure you want to trigger the splitting of the selected commission?')){
            dis_url = '<?php echo site_url('commission/processSplit') . '/'; ?>' + sch_id;
            console.log(dis_url);
//            $('#close-button').fadeOut();
            $('.comm-pay-btn').fadeOut();
            $('.comm-delete-btn').fadeOut();
            $('#loading-image-' + proc_type + '-' + sch_id).fadeIn();
            $('#loading-image-modal').fadeIn();
            $('#proceed-button-modal').fadeOut();
            $('#close-button-modal').fadeOut();
            $.ajax({
                type: 'POST',
                url: dis_url,
                data_type: 'json',
                data: 'narration=' + narration,
                success: function(data){
//                    console.log("DONE! \n" + data.msg);
                    if(data.status){
                        $('.comm-pay-btn-container-' + sch_id).html('<span class="label label-success">Split processed</span> <button type="button" id="show-details-btn-' + sch_id + '" onclick="showSplit(' + sch_id + ', 2)" class="btn btn-info show-details-btn">Show Split</button><div style="width: 100%; display: none;" id="loading-image-display-only-2-' + sch_id + '" class="progress-bar progress-bar-danger progress-bar-striped active">Please wait...</div>');
                        $('#split-details-modal').modal('hide');
                    } else {
                        document.getElementById('split_narration').focus();
                        $('#proceed-button-modal').fadeIn();
                        $('#close-button-modal').fadeIn();
                        alert(data.msg);
                    }
                    $('#loading-image-' + proc_type + '-' + sch_id).fadeOut();
                    $('#loading-image-modal').fadeOut();
                    $('.comm-pay-btn').fadeIn();
                    $('.comm-delete-btn').fadeIn();
                }
            });
        }
    }
    
    
    function showSplit(sch_id, proc_type){
        dis_url = '<?php echo site_url('commission/showSplit') . '/'; ?>' + sch_id;
        $('.comm-pay-btn').fadeOut();
        $('.comm-delete-btn').fadeOut();
        $('.show-details-btn').fadeOut();
        $('#loading-image-display-only-2-' + sch_id).fadeIn();
        console.log(dis_url);
        $.ajax({
            type: 'GET',
            url: dis_url,
            data_type: 'json',
            success: function(data){
//                    console.log("DONE! \n" + JSON.stringify(data));
//                    $('.comm-pay-btn-container-' + sch_id).html('<span class="label label-success">Commission processed</span>');
                $('.comm-pay-btn').fadeIn();
                $('.comm-delete-btn').fadeIn();
                $('.show-details-btn').fadeIn();
                $('#loading-image-display-only-2-' + sch_id).fadeOut();

                dis_display = '';
                dis_len = data.beneficiaries.length;
                d = data.beneficiaries;

                dis_display = dis_display + '<table class="table table-striped table-bordered">';
                dis_display = dis_display + '<thead><tr>';
                dis_display = dis_display + '<th>S/NO</th>';
                dis_display = dis_display + '<th>NAME</th>';
                dis_display = dis_display + '<th>ACCOUNT NUMBER</th>';
                dis_display = dis_display + '<th>BANK CODE</th>';
                dis_display = dis_display + '<th>AMOUNT (&#8358;)</th>';
                dis_display = dis_display + '<th>SHARE (%)</th>';
                dis_display = dis_display + '</tr></thead><tbody>';

                for(k = 0; k < dis_len; k++){
                    dis_display = dis_display + '<tr><td>' + ( k + 1) + '. </td>';
                    dis_display = dis_display + '<td>' + d[k].accname + '</td>';
                    dis_display = dis_display + '<td>' + d[k].accno + '</td>';
                    dis_display = dis_display + '<td>' + d[k].bankcode + '</td>';
                    dis_display = dis_display + '<td>' + d[k].amount_formatted + '</td>';
                    dis_display = dis_display + '<td>' + d[k].share + '</td></tr>';
                }
                dis_display = dis_display + '</tbody></table>';

                $('#split-details-display-only').html(dis_display);
                $('#split-details-display-only-modal').modal('show');
            }
        });
    }
    
    
    function createCommission(dis_form){
        if(confirm('Are you sure?')){
            dis_form.submit();
        }
    }
    
    
    
    function deleteCommission(id){
        if(confirm('Are you sure you want to delete this commission?')){
            dis_url = '<?php echo site_url('commission/deleteCommission') . '/'; ?>' + id;
//            console.log(dis_url);
//            $('#close-button').fadeOut();
            $('.comm-delete-btn').fadeOut();
            $('.comm-pay-btn').fadeOut();
            $('#loading-del-image-' + id).fadeIn();
            $.ajax({
                type: 'GET',
                url: dis_url,
                data_type: 'json',
//                data: 'narration=' + narration,
                success: function(data){
//                    console.log("DONE! \n" + data.msg);
                    if(data.status){
                        alert('Deleted successfully!');
                        location.reload();
                    } else {
                        $('.comm-delete-btn').fadeIn();
                        $('.comm-pay-btn').fadeIn();
                        alert(data.msg);
                    }
                    $('#loading-del-image-' + id).fadeOut();
                }
            });
        }
    }
    
    
    
    
</script>