<div class="row">
<?php
    $dis_sys = $this->system_authorization;
    if(!empty($msg)){
        echo $msg;
    }
?>
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-body">
                <div class="form-group">
                    Upload the breakdown of a deposit into the collection bank account.
                </div>
                <form id="create-form" role="form" method="post" enctype="multipart/form-data">
<!--                    <div class="form-group">
                        <label for="totalamount">TOTAL AMOUNT (&#8358;):</label>
                        <input value="<?php echo set_value('totalamount'); ?>" type="number" required="required" name="totalamount" id="totalamount" placeholder="Total Amount Here" class="form-control" />
                    </div>-->
                    <div class="form-group">
                        <label for="accno">ACCOUNT NUMBER:</label>
                        <input value="<?php echo ($_POST ? set_value('accno') : '1007061436'); ?>" type="text" required="required" name="accno" id="accno" placeholder="Account Number Here" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="accname">ACCOUNT NAME:</label>
                        <input value="<?php echo ($_POST ? set_value('accname') : 'UPPERLINK LIMITED COLLECTIONS ACC'); ?>" type="text" required="required" name="accname" id="accname" placeholder="Account Name Here" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="bank">BANK:</label>
<?php
    $bank_dd = array(
        '0' => '--Select Bank--'
    );
    foreach($banks as $row){
        $bank_dd[$row['bank_id']] = $row['bank_name'];
    }
    echo form_dropdown('bank', $bank_dd, ($_POST ? set_value('bank') : 11), 'id="bank" class="form-control" required="required"');
?>
                    </div>
                    <div class="form-group">
                        <label for="depositdate">DEPOSIT DATE <em>(The date the money was deposited into the bank account)</em> :</label>
                        <input value="<?php echo set_value('depositdate'); ?>" type="text" data-min-view="2" required="required" name="depositdate" id="depositdate" placeholder="The date the money was deposited into the bank account" class="form-control date datetimepicker" />
                    </div>
                    <div class="form-group">
                        <label for="breakdown">
                            BREAKDOWN (Keystone Bank Account Statement) :
                        </label>
                        <input type="file" required="required" name="breakdown" id="breakdown" class="form-control" />
                    </div>
                    <div class="form-group" id="btn-container">
                        <button type="button" class="btn btn-space btn-danger" onclick="confirmUpload(this.form);"><i class="icon s7-plus"></i> Upload Breakdown</button>
                        <?php echo ($dis_sys->hasAccess('bankacc-entries-listall') ? anchor('bankaccount/listall', '<i class="icon s7-look"></i>View Existing Entries', 'class="btn btn-warning"') : ''); ?>
                        <?php echo anchor('project/downloadProjectKeys', 'Download Project Keys'); ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



<style type="text/css">
    label { font-weight: bold; }
</style>


<script type="text/javascript">
    function confirmUpload(dis_form){
        if(confirm('Are you sure you want to save this? IT CANNOT BE REVERSED.')){
            $('#btn-container').fadeOut();
            dis_form.submit();
        }
    }
    
</script>