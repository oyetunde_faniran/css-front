<?php
    $dis_sys = $this->system_authorization;
    echo $msg;
    
    $project_dd = [
        '0' => '--Select Project--'
    ];
    foreach ($projects as $p){
        $project_dd[$p['prj_id']] = $p['prj_name'];
    }
?>


<?php
//    die('<pre>' . print_r($trans, true));
    $display = '<div class="table-responsive"><table id="report-table" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>S/NO</th>
                            <th>PROJECT</th>
                            <th>NARRATION</th>
                            <th>AMOUNT (&#8358;)</th>
                            <th>TYPE</th>
                            <th>REFERENCE</th>
                            <th>TRANS. DATE</th>
                            <th>VALUE DATE</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>S/NO</th>
                            <th>PROJECT</th>
                            <th>NARRATION</th>
                            <th>AMOUNT (&#8358;)</th>
                            <th>TYPE</th>
                            <th>REFERENCE</th>
                            <th>TRANS. DATE</th>
                            <th>VALUE DATE</th>
                        </tr>
                    </tfoot>
                    <tbody>';
    $sno = $total = 0;

    foreach($statement_records as $t){
        if($t['transtype'] == 1){
            $disabled = !$allow_edit ? 'disabled="disabled"' : '';
            $transtype = '<div class="label label-success">Credit</div>';
            $project_dd_view = form_dropdown('project[' . $t['bdeets_id'] . ']', $project_dd, $t['project_id'], 'class="form-control" ' . $disabled);
            $row_style = $t['project_id'] == 0 ? 'style="color: #ec4444;"' : '';
            $unassigned_flag = $t['project_id'] == 0 ? '<span class="label label-danger">Unassigned</span>' : '';
        } else {
            $transtype = '<div class="label label-danger">Debit</div>';
            $project_dd_view = '<div class="label label-danger">Not applicable</div>';
            $row_style = '';
            $unassigned_flag = '';
        }
        $display .= '<tr>
                        <td>' . (++$sno) . '.</td>
                        <td>' . $project_dd_view . '<br />' . $unassigned_flag . '</td>
                        <td>' . $t['details'] . '</td>
                        <td>' . number_format($t['amount'], 2) . '</td>
                        <td>' . $transtype . '</td>
                        <td>' . $t['reference'] . '</td>
                        <td>' . $t['transdate'] . '</td>
                        <td>' . $t['valuedate'] . '</td>
                    </tr>';
        $total += $t['amount'];
    }
    $display .= '</tbody></table></div>';
    $btns = '<div style="margin: 20px 0px;">STATUS: <span class="label label-success">' . $current_status_display . '</span> ';
    $btns .= $dis_sys->hasAccess('bankacc-assign2project') && $allow_edit ? '<button class="btn btn-danger" type="Submit">Save Client/Project Assignment</button>' : '';
    $btns .= $dis_sys->hasAccess('bankacc-assign2project-forward') && $allow_edit ? ' <button onclick="forward4Approval(' . $deposit_id . ')" class="btn btn-danger" type="button">Forward for approval</button>' : '';
    $btns .= $dis_sys->hasAccess('bankacc-assign2project-approve') && $current_status != 4 && $current_status != 5 && $current_status != 1 ? ' <button onclick="approveAssignment(' . $deposit_id . ', 1)" class="btn btn-danger" type="button">Approve</button>' . ' <button onclick="approveAssignment(' . $deposit_id . ', 2)" class="btn btn-danger" type="button">Disapprove</button>' : '';
    $btns .= $current_status == 4 ? ' <a target="_blank" href="' . site_url("reports/comms/{$deposit_id}/{$bank_deposit[0]['datelogged']}") . '" class="btn btn-danger">View Uploaded Commission</a>' : '';
    $btns .= '</div>';
//    die('<pre>' . print_r($bank_deposit, true));
    $progress_bar = '<div style="width: 100%; display: none; margin: 20px 0px;" class="progress-bar progress-bar-danger progress-bar-striped active loading-image">Please, wait...</div>';
    echo '<form action="" method="post">' . $progress_bar . $btns . $display . $btns . $progress_bar . '</form>';
?>




<script>
    dis_sch_id = 0
    dis_proc_type = 0;
    function initSplit(sch_id, proc_type){
//        alert('Work on-going!');
        
//        if(confirm('Are you sure you want to initiate the splitting of the selected commission?')){
        if(true){
            dis_url = '<?php echo site_url('commission/initSplit') . '/'; ?>' + sch_id;
            console.log(dis_url);
            dis_sch_id = sch_id;
            dis_proc_type = proc_type;
//            return 0;
//            $('#close-button').fadeOut();
            $('.comm-pay-btn').fadeOut();
            $('.comm-delete-btn').fadeOut();
            $('#loading-image-' + proc_type + '-' + sch_id).fadeIn();
            $.ajax({
                type: 'GET',
                url: dis_url,
                data_type: 'json',
                success: function(data){
//                    console.log("DONE! \n" + JSON.stringify(data));
//                    $('.comm-pay-btn-container-' + sch_id).html('<span class="label label-success">Commission processed</span>');
                    $('.comm-pay-btn').fadeIn();
                    $('.comm-delete-btn').fadeIn();
                    $('#loading-image-' + proc_type + '-' + sch_id).fadeOut();
                    
                    dis_display = '';
                    dis_len = data.beneficiaries.length;
                    d = data.beneficiaries;
                    
                    dis_display = dis_display + '<table class="table table-striped table-bordered">';
                    dis_display = dis_display + '<thead><tr>';
                    dis_display = dis_display + '<th>S/NO</th>';
                    dis_display = dis_display + '<th>NAME</th>';
                    dis_display = dis_display + '<th>ACCOUNT NUMBER</th>';
                    dis_display = dis_display + '<th>BANK CODE</th>';
                    dis_display = dis_display + '<th>AMOUNT (&#8358;)</th>';
                    dis_display = dis_display + '<th>SHARE (%)</th>';
                    dis_display = dis_display + '</tr></thead><tbody>';
                    
                    for(k = 0; k < dis_len; k++){
                        dis_display = dis_display + '<tr><td>' + ( k + 1) + '. </td>';
                        dis_display = dis_display + '<td>' + d[k].accname + '</td>';
                        dis_display = dis_display + '<td>' + d[k].accno + '</td>';
                        dis_display = dis_display + '<td>' + d[k].bankcode + '</td>';
                        dis_display = dis_display + '<td>' + d[k].amount_formatted + '</td>';
                        dis_display = dis_display + '<td>' + d[k].share + '</td></tr>';
                    }
                    dis_display = dis_display + '</tbody></table>';
                    
                    $('#split-details').html(dis_display);
                    $('#split-details-modal').modal('show');
                }
            });
        }
    }
    
    
    
    function processSplit(){
        sch_id = dis_sch_id;
        proc_type = dis_proc_type;
        narration = $('#split_narration').val();
        if(narration == ''){
//            alert('Please, enter a valid narration');
//            return 0;
        }
        
        if(confirm('Are you sure you want to trigger the splitting of the selected commission?')){
            dis_url = '<?php echo site_url('commission/processSplit') . '/'; ?>' + sch_id;
            console.log(dis_url);
//            $('#close-button').fadeOut();
            $('.comm-pay-btn').fadeOut();
            $('.comm-delete-btn').fadeOut();
            $('#loading-image-' + proc_type + '-' + sch_id).fadeIn();
            $('#loading-image-modal').fadeIn();
            $('#proceed-button-modal').fadeOut();
            $('#close-button-modal').fadeOut();
            $.ajax({
                type: 'POST',
                url: dis_url,
                data_type: 'json',
                data: 'narration=' + narration,
                success: function(data){
//                    console.log("DONE! \n" + data.msg);
                    if(data.status){
                        $('.comm-pay-btn-container-' + sch_id).html('<span class="label label-success">Split processed</span> <button type="button" id="show-details-btn-' + sch_id + '" onclick="showSplit(' + sch_id + ', 2)" class="btn btn-info show-details-btn">Show Split</button><div style="width: 100%; display: none;" id="loading-image-display-only-2-' + sch_id + '" class="progress-bar progress-bar-danger progress-bar-striped active">Please wait...</div>');
                        $('#split-details-modal').modal('hide');
                    } else {
                        document.getElementById('split_narration').focus();
                        $('#proceed-button-modal').fadeIn();
                        $('#close-button-modal').fadeIn();
                        alert(data.msg);
                    }
                    $('#loading-image-' + proc_type + '-' + sch_id).fadeOut();
                    $('#loading-image-modal').fadeOut();
                    $('.comm-pay-btn').fadeIn();
                    $('.comm-delete-btn').fadeIn();
                }
            });
        }
    }
    
    
    function showSplit(sch_id, proc_type){
        dis_url = '<?php echo site_url('commission/showSplit') . '/'; ?>' + sch_id;
        $('.comm-pay-btn').fadeOut();
        $('.comm-delete-btn').fadeOut();
        $('.show-details-btn').fadeOut();
        $('#loading-image-display-only-2-' + sch_id).fadeIn();
        console.log(dis_url);
        $.ajax({
            type: 'GET',
            url: dis_url,
            data_type: 'json',
            success: function(data){
//                    console.log("DONE! \n" + JSON.stringify(data));
//                    $('.comm-pay-btn-container-' + sch_id).html('<span class="label label-success">Commission processed</span>');
                $('.comm-pay-btn').fadeIn();
                $('.comm-delete-btn').fadeIn();
                $('.show-details-btn').fadeIn();
                $('#loading-image-display-only-2-' + sch_id).fadeOut();

                dis_display = '';
                dis_len = data.beneficiaries.length;
                d = data.beneficiaries;

                dis_display = dis_display + '<table class="table table-striped table-bordered">';
                dis_display = dis_display + '<thead><tr>';
                dis_display = dis_display + '<th>S/NO</th>';
                dis_display = dis_display + '<th>NAME</th>';
                dis_display = dis_display + '<th>ACCOUNT NUMBER</th>';
                dis_display = dis_display + '<th>BANK CODE</th>';
                dis_display = dis_display + '<th>AMOUNT (&#8358;)</th>';
                dis_display = dis_display + '<th>SHARE (%)</th>';
                dis_display = dis_display + '</tr></thead><tbody>';

                for(k = 0; k < dis_len; k++){
                    dis_display = dis_display + '<tr><td>' + ( k + 1) + '. </td>';
                    dis_display = dis_display + '<td>' + d[k].accname + '</td>';
                    dis_display = dis_display + '<td>' + d[k].accno + '</td>';
                    dis_display = dis_display + '<td>' + d[k].bankcode + '</td>';
                    dis_display = dis_display + '<td>' + d[k].amount_formatted + '</td>';
                    dis_display = dis_display + '<td>' + d[k].share + '</td></tr>';
                }
                dis_display = dis_display + '</tbody></table>';

                $('#split-details-display-only').html(dis_display);
                $('#split-details-display-only-modal').modal('show');
            }
        });
    }
    
    
    function createCommission(dis_form){
        if(confirm('Are you sure?')){
            dis_form.submit();
        }
    }
    
    
    
    function deleteCommission(id){
        if(confirm('Are you sure you want to delete this commission?')){
            dis_url = '<?php echo site_url('commission/deleteCommission') . '/'; ?>' + id;
//            console.log(dis_url);
//            $('#close-button').fadeOut();
            $('.comm-delete-btn').fadeOut();
            $('.comm-pay-btn').fadeOut();
            $('#loading-del-image-' + id).fadeIn();
            $.ajax({
                type: 'GET',
                url: dis_url,
                data_type: 'json',
//                data: 'narration=' + narration,
                success: function(data){
//                    console.log("DONE! \n" + data.msg);
                    if(data.status){
                        alert('Deleted successfully!');
                        location.reload();
                    } else {
                        $('.comm-delete-btn').fadeIn();
                        $('.comm-pay-btn').fadeIn();
                        alert(data.msg);
                    }
                    $('#loading-del-image-' + id).fadeOut();
                }
            });
        }
    }
    
    
    
    function forward4Approval(deposit_id){
        if(!confirm('Are you sure you want to forward this for approval?')){
            return 0;
        }
        dis_url = '<?php echo site_url('bankaccount/forward_assign2project') . '/'; ?>' + deposit_id;
        $('.loading-image').fadeIn();
        $.ajax({
            type: 'GET',
            url: dis_url,
            data_type: 'json',
            success: function(data){
//                console.log(data);
                $('.loading-image').fadeOut();
                if(data.status){
                    alert('Successfully forwarded. Now reloading this page.');
                    location.reload();
                } else {
                    alert(data.msg);
                }
            }
        });
    }
    
    
    function approveAssignment(deposit_id, approval_type){
        if(approval_type == 1){
            dis_action = 'approve';
            dis_action_past_tense = 'approved';
        } else {
            dis_action = 'disapprove';
            dis_action_past_tense = 'dispproved';
        }
        if(!confirm('Are you sure you want to ' + dis_action + ' this?')){
            return 0;
        }
        dis_url = '<?php echo site_url('bankaccount/approve_assign2project') . '/'; ?>' + deposit_id + '/' + approval_type;
        $('.loading-image').fadeIn();
        $.ajax({
            type: 'GET',
            url: dis_url,
            data_type: 'json',
            success: function(data){
//                console.log(data);
                $('.loading-image').fadeOut();
                if(data.status){
                    alert('Successfully ' + dis_action_past_tense + '. Now reloading this page.');
                    location.reload();
                } else {
                    alert(data.msg);
                }
            }
        });
    }
    
    
    
</script>