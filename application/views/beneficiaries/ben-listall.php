<?php
    $dis_sys = $this->system_authorization;
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-body">
<?php
    if($dis_sys->hasAccess('beneficiary-add')){
        echo '<div>
                    <button data-toggle="modal" data-target="#create-modal" type="button" class="btn btn-space btn-danger"><i class="icon s7-plus"></i> Add Beneficiary</button>
                </div>';
    }
?>
                <table class="table table-condensed table-hover table-bordered table-striped" id="report-table">
                    <thead>
                        <tr>
                            <td>S/NO</td>
                            <td>NAME</td>
                            <td>ACC. NAME</td>
                            <td>ACC. NO</td>
                            <td>BANK</td>
                            <td>STATUS</td>
                            <td>DATE ADDED</td>
                            <td>ACTION</td>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <td>S/NO</td>
                            <td>NAME</td>
                            <td>ACC. NAME</td>
                            <td>ACC. NO</td>
                            <td>BANK</td>
                            <td>STATUS</td>
                            <td>DATE ADDED</td>
                            <td>ACTION</td>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php
                        $display = '';
                        $sno = 0;
                        foreach ($records as $c) {
                            $id = $c['ben_id'];
                            $display .= '<tr>
						<td>' . ( ++$sno) . '.</td>
						<td>' . $c['ben_name'] . '</td>
                        <td>' . $c['ben_accname'] . '</td>
                        <td>' . $c['ben_accno'] . '</td>
                        <td>' . $c['bank_name'] . '</td>
                        <td>' . ($c['ben_enabled'] == '1' ? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>') . '</td>
                        <td>' . $c['ben_dateadded'] . '</td>
                        <td>'
                            . '<button data-toggle="modal" data-target="#form-edit-client" class="btn btn-space btn-danger" onclick="getBenDeets(' . $id . ')"><i class="icon s7-look"></i> View / Edit</button>'
                            . '</td>
					</tr>';
                        }
                        echo $display;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div id="create-modal" tabindex="-1" role="dialog" class="modal fade colored-header">
    <div class="modal-dialog custom-width">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i
                        class="icon s7-close"></i></button>
                <h3 class="modal-title">Create Beneficiary</h3>
            </div>
            <form id="create-form">
                <div id="msg-container-create-modal"></div>
                <div class="modal-body form" style="overflow: auto; max-height: 400px;">
                    <div class="form-group">
                        <label for="name">NAME:</label>
                        <input type="text" required="required" name="name" id="name" placeholder="Beneficiary name here" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="email">E-MAIL:</label>
                        <input type="email" required="required" name="email" id="email" placeholder="E-mail here" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="accname">ACCOUNT NAME:</label>
                        <input type="text" required="required" name="accname" id="accname" placeholder="Account name here" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="accno">ACCOUNT NO:</label>
                        <input type="text" required="required" name="accno" id="accno" placeholder="Account number here" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="bank">BANK:</label>
<?php
    $bank_dd = array(
        '0' => '--Select Bank--'
    );
    foreach($banks as $row){
        $bank_dd[$row['bank_id']] = $row['bank_name'];
    }
    echo form_dropdown('bank', $bank_dd, 0, 'id="bank" class="form-control"');
?>
                    </div>
                </div>
                <div class="modal-footer">
                    <div id="loading-image-create-modal" style="width: 100%; display: none;" class="progress-bar progress-bar-danger progress-bar-striped active">Please, wait...</div>
                    <button type="button" id="close-button-create-modal" data-dismiss="modal" class="btn btn-default md-close">Close</button>
                    <button type="button" id="save-button-create-modal" class="btn btn-primary" onclick="doCreate(this.form);">Create Beneficiary</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div id="form-edit-client" tabindex="-1" role="dialog" class="modal fade colored-header">
    <div class="modal-dialog custom-width">
        <div class="modal-content">
            <?php echo form_open(site_url('beneficiary/updateBeneficiary')); ?>
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i class="icon s7-close"></i></button>
                <h3 class="modal-title">View/Edit Split</h3>
            </div>
            <div class="modal-body form" style="overflow: auto; max-height: 400px;">
                <div id="edit-client-form-container">
                    <div class="form-group">
                        <label for="name-edit">NAME:</label>
                        <input type="text" required="required" name="name" id="name-edit" placeholder="Beneficiary name here" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="email-edit">E-MAIL:</label>
                        <input type="email" required="required" name="email" id="email-edit" placeholder="E-mail here" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="accname-edit">ACCOUNT NAME:</label>
                        <input type="text" required="required" name="accname" id="accname-edit" placeholder="Account name here" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="accno-edit">ACCOUNT NO:</label>
                        <input type="text" required="required" name="accno" id="accno-edit" placeholder="Account number here" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="bank-edit">BANK:</label>
<?php
    $bank_dd = array(
        '0' => '--Select Bank--'
    );
    foreach($banks as $row){
        $bank_dd[$row['bank_id']] = $row['bank_name'];
    }
    echo form_dropdown('bank', $bank_dd, 0, 'id="bank-edit" class="form-control"');
?>
                    </div>
                    <div class="form-group">
                        <label for="enable-edit">ENABLED:</label>
<?php
    $enable_dd = array(
        '0' => 'No',
        '1' => 'Yes'
    );
    echo form_dropdown('enabled', $enable_dd, 0, 'id="enable-edit" class="form-control"');
?>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <div id="loading-image-edit-body" style="width: 100%; display: none;" class="progress-bar progress-bar-danger progress-bar-striped active">Please, wait...</div>
                <button type="button" id="close-button-edit" data-dismiss="modal" class="btn btn-default md-close">Close</button>
<?php
    echo $dis_sys->hasAccess('beneficiary-update') ? '<button type="button" id="save-button-edit" class="btn btn-primary" onclick="updateBeneficiary(this.form);">Update Details</button>' : '';
?>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>


<form id="ben-user-create-form">
    <input id="ben_firstname" name="firstname" value="" type="hidden"/>
    <input id="ben_surname" name="surname" value="" type="hidden"/>
    <input id="ben_usergroup" name="usergroup" value="<?php echo COMM_BENEFICIARY_USERGROUP; ?>" type="hidden"/>
    <input id="ben_email" name="email" value="" type="hidden"/>
</form>


<style type="text/css">
    label { font-weight: bold; }
</style>


<script type="text/javascript">
    comm_config_hidden = true;
    edit_id = 0;
    var dis_timer = 0;
    function showCommConfig(do_edit = false){
        dis_id = do_edit ? 'commission-config-container-edit' : 'commission-config-container';
        if(comm_config_hidden){
            $('#' + dis_id).fadeIn()
        } else {
            $('#' + dis_id).fadeOut()
        }
        comm_config_hidden = !comm_config_hidden;
    }
    
    
    function doCreate(dis_form){
        form_data = $(dis_form).serializeArray();
        dis_url = '<?php echo site_url('beneficiary/addBeneficiary'); ?>';
        $('#close-button-create-modal').fadeOut();
        $('#save-button-create-modal').fadeOut();
        $('#loading-image-create-modal').fadeIn();
        $.ajax({
            type: 'POST',
            url: dis_url,
            data_type: 'json',
            data: form_data,
            success: function(data){
                if(data.status){
                    //Create the beneficiary as a user
                    $('#ben_firstname').val($('#name').val());
                    $('#ben_surname').val($('#name').val());
                    $('#ben_email').val($('#email').val());
                    form_data_user = $('#ben-user-create-form').serializeArray();
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo site_url('sys-admin/user/add'); ?>',
                        data_type: 'json',
                        data: form_data_user,
                        success: function(data){
                        }
                    });
                    
                    dis_timer = setInterval(reloadList, 3000);
                    
//                    window.location = '<?php echo site_url('beneficiary'); ?>';
                } else {
                    $('#close-button-create-modal').fadeIn();
                    $('#save-button-create-modal').fadeIn();
                }
                $('#loading-image-create-modal').fadeOut();
                $('#msg-container-create-modal').html(data.msg);
                $('#msg-container-create-modal').fadeIn();
            }
        });
    }
    
    
    function getBenDeets(id){
        edit_id = id;
        dis_url = '<?php echo site_url('beneficiary/getBeneficiary') . '/'; ?>' + id;
        $('#save-button-edit').fadeOut();
        $('#loading-image-edit-body').fadeIn();
        $('#edit-client-form-container').fadeOut();
        $('#msg-container-edit').fadeOut();
        $.ajax({
            type: 'GET',
            url: dis_url,
            data_type: 'json',
            success: function(data){
                $('#loading-image-edit-body').fadeOut();
                // console.log(data);
                if(data.status){
                    $('#name-edit').val(data.bdata.name);
                    $('#email-edit').val(data.bdata.email);
                    $('#accname-edit').val(data.bdata.accname);
                    $('#accno-edit').val(data.bdata.accno);
                    $('#bank-edit').val(data.bdata.bank);
                    $('#enable-edit').val(data.bdata.enabled);

                    $('#edit-client-form-container').fadeIn();
                    $('#close-button-edit').fadeIn();
                    $('#save-button-edit').fadeIn();
                } else {
                    $('#msg-container-edit').html('Unknown Beneficiary!!!');
                }
            }
        });
    }
    
    
    function initAddBox(){
        comm_config_hidden = true;
        document.getElementById('computecommission').checked = false;
        $('#commission-config-container').fadeOut();
    }


    function updateBeneficiary(dis_form){
        form_data = $(dis_form).serializeArray();
        dis_url = '<?php echo site_url('beneficiary/updateBeneficiary') . '/'; ?>' + edit_id;
        $('#close-button-edit').fadeOut();
        $('#save-button-edit').fadeOut();
        $('#loading-image-edit').fadeIn();
        $.ajax({
            type: 'POST',
            url: dis_url,
            data_type: 'json',
            data: form_data,
            success: function(data){
                // console.log(data);
                $('#loading-image-edit').fadeOut();
                $('#msg-container-edit').html(data.msg);
                $('#msg-container-edit').fadeIn();
//                console.log(data);
//                 $('#close-button-edit').fadeIn();
//                 $('#save-button-edit').fadeIn();
                if(data.status){
                    dis_timer = setInterval(reloadList, 2000);
                } else {
                    $('#close-button-edit').fadeIn();
                    $('#save-button-edit').fadeIn();
                }
            }
        });
    }


    function reloadList(){
        window.location = '<?php  echo site_url('beneficiary'); ?>';
    }
    
    
//    function reloadClients(){
//        dis_url = '<?php //echo site_url('client/getClients'); ?>';
//        clearTimeout(dis_timer);
//        $.ajax({
//            type: 'POST',
//            url: dis_url,
//            data_type: 'json',
//            data: form_data,
//            success: function(data){
//                console.log(data);
//                
////                if(data.status){
////
////                } else {
////                }
//            }
//        });
//    }
    
</script>