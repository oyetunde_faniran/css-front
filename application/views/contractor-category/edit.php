
<div class="panel panel-default col-md-6">
    <div class="panel-body">
        <?php
        $form_display = '';
        try {
            //Confirm that the category was found and the details were successfully loaded
            if (empty($category_details)) {
                throw new Exception('Unknown category selected.');
            }
            
            $form_display .=!empty($msg) ? $msg : '';
            $attr = array("role" => "form", "id" => "catfrm");
            $form_display .= form_open('', $attr)
                    . '<div class="form-group">
                <label>Category Name</label>
                ' . form_input('category', $category_details['accat_name'], 'class=" form-control" id="category" placeholder="Enter the name of the category" oldautocomplete="remove" autocomplete="off" required="required"') . '
            </div>

            <div class="form-group ">
                <label for="catactive" class="control-label col-lg-3">Active</label>
                <div class="col-lg-6">
                ' . form_checkbox('catactive', 1, ($category_details['accat_enabled'] == '1'), 'id="catactive"') . '
                </div>
            </div>
            
            <div class="spacer text-right">
                <button type="submit" class="btn btn-space btn-primary">Update Category</button>
            </div>'
                    . form_close();
            
        } catch (Exception $ex) {
            $form_display = $ex->getMessage();
        }
        
        $form_display .= '<p>' . anchor("contractor_category/view", '&laquo; Back to List of Categories') . '</p>';
        echo $form_display;
        ?>
    </div>
</div>

