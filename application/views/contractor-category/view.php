<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-body">
                <table class="table table-condensed table-hover table-bordered table-striped" id="report-table">
                    <thead>
                        <tr>
                            <td>S/NO</td>
                            <td>CATEGORY NAME</td>
                            <td>STATUS</td>
                            <td>ACTION</td>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <td>S/NO</td>
                            <td>CATEGORY NAME</td>
                            <td>STATUS</td>
                            <td>ACTION</td>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php
                        $display = '';
                        $sno = 0;
                        foreach ($cats as $cat) {
                            $id = $cat['accat_id'];
                            $display .= '<tr>
						<td>' . ( ++$sno) . '.</td>
						<td>' . $cat['accat_name'] . '</td>
						<td>' . ($cat['accat_enabled'] == '1' ? '<span class="label label-info">Active</span>' : '<span class="label label-danger">Inactive</span>') . '</td>
                                                <td>  ' . anchor("contractor_category/edit/$id", '<i class="icon s7-edit"></i> Edit',' class="btn btn-space btn-success btn-xs"') .'</td>
					</tr>';
                        }
                        echo $display;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>