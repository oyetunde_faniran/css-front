
<div class="panel panel-default col-md-6">
    <div class="panel-body">
        <?php
        $form_display = '';
        $form_display .=!empty($msg) ? $msg : '';
        $attr = array("role" => "form", "id" => "catfrm");
        $form_display .= form_open('', $attr)
                . '<div class="form-group">
                <label>Category Name</label>
                ' . form_input('category', set_value('category'), 'class=" form-control" id="category" placeholder="Enter the name of the category" oldautocomplete="remove" autocomplete="off" required="required"') . '
            </div>

            <div class="form-group ">
                <label for="catactive" class="control-label col-lg-3">Active</label>
                <div class="col-lg-6">
                ' . form_checkbox('catactive', 1, set_value('catactive'), 'id="catactive" checked="checked"') . '
                </div>
            </div>
            
            <div class="spacer text-right">
                <button type="submit" class="btn btn-space btn-primary">Add Category</button>
            </div>'
                . form_close();
        $form_display .= '<p>' . anchor("contractor_category/view", '&laquo; Back to List of Categories') . '</p>';
        echo $form_display;
        ?>
    </div>
</div>

