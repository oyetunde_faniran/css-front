<?php
$msg = !empty($msg) ? $msg : "";
?>
    <div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $title ?></h3>
    </div>
<?php
if (!empty($response)) {
    $responseStatus = $response['0'];
    $responseMessage = $response['1'];
    $bbatchNo = urlencode(base64_encode($_SESSION['myBatchNo']));

    $reqLink = site_url("payments/schedule/requeryBatch/$bbatchNo");
    $dLink = "<a href=$reqLink  class='btn btn-primary btn-lg'>Get Transaction Status</a>";
    ?>
    <div class="panel-body">
        <?php echo $msg ?>
        <div class="col-md-3">

        </div>
        <div class="col-md-6 center-block">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-actions">
                    <tbody>
                    <tr>
                        <td style="font-size: 16px;"><strong>Batch No: </strong><?php echo $_SESSION['myBatchNo'] ?>
                            <br>
                            <strong>Payment Gateway Response:</strong> <?php echo $response['1'] ?><br><br>
                            <?php echo $dLink ?>
                        </td>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-3">

        </div>
    </div>

    </div>
<?php } else {
    echo "Invalid Schedule";
} ?>