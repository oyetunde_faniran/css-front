<div class="row">
    <form name="spay" method="POST" action="">
        <div class="col-md-12">
            <?php
            $msg = !empty($msg) ? $msg : '';
            echo $msg;
            ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo $title ?></h3>
                <span class="pull-right"><input type="submit" class="btn btn-success" name="process" value="Requery"></span>

                </div>

                <div class="panel-body">

                    <?php if (empty($paymentFiles)) {
                        echo "No Payment Schedule";
                    } else { // print_r($paymentFiles);?>
                        <div class="table-responsive" id="customers2">

                            <table class="table table-bordered table-striped datatable" id="DataTables_Table_0">
                                <thead>
                                <tr>
                                    <th>S/No</th>
                                    <th>Name</th>
                                    <th>Bank</th>
                                    <th>Account Number</th>
                                    <th>Payment Status</th>
                                    <th>Amount</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $count = 1;
                                $total = 0;
                                $dstatus = $paymentFiles[0]['paymentStatus'];
                                $description = $paymentFiles[0]['paymentFileDescription'];

                                $encrytedPaymentFileId = $this->basic_functions->encryptGetData($paymentFiles[0]['paymentFileId']);
                                $dLink = "";
                                if ($dstatus == '2') {
                                    $dLink = anchor('payments/schedule/doauth/' . $encrytedPaymentFileId, '<button class="btn btn-success"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Authorize">Authorize Payment</button>');
                                }

                                foreach ($paymentFiles as $u) {
                                    $detailId = $u['paymentDetailsId'];
                                    $description = $u['paymentFileDescription'];
                                    $amount = $u['amount'];
                                    $famount = number_format($amount, 2);
                                    $total += $amount;
                                    $status = $u['paymentStatus'];
                                    $check = "";
                                    $class2 = "";
                                    $data = "";
                                    if ($u['paidStatus'] == '00') {
                                        $pstatus = "Paid";
                                        $class = "success";
                                    } elseif ($u['pendingStatus'] == '01') {
                                        //has been sent sent but still pending
                                        $pstatus = "Still Processing";
                                        $class = "warning";
                                    } elseif ($u['failedStatus'] != '') {
                                        //has been sent but still pending
                                        $pstatus = "Pending";
                                        $reason = $u['failedStatus'];
                                        $class = "warning";
                                        $class2 = " popover-dismiss ";
                                        $data = " data-content='And here some amazing content. It's very engaging. Right?' data-original-title='Dismissible popover'";
                                        $check = "<input class='checkbox1'  type='checkbox' name=indiv[$detailId] value='$amount'>";
                                    } else {
                                        $pstatus = "Not Paid";
                                        $class = 'danger';
                                        $check = "<input class='checkbox1'  type='checkbox' name=indiv[$detailId] value='$amount'>";
                                    }
                                    ?>
                                    <tr id="trow_<?php echo $count ?>">
                                        <td class="text-center"><?php echo $check;
                                            echo $count ?></td>
                                        <td><?php echo stripslashes($u['beneficiarySurname']) . " " . stripslashes($u['beneficiaryFirstname']) . " " . stripslashes($u['beneficiaryOthernames']) ?></td>
                                        <td><?php echo stripslashes($u['bankName']) ?></td>
                                        <td><?php echo stripslashes($u['beneficiaryAccountNo']) ?></td>
                                        <td>
                                            <?php if ($pstatus == "Failed") {
                                                ?>
                                                <button type="button" class="btn btn-danger popover-dismiss"
                                                        data-content="<?php echo $reason ?>"
                                                        data-original-title="Reason"><?php echo $pstatus ?></button>
                                                <?php
                                            } else { ?>
                                                <span
                                                    class="label label-<?php echo $class; ?> label-form"><?php echo $pstatus ?></span>
                                            <?php } ?>
                                        </td>
                                        <td align="right"><?php echo $famount ?></td>
                                    </tr>
                                    <?php $count++;
                                } ?>
                                </tbody>
                                <tfoot>
                                <tr id="trow_<?php echo $count ?>">

                                    <td align="center" colspan="5"><strong>TOTAL</strong></td>

                                    <td align="right"><input type="hidden" value="<?php echo $description ?>"
                                                             name="description"><?php $ftotal = number_format($total, 2);
                                        echo $ftotal ?></td>


                                </tr>
                                </tfoot>
                            </table>

                        </div>
                    <?php } ?>


                </div>
                <div class="panel-footer">
                    <div class="pull-right"><?php echo $dLink ?></div>
                </div>
            </div>
        </div>
    </form>
</div>