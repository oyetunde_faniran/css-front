<?php $msg = !empty($msg) ? $msg : ""; ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $title ?></h3>
    </div>
    <?php
    if (!empty($transactionsTotal)) {
        ?>
        <div class="panel-body">
            <?php echo $msg ?>
            <div class="col-md-8 center-block">
                <div class="table-responsive">
                    <form action="" method="POST" autocomplete="off">
                        <table class="table table-bordered table-striped table-actions">
                            <tbody>
                            <tr>
                                <td><strong>Total Amount</strong></td>
                                <td>&#8358; <?php echo number_format($transactionsTotal, 2) ?></td>
                            </tr>

                            <?php if ($_SESSION['ga_enabled']) { ?>

                                <tr>
                                    <td><strong>GA Code</strong></td>
                                    <td>
                                        <input name="dtoken" id="ga_code" maxlength="6" type="text" size="25"
                                               placeholder="Enter the code from your GA App">
                                    </td>
                                </tr>

                            <?php } else { ?>

                                <tr>
                                    <td><strong>Token</strong></td>
                                    <td>
                                        <p style="color:red; font-size:14px">Please check the mail inbox/SPAM folder
                                            of <?php echo $_SESSION['email'] ?> for the token.</p>
                                        <input type="password" name="dtoken" size="10">
                                    </td>
                                </tr>

                            <?php } ?>

                            <tr>
                                <td colspan="2" align="right">
                                    <input type="submit" name="pay" class='btn btn-primary' value="Pay Now">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    <?php } else {
        echo "<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Invalid Token<br><br><br>";
    } ?>
</div>