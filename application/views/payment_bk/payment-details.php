<?php $dis_sys = $this->system_authorization; ?>

<div class="row">
    <div class="col-md-12">
        <?php
        $msg = !empty($msg) ? $msg : '';
        echo $msg;
        ?>
        <div class="panel panel-default">

            <div class="panel-heading">
                <h3 class="panel-title"><?php echo $title ?></h3>
            </div>

            <div class="panel-body">

                <?php
                if (empty($payment_file_group)) {
                    echo "No Payment Schedule";
                } else {
                    $back = '<button class="btn btn-warning"  onclick="window.history.back()">Back to Payments List</button>';
                    $encryptedPaymentFileGroupId = $this->basic_functions->encryptGetData($payment_file_group['id']);
                    $aLink = "";

                    if ($payment_file_group['status'] == PAYMENT_AUTHORIZED) {
                        $file_group_status = "<span class='label label-info'>AUTHORIZED</span>";

                        if (empty($payment_file_group['payment_files'][0]['transaction_batch'])) {
                            if ($dis_sys->hasAccess('make-payment')) {
                                $aLink = anchor('payments/schedule/makePayment/' . $encryptedPaymentFileGroupId, '<button class="btn btn-success"  data-original-title="Make Payment">Make Payment</button>');
                            }
                        } else {
                            $aLink = anchor('payments/schedule/report_details/' . $encryptedPaymentFileGroupId, '<button class="btn btn-success"  data-original-title="Report">Show Report</button>');
                            $file_group_status = '<span class="label label-info label-form">PROCESSED</span>';
                        }
                    } elseif ($payment_file_group['status'] == PAYMENT_REJECTED) {
                        $file_group_status = "<span class='label label-danger'>REJECTED</span>";
                    } elseif ($payment_file_group['status'] == PAYMENT_APPROVED) {
                        $file_group_status = "<span class='label label-info'>APPROVED</span>";

                        if ($dis_sys->hasAccess('schedule-do-auth')) {
                            $auth_method = $payment_file_group['payment_files'][0]['paymentFileType'] == 3 ? 'doauth' : 'doauth2';
                            $aLink = anchor('payments/schedule/' . $auth_method . '/' . $encryptedPaymentFileGroupId, '<button class="btn btn-success"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Authorize">Authorize Payment</button>');
                            $aLink .= " " . anchor('payments/schedule/reject/' . $encryptedPaymentFileGroupId, '<button class="btn btn-danger"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Reject">Reject Payment</button>');
                        }
                    } elseif ($payment_file_group['status'] == PAYMENT_FORWARDED) {
                        $file_group_status = "<span class='label label-warning'>FORWARDED</span>";

                        if ($dis_sys->hasAccess('payment-schedule-approve')) {
                            $aLink = anchor('payments/schedule/approve/' . $encryptedPaymentFileGroupId, '<button class="btn btn-success"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Approve">Approve Payment</button>');
                            $aLink .= " " . anchor('payments/schedule/reject/' . $encryptedPaymentFileGroupId, '<button class="btn btn-danger"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Reject">Reject Payment</button>');
                        }
                    } else {
                        $file_group_status = "<span class='label label-default'>PENDING</span>";

                        if ($dis_sys->hasAccess('payment-schedule-forward')) {
                            $aLink = anchor('payments/schedule/forward/' . $encryptedPaymentFileGroupId, '<button class="btn btn-success"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Forward">Forward Payment</button>');
                        }
                    }

                    ?>

                    <table class="table table-bordered">
                        <tr>
                            <th>NARRATION</th>
                            <td><?php echo $payment_file_group['narration'] ?></td>
                            <th>PAYMENT TYPE</th>
                            <td><?php echo $payment_file_group['payment_files'][0]['paymentFileType'] == "1" ? "Salary Payment" : ($payment_file_group['payment_files'][0]['paymentFileType'] == "2" ? "Vendor Payment" : "Fund Sweeping"); ?></td>
                            <th>STATUS</th>
                            <td><?php echo $file_group_status ?></td>
                        </tr>
                        <tr>
                            <th>TOTAL NUMBER OF BENEFICIARIES</th>
                            <td><?php echo number_format($payment_file_group['num_credit_accounts']) ?></td>
                            <th>TOTAL AMOUNT</th>
                            <td>&#8358;
                                <?php
                                $total_amount = 0;
                                foreach ($payment_file_group['payment_files'] as $file) {
                                    foreach ($file['beneficiaries'] as $beneficiary) {
                                        $total_amount += $beneficiary['amount'];
                                    }
                                }
                                echo number_format($total_amount, 2);
                                ?>
                            </td>
                            <th></th>
                            <td></td>
                        </tr>
                        <tr>
                            <th>INITIATED BY</th>
                            <td><?php echo $payment_file_group['initiator'] ?></td>
                            <?php
                            if ($payment_file_group['approver']) {
                                $approver_th = 'APPROVED BY';
                                $approver_td = $payment_file_group['approver'];
                            } elseif ($payment_file_group['rejecter']) {
                                list ($rejected_by, $rejected_for) = explode(":", $payment_file_group['rejecter']);
                                $approver_th = 'REJECTED BY';
                                $approver_td = $rejected_by . '<p>REASON: ' . $rejected_for . '</p>';
                            }
                            ?>
                            <th><?php echo !empty($approver_th) ? $approver_th : '' ?></th>
                            <td><?php echo !empty($approver_td) ? $approver_td : '' ?></td>

                            <?php
                            if ($payment_file_group['authorizer']) {
                                $authorizer_th = 'AUTHORIZED BY';
                                $authorizer_td = $payment_file_group['authorizer'];
                            } elseif ($payment_file_group['rejecter'] && $payment_file_group['approver']) {
                                list ($rejected_by, $rejected_for) = explode(":", $payment_file_group['rejecter']);
                                $authorizer_th = 'REJECTED BY';
                                $authorizer_td = $rejected_by . '<p>REASON: ' . $rejected_for . '</p>';
                            }
                            ?>
                            <th><?php echo !empty($authorizer_th) ? $authorizer_th : '' ?></th>
                            <td><?php echo !empty($authorizer_td) ? $authorizer_td : '' ?></td>
                        </tr>
                    </table>

                    <div class="table-responsive" id="customers2">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Source Accounts</th>
                                <th>Beneficiaries</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($payment_file_group['payment_files'] as $file) { ?>
                                <tr>
                                    <td><?php echo $file['source_account']['acc_name'] . " - " . $file['source_account']['bank_name'] . " - " . $file['source_account']['acc_number'] ?></td>
                                    <td>
                                        <table class="table table-bordered table-condensed">
                                            <tr>
                                                <th>#</th>
                                                <th>Account Name</th>
                                                <th>Bank Name</th>
                                                <th>Account Number</th>
                                                <th>Amount (&#8358;)</th>
                                            </tr>

                                            <?php
                                            $i = 0;
                                            foreach ($file['beneficiaries'] as $beneficiary) {
                                                echo "<tr><td>" . ++$i . "</td><td>" . stripslashes($beneficiary['beneficiarySurname']) . "</td><td>" . stripslashes($beneficiary['bankName']) . "</td><td>" . stripslashes($beneficiary['beneficiaryAccountNo']) . "</td><td>" . number_format($beneficiary['amount'], 2) . "</td></tr>";
                                            }
                                            ?>
                                        </table>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                <?php } ?>
            </div>
            <div class="panel-footer">
                <div class="pull-left"><?php echo $back ?></div>
                <div class="pull-right"><?php echo $aLink ?></div>
                <br class="clearfix">
            </div>
        </div>
    </div>
</div>