<?php
$msg = !empty($msg) ? $msg : "";

?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $title ?></h3>
    </div>
    <?php
    if(!empty($account)){
    $merchantAccountId =  $account['merchantAccountId'];
    $merchantId = $account['merchantId'];
    $encryptedMerchantId = $this->basic_functions->encryptGetData($merchantId);
    $encryptedMerchantAccountId = $this->basic_functions->encryptGetData($merchantAccountId);

    ?>
    <div class="panel-body">
        <?php echo $msg ?>
        <div class="col-md-6 center-block">
            <div class="table-responsive">
                <form id="validate"  class="form-horizontal" action="" method="POST">
                    <table class="table table-bordered table-striped table-actions">

                    <tbody>
                    <tr >
                        <td width="30%"><strong>Account No</strong></td>
                        <td><?php echo  $account['accountNo'] ?></td>

                    </tr>
                    <tr >
                        <td><strong>Bank</strong></td>
                        <td><?php echo stripslashes($account['bankName']) ?></td>
                    </tr>
                    <tr>
                        <td><strong>CardPan</strong></td>
                        <td><?php echo $maskedCardPan ?></td>
                    </tr>
                    <tr>
                        <td><strong>Old PIN</strong></td>
                        <td><input type="password" name="oldPIN" class="validate[required] form-control"></td>
                    </tr>
                    <tr>
                        <td><strong>New PIN</strong></td>
                        <td><input type="password" name="newPIN" class="validate[required] form-control" id="newPIN"></td>
                    </tr>
                    <tr>
                        <td><strong>Confirm New PIN</strong></td>
                        <td><input type="password" name="newPIN2" class="validate[required,equals[newPIN]] form-control"></td>
                    </tr>
                    <tr>
                        <td colspan="2" >
                            <input type="submit" class="btn btn-success pull-right" value="Change PIN">
                        </td>
                    </tr>



                    </tbody>
                </table>
                    </form>
            </div>
        </div>


    </div>

</div>
<?php }else{ echo "Invalid Account";}
//echo "<script type='text/javascript' src='".site_url('js/plugins/jquery-validation/jquery.validate.js')."'></script>";
