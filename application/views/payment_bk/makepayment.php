<div class="row">
    <form name="spay" method="POST" action="">
        <div class="col-md-12">
            <?php
            $msg = !empty($msg) ? $msg : '';
            echo $msg;
            ?>
            <div class="panel panel-default">

                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo $title ?></h3>
                    <br>
                    <span>
                        <input type="submit" class="btn btn-success" name="process" value="Process Checked">
                    </span>
                </div>

                <div class="panel-body">

                    <?php if (empty($paymentFileGroup)) {
                        echo "No Payment Schedule";
                    } else { ?>
                        <div class="table-responsive" id="customers2">
                            <table class="table table-bordered table-striped" id="DataTables_Table_0">
                                <thead>
                                <tr>
                                    <th>S/No</th>
                                    <th colspan="6">Narration</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr id="trow_1">
                                    <td>1 <input class='checkbox1' type='checkbox' name="id"
                                                 value="<?php echo $paymentFileGroup['id'] ?>"></td>
                                    <td colspan="6"><?php echo $paymentFileGroup['narration'] ?></td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th>Bank</th>
                                    <th>Account Name</th>
                                    <th>Account Number</th>
                                    <th style="text-align: right">Amount (&#8358;)</th>
                                    <th>Payment Status</th>
                                    <th>Source Account Details</th>
                                </tr>
                                <?php
                                $count = 2;
                                $total_amount = 0;
                                $dstatus = $paymentFileGroup['status'];
                                $description = $paymentFileGroup['narration'];

                                $encryptedPaymentFileGroupId = $this->basic_functions->encryptGetData($paymentFileGroup['id']);
                                $dLink = "";

                                /*if ($dstatus == PAYMENT_APPROVED) {
                                    $dLink = anchor('payments/schedule/doauth/' . $encryptedPaymentFileGroupId, '<button class="btn btn-success"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Authorize">Authorize Payment</button>');
                                }*/

                                foreach ($paymentFileGroup['payment_files'] as $file) {
                                    foreach ($file['beneficiaries'] as $beneficiary) {
                                        $total_amount += $beneficiary['amount'];

                                        $data = "";
                                        if ($beneficiary['paidStatus'] == '00') {
                                            $pstatus = "Paid";
                                            $class = "success";
                                        } elseif ($beneficiary['pendingStatus'] == '01') {
                                            //has been sent sent but still pending
                                            $pstatus = "Still Processing";
                                            $class = "warning";
                                        } elseif ($beneficiary['failedStatus'] != '') {
                                            //has been sent but still pending
                                            $pstatus = "Failed";
                                            $reason = $beneficiary['failedStatus'];
                                            $class = "danger";
                                            $class2 = " popover-dismiss ";
                                            $data = " data-content='And here some amazing content. It's very engaging. Right?' data-original-title='Dismissible popover'";
                                        } else {
                                            $pstatus = "Not Paid";
                                            $class = 'danger';
                                        }
                                        ?>
                                        <tr id="trow_<?php echo $count ?>">
                                            <td></td>
                                            <td><?php echo stripslashes($beneficiary['bankName']) ?></td>
                                            <td><?php echo stripslashes($beneficiary['beneficiarySurname']) . " " . stripslashes($beneficiary['beneficiaryFirstname']) . " " . stripslashes($beneficiary['beneficiaryOthernames']) ?></td>
                                            <td><?php echo stripslashes($beneficiary['beneficiaryAccountNo']) ?></td>
                                            <td align="right"><?php echo number_format($beneficiary['amount'], 2) ?></td>
                                            <td>
                                                <?php if ($pstatus == "Failed") {
                                                    ?>
                                                    <button type="button" class="btn btn-danger popover-dismiss"
                                                            data-content="<?php echo $reason ?>"
                                                            data-original-title="Reason"><?php echo $pstatus ?></button>
                                                    <?php
                                                } else { ?>
                                                    <span
                                                        class="label label-<?php echo $class; ?> label-form"><?php echo $pstatus ?></span>
                                                <?php } ?>
                                            </td>
                                            <td><?php echo $beneficiary['sourceBank'] ?></td>
                                        </tr>
                                        <?php $count++;
                                    }
                                } ?>
                                </tbody>
                                <tfoot>
                                <tr id="trow_<?php echo $count ?>">
                                    <th style="text-align: right" colspan="4"><strong>TOTAL</strong></th>
                                    <th style="text-align: right"><?php echo number_format($total_amount, 2); ?></th>
                                    <td></td>
                                    <td></td>
                                </tr>
                                </tfoot>
                            </table>

                        </div>
                    <?php } ?>
                </div>
                <div class="panel-footer">
                    <div class="pull-right"><?php echo $dLink ?></div>
                </div>
            </div>
        </div>
    </form>
</div>