<?php $msg = !empty($msg) ? $msg : ""; ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $title ?></h3>
    </div>
    <div class="panel-body">
        <?php echo $msg ?>
        <div class="col-md-12 center-block">
            <table class="table table-bordered table-striped" id="bulk-payment-files">
                <thead>
                <tr>
                    <th>#</th>
                    <th>File Name</th>
                    <th>Date Uploaded</th>
                    <th>Status</th>
                    <th>Description</th>
                    <th>Summary</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 0;
                if (!empty($bulk_payment_files)) {
                    foreach ($bulk_payment_files AS $file) {
                        $pLink = $rLink = $sLink = $dLink = "";

                        $payable_accounts = array();

                        if (!empty($file['total_valid_accounts'])) $payable_accounts['valid'] = $file['total_valid_accounts'];
                        if (!empty($file['total_swapped_accounts'])) $payable_accounts['swapped'] = $file['total_swapped_accounts'];
                        if (!empty($file['total_forced_accounts'])) $payable_accounts['forced'] = $file['total_forced_accounts'];

                        $total_payable = array_sum($payable_accounts);

                        if ($file['file_av_status'] == '2') {
                            $status = '<span class="label label-danger">ACCOUNT VALIDATION FAILED</span>';
                        } elseif ($file['file_av_status'] == '1') {
                            $status = '<span class="label label-default">ACCOUNT VALIDATION PENDING</span>';
                            $pLink = anchor('payments/schedule/validateBulkPaymentAccounts/' . $file['file_id'], '<button class="btn btn-xs btn-success">Validate Accounts</button>');
                        } else {
                            if (in_array($file['file_av_response_code'], array("00", "12"))) {
                                $status = '<span class="label label-info">ACCOUNT VALIDATION COMPLETE</span>';
                                if ($total_payable > 0 && empty($file['bpf_payment_file_group'])) {
                                    $pLink = anchor('payments/schedule/processBulkPaymentFile/' . $file['file_id'], '<button class="btn btn-xs btn-success">Process file</button>');
                                }

                                if ($total_payable != $file['total_accounts'])
                                    $sLink = anchor('payments/schedule/runSwapWizard/' . $file['file_id'], '<button class="btn btn-xs btn-danger">Run Swap Wizard</button>');

                            } else {
                                $status = '<span class="label label-warning">ACCOUNT VALIDATION IN PROGRESS</span>';
                            }
                        }

                        $status_label = in_array($file['file_av_response_code'], array('16', '06', '00', '12')) ? 'success' : 'danger';

                        $aLink = anchor('payments/schedule/listBulkPaymentAccounts/' . $file['file_id'], '<button class="btn btn-xs btn-info">List Accounts</button>');

                        if (in_array($file['file_av_response_code'], array("16", "06"))) {
                            $rLink = anchor('payments/schedule/getAccountValidationStatus/' . $file['file_id'], '<button class="btn btn-xs btn-success">Get Status</button>');
                        }

                        if (file_exists(str_replace('input', 'output', $file['file_path'])))
                            $dLink = anchor('payments/schedule/downloadBulkPaymentFilePaymentReport/' . $file['file_id'], '<button class="btn btn-xs btn-warning">Download Payment Report</button>');
                        ?>
                        <tr>
                            <td><?php echo ++$i ?></td>
                            <td><?php echo $file['file_name'] ?></td>
                            <td><?php echo $file['created_at'] ?></td>
                            <td><?php echo $status ?></td>
                            <td>
                                <?php
                                if (!empty($file['file_av_response_description'])) {
                                    echo '<span class="label label-form label-' . $status_label . '">' . $file['file_av_response_description'] . '</span>';
                                }
                                ?>
                            </td>
                            <td><?php echo $total_payable . " valid account(s) out of " . intval($file['total_accounts']) ?></td>
                            <td><?php echo $aLink . " " . $sLink . " " . $pLink . " " . $rLink . " " . $dLink ?></td>
                        </tr>
                    <?php }
                } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>