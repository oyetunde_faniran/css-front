<?php $msg = !empty($msg) ? $msg : ""; ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $title ?></h3>
    </div>
    <div class="panel-body">

        <?php echo $msg ?>

        <div class="col-md-6 center-block">
            <form action="<?php echo site_url('payments/schedule/validateBulkPaymentFile') ?>" method="POST">
                <input type="hidden" value="<?php echo $file_name ?>" name="file_name">
                <input type="hidden" value="<?php echo $description ?>" name="description">
                <input type="hidden" value="<?php echo $originalName ?>" name="client_file_name">
                <table class="table table-bordered table-striped table-actions">
                    <tbody>
                    <tr>
                        <td><strong>Uploaded File Name</strong></td>
                        <td><?php echo $originalName ?></td>
                    </tr>
                    <tr>
                        <td><strong>Description</strong></td>
                        <td><?php echo $description ?></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                            <?php if (!empty($error_report)) { ?>
                                <a class="btn btn-danger" href="<?php echo site_url('payments/schedule/downloadBulkPaymentFileErrorReport/' . str_replace(" ", "__", $error_report)) ?>">Download Error Report</a>
                            <?php } else { ?>
                                <input type="submit" value="Validate File" name="validate" class="btn btn-success">
                            <?php } ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>