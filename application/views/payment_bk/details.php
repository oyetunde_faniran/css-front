<div class="row">
    <div class="col-md-12">
        <?php
        $msg = !empty($msg) ? $msg : '';
        echo $msg;

        ?>
        <div class="panel panel-default">

            <div class="panel-heading">
                <h3 class="panel-title"><?php echo $title ?></h3>
            </div>

            <div class="panel-body">

                <?php if (empty($paymentFiles)) {
                    echo "No Payment Schedule";
                } else { //print_r($paymentFiles); ?>
                    <div class="table-responsive" id="customers2">

                        <table class="table table-bordered table-striped datatable" id="datatable">
                            <thead>
                            <tr>
                                <th>S/No</th>
                                <th>Beneficiary Name</th>
                                <th>Bank</th>
                                <th>Account Number</th>
                                <?php if ($paymentType == 3) {
                                    $cols = 7;
                                    ?>
                                    <th>Source Account Name</th>
                                    <th>Source Bank</th>
                                    <th>Source Account</th>
                                <?php } ?>
                                <th>Amount</th>
                                <th>Payment Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $count = 1;
                            $total = 0;
                            $cols = 4;
                            $dstatus = $paymentFiles[0]['paymentStatus'];
                            $encrytedPaymentFileId = $this->basic_functions->encryptGetData($paymentFiles[0]['paymentFileId']);
                            $backLink = anchor('payments/schedule/listPayment', '<button class="btn btn-warning"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Authorized">View all Authorized Payments</button>');

                            if ($dstatus == PAYMENT_AUTHORIZED) {
                                $dLink = anchor('payments/schedule/makePayment/' . $encrytedPaymentFileId, '<button class="btn btn-success"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Authorize">Make Payment</button>');
                            }
                            foreach ($paymentFiles as $u) {

                                $amount = $u['amount'];
                                $famount = number_format($amount, 2);
                                $total += $amount;
                                $status = $u['paymentStatus'];


                                //transaction has been authorized so show payment status
                                if ($u['paidStatus'] == '00') {
                                    $pstatus = "Paid";
                                    $class = "success";
                                } elseif ($u['pendingStatus'] == '01') {
                                    //has been sent sent but still pending
                                    $pstatus = "Still Processing";
                                    $class = "warning";
                                } elseif ($u['failedStatus'] != '') {
                                    //has been sent sent but still pending
                                    $pstatus = "Failed";
                                    $reason = $u['failedStatus'];
                                    $class = "danger";
                                    $class2 = " popover-dismiss ";
                                    $data = " data-content='$reason' data-original-title='Failure Description'";

                                } else {
                                    $pstatus = "Not Paid";
                                    $class = 'danger';

                                }

                                ?>
                                <tr id="trow_<?php echo $count ?>">
                                    <td class="text-center"><?php echo $count ?></td>
                                    <td><?php echo stripslashes($u['beneficiarySurname']) . " " . stripslashes($u['beneficiaryFirstname']) . " " . stripslashes($u['beneficiaryOthernames']) ?></td>
                                    <td><?php echo stripslashes($u['bankName']) ?></td>
                                    <td><?php echo stripslashes($u['beneficiaryAccountNo']) ?></td>
                                    <?php if ($paymentType == 3) {
                                        $cols = 7;

                                        $sourceBank = $u['sourceBank'];
                                        $sourceBankArray = explode("-", $sourceBank);
                                        $sourceBankName = $sourceBankArray[0];
                                        $sourceAccountNo = $sourceBankArray[1];
                                        $sourceAccountName = $sourceBankArray[2]; ?>
                                        <td align="right"><?php echo $sourceAccountName ?></td>
                                        <td align="right"><?php echo $sourceBankName ?></td>
                                        <td align="right"><?php echo $sourceAccountNo ?></td>
                                    <?php } ?>

                                    <td align="right"><?php echo $famount ?></td>
                                    <?php if ($pstatus != "Failed") { ?>
                                        <td><span
                                                class="label label-<?php echo $class; ?> label-form"><?php echo $pstatus ?></span>
                                        </td>
                                    <?php } else {
                                        ?>
                                        <td>
                                            <button type="button" class="btn btn-danger popover-dismiss"
                                                    data-content="<?php echo $reason ?>"
                                                    data-original-title="Reason"><?php echo $pstatus ?></button>
                                        </td>
                                        <?php
                                    } ?>

                                </tr>
                                <?php $count++;
                            } ?>
                            </tbody>
                            <tfoot>
                            <tr id="trow_<?php echo $count ?>">

                                <td align="center" colspan="<?php echo $cols ?>"><strong>TOTAL</strong></td>

                                <td align="right"><?php $ftotal = number_format($total, 2);
                                    echo $ftotal ?></td>
                                <td></td>


                            </tr>
                            </tfoot>
                        </table>
                    </div>
                <?php } ?>


            </div>
            <div class="panel-footer">
                <div class="pull-left"><?php echo $backLink ?></div>
                <div class="pull-right"><?php echo $dLink ?></div>
            </div>
        </div>
    </div>
</div>