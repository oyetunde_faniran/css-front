<?php $msg = !empty($msg) ? $msg : ""; ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $title ?></h3>
    </div>

    <?php
    if (!empty($payment_file_group)) {
        $paymentFileGroupId = $payment_file_group['id'];
        $encryptedPaymentFileGroupId = $this->basic_functions->encryptGetData($paymentFileGroupId);
        $paymentStatus = $payment_file_group['status'];

        //Form the account dropdown options
        $acc_dd[0] = '--Select Account--';
        $bank_name = '';
        $inner_array = array();
        if (!empty($accs)) {
            foreach ($accs as $acc) {
                if (intval($acc['accbal_amount']) < ($total['amount'] + 105))
                    continue;

                //If the bank name is changing and it's not the first one, add it to the final array
                if ($bank_name != '' && $bank_name != $acc['bank_name']) {
                    $acc_dd[$bank_name] = $inner_array;
                    $inner_array = array();
                }

                $inner_array[$acc['acc_id']] = $acc['acc_name'] . ' - ' . $acc['acc_number'] . ' (&#8358; ' . number_format($acc['accbal_amount'], 2) . ')';

                //Update the name of the current bank
                $bank_name = $acc['bank_name'];
            }
        }

        if (!empty($inner_array)) {
            $acc_dd[$bank_name] = $inner_array;
        }

        //echo $authorize;
        $dLink = false;
        $pay = false;
        if ($paymentStatus == '2' AND !empty($accounts)) {
            $dLink = true;
        }

        if ($paymentStatus == '3') {
            $pay = true;
        }
        ?>

        <div class="panel-body">
            <?php echo $msg ?>
            <div class="col-md-6 center-block">
                <div class="table-responsive">
                    <form action="" method="POST">
                        <table class="table table-bordered table-striped table-actions">
                            <tbody>
                            <tr>
                                <td width="30%"><strong>Payment Description</strong></td>
                                <td><?php echo stripslashes($payment_file_group['narration']) ?></td>
                            </tr>
                            <tr>
                                <td><strong>Total Amount</strong></td>
                                <td>&#8358; <?php echo number_format($total['amount'], 2) ?></td>
                            </tr>
                            <tr>
                                <td>Source Account</td>
                                <td>
                                    <?php echo form_dropdown('sourceAccount', $acc_dd, null, 'class="form-control"'); ?>
                                </td>
                            </tr>
                            <?php if ($dLink) { ?>
                                <tr>
                                    <td colspan="2" align="right">
                                        <input type="hidden" value="<?php echo $paymentFileGroupId ?>" name="payfile">
                                        <input type="submit" name="authorize" class='btn btn-primary'
                                               value="Authorize Payment">
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
            <?php if (!empty($mandates)) { ?>
                <div class="col-md-6 center-block">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-actions">
                            <thead>
                            <tr>
                                <th>S/no</th>
                                <th>Bank</th>
                                <th>Account No</th>
                                <th>Date Authorized</th>
                                <th>Authorized By</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $count = 0;
                            foreach ($mandates as $m) {
                                ?>
                                <tr>
                                    <td><?php echo ++$count; ?></td>
                                    <td><?php echo $m['bankName'] ?></td>
                                    <td><?php echo $m['accountNo'] ?></td>
                                    <td><?php echo $m['user_surname'] . " " . $m['user_firstname'] ?></td>
                                    <td><?php echo $this->basic_functions->formatDate($m['authorizeTime']) ?></td>
                                </tr>
                                <?php
                            }

                            if ($pay) {
                                ?>
                                <tr>
                                    <td colspan="3" align="right">
                                        Minimum Signatories met
                                    </td>
                                    <td colspan="2" align="right">
                                        <a href="<?php echo base_url("payments/schedule/makePayment/$encryptedPaymentFileGroupId") ?>"
                                           class='btn btn-primary'>Make Payment</a>
                                    </td>
                                </tr>
                                <?php
                            } else {
                                ?>
                                <tr>
                                    <td colspan="3" align="right">
                                        Awaiting Next Level Authorization
                                    </td>
                                    <td colspan="2" align="right">

                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                        </form>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php } else {
        echo "Invalid Schedule";
    } ?>
</div>
