<?php $dis_sys = $this->system_authorization; ?>

<div class="row">
    <div class="col-md-12">
        <?php
        $msg = !empty($msg) ? $msg : '';
        echo $msg;
        ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo $title ?></h3>
            </div>

            <div class="panel-body">
                <?php
                if (empty($paymentFileGroups)) {
                    echo "No Payment Schedule found.";
                } else {
                    ?>
                    <div class="table-responsive" id="customers2">
                        <table class="table table-bordered table-striped" id="payments-list">
                            <thead>
                            <tr>
                                <th>S/No</th>
                                <th>Narration</th>
                                <td>Payment Type</td>
                                <th>Status</th>
                                <th style="text-align: right">Total Amount (&#8358;)</th>
                                <th>Date Initiated</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $count = 0;
                            foreach ($paymentFileGroups as $group) {
                                $total_amount = 0;
                                $aLink = "";
                                $encryptedPaymentFileGroupId = $this->basic_functions->encryptGetData($group['id']);

                                if ($group['status'] == PAYMENT_PENDING) {
                                    $dStatus = '<span class="label label-default label-form">PENDING</span>';
                                    if ($dis_sys->hasAccess('payment-schedule-forward')) {
                                        $aLink = anchor('payments/schedule/forward/' . $encryptedPaymentFileGroupId, '<button class="btn btn-xs btn-success"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Forward">Forward Payment</button>');
                                    }
                                } elseif ($group['status'] == PAYMENT_FORWARDED) {
                                    $dStatus = '<span  class="label label-warning">FORWARDED</span>';
                                    if ($dis_sys->hasAccess('payment-schedule-approve')) {
                                        $aLink = anchor('payments/schedule/approve/' . $encryptedPaymentFileGroupId, '<button class="btn btn-xs btn-success"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Approve">Approve Payment</button>');
                                        $aLink .= " " . anchor('payments/schedule/reject/' . $encryptedPaymentFileGroupId, '<button class="btn btn-xs btn-danger"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Reject">Reject Payment</button>');
                                    }
                                } elseif ($group['status'] == PAYMENT_APPROVED) {
                                    $dStatus = '<span  class="label label-info">APPROVED</span>';
                                    if ($dis_sys->hasAccess('schedule-do-auth')) {
                                        $auth_method = $group['payment_files'][0]['paymentFileType'] == 3 ? 'doauth' : 'doauth2';
                                        $aLink = anchor('payments/schedule/' . $auth_method . '/' . $encryptedPaymentFileGroupId, '<button class="btn btn-xs btn-success"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Authorize">Authorize Payment</button>');
                                        $aLink .= " " . anchor('payments/schedule/reject/' . $encryptedPaymentFileGroupId, '<button class="btn btn-xs btn-danger"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Reject">Reject Payment</button>');
                                    }
                                } elseif ($group['status'] == PAYMENT_REJECTED) {
                                    $dStatus = '<span class="label label-danger label-form">REJECTED</span>';
                                } elseif ($group['status'] == PAYMENT_AUTHORIZED) {
                                    $dStatus = '<span class="label label-info label-form">AUTHORIZED</span>';

                                    //show the Make Payment link only if a transaction batch has not been generated for any of the files in the group
                                    if (empty($group['payment_files'][0]['transaction_batch'])) {
                                        if ($dis_sys->hasAccess('make-payment')) {
                                            $aLink = anchor('payments/schedule/makePayment/' . $encryptedPaymentFileGroupId, '<button class="btn btn-xs btn-success"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Pay">Make Payment</button>');
                                        }
                                    } else {
                                        $aLink = anchor('payments/schedule/report_details/' . $encryptedPaymentFileGroupId, '<button class="btn btn-xs btn-success"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Report">Show Report</button>');
                                        $dStatus = '<span class="label label-info label-form">PROCESSED</span>';
                                    }
                                }

                                $dLink = anchor('payments/schedule/paymentDetail/' . $encryptedPaymentFileGroupId, '<button class="btn btn-xs btn-info"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Details">Details</button>');

                                $payment_type = "";

                                foreach ($group['payment_files'] as $file) {
                                    if (empty($payment_type)) {
                                        $payment_type = $file['paymentFileType'] == "2" ? "Vendor Payment" : ($file['paymentFileType'] == "3" ? "Fund Sweeping" : "Salary Payment");
                                    }

                                    foreach ($file['beneficiaries'] as $beneficiary) {
                                        $total_amount += $beneficiary['amount'];
                                    }
                                }
                                ?>
                                <tr>
                                    <td class="text-center"><?php echo ++$count ?></td>
                                    <td><strong><?php echo $group['narration'] ?></strong></td>
                                    <td><?php echo $payment_type ?></td>
                                    <td><?php echo $dStatus ?></td>
                                    <td style="text-align: right">
                                        <strong><?php echo number_format($total_amount, 2) ?></strong>
                                    </td>
                                    <td><?php echo $this->basic_functions->formatDate($group['created_at']) ?></td>
                                    <td><?php echo $dLink . " " . $aLink ?></td>
                                </tr>
                                <?php
                            } ?>
                            </tbody>
                        </table>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>