<?php $msg = !empty($msg) ? $msg : ""; ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $title ?></h3>
    </div>
    <div class="panel-body">
        <?php echo $msg ?>
        <div class="col-md-12 center-block">
            <table class="table table-bordered table-striped" id="bulk-payment-files">
                <thead>
                <tr>
                    <th>#</th>
                    <th>File Name</th>
                    <th>Date Uploaded</th>
                    <th>Status</th>
                    <th>Description</th>
                    <th>Summary</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 0;
                if (!empty($bulk_payment_files)) {
                    foreach ($bulk_payment_files AS $file) {
                        $pLink = $rLink = $sLink = "";

                        if ($file['avs_status'] == '2') {
                            $status = '<span class="label label-danger">FAILED</span>';
                        } elseif ($file['avs_status'] == '1') {
                            $status = '<span class="label label-default">PENDING</span>';
                            $pLink = anchor('payments/schedule_2/resendAccountValidationSchedule/' . $file['avs_schedule_id'], '<button class="btn btn-xs btn-success">Resend file</button>');
                        } else {
                            if (in_array($file['avs_response_code'], array("00", "12"))) {
                                $status = '<span class="label label-info">VALIDATION COMPLETE</span>';
                                if ($file['total_validated'] > 0) {
                                    $pLink = anchor('payments/schedule/processBulkPaymentFile/' . $file['avs_schedule_id'], '<button class="btn btn-xs btn-success">Process file</button>');
                                }
                                $sLink = anchor('payments/schedule_2/swapNames/' . $file['avs_schedule_id'], '<button class="btn btn-xs btn-success">Swap Names in File</button>');
                            } else {
                                $status = '<span class="label label-warning">VALIDATION IN PROGRESS</span>';
                            }
                        }

                        $status_label = in_array($file['avs_response_code'], array('16', '06', '00', '12')) ? 'success' : 'danger';

                        $aLink = anchor('payments/schedule_2/listBulkPaymentAccounts/' . $file['avs_schedule_id'], '<button class="btn btn-xs btn-info">List Accounts</button>');

                        if (in_array($file['avs_response_code'], array("16", "06"))) {
                            $rLink = anchor('payments/schedule_2/getAccountValidationStatus/' . $file['avs_schedule_id'], '<button class="btn btn-xs btn-success">Get Status</button>');
                        }
                        ?>
                        <tr>
                            <td><?php echo ++$i ?></td>
                            <td><?php echo $file['avs_file_description'] ?></td>
                            <td><?php echo $file['avs_created_at'] ?></td>
                            <td><?php echo $status ?></td>
                            <td><span
                                    class="label label-<?php echo $status_label ?> label-form"><?php echo $file['avs_response_description'] ?>
                            <td><?php echo $file['total_validated'] . " valid account(s)"  ?></td>
                            <td><?php echo $aLink . " " . $sLink . " " . $pLink . " " . $rLink ?></td>
                        </tr>
                    <?php }
                } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>