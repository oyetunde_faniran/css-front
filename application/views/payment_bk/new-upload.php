<?php $msg = !empty($msg) ? $msg : ""; ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $title ?></h3>
    </div>
    <div class="panel-body">
        <?php echo $msg ?>
        <div class="col-md-6 center-block">
            <form action="" method="POST" enctype="multipart/form-data">
                <table class=" table table-bordered">
                    <tr>
                        <td>Select File To Upload:</td>
                        <td>
                            <input type="file" name="userfile"/>
                            <?php echo anchor('files/Sample-Bulk-Payment-File.xlsx', "Download Sample File") ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Description:</td>
                        <td><input type="text" name="description" class="form-control" value="<?php echo set_value('description') ?>"></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                            <input type="submit" name="submit" value="Upload" class="btn btn-success"/>
                        </td>
                    </tr>
                </table>
                <br/>
            </form>
        </div>
    </div>
</div>