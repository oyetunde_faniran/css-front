<?php $msg = !empty($msg) ? $msg : ""; ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $title ?></h3>
    </div>
    <div class="panel-body">
        <?php echo $msg ?>
        <div class="col-md-12 center-block">
            <form action="<?php echo site_url('payments/schedule/forceSwap') ?>" method="post">
                <?php echo !empty($page_links) ? $page_links : "" ?>
                <input type="hidden" name="bulk_payment_file_id" value="<?php echo $file_id ?>">
                <button type="submit" name="submit" value="force_swap" class="btn btn-warning">FORCE SWAP ACCOUNT NAMES</button>
                &nbsp;&nbsp;&nbsp;
                <button type="submit" name="submit" value="force_allow" class="btn btn-danger">FORCE ALLOW ACCOUNT</button>
                &nbsp;&nbsp;&nbsp;
                <button type="submit" name="submit" value="force_allow_all" class="btn btn-danger">FORCE ALLOW ALL</button>
                
                <table class="table table-bordered table-striped table-condensed" id="bulkupload-accounts">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Original Account Name</th>
                        <th>Valid Account Name</th>
                        <th>Account Number</th>
                        <th>Bank Code</th>
                        <th>Status</th>
                        <th>Description</th>
                        <th><input type="checkbox" id="check_all" onclick="check_All()"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 0;
                    if (!empty($bulk_payment_accounts)) {
                        foreach ($bulk_payment_accounts AS $account) {
                            $response_description = $error_reason = '';

                            if ($account['bpd_av_status'] == '2') {
                                $status = '<span class="label label-danger">VALIDATION FAILED</span>';
                                $response_description = '<span class="label label-danger">' . $account['bpd_av_response_description'] . '</span>';

                                if (!empty($account['bpd_av_error_reason']) && $account['bpd_av_error_reason'] != $account['bpd_av_response_description']) {
                                    $error_reason = '<br><span class="label label-info">' . $account['bpd_av_error_reason'] . '</span>';
                                }
                            } elseif ($account['bpd_av_status'] == '1') {
                                $status = '<span class="label label-warning">VALIDATION PENDING</span>';
                            } elseif ($account['bpd_av_status'] == '4') {
                                $status = '<span class="label label-info">ACCOUNT NAME SWAPPED</span>';
                            } elseif ($account['bpd_av_status'] == '5') {
                                $status = '<span class="label label-info">ACCOUNT FORCE INCLUDED</span>';
                            } else {
                                $status = '<span class="label label-info">VALID</span>';
                            }

                            //display a check box to allow force swapping of account names
                            //this applies only to those with name mismatch that have not already been swapped automatically
                            //$swap_box = ($account['bpd_av_response_code'] == '08' && $account['bpd_av_status'] == '2') ? '<input type="checkbox" name="accounts_to_swap[]" value="' . $account['bpd_id'] . '">' : '';
                            //this applies only to those that have not already been swapped
                            $swap_box = $account['bpd_av_status'] == '2' || $account['bpd_av_status'] == '1' ? '<input type="checkbox" name="accounts_to_swap[]" value="' . $account['bpd_id'] . '" class="check">' : '';
                            ?>
                            <tr>
                                <td><?php echo ++$i ?></td>
                                <td><?php echo $account['bpd_account_name'] ?></td>
                                <td><?php echo $account['bpd_correct_account_name'] ?></td>
                                <td><?php echo $account['bpd_account_no'] ?></td>
                                <td><?php echo $account['bpd_bank_code'] ?></td>
                                <td><?php echo $status ?></td>
                                <td><?php echo $response_description . $error_reason ?></td>
                                <td><?php echo $swap_box ?></td>
                            </tr>
                        <?php }
                    } ?>
                    </tbody>
                </table>

                <?php echo !empty($page_links) ? $page_links : "" ?>
                <input type="hidden" name="bulk_payment_file_id" value="<?php echo $file_id ?>">
                <button type="submit" name="submit" value="force_swap" class="btn btn-warning">FORCE SWAP ACCOUNT NAMES</button>
                &nbsp;&nbsp;&nbsp;
                <button type="submit" name="submit" value="force_allow" class="btn btn-danger">FORCE ALLOW ACCOUNT</button>
                &nbsp;&nbsp;&nbsp;
                <button type="submit" name="submit" value="force_allow_all" class="btn btn-danger">FORCE ALLOW ALL</button>
            </form>
        </div>
    </div>
</div>

<script>
    $("#check_all").change(function() {
        alert('Checking all');
    });

    function check_All() {
        var c = $(".check");

        $.each(c, function(key, obj) {
            obj.checked = true;
        })

        if (document.getElementById("check_all").checked) {
            $.each(c, function(key, obj) {
                obj.checked = true;
            })
        } else {
            $.each(c, function(key, obj) {
                obj.checked = false;
            })
        }
    }
</script>