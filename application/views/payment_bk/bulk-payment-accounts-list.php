<?php $msg = !empty($msg) ? $msg : ""; ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $title ?></h3>
    </div>
    <div class="panel-body">
        <?php echo $msg ?>
        <div class="col-md-12 center-block">
            <table class="table table-bordered table-striped" id="bulkupload-accounts">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Account Name</th>
                    <th>Account Number</th>
                    <th>Bank Code</th>
                    <th>Date Uploaded</th>
                    <th>Status</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 0;
                if (!empty($bulk_payment_accounts)) {
                    foreach ($bulk_payment_accounts AS $account) {
                        $response_description = $error_reason = '';

                        if ($account['ava_status'] == '2') {
                            $status = '<span class="label label-danger">FAILED</span>';
                            $response_description = '<span class="label label-danger">' . $account['ava_response_description'] . '</span>';

                            if (!empty($account['ava_error_reason']) && $account['ava_error_reason'] != $account['ava_response_description']) {
                                $error_reason = '<br><span class="label label-info">' . $account['ava_error_reason'] . '</span>';
                            }
                        } elseif ($account['ava_status'] == '1') {
                            $status = '<span class="label label-warning">IN PROGRESS</span>';
                        } else {
                            $status = '<span class="label label-info">VALIDATED</span>';
                        }
                        ?>
                        <tr>
                            <td><?php echo ++$i ?></td>
                            <td><?php echo $account['ava_account_name'] ?></td>
                            <td><?php echo $account['ava_account_no'] ?></td>
                            <td><?php echo $account['ava_bank_code'] ?></td>
                            <td><?php echo $account['ava_created_at'] ?></td>
                            <td><?php echo $status ?></td>
                            <td><?php echo $response_description . $error_reason ?></td>
                        </tr>
                    <?php }
                } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>