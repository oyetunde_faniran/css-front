<?php $msg = !empty($msg) ? $msg : ""; ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $title ?></h3>
    </div>
    <?php
    if ($paymentFileGroup) {
        $encryptedPaymentFileGroupId = $this->basic_functions->encryptGetData($paymentFileGroup['id']);
        ?>
        <div class="panel-body">
            <?php echo $msg ?>
            <div class="col-md-8 center-block">
                <div class="table-responsive">
                    <form action="" method="POST" autocomplete="off">
                        <table class="table table-bordered table-striped table-actions">
                            <tbody>
                            <tr>
                                <td><strong>Payment File Description</strong></td>
                                <td><?php echo $paymentFileGroup['narration'] ?></td>
                            </tr>
                            <tr>
                                <td><strong>Reason</strong></td>
                                <td><input type="text" name="reject_reason" size="50"
                                           style="padding: 7px"
                                           placeholder="Please enter the reason for rejecting this payment"></td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right">
                                    <input type="submit" name="reject" class='btn btn-primary' value="Reject">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    <?php } else {
        echo "<p style='padding: 20px'>Unknown Payment</p>";
    } ?>
</div>