<div class="row">
    <div class="col-md-12">
        <?php
        $msg =  !empty($msg) ? $msg : '';
        echo $msg;

        ?>
        <div class="panel panel-default">

            <div class="panel-heading">
                <h3 class="panel-title"><?php echo $title ?></h3>


            </div>

            <div class="panel-body">

                <?php if(empty($accounts)){ echo "No Account";} else{

                    ?>
                    <div class="table-responsive" id="customers2">

                        <table class="table table-bordered table-striped ">
                            <thead>
                            <tr>
                                <th>S/No</th>
                                <th>Account Number</th>
                                <th>Bank</th>
                                <th>Check Balance</th>
                                <th>Change Pin</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $count = 1;
                            foreach ($accounts as $u){
                                $bankName = $u['bankName'];
                                $accountNo = $u['accountNo'];
                                $merchantAccountId = $u['merchantAccountId'];
                                $encMerchantAccountId = $this->basic_functions->encryptGetData($merchantAccountId);

                                ?>
                                <tr id="trow_<?php echo $count ?>">
                                    <td class="text-center"><?php echo $count ?></td>
                                    <td><?php echo $accountNo ?></td>
                                    <td><?php echo $bankName  ?></td>
                                    <td><a class="btn btn-success" href=<?php echo base_url("payments/schedule/checkBalance/$encMerchantAccountId")  ?> > Check Balance </a></td>
                                    <td><a class="btn btn-success" href=<?php echo base_url("payments/schedule/changePin/$encMerchantAccountId")  ?> > Change Pin </a></td>


                                </tr>
                                <?php $count++; } ?>
                            </tbody>

                        </table>
                    </div>
                <?php } ?>


            </div>
            <div class="panel-footer"></div>
        </div></div></div>