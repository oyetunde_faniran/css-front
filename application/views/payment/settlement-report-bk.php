<style>
    td.num, th.num {
        text-align: right;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-body">
                <table class="table table-condensed table-hover table-bordered table-striped" id="report-table">
                    <thead>
                    <tr>
                        <td>S/NO</td>
                        <td>FILE DESCRIPTION</td>
                        <td>PAYMENT TYPE</td>
                        <td class="num">NO. OF DEBIT ACCOUNTS</td>
                        <td class="num">NO. OF CREDIT ACCOUNTS</td>
                        <td class="num">NO. OF SUCCESSFUL TRANSACTIONS</td>
                        <td class="num">TOTAL AMOUNT(&#8358;)</td>
                        <td class="num">TOTAL AMOUNT SETTLED(&#8358;)</td>
                        <td>DATE PROCESSED</td>
                        <td>PROCESSED BY</td>
                        <td>STATUS</td>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <td>S/NO</td>
                        <td>FILE DESCRIPTION</td>
                        <td>PAYMENT TYPE</td>
                        <td class="num">NO. OF DEBIT ACCOUNTS</td>
                        <td class="num">NO. OF CREDIT ACCOUNTS</td>
                        <td class="num">NO. OF SUCCESSFUL TRANSACTIONS</td>
                        <td class="num">TOTAL AMOUNT(&#8358;)</td>
                        <td class="num">TOTAL AMOUNT SETTLED(&#8358;)</td>
                        <td>DATE PROCESSED</td>
                        <td>PROCESSED BY</td>
                        <td>STATUS</td>
                    </tr>
                    </tfoot>
                    <tbody>
                    <?php
                    $display = '';
                    $sno = 0;
                    foreach ($payment_file_groups as $group) {
                        $num_successful_transactions = $total_amount = $total_amount_successful_transactions = 0;
                        $details = '<br>' . anchor('payments/schedule/report_details/' . $group['id'], '<button class="btn btn-xs btn-success"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Authorize">Details</button>');

                        $payment_type = "";

                        foreach ($group['payment_files'] as $file) {
                            if (empty($payment_type)) {
                                $payment_type = $file['paymentFileType'] == "2" ? "Vendor Payment" : ($file['paymentFileType'] == "3" ? "Fund Sweeping" : "");
                            }

                            foreach ($file['beneficiaries'] as $beneficiary) {
                                $total_amount += $beneficiary['amount'];

                                if ($beneficiary['paidStatus'] == '00') {
                                    $num_successful_transactions++;
                                    $total_amount_successful_transactions += $beneficiary['amount'];
                                }
                            }
                        }

                        if ($file['transaction_batch']['status'] == '0') {
                            $status = $total_amount_successful_transactions == $total_amount ? '<span class="label label-info">Fully Settled</span>' : ($total_amount_successful_transactions > 0 && $total_amount_successful_transactions < $total_amount ? '<span class="label label-warning">Partially Settled</span>' : '<span class="label label-default">Pending</span>');
                        } elseif ($file['transaction_batch']['status'] == '2') {
                            $status ='<span class="label label-danger">Failed</span>';
                        } else {
                            $status = '<span class="label label-default">Pending</span>';
                        }

                        $display .= '<tr>
                                        <td>' . (++$sno) . '.</td>
                                        <td>' . $group['narration'] . '</td>
                                        <td>' . $payment_type . '</td>
                                        <td class="num">' . $group['num_debit_accounts'] . '</td>
                                        <td class="num">' . $group['num_credit_accounts'] . '</td>
                                        <td class="num">' . $num_successful_transactions . '</td>
                                        <td class="num">' . number_format($total_amount, 2) . '</td>
                                        <td class="num">' . number_format($total_amount_successful_transactions, 2) . '</td>
                                        <td>' . $group['created_at'] . '</td>
                                        <td>' . $group['initiator'] . '</td>
                                        <td>' . $status . $details . '</td>
                                    </tr>';
                    }
                    echo $display;
                    ?>
                    </tbody>
                </table>
                <?php
                echo anchor('reports', '&laquo; Back to Reports Home', 'class="btn btn-danger"');
                ?>
            </div>
        </div>
    </div>
</div>