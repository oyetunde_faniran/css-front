<?php
$msg = !empty($msg) ? $msg : "";

?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $title ?></h3>
    </div>
    <?php
    if(!empty($account)){
    $merchantAccountId =  $account['merchantAccountId'];
    $merchantId = $account['merchantId'];
    $encryptedMerchantId = $this->basic_functions->encryptGetData($merchantId);
    $encryptedMerchantAccountId = $this->basic_functions->encryptGetData($merchantAccountId);

    ?>
    <div class="panel-body">
        <?php echo $msg ?>
        <div class="col-md-6 center-block">
            <div class="table-responsive">
                 <table class="table table-bordered table-striped table-actions">

                        <tbody>
                        <tr >
                            <td width="30%"><strong>Account No</strong></td>
                            <td><?php echo  $account['accountNo'] ?></td>
                        </tr>
                        <tr >
                            <td><strong>Bank</strong></td>
                            <td><?php echo stripslashes($account['bankName']) ?></td>
                        </tr>
                        <tr>
                            <td><strong>CardPan</strong></td>
                            <td><?php echo $maskedCardPan ?></td>
                        </tr>
                        <tr>
                            <td><strong>Balance (in Naira)</strong></td>
                            <td><?php echo  $balance ?></td>
                        </tr>
                        </tbody>
                    </table>

            </div>
        </div>


    </div>

</div>
<?php }else{ echo "Invalid Account";} ?>
