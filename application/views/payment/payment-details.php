<?php $dis_sys = $this->system_authorization; ?>

<div class="row">
    <div class="col-md-12">
        <?php
        $msg = !empty($msg) ? $msg : '';
        echo $msg;
        ?>
        <div class="panel panel-default">

            <div class="panel-heading">
                <h3 class="panel-title"><?php echo $title ?></h3>
            </div>

            <div class="panel-body">

                <?php
                if (empty($payment_file_group)) {
                    echo "No Payment File Group!";
                } else {
                    $back = '<a href="' . site_url("payments/schedule/allPayments") . '" class="btn btn-warning">Back to Payments List</a>';
                    $encryptedPaymentFileGroupId = $this->basic_functions->encryptGetData($payment_file_group['id']);
                    $aLink = "";

                    if ($payment_file_group['status_description'] == 'AUTHORIZED') {
                        $file_group_status = "<span class='label label-success'>AUTHORIZED</span>";

                        if (empty($payment_file_group['psg_id'])) {
                            if ($dis_sys->hasAccess('make-payment')) {
                                $aLink = anchor('payments/schedule/makePayment/' . $encryptedPaymentFileGroupId, '<button class="btn btn-info">Make Payment</button>');
                            }
                        } else {
                            $aLink = anchor('payments/schedule/settlementReportDetails/' . $payment_file_group['psg_id'], '<button class="btn btn-info">Show Report</button>');
                            $file_group_status = '<span class="label label-success">PROCESSED</span>';
                        }
                    } elseif ($payment_file_group['status_description'] == 'REJECTED') {
                        $file_group_status = "<span class='label label-danger'>REJECTED</span>";
                    } elseif ($payment_file_group['status_description'] == 'APPROVED') {
                        $file_group_status = "<span class='label label-success'>APPROVED</span>";

                        if ($dis_sys->hasAccess('schedule-do-auth')) {
                            $auth_method = $payment_file_group['payment_type'] == 'FUND SWEEPING' ? 'doauth' : 'doauth2';
                            $aLink = anchor('payments/schedule/' . $auth_method . '/' . $encryptedPaymentFileGroupId, '<button class="btn btn-info">Authorize Payment</button>');
                            $aLink .= " " . anchor('payments/schedule/reject/' . $encryptedPaymentFileGroupId, '<button class="btn btn-danger">Reject Payment</button>');
                        }
                    } elseif ($payment_file_group['status_description'] == 'FORWARDED') {
                        $file_group_status = "<span class='label label-warning'>FORWARDED</span>";

                        if ($dis_sys->hasAccess('payment-schedule-approve')) {
                            $aLink = anchor('payments/schedule/approve/' . $encryptedPaymentFileGroupId, '<button class="btn btn-info">Approve Payment</button>');
                            $aLink .= " " . anchor('payments/schedule/reject/' . $encryptedPaymentFileGroupId, '<button class="btn btn-danger">Reject Payment</button>');
                        }
                    } else {
                        $file_group_status = "<span class='label label-default'>PENDING</span>";

                        if ($dis_sys->hasAccess('payment-schedule-forward')) {
                            $aLink = anchor('payments/schedule/forward/' . $encryptedPaymentFileGroupId, '<button class="btn btn-info">Forward Payment</button>');
                        }
                    }
                    ?>

                    <table class="table table-bordered">
                        <tr>
                            <th>NARRATION</th>
                            <td><?php echo $payment_file_group['narration'] ?></td>
                            <th>PAYMENT TYPE</th>
                            <td><?php echo $payment_file_group['payment_type'] ?></td>
                            <th>STATUS</th>
                            <td><?php echo $file_group_status ?></td>
                        </tr>
                        <tr>
                            <th>TOTAL BENEFICIARIES</th>
                            <td><?php echo number_format($payment_file_group['num_credit_accounts']) ?></td>
                            <th>TOTAL AMOUNT</th>
                            <td>&#8358; <?php echo number_format($payment_file_group['total_amount'], 2); ?></td>
                            <th></th>
                            <td></td>
                        </tr>
                        <tr>
                            <th>INITIATED BY</th>
                            <td><?php echo $payment_file_group['initiator'] ?></td>
                            <?php
                            if (!empty($payment_file_group['history'])) {
                                $history = explode(",", $payment_file_group['history']);

                                foreach ($history as $history_details) {
                                    if (strpos($history_details, 'APPROVED:') !== false) {
                                        $approver_title = 'APPROVED BY';
                                        $approval_details = explode(":", $history_details);
                                        $approver_detail = $approval_details[1];
                                    } elseif (strpos($history_details, 'REJECTED:') !== false) {
                                        $approver_title = 'REJECTED BY';
                                        $rejection_details = explode(":", $history_details);
                                        $approver_detail = $rejection_details[1] . '<p>REASON: ' . $rejection_details[2] . '</p>';
                                    }
                                }
                            }
                            ?>
                            <th><?php echo !empty($approver_title) ? $approver_title : '' ?></th>
                            <td><?php echo !empty($approver_detail) ? $approver_detail : '' ?></td>

                            <?php
                            if (!empty($payment_file_group['history'])) {
                                foreach ($history as $history_details) {
                                    if (strpos($history_details, 'AUTHORIZED:') !== false) {
                                        $authorizer_title = 'AUTHORIZED BY';
                                        $authorization_details = explode(":", $history_details);
                                        $authorizer_detail = $authorization_details[1];
                                    } elseif (strpos($history_details, 'REJECTED:') !== false) {
                                        $authorizer_title = 'REJECTED BY';
                                        $rejection_details = explode(":", $history_details);
                                        $authorizer_detail = $rejection_details[1] . '<p>REASON: ' . $rejection_details[2] . '</p>';
                                    }
                                }
                            }
                            ?>
                            <th><?php echo !empty($authorizer_title) ? $authorizer_title : '' ?></th>
                            <td><?php echo !empty($authorizer_detail) ? $authorizer_detail : '' ?></td>
                        </tr>
                        <tr>
                            <td colspan="3"><?php echo $back ?></td>
                            <td colspan="3"><?php echo $aLink ?></td>
                        </tr>
                    </table>

                    <div class="table-responsive" id="customers2">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>SOURCE ACCOUNTS</th>
                                <th>BENEFICIARIES</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($payment_files as $file) { ?>
                                <tr>
                                    <td style="vertical-align: top;">
                                        <table class="table table-bordered table-condensed">
                                            <tr>
                                                <th>Account Name</th>
                                                <th>Account Number</th>
                                                <th>Bank</th>
                                            </tr>
                                            <tr>
                                                <td><?php echo $file['source_account_name'] ?></td>
                                                <td><?php echo $file['source_account_no'] ?></td>
                                                <td><?php echo $file['source_account_bank_name'] ?></td>
                                            </tr>
                                        </table>
                                    <td>
                                        <table class="table table-bordered table-condensed">
                                            <tr>
                                                <th style="text-align: right">#</th>
                                                <th>Account Name</th>
                                                <th>Account Number</th>
                                                <th>Bank Name</th>
                                                <th style="text-align: right">Amount (&#8358;)</th>
                                            </tr>

                                            <?php
                                            $i = 0;
                                            foreach ($payment_file_beneficiaries as $beneficiary) {
                                                if ($beneficiary['payment_file'] == $file['payment_file_id'])
                                                    echo "<tr><td style='text-align: right'>" . ++$i . "</td><td>" . stripslashes($beneficiary['beneficiary_account_name']) . "</td><td>" . stripslashes($beneficiary['beneficiary_account_no']) . "</td><td>" . stripslashes($beneficiary['beneficiary_bank_name']) . "</td><td style='text-align: right'>" . number_format($beneficiary['beneficiary_amount'], 2) . "</td></tr>";
                                            }
                                            ?>
                                        </table>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                <?php } ?>
            </div>
            <div class="panel-footer">
                <div class="pull-left"><?php echo $back ?></div>
                <div class="pull-right"><?php echo $aLink ?></div>
                <br class="clearfix">
            </div>
        </div>
    </div>
</div>