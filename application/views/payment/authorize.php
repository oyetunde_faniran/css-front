<?php $dis_sys = $this->system_authorization; ?>

<div class="row">
    <div class="col-md-12">
        <?php
        $msg = !empty($msg) ? $msg : '';
        echo $msg;
        ?>
        <div class="panel panel-default">

            <div class="panel-heading">
                <h3 class="panel-title"><?php echo $title ?></h3>
            </div>

            <div class="panel-body">
                <?php
                if (empty($paymentFileGroups)) {
                    echo "No Payment Schedule";
                } else {
                    ?>
                    <div class="table-responsive" id="customers2">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>S/No</th>
                                <th>File Name</th>
                                <th>Narration</th>
                                <th>Status</th>
                                <th style="text-align: right">Total Amount (&#8358;)</th>
                                <th>Uploaded By</th>
                                <th>Uploaded Date</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $count = 0;
                            $back = anchor('payments/schedule/forwardList/', '<button class="btn btn-warning"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Back">Back to Payments List</button>');
                            foreach ($paymentFileGroups as $group) {
                                $total_amount = 0;
                                $aLink = $aLink2 = "";
                                $encryptedPaymentFileGroupId = $this->basic_functions->encryptGetData($group['id']);

                                if ($group['status'] == PAYMENT_PENDING) {
                                    $dStatus = '<span class="label label-default label-form">PENDING</span>';
                                } elseif ($group['status'] == PAYMENT_FORWARDED) {
                                    $dStatus = '<span  class="label label-warning">FORWARDED</span>';
                                    if ($dis_sys->hasAccess('payment-schedule-approve')) {
                                        $aLink = anchor('payments/schedule/approve/' . $encryptedPaymentFileGroupId, '<button class="btn btn-success"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Approve">Approve Payment</button>');
                                    }
                                } elseif ($group['status'] == PAYMENT_APPROVED) {
                                    $dStatus = '<span  class="label label-success">APPROVED</span>';
                                    if ($dis_sys->hasAccess('schedule-do-auth')) {
                                        $auth_method = $group['payment_files'][0]['paymentFileType'] == 3 ? 'doauth' : 'doauth2';
                                        $aLink = anchor('payments/schedule/' . $auth_method . '/' . $encryptedPaymentFileGroupId, '<button class="btn btn-success"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Authorize">Authorize Payment</button>');
                                        $aLink2 = anchor('payments/schedule/reject/' . $encryptedPaymentFileGroupId, '<button class="btn btn-danger"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Reject">Reject Payment</button>');
                                    }
                                } else if ($group['status'] == PAYMENT_REJECTED) {
                                    $dStatus = '<span class="label label-danger label-form">REJECTED</span>';
                                    $dStatus .= "<br>" . $group['reject_reason'];
                                } else if ($group['status'] == PAYMENT_AUTHORIZED) {
                                    $dStatus = '<span class="label label-success label-form">AUTHORIZED</span>';
                                }

                                $dLink = anchor('payments/schedule/authorizeDetails/' . $encryptedPaymentFileGroupId, '<button class="btn btn-success"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Details">Details</button>');

                                foreach($group['payment_files'] as $file) {
                                    foreach ($file['beneficiaries'] as $beneficiary) {
                                        $total_amount += $beneficiary['amount'];
                                    }
                                }
                                ?>
                                <tr>
                                    <td class="text-center"><?php echo ++$count ?></td>
                                    <td><strong><?php echo $group['name'] ?></strong></td>
                                    <td><strong><?php echo $group['narration'] ?></strong></td>
                                    <td><?php echo $dStatus ?></td>
                                    <td style="text-align: right"><strong><?php echo number_format($total_amount, 2) ?></strong></td>
                                    <td><?php echo stripslashes($group['initiator']) ?></td>
                                    <td><?php echo $this->basic_functions->formatDate($group['created_at']) ?></td>
                                    <td><?php echo $dLink . " " . $aLink . " " . $aLink2 ?></td>
                                </tr>
                                <?php
                            } ?>
                            </tbody>
                        </table>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>