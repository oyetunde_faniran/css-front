<?php
$msg = !empty($msg) ? $msg : "";

//Form the account dropdown options
$acc_dd[0] = '--Select Account--';
$bank_name = '';
$inner_array = array();
foreach ($accs as $acc) {
    //If the bank name is changing and it's not the first one, add it to the final array
    if ($bank_name != '' && $bank_name != $acc['bank_name']) {
        $acc_dd[$bank_name] = $inner_array;
        $inner_array = array();
    }

    $inner_array[$acc['acc_id']] = $acc['acc_name'] . ' - ' . $acc['acc_number'] . ' (&#8358; ' . number_format($acc['accbal_amount'], 2) . ')';

    //Update the name of the current bank
    $bank_name = $acc['bank_name'];
}
if (!empty($inner_array)) {
    $acc_dd[$bank_name] = $inner_array;
}

?>
<?php echo $msg ?>
    <div class="panel panel-default">

    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $title ?></h3>
    </div>
<?php
if (!empty($payment_file_group)) {
    ?>

    <div class="panel-body">
        <div class="col-md-6 center-block">
            <div class="table-responsive">
                <form action="" method="POST">
                    <table class="table table-bordered table-striped table-actions">
                        <tbody>
                        <tr>
                            <td><strong>Payment Description</strong></td>
                            <td><?php echo stripslashes($payment_file_group['narration']) ?></td>
                        </tr>
                        <tr>
                            <td><strong>Number Of Beneficiaries</strong></td>
                            <td><?php echo $payment_file_group['num_credit_accounts'] ?></td>
                        </tr>
                        <tr>
                            <td><strong>Transaction Amount</strong></td>
                            <td>&#8358;
                                <?php
                                $total_amount = 0;
                                $charges = 0;
                                foreach ($payment_file_group['payment_files'] as $file) {
                                    foreach ($file['beneficiaries'] as $beneficiary) {
                                        $total_amount += $beneficiary['amount'];
                                        $charges += FUNDS_TRANSFER_TRANSACTION_CHARGE;
                                    }
                                }
                                echo number_format($total_amount, 2);
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td><strong>Transaction Charges</strong></td>
                            <td> &#8358; <?php echo number_format($charges, 2); ?></td>
                        </tr>
                        <tr>
                            <td><strong>Total</strong></td>
                            <td style="font-size: 16px;">
                                </strong>&#8358; <?php echo number_format(($total_amount + $charges), 2); ?></strong>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right">
                                <input type="hidden" value="<?php echo $payment_file_group['id'] ?>" name="file_group_id">
                                <input type="submit" name="process" class='btn btn-primary' value="Make Payment">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>

    </div>
<?php } else {
    echo "Empty Transaction!!";
} ?>