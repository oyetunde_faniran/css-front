<?php $msg = !empty($msg) ? $msg : ""; ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $title ?></h3>
    </div>
    <div class="panel-body">
        <?php echo $msg ?>
        <div class="col-md-12 center-block">
            <form action="<?php echo site_url('payments/bulk_payment/forceSwap') ?>" method="post">
                <?php echo !empty($page_links) ? $page_links : "" ?>

                <input type="hidden" name="bulk_payment_file_id" value="<?php echo $file_id ?>">

                <button type="submit" name="submit" value="force_swap" class="btn btn-warning">FORCE SWAP ACCOUNT NAMES</button>
                &nbsp;&nbsp;&nbsp;
                <button type="submit" name="submit" value="force_allow" class="btn btn-danger">FORCE ALLOW ACCOUNT</button>
                &nbsp;&nbsp;&nbsp;
                <button type="submit" name="submit" value="force_allow_all" class="btn btn-danger">FORCE ALLOW ALL</button>

                <table class="table table-bordered table-striped table-condensed" id="bulkupload-accounts">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Original Account Name</th>
                        <th>Correct Account Name</th>
                        <th>Account Number</th>
                        <th>Bank Code</th>
                        <th>Account Validation Status</th>
                        <th>Description</th>
                        <th><input type="checkbox" id="check_all" onclick="check_All()"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = $offset;
                    if (!empty($bulk_payment_beneficiaries)) {

                        foreach ($bulk_payment_beneficiaries AS $account) {
                            //exclude all those with no Account Name, Account Number, Bank Code, or Amount
                            if (empty($account['beneficiary_account_name']) || empty($account['beneficiary_account_no']) || empty($account['beneficiary_bank_code']) || empty(intval($account['beneficiary_amount']))) {
                                continue;
                            }

                            $response_description = $error_reason = '';

                            if ($account['beneficiary_account_validation_status_description'] == 'INVALID') {
                                $account_validation_status = '<span class="label label-danger">INVALID</span>';
                                $response_description = '<span class="label label-danger">' . $account['beneficiary_account_validation_response_description'] . '</span>';

                                if (!empty($account['beneficiary_account_validation_error_reason']) && $account['beneficiary_account_validation_error_reason'] != $account['beneficiary_account_validation_response_description']) {
                                    $error_reason = '<br><span class="label label-info">' . $account['beneficiary_account_validation_error_reason'] . '</span>';
                                }
                            } elseif ($account['beneficiary_account_validation_status_description'] == 'IN PROGRESS') {
                                $account_validation_status = '<span class="label label-warning">IN PROGRESS</span>';
                            } elseif ($account['beneficiary_account_validation_status_description'] == 'PENDING') {
                                $account_validation_status = '<span class="label label-default">PENDING</span>';
                            } elseif ($account['beneficiary_account_validation_status_description'] == 'ACCOUNT NAME SWAPPED') {
                                $account_validation_status = '<span class="label label-info">ACCOUNT NAME SWAPPED</span>';
                            } elseif ($account['beneficiary_account_validation_status_description'] == 'ACCOUNT FORCED') {
                                $account_validation_status = '<span class="label label-info">ACCOUNT FORCE INCLUDED</span>';
                            } elseif ($account['beneficiary_account_validation_status_description'] == 'VALID') {
                                $account_validation_status = '<span class="label label-info">VALID</span>';
                            } else {
                                $account_validation_status = "";
                            }

                            //display a check box to allow force swapping of account names
                            //this applies only to those with name mismatch that have not already been swapped automatically
                            //$swap_box = ($account['bpd_av_response_code'] == '08' && $account['bpd_av_status'] == '2') ? '<input type="checkbox" name="accounts_to_swap[]" value="' . $account['bpd_id'] . '">' : '';
                            //this applies only to those that have not already been swapped
                            if ($account['beneficiary_account_validation_status_description'] == 'PENDING'
                                || $account['beneficiary_account_validation_status_description'] == 'INVALID') {
                                $swap_box = '<input type="checkbox" name="accounts_to_swap[]" value="' . $account['beneficiary_id'] . '" class="check">';
                            } else
                                $swap_box = '';
                            ?>
                            <tr>
                                <td><?php echo ++$i ?></td>
                                <td><?php echo $account['beneficiary_account_name'] ?></td>
                                <td><?php echo $account['beneficiary_correct_account_name'] ?></td>
                                <td><?php echo $account['beneficiary_account_no'] ?></td>
                                <td><?php echo $account['beneficiary_bank_code'] ?></td>
                                <td><?php echo $account_validation_status ?></td>
                                <td><?php echo $response_description . $error_reason ?></td>
                                <td><?php echo $swap_box ?></td>
                            </tr>
                        <?php }
                    } ?>
                    </tbody>
                </table>

                <?php echo !empty($page_links) ? $page_links : "" ?>
                <input type="hidden" name="bulk_payment_file_id" value="<?php echo $file_id ?>">
                <button type="submit" name="submit" value="force_swap" class="btn btn-warning">FORCE SWAP ACCOUNT NAMES</button>
                &nbsp;&nbsp;&nbsp;
                <button type="submit" name="submit" value="force_allow" class="btn btn-danger">FORCE ALLOW ACCOUNT</button>
                &nbsp;&nbsp;&nbsp;
                <button type="submit" name="submit" value="force_allow_all" class="btn btn-danger">FORCE ALLOW ALL</button>
            </form>
        </div>
    </div>
</div>

<script>
    $("#check_all").change(function() {
        alert('Checking all');
    });

    function check_All() {
        var c = $(".check");

        $.each(c, function(key, obj) {
            obj.checked = true;
        })

        if (document.getElementById("check_all").checked) {
            $.each(c, function(key, obj) {
                obj.checked = true;
            })
        } else {
            $.each(c, function(key, obj) {
                obj.checked = false;
            })
        }
    }
</script>