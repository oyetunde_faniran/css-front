<style>
    td.num, th.num {
        text-align: right;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-body">
                <table class="table table-condensed table-hover table-bordered table-striped" id="report-table">
                    <thead>
                    <tr>
                        <td>S/NO</td>
                        <td>FILE DESCRIPTION</td>
                        <td>PAYMENT TYPE</td>
                        <td class="num">DEBIT ACCOUNTS</td>
                        <td class="num">CREDIT ACCOUNTS</td>
                        <td class="num">SUCCESSFUL TRANSACTIONS</td>
                        <td class="num">TOTAL AMOUNT(&#8358;)</td>
                        <td class="num">TOTAL AMOUNT SETTLED(&#8358;)</td>
                        <td>DATE PROCESSED</td>
                        <td>PROCESSED BY</td>
                        <td>STATUS</td>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <td>S/NO</td>
                        <td>FILE DESCRIPTION</td>
                        <td>PAYMENT TYPE</td>
                        <td class="num">DEBIT ACCOUNTS</td>
                        <td class="num">CREDIT ACCOUNTS</td>
                        <td class="num">SUCCESSFUL TRANSACTIONS</td>
                        <td class="num">TOTAL AMOUNT(&#8358;)</td>
                        <td class="num">TOTAL AMOUNT SETTLED(&#8358;)</td>
                        <td>DATE PROCESSED</td>
                        <td>PROCESSED BY</td>
                        <td>STATUS</td>
                    </tr>
                    </tfoot>
                    <tbody>
                    <?php
                    $display = '';
                    $sno = 0;
                    if (!empty($payment_schedule_groups)) {

                        foreach ($payment_schedule_groups as $group) {
                            $details = '<br>' . anchor('payments/schedule/settlementReportDetails/' . $group['schedule_group_id'], '<button class="btn btn-xs btn-default">Details</button>');

                            $summary_details = explode(",", $group['summary']);

                            $total_amount_settled = $total_amount_in_progress = $total_amount_pending = $total_amount_failed = 0;
                            $total_beneficiaries_settled = $total_beneficiaries_in_progress = $total_beneficiaries_pending = $total_beneficiaries_failed = 0;

                            foreach ($summary_details as $summary_detail) {
                                $trans_status_details = explode(":", $summary_detail);

                                if ($trans_status_details['0'] == 'PAID') {
                                    $total_amount_settled = $trans_status_details['1'];
                                    $total_beneficiaries_settled = $trans_status_details['2'];
                                } elseif ($trans_status_details['0'] == 'PENDING') {
                                    $total_amount_pending = $trans_status_details['1'];
                                    $total_beneficiaries_pending = $trans_status_details['2'];
                                } elseif ($trans_status_details['0'] == 'IN PROGRESS') {
                                    $total_amount_in_progress = $trans_status_details['1'];
                                    $total_beneficiaries_in_progress = $trans_status_details['2'];
                                } elseif ($trans_status_details['0'] == 'FAILED') {
                                    $total_amount_failed = $trans_status_details['1'];
                                    $total_beneficiaries_failed = $trans_status_details['2'];
                                }
                            }

                            if ($total_amount_settled > 0) {
                                $status = $total_amount_settled == $group['total_amount'] ? '<span class="label label-success">FULLY SETTLED</span>' : '<span class="label label-info">PARTIALLY SETTLED</span>';
                            } elseif ($total_amount_pending > 0 || $total_amount_in_progress > 0) {
                                $status ='<span class="label label-warning">IN PROGRESS</span>';
                            } elseif ($total_amount_failed > 0) {
                                $status ='<span class="label label-danger">FAILED</span>';
                            }

                            $debit_account =  (!empty($group['debit_account'])) ? str_replace("|", " ", $group['debit_account']) : '';
                            $credit_account =  (!empty($group['credit_account'])) ? str_replace("|", " ", $group['credit_account']) : '';

                            $display .= '<tr>
                                            <td>' . (++$sno) . '.</td>
                                            <td>' . $group['narration'] . '</td>
                                            <td>' . $group['payment_type_name'] . '</td>
                                            <td class="num">' . $group['num_debit_accounts'] . '<br><b>' . $debit_account . '</b></td>
                                            <!--<td class="num">' . $group['total_beneficiaries'] . '<br><b>' . $credit_account . '</td>-->
                                            <td class="num">' . $group['num_credit_accounts'] . '<br><b>' . $credit_account . '</td>
                                            <td class="num">' . $total_beneficiaries_settled . '</td>
                                            <td class="num">' . number_format($group['total_amount'], 2) . '</td>
                                            <td class="num">' . number_format($total_amount_settled, 2) . '</td>
                                            <td>' . $group['created_at'] . '</td>
                                            <td>' . $group['initiator'] . '</td>
                                            <td>' . $status . $details . '</td>
                                        </tr>';
                        }
                    }
                    echo $display;
                    ?>
                    </tbody>
                </table>
                <?php //echo anchor('reports', '&laquo; Back to Reports Home', 'class="btn btn-danger"'); ?>
            </div>
        </div>
    </div>
</div>