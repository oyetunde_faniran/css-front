<?php $msg = !empty($msg) ? $msg : ""; ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $title ?></h3>
    </div>
    <div class="panel-body">
        <?php echo $msg ?>
        <div class="col-md-12 center-block">
            <form action="<?php echo site_url('payments/schedule/validateBulkPaymentFile') ?>" method="POST">
                <input type="hidden" value="<?php echo $fname ?>" name="fname">
                <input type="hidden" value="<?php echo $description ?>" name="description">
                <table class="table table-bordered table-striped " id="bulkkkk-payment-file">
                    <thead>
                    <tr>
                        <td colspan="8" align="right">
                            <?php if (!$proceed) { ?>
                                <a href="<?php echo base_url('payments/schedule/uploadBulkPaymentFile') ?>"
                                   class="btn btn-info">Upload New File</a>
                            <?php } else {
                                if ($done) { ?>
                                    <a href="<?php echo base_url('payments/schedule/listBulkPaymentFiles/') ?>"
                                       class="btn btn-info">View List</a>
                                <?php } else { ?>
                                    <button type="submit" class="btn btn-danger">Validate Next Batch</button>
                                <?php }
                            } ?>
                        </td>
                    </tr>
                    <tr>
                        <th>S/No</th>
                        <th>Beneficiary Account Name</th>
                        <th>Account No</th>
                        <th>Bank Code</th>
                        <th>Amount</th>
                        <th>Unique ID</th>
                        <th>Status</th>
                        <th>Error Reason</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (!empty($excel_data)) {
                        $i = 0;
                        foreach ($excel_data AS $key => $excel_value) {
                            ?>
                            <tr>
                                <td><?php echo $key ?></td>
                                <td><?php echo !empty($excel_value['account_name']) ? $excel_value['account_name'] : ""; ?></td>
                                <td><?php echo !empty($excel_value['account_no']) ? $excel_value['account_no'] : ""; ?></td>
                                <td><?php echo !empty($excel_value['bank_code']) ? $excel_value['bank_code'] : ""; ?></td>
                                <td><?php echo !empty($excel_value['amount']) ? $excel_value['amount'] : ""; ?></td>
                                <td><?php echo !empty($excel_value['unique_id']) ? $excel_value['unique_id'] : ""; ?></td>
                                <td><?php echo !empty($excel_value['valid']) ? '<span class="btn btn-sm btn-info">Valid</span>' : '<span class="btn btn-danger">Invalid</span>'; ?></td>
                                <td><?php echo !empty($excel_value['error_description']) ? $excel_value['error_description'] : ""; ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="8" align="right">
                            <?php if (!$proceed) { ?>
                                <a href="<?php echo base_url('payments/schedule/uploadBulkPaymentFile') ?>"
                                   class="btn btn-info">Upload New File</a>
                            <?php } else {
                                if ($done) { ?>
                                    <a href="<?php echo base_url('payments/schedule/listBulkPaymentFiles/') ?>"
                                       class="btn btn-info">View List</a>
                                <?php } else { ?>
                                    <button type="submit" class="btn btn-danger">Validate Next Batch</button>
                                <?php }
                            } ?>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </form>
        </div>
    </div>
</div>