<?php $msg = !empty($msg) ? $msg : ""; ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $title ?></h3>
    </div>
    <div class="panel-body">
        <?php echo $msg ?>
        <div class="col-md-12 center-block">
            <table class="table table-bordered table-striped table-condensed" id="bulk-payment-files">
                <thead>
                <tr>
                    <th>#</th>
                    <th>FILE NAME</th>
                    <th>DATE UPLOADED</th>
                    <th>CONTENT VALIDATION STATUS</th>
                    <th>ACCOUNT VALIDATION STATUS</th>
                    <th>PAYMENT STATUS</th>
                    <th>ACTIONS</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 0;
                if (!empty($bulk_payment_files)) {

                    foreach ($bulk_payment_files AS $file) {
                        $processFile = $validateAccounts = $swapAccounts = $downloadContentValidationReport = $downloadAccountValidationReport = $downloadPaymentReport = $delete_file = "";
                        $content_validation_status = $content_validation_status_progress = $account_validation_status = $payment_status = "";

                        if ($file['content_validation_status_description'] == 'PENDING') {
                            $content_validation_status = '<span class="label label-default">PENDING</span>';
                        } elseif ($file['content_validation_status_description'] == 'IN PROGRESS') {
                            $content_validation_status = '<span class="label label-warning">IN PROGRESS</span>';
                        } elseif ($file['content_validation_status_description'] == 'COMPLETED') {
                            $content_validation_status = '<span class="label label-success">COMPLETED</span>';
                            $downloadContentValidationReport = anchor('payments/bulk_payment/downloadContentValidationReport/' . $file['file_id'], '<button class="btn btn-xs btn-default">Download Content Validation Report</button>', 'target="_blank"');
                        }

                        $total_records = $file['total_records'];
                        $valid_records = $validated_records = 0;
                        $cv_summary = !empty($file['content_validation_summary']) ? explode(",", $file['content_validation_summary']) : array();
                        if (!empty($cv_summary)) {
                            foreach ($cv_summary as $details) {
                                list($cv_status, $cv_status_records) = explode(":", $details);
                                if ($cv_status == "VALID")
                                    $valid_records = $cv_status_records;
                                $validated_records += $cv_status_records;
                            }
                        }
                        $content_validation_summary = "<span class='badge badge-info'>Valid: " . number_format($valid_records) . " | Total: " . number_format($total_records) . "</span>";

                        if ($file['content_validation_status_description'] != 'COMPLETED') {
                            $cv_progress = $total_records > 0 ? $valid_records / $total_records * 100 : 0;
                            $content_validation_status_progress = '<span class="badge badge-info">' . round($cv_progress, 2) . '% - ' . number_format($validated_records) . '</span>';
                        }

                        if ($file['account_validation_status_description'] == 'FAILED') {
                            $account_validation_status = '<span class="label label-danger">FAILED</span>';
                        } elseif ($file['account_validation_status_description'] == 'IN PROGRESS') {
                            $account_validation_status = '<span class="label label-warning">IN PROGRESS</span>';
                        } elseif ($file['account_validation_status_description'] == 'PENDING') {
                            $account_validation_status = '<span class="label label-default">PENDING</span>';
                        } elseif ($file['account_validation_status_description'] == 'COMPLETED') {
                            $account_validation_status = '<span class="label label-success">COMPLETED</span>';
                        }

                        $valid_accounts = $swapped_accounts = $forced_accounts = $accounts_pending = $accounts_in_progress = $invalid_accounts = $total_accounts = $total_payable_accounts = 0;
                        $av_summary = !empty($file['account_validation_summary']) ? explode(",", $file['account_validation_summary']) : array();
                        if (!empty($av_summary)) {
                            foreach ($av_summary as $details) {
                                list($av_status, $av_status_accounts) = explode(":", $details);

                                if ($av_status == "VALID")
                                    $valid_accounts = $av_status_accounts;
                                elseif ($av_status == "ACCOUNT NAME SWAPPED")
                                    $swapped_accounts = $av_status_accounts;
                                elseif ($av_status == "ACCOUNT FORCED")
                                    $forced_accounts = $av_status_accounts;
                                elseif ($av_status == "INVALID")
                                    $invalid_accounts = $av_status_accounts;
                                elseif ($av_status == "IN PROGRESS")
                                    $accounts_in_progress = $av_status_accounts;
                                elseif ($av_status == "PENDING")
                                    $accounts_pending = $av_status_accounts;
                            }

                            $total_accounts = $valid_accounts + $swapped_accounts + $forced_accounts + $accounts_pending + $accounts_in_progress + $invalid_accounts;
                            $total_payable_accounts = $valid_accounts + $swapped_accounts + $forced_accounts;
                        }

                        $total_accounts = $valid_records;

                        $account_validation_summary = "<span class='badge badge-info'>Valid: " . number_format($total_payable_accounts) . " | Total: " . number_format($total_accounts) . "</span>";

                        if ($file['account_validation_status_description'] != 'PENDING')
                            $downloadAccountValidationReport = anchor('payments/bulk_payment/downloadAccountValidationReport/' . $file['file_id'], '<button class="btn btn-xs btn-default">Download Account Validation Report</button>', 'target="_blank"');

                        if ($file['account_validation_status_description'] != 'PENDING' && $total_payable_accounts < $total_accounts)
                            $swapAccounts = anchor('payments/bulk_payment/runSwapWizard/' . $file['file_id'], '<button class="btn btn-xs btn-danger">Run Swap Wizard</button>');

                        $listAccounts = anchor('payments/bulk_payment/listAccounts/' . $file['file_id'], '<button class="btn btn-xs btn-default">List Accounts</button>');

                        //show button for validating accounts
                        //if ($total_records > 0 && $valid_records == $total_records && $file['account_validation_status_description'] != 'COMPLETED')
                        if ($total_records > 0 && $file['account_validation_status_description'] != 'COMPLETED')
                            $validateAccounts = anchor('payments/bulk_payment/validateAccounts/' . $file['file_id'], '<button class="btn btn-xs btn-info">Validate Accounts</button>');

                        //show button for processing file
                        if ($total_payable_accounts > 0 && empty($file['payment_file_group']))
                            $processFile = anchor('payments/bulk_payment/processFile/' . $file['file_id'], '<button class="btn btn-xs btn-info">Process file</button>');

                        if (!empty($file['payment_file_group'])) {
                            $payment_status = '<span class="label label-warning">IN PROGRESS</span>';
                            $downloadPaymentReport = anchor('payments/bulk_payment/downloadPaymentReport/' . $file['file_id'], '<button class="btn btn-xs btn-default">Download Payment Report</button>', 'target="_blank"');
                        }

                        if (empty($file['payment_file_group']))
                            $delete_file = anchor('payments/bulk_payment/deleteFile/' . $file['file_id'], '<button class="btn btn-xs btn-danger">Delete File</button>');

                        ?>
                        <tr>
                            <td><?php echo ++$i ?></td>
                            <td><?php echo $file['file_name'] ?></td>
                            <td><?php echo $file['created_at'] ?></td>
                            <td><?php echo $content_validation_status . " " . $content_validation_status_progress . "<br>" . $content_validation_summary . "<br>" . $downloadContentValidationReport ?></td>
                            <td><?php echo $account_validation_status . "<br>" . $account_validation_summary . "<br>" . $downloadAccountValidationReport ?></td>
                            <td><?php echo $payment_status . " " . $downloadPaymentReport ?></td>
                            <td><?php echo $listAccounts . " " . $validateAccounts . " " . $swapAccounts . " " . $processFile . " " . $delete_file ?></td>
                        </tr>
                    <?php }
                } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>