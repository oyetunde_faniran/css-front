<?php $dis_sys = $this->system_authorization; ?>

<div class="row">
    <div class="col-md-12">
        <?php
        $msg = !empty($msg) ? $msg : '';
        echo $msg;
        ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo $title ?></h3>
            </div>

            <div class="panel-body">
                <?php
                if (empty($paymentFileGroups)) {
                    echo "No Payment File Group found.";
                } else {
                    ?>
                    <div class="table-responsive" id="customers2">
                        <table class="table table-bordered table-striped" id="payments-list">
                            <thead>
                            <tr>
                                <th style="text-align: right">S/No</th>
                                <th>Description</th>
                                <td>Payment Type</td>
                                <td>Beneficiary Account Name</td>
                                <th style="text-align: right">Amount (&#8358;)</th>
                                <th>Status</th>
                                <th>Date Initiated</th>
                                <th>Actions</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php
                            $count = 0;

                            foreach ($paymentFileGroups as $group) {
                                $aLink = "";
                                $downloadPaymentReport = "";
                                $encryptedPaymentFileGroupId = $this->basic_functions->encryptGetData($group['id']);

                                if ($group['status_description'] == 'PENDING') {
                                    $dStatus = '<span class="label label-default">PENDING</span>';
                                    if ($dis_sys->hasAccess('payment-schedule-forward')) {
                                        $aLink = anchor('payments/schedule/forward/' . $encryptedPaymentFileGroupId, '<button class="btn btn-xs btn-info">Forward Payment</button>');
                                    }
                                } elseif ($group['status_description'] == 'FORWARDED') {
                                    $dStatus = '<span  class="label label-warning">FORWARDED</span>';
                                    if ($dis_sys->hasAccess('payment-schedule-approve')) {
                                        $aLink = anchor('payments/schedule/approve/' . $encryptedPaymentFileGroupId, '<button class="btn btn-xs btn-info">Approve Payment</button>');
                                        $aLink .= " " . anchor('payments/schedule/reject/' . $encryptedPaymentFileGroupId, '<button class="btn btn-xs btn-danger">Reject Payment</button>');
                                    }
                                } elseif ($group['status_description'] == 'APPROVED') {
                                    $dStatus = '<span  class="label label-success">APPROVED</span>';
                                    if ($dis_sys->hasAccess('schedule-do-auth')) {
                                        $auth_method = $group['payment_type'] == 'FUND SWEEPING' ? 'doauth' : 'doauth2';
                                        $aLink = anchor('payments/schedule/' . $auth_method . '/' . $encryptedPaymentFileGroupId, '<button class="btn btn-xs btn-info">Authorize Payment</button>');
                                        $aLink .= " " . anchor('payments/schedule/reject/' . $encryptedPaymentFileGroupId, '<button class="btn btn-xs btn-danger">Reject Payment</button>');
                                    }
                                } elseif ($group['status_description'] == 'REJECTED') {
                                    $dStatus = '<span class="label label-danger">REJECTED</span>';
                                } elseif ($group['status_description'] == 'AUTHORIZED') {
                                    $dStatus = '<span class="label label-success">AUTHORIZED</span>';

                                    //show the Make Payment link only if a transaction batch has not been generated for any of the files in the group
                                    if (empty($group['psg_id'])) {
                                        if ($dis_sys->hasAccess('make-payment')) {
                                            $aLink = anchor('payments/schedule/makePayment/' . $encryptedPaymentFileGroupId, '<button class="btn btn-xs btn-info">Make Payment</button>');
                                        }
                                    } else {
                                        $dStatus = '<span class="label label-success">PROCESSED</span>';
                                        $aLink = anchor('payments/schedule/settlementReportDetails/' . $group['psg_id'], '<button class="btn btn-xs btn-info">Show Report</button>');
                                        if ($group['payment_type'] == 'AUTOMATED FUND SWEEPING')
                                            $downloadPaymentReport = anchor('payments/schedule/downloadAutoFundSweepPaymentReport/' . $group['id'], '<button class="btn btn-xs btn-dark">Download Payment Report</button>');
                                    }
                                }

                                $dLink = anchor('payments/schedule/paymentDetail/' . $encryptedPaymentFileGroupId, '<button class="btn btn-xs btn-default">Details</button>');
                                ?>

                                <tr>
                                    <td style="text-align: right"><?php echo ++$count ?></td>
                                    <td><strong><?php echo $group['narration'] ?></strong></td>
                                    <td><?php echo $group['payment_type'] ?></td>
                                    <td><?php echo $group['beneficiary_account_name'] ?></td>
                                    <td style="text-align: right">
                                        <strong><?php echo number_format($group['total_amount'], 2) ?></strong>
                                    </td>
                                    <td><?php echo $dStatus ?></td>
                                    <td><?php echo $this->basic_functions->formatDate($group['created_at']) ?></td>
                                    <td><?php echo $dLink . " " . $aLink . " " . $downloadPaymentReport ?></td>
                                </tr>

                                <?php
                            } ?>
                            </tbody>
                        </table>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>