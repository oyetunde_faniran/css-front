<div class="row">
    <form name="spay" method="POST" action="">
        <div class="col-md-12">
            <?php
            $msg = !empty($msg) ? $msg : '';
            echo $msg;
            ?>
            <div class="panel panel-default">

                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo $title ?></h3>
                    <br>
                    <span>
                        <input type="submit" class="btn btn-success" name="process" value="Process Checked">
                    </span>
                </div>

                <div class="panel-body">

                    <?php if (empty($paymentFileGroup)) {
                        echo "No Payment File Group";
                    } else { ?>
                        <div class="table-responsive" id="customers2">
                            <table class="table table-bordered table-striped" id="DataTables_Table_0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Narration</th>
                                    <th>Total Amount (&#8358;)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr id="trow_1">
                                    <td>1 <input class='checkbox1' type='checkbox' name="id"
                                                 value="<?php echo $paymentFileGroup['id'] ?>"></td>
                                    <td><?php echo $paymentFileGroup['narration'] ?></td>
                                    <td><?php echo number_format($paymentFileGroup['total_amount'], 2) ?></td>
                                </tr>
                                </tbody>
                            </table>

                        </div>
                    <?php } ?>
                </div>
                <div class="panel-footer">
                    <div class="pull-right"><?php echo "" ?></div>
                </div>
            </div>
        </div>
    </form>
</div>