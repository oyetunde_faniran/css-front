<style>
    td.num, th.num {
        text-align: right;
    }

    table.details td {
        padding: 5px;
        width: 100px
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-body">
                <table class="table table-condensed table-hover table-bordered table-striped" id="report-table">
                    <thead>
                    <tr>
                        <th>S/N</th>
                        <th>SOURCE ACCOUNTS</th>
                        <th>BENEFICIARIES</th>
                        <th>PROCESSED BY</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    $display = '';
                    $total = $sno = 0;

                    foreach ($payment_file_group['payment_files'] as $file) {
                        $display .= '<tr>
                                        <td>' . (++$sno) . '.</td>
                                        <td>
                                            <table class="details">
                                                <tr>
                                                    <td>' . $file['source_account']['bank']['bank_name'] . '</td>
                                                    <td>' . $file['source_account']['acc_name'] . '</td>
                                                    <td>' . $file['source_account']['acc_number'] . '</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table class="details">';

                        foreach ($file['beneficiaries'] as $beneficiary) {
                            $encBatchNo = urlencode(base64_encode($file['transaction_batch']['batchNo']));
                            
                            if ($beneficiary['transaction']['sentStatus'] == "1") {
                                //the schedule has been uploaded
                                $status = ($beneficiary['transaction']['paymentStatus'] == '00' ? '<span class="label label-info">Successful</span>' : ($beneficiary['transaction']['paymentStatus'] == '01' ? '<span class="label label-warning">In Progress</span>' : '<span class="label label-danger">Failed</span> <small>' . $beneficiary['transaction']['statusDescription'] . '</small>'));
                                $action = $beneficiary['transaction']['paymentStatus'] == '01' ? '<br>' . anchor('payments/schedule/requeryBatch/' . $encBatchNo, '<button class="btn btn-xs btn-success"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Requery">Requery</button>') : '';   
                            } else {
                                //schedule has not been uploaded at all
                                $status = '<span class="label label-default">Not Sent</span>';
                                $action = '<br>' . anchor('payments/schedule/resendSchedule/' . $file['payment_file_group_id'], '<button class="btn btn-xs btn-success"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Resend">Resend</button>');
                            }

                            $display .= '<tr>
                                            <td>' . $beneficiary['bank']['bank_name'] . '</td>
                                            <td>' . $beneficiary['beneficiarySurname'] . '</td>
                                            <td>' . $beneficiary['beneficiaryAccountNo'] . '</td>
                                            <td class="num"> &#8358;' . number_format($beneficiary['amount'], 2) . '</td>
                                            <td>' . $beneficiary['transaction']['processedDate'] . '</td>
                                            <td>' . $status . $action . '</td>
                                         </tr>';
                        }

                        $display .= '</table></td>
                                    <td>' . $payment_file_group['authorizer'] . '</td>
                                </tr>';
                    }
                    echo $display;
                    ?>
                    </tbody>
                </table>
                <?php echo anchor('payments/schedule/reports', '&laquo; Back to Reports Home', 'class="btn btn-danger"'); ?>
            </div>
        </div>
    </div>
</div>