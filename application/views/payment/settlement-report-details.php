<style>
    td.num, th.num {
        text-align: right;
    }

    table.details td, table.details th {
        padding: 5px;
    }
    table.details th {
        outline:1px solid;
        background: #AAAAAA;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-body">
                <table class="table table-condensed table-hover table-bordered table-striped" id="report-table">
                    <thead>
                    <tr>
                        <th class="num">#</th>
                        <th>SOURCE ACCOUNTS</th>
                        <th>BENEFICIARIES</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    $display = '';
                    $total = $sno = 0;
                    if (!empty($payment_schedules)) {

                        foreach ($payment_schedules as $schedule) {
                            $display .= '<tr>
                                            <td class="num" style="vertical-align:top">' . (++$sno) . '.</td>
                                            <td style="vertical-align:top">
                                                <b> '.$schedule['source_account_bank_name'].'</b><br/> -
                                                 '.$schedule['source_account_name'].'
                                                 ('.$schedule['source_account_no'].')
                                                 <br/>
                                                 -<small><i>Ref #'.$schedule['schedule_ref'].'</i></small>
                                                 <hr style="margin:5px 0"/><br/>
                                                 '.sweet_date($payment_schedule_group['created_at'],true).'
                                            </td>
                                            <td>
                                                <table class="details">
                                                    <tr>
                                                        <th class="num">#</th>
                                                        <th>Bank</th>
                                                        <th>Account Name</th>
                                                        <th>Account Number</th>
                                                        <th class="num">Amount (&#8358;)</th>
                                                        <th></th>
                                                    </tr>';

                            $k = 0;
                            foreach ($payment_schedule_beneficiaries as $beneficiary) {

                                $action = '';

                                if ($beneficiary['payment_schedule'] == $schedule['payment_schedule_id']) {

                                    if ($beneficiary['beneficiary_payment_status'] == "PAID") {
                                        $status = '<span class="label label-success">PAID</span>';
                                    } elseif ($beneficiary['beneficiary_payment_status'] == "PENDING") {
                                        $status = '<span class="label label-warning">IN PROGRESS</span>';
                                    } elseif ($beneficiary['beneficiary_payment_status'] == "IN PROGRESS") {
                                        $status = '<span class="label label-warning">IN PROGRESS</span>';
                                        $action = ' ' . anchor('payments/schedule/getPaymentScheduleStatus/' . $beneficiary['payment_schedule'], '<button class="btn btn-xs btn-default">Get Status</button>');
                                    } elseif ($beneficiary['beneficiary_payment_status'] == "FAILED") {
                                        $status = '<span class="label label-danger">FAILED</span>';
                                        $status .= ' <span class="label label-info" style="font-size: 8px !important;">' . $beneficiary['beneficiary_payment_response_description'] . '</span>';

                                        if ($beneficiary['beneficiary_payment_response_description'] == 'UNKNOWN UNIQUE ID')
                                            $action = ' ' . anchor('payments/schedule/resendPaymentSchedule/' . $beneficiary['payment_schedule'], '<button class="btn btn-xs btn-default">Resend</button>');
                                    }

                                    $display .= '<tr>
                                                    <td class="num">' . (++$k) . '</td>
                                                    <td>' . $beneficiary['beneficiary_bank_name'] . '</td>
                                                    <td>' . $beneficiary['beneficiary_account_name'] . '</td>
                                                    <td>' . $beneficiary['beneficiary_account_no'] . '</td>
                                                    <td class="num">' . number_format($beneficiary['beneficiary_amount'], 2) . '</td>
                                                    <td>' . $status . $action . '</td>
                                                 </tr>';
                                }

                            }

                            $display .= '</table></td>
                                </tr>';
                        }
                        echo $display;
                    }
                    ?>
                    </tbody>
                </table>
                <?php echo anchor('payments/schedule/settlementReport', '&laquo; Back to Reports Home', 'class="btn btn-danger"'); ?>
            </div>
        </div>
    </div>
</div>