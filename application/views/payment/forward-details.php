<div class="row">
    <div class="col-md-12">
        <?php
        $msg = !empty($msg) ? $msg : '';
        echo $msg;
        ?>
        <div class="panel panel-default">

            <div class="panel-heading">
                <h3 class="panel-title"><?php echo $title ?></h3>
            </div>

            <div class="panel-body">

                <?php
                if (empty($payment_file_group)) {
                    echo "No Payment Schedule";
                } else {
                    $encryptedPaymentFileGroupId = $this->basic_functions->encryptGetData($payment_file_group['id']);
                    $dLink = $dLink2 = "";
                    $desc = 'CREATED BY';

                    if ($payment_file_group['status'] == PAYMENT_AUTHORIZED) {
                        $file_group_status = "<span  class='label label-success'>AUTHORIZED</span>";
                    } elseif ($payment_file_group['status'] == PAYMENT_APPROVED) {
                        $file_group_status = "<span  class='label label-success'>APPROVED</span>";
                        $desc = 'APPROVED BY';
                        $dLink = anchor('payments/schedule/doauth/' . $encryptedPaymentFileGroupId, '<button class="btn btn-success"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Authorize">Authorize Payment</button>');
                        $dLink2 = anchor('payments/schedule/reject/' . $encryptedPaymentFileGroupId, '<button class="btn btn-danger"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Reject">Reject Payment</button>');
                    } elseif ($payment_file_group['status'] == PAYMENT_REJECTED) {
                        $file_group_status = "<span  class='label label-danger'>REJECTED</span>";
                        $desc = 'REJECTED BY';
                    } elseif ($payment_file_group['status'] == PAYMENT_FORWARDED) {
                        $file_group_status = "<span  class='label label-warning'>FORWARDED</span>";
                    } else {
                        $file_group_status = "<span  class='label label-default'>PENDING</span>";
                        $dLink = anchor('payments/schedule/forward/' . $encryptedPaymentFileGroupId, '<button class="btn btn-success"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Forward">Forward Payment</button>');
                    }
                    ?>
                    <div class="table-responsive" id="customers2">
                        <div class="col-md-5"><h4>NARRATION: <?php echo $payment_file_group['narration'] ?></h4></div>
                        <div class="col-md-5"><h4><?php echo $desc . ": " . $payment_file_group['initiator'] ?></h4></div>
                        <div class="col-md-2"><h4>STATUS: <?php echo $file_group_status ?></h4></div>

                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Source Accounts</th>
                                <th>Beneficiaries</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($payment_file_group['payment_files'] as $file) { ?>
                                <tr>
                                    <td><?php echo $file['source_account']['acc_name'] . " - " . $file['source_account']['bank_name'] . " - " . $file['source_account']['acc_number'] ?></td>
                                    <td>
                                        <?php
                                        $display = array();
                                        foreach ($file['beneficiaries'] as $beneficiary) {
                                            $str = stripslashes($beneficiary['beneficiarySurname']) . " - " . stripslashes($beneficiary['bankName']) . " - " . stripslashes($beneficiary['beneficiaryAccountNo']) . " - &#8358;" . number_format($beneficiary['amount'], 2);
                                            $display[] = $str;
                                        }
                                        echo implode("<br>", $display);
                                        ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>

                        </table>
                    </div>
                <?php } ?>
            </div>
            <div class="panel-footer">
                <div class="pull-right"><?php echo $dLink . " " . $dLink2 ?></div>
                <br class="clearfix">
            </div>
        </div>
    </div>
</div>