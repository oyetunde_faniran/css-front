<?php
    $dis_sys = $this->system_authorization;
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-body">
<?php
    if($dis_sys->hasAccess('project-add')){
        echo '<div>
                    <button data-toggle="modal" data-target="#create-modal" type="button" class="btn btn-space btn-danger"><i class="icon s7-plus"></i> Add Project</button>
                </div>';
    }
?>
                <table class="table table-condensed table-hover table-bordered table-striped" id="report-table">
                    <thead>
                        <tr>
                            <td>S/NO</td>
                            <td>NAME</td>
                            <td>UPLOAD KEY</td>
                            <td>CLIENT</td>
                            <td>DESCRIPTION</td>
                            <td>STATUS</td>
                            <td>DATE ADDED</td>
                            <td>ACTION</td>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <td>S/NO</td>
                            <td>NAME</td>
                            <td>UPLOAD KEY</td>
                            <td>CLIENT</td>
                            <td>DESCRIPTION</td>
                            <td>STATUS</td>
                            <td>DATE ADDED</td>
                            <td>ACTION</td>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php
                        $display = '';
                        $sno1 = 0;
                        foreach ($records as $c) {
                            $id = $c['prj_id'];
                            $display .= '<tr>
                                            <td>' . ( ++$sno1) . '.</td>
                                            <td>' . $c['prj_name'] . '</td>
                                            <td>' . $c['prj_key'] . '</td>
                                            <td>' . $c['client_name'] . '</td>
                                            <td>' . $c['prj_description'] . '</td>
                                            <td>' . ($c['prj_enabled'] == '1' ? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>') . '</td>
                                            <td>' . $c['prj_dateadded'] . '</td>
                                            <td>'
                                            . '<!--<button disabled="disabled" data-toggle="modal" data-target="#form-edit-client" class="btn btn-space btn-danger" onclick="getClientDeets(' . $id . ')"><i class="icon s7-look"></i> View / Edit</button>-->'
                                            . '<button data-toggle="modal" data-target="#form-edit-client" class="btn btn-space btn-warning" onclick="getSplitDeets(' . $id . ')"><i class="icon s7-look"></i> View / Edit Split</button>'
                                            . '</td>
					</tr>';
                        }
                        echo $display;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div id="create-modal" tabindex="-1" role="dialog" class="modal fade colored-header">
    <div class="modal-dialog custom-width">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i
                        class="icon s7-close"></i></button>
                <h3 class="modal-title">Create Project</h3>
            </div>
            <form id="create-form">
                <div id="msg-container-create-modal"></div>
                <div class="modal-body form" style="overflow: auto; max-height: 400px;">
                    <div class="form-group">
                        <label for="name">NAME:</label>
                        <input type="text" required="required" name="name" id="name" placeholder="Project name here" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="desc">DESCRIPTION:</label>
                        <input type="text" required="required" name="desc" id="desc" placeholder="Description here" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="client">CLIENT:</label>
<?php
    $client_dd = array(
        '0' => '--Select Client--'
    );
    foreach($clients as $row){
        $client_dd[$row['client_id']] = $row['client_name'];
    }
    echo form_dropdown('client', $client_dd, 0, 'id="client" class="form-control"');
?>
                    </div>
                    <div class="form-group">
                        <label for="when2split">WHEN TO SPLIT COMMISSION:</label>
<?php
    $when2split_dd = array(
        '0' => '--Select--',
        '1' => 'Split Transaction By Transaction',
        '2' => 'Aggregate & Split All At Month End'
    );
    echo form_dropdown('when2split', $when2split_dd, 0, 'id="when2split" class="form-control"');
?>
                    </div>
                    <div class="form-group">
                        <label for="defaultprj">DEFAULT CLIENT PROJECT:</label>
<?php
    $defaultprj_dd = array(
        '0' => 'Not Default',
        '1' => 'Default Project'
    );
    echo form_dropdown('defaultprj', $defaultprj_dd, 0, 'id="defaultprj" class="form-control"');
?>
                    </div>
                    <div class="form-group">
                        <label>DEDUCTIONS:</label>
<?php
    foreach ($deductions as $ded){
        $obj_id = 'deduction-' . $ded['ded_id'];
        switch ($ded['ded_type']){
            case 1: //Fixed
                $amount_display = " (&#8358;{$ded['ded_amount']})";
                break;
            case 2: //Percentage
                $amount_display = " ({$ded['ded_amount']}%)";
                break;
            case 3: //Per mille
                $amount_display = " (&#8358;{$ded['ded_amount']} per mille)";
                break;
        }
        echo '<div>'
                . form_checkbox('deductions[]', $ded['ded_id'], 0, 'id="' . $obj_id . '" class="form-controlxxx"')
                . '<label for="' . $obj_id . '">' . $ded['ded_description'] . $amount_display . '</label>'
                . '</div>';
    }
?>
                    </div>
                </div>
                <div class="modal-footer">
                    <div id="loading-image-create-modal" style="width: 100%; display: none;" class="progress-bar progress-bar-danger progress-bar-striped active">Please, wait...</div>
                    <button type="button" id="close-button-create-modal" data-dismiss="modal" class="btn btn-default md-close">Close</button>
                    <button type="button" id="save-button-create-modal" class="btn btn-primary" onclick="doCreate(this.form);">Create Project</button>
                </div>
            </form>
        </div>
    </div>
</div>



<div id="form-edit-client" tabindex="-1" role="dialog" class="modal fade colored-header">
    <div class="modal-dialog custom-width">
        <div class="modal-content">
            <?php echo form_open(site_url('splitter/editSplit')); ?>
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i class="icon s7-close"></i></button>
                <h3 class="modal-title">View/Edit Split</h3>
            </div>
            <div class="modal-body form" style="overflow: auto; max-height: 400px;">
                <div id="loading-image-edit-body" style="width: 100%; display: none;" class="progress-bar progress-bar-danger progress-bar-striped active">Fetching details...</div>
                <div id="msg-container-edit"></div>
                <div id="edit-client-form-container" style="display:none;">
<?php
    $sno = 0;
    $bens_display = '<h3><strong>CLIENT:</strong> <span id="client-container"></span></h3>
                    <h3><strong>PROJECT:</strong> <span id="project-container"></span></h3>
                    <h3><strong>SPLIT TYPE</strong></h3>
                    <div>
                        <select name="type" id="type" class="form-control" onchange="setUnit(this.value);">
                            <option value="2">Percentage (%)</option>
                            <option value="1">Fixed Amount (&#8358;)</option>
                        </select>
                    </div>';
    $bens_display .= '<h3><strong>SPLIT</strong></h3>
                        <table class="table table-striped table-bordered">
                        <tbody>
                            <tr>
                                <td>S/NO</td>
                                <td>BENEFICIARY</td>
                                <td>SHARE (<span id="unit-container"></span>)</td>
                            </tr>
                        </tbody>';
    foreach ($bens as $b){
        $bens_display .= '<tr>
                            <td>' . (++$sno) . '.</td>
                            <td>' . $b['ben_name'] . '</td>
                            <td><input type="number" name="ben_share[' . $b['ben_id'] . ']" id="ben-share-input-' . $b['ben_id'] . '" class="form-control ben-share-input" value="0" /></td>
                        </tr>';
    }
    $bens_display .= '</table>';
    echo $bens_display;
?>
                </div>
            </div>
        
            <div class="modal-footer">
                <div id="loading-image-edit" style="width: 100%; display: none;" class="progress-bar progress-bar-danger progress-bar-striped active">Please, wait...</div>
                <button type="button" id="close-button-edit" data-dismiss="modal" class="btn btn-default md-close">Close</button>
<?php
    echo $dis_sys->hasAccess('project-update-split-config') ? '<button type="button" id="save-button-edit" class="btn btn-primary" onclick="updateSplit(this.form);">Update Split</button>' : '';
?>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>



<style type="text/css">
    label { font-weight: bold; }
</style>


<script type="text/javascript">
    comm_config_hidden = true;
    edit_id = 0;
    var dis_timer = 0;
    function showCommConfig(do_edit = false){
        dis_id = do_edit ? 'commission-config-container-edit' : 'commission-config-container';
        if(comm_config_hidden){
            $('#' + dis_id).fadeIn()
        } else {
            $('#' + dis_id).fadeOut()
        }
        comm_config_hidden = !comm_config_hidden;
    }
    
    
    function doCreate(dis_form){
        form_data = $(dis_form).serializeArray();
        dis_url = '<?php echo site_url('project/addProject'); ?>';
        $('#close-button-create-modal').fadeOut();
        $('#save-button-create-modal').fadeOut();
        $('#loading-image-create-modal').fadeIn();
        $.ajax({
            type: 'POST',
            url: dis_url,
            data_type: 'json',
            data: form_data,
            success: function(data){
                $('#loading-image-create-modal').fadeOut();
                $('#msg-container-create-modal').html(data.msg);
                $('#msg-container-create-modal').fadeIn();
                if(data.status){
//                    dis_timer = setInterval(reloadClients, 2000);
                    window.location = '<?php echo site_url('project'); ?>';
                } else {
                    $('#close-button-create-modal').fadeIn();
                    $('#save-button-create-modal').fadeIn();
                }
            }
        });
    }
    
    
    function getSplitDeets(id){
        edit_id = id;
        dis_url = '<?php echo site_url('project/getSplitConfig') . '/'; ?>' + id;
        console.log(dis_url);
        $('#save-button-edit').fadeOut();
        $('#loading-image-edit-body').fadeIn();
        $('#edit-client-form-container').fadeOut();
        $('#msg-container-edit').fadeOut();
        $.ajax({
            type: 'GET',
            url: dis_url,
            data_type: 'json',
            success: function(data){
                $('#loading-image-edit-body').fadeOut();
                console.log(JSON.stringify(data));
                if(data.status){
                    if(data.cdata){
                        console.log(data);
                        ben_count = data.cdata.length;
                        $('.ben-share-input').val(0);
                        $('.ben-share-input').css('background-color', '#FFF');
                        $('.ben-share-input').css('color', '#000');
                        for(k = 0; k < ben_count; k++){
                            $('#ben-share-input-' + data.cdata[k].ben_id).val(data.cdata[k].share);
                            $('#ben-share-input-' + data.cdata[k].ben_id).css('background-color', '#999');
                            $('#ben-share-input-' + data.cdata[k].ben_id).css('color', '#FFF');
    //                        console.log(data.cdata[k]);
                        }
                        
                        $('#type').val(data.type);
                        setUnit(data.type);
                        $('#project-container').html(data.project);
                        $('#client-container').html(data.client);
                    }

                    $('#edit-client-form-container').fadeIn();
                    $('#close-button-edit').fadeIn();
                    $('#save-button-edit').fadeIn();
                } else {
                    $('#msg-container-edit').html('Unknown Client!!!');
                }
            }
        });
    }
    
    
    function initAddBox(){
        comm_config_hidden = true;
        document.getElementById('computecommission').checked = false;
        $('#commission-config-container').fadeOut();
    }
    
    
    function updateClientDeets(dis_form){
        form_data = $(dis_form).serializeArray();
        dis_url = '<?php echo site_url('client/updateClient') . '/'; ?>' + edit_id;
        $('#close-button-edit').fadeOut();
        $('#save-button-edit').fadeOut();
        $('#loading-image-edit').fadeIn();
        $.ajax({
            type: 'POST',
            url: dis_url,
            data_type: 'json',
            data: form_data,
            success: function(data){
                $('#loading-image-edit').fadeOut();
                $('#msg-container-edit').html(data.msg);
                $('#msg-container-edit').fadeIn();
                if(data.status){
//                    dis_timer = setInterval(reloadClients, 2000);
                    window.location = '<?php echo site_url('client'); ?>';
                } else {
                    $('#close-button-edit').fadeIn();
                    $('#save-button-edit').fadeIn();
                }
            }
        });
    }
    
    
    function updateSplit(dis_form){
        form_data = $(dis_form).serializeArray();
        dis_url = '<?php echo site_url('splitter/updateSplit') . '/'; ?>' + edit_id;
        $('#close-button-edit').fadeOut();
        $('#save-button-edit').fadeOut();
        $('#loading-image-edit').fadeIn();
        $.ajax({
            type: 'POST',
            url: dis_url,
            data_type: 'json',
            data: form_data,
            success: function(data){
                $('#loading-image-edit').fadeOut();
                $('#msg-container-edit').html(data.msg);
                $('#msg-container-edit').fadeIn();
//                console.log(data);
                $('#close-button-edit').fadeIn();
                $('#save-button-edit').fadeIn();
//                if(data.status){
//                    dis_timer = setInterval(reloadClients, 2000);
//                    window.location = '<?php // echo site_url('project'); ?>';
//                } else {
//                    $('#close-button-edit').fadeIn();
//                    $('#save-button-edit').fadeIn();
//                }
            }
        });
    }



    function setUnit(unit_type){
        dis_unit = unit_type == 1 ? '&#8358;' : '%';
        $('#unit-container').html(dis_unit);
    }


</script>