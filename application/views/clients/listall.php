<?php
    $dis_sys = $this->system_authorization;
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-body">
<?php
    if($dis_sys->hasAccess('client-add')){
        echo '<div>
                    <button data-toggle="modal" data-target="#form-bp1" type="button" class="btn btn-space btn-danger" onclick="initAddBox();"><i class="icon s7-plus"></i> Add Client</button>
                </div>';
    }
?>
                <table class="table table-condensed table-hover table-bordered table-striped" id="report-table">
                    <thead>
                        <tr>
                            <td>S/NO</td>
                            <td>NAME</td>
                            <td>CODE</td>
                            <!--<td>E-MAIL</td>-->
                            <td>COMM. CONFIGURED</td>
                            <td>STATUS</td>
                            <td>DATE ADDED</td>
                            <td>&nbsp;</td>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <td>S/NO</td>
                            <td>NAME</td>
                            <td>CODE</td>
                            <!--<td>E-MAIL</td>-->
                            <td>COMM. CONFIGURED</td>
                            <td>STATUS</td>
                            <td>DATE ADDED</td>
                            <td>&nbsp;</td>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php
                        $display = '';
                        $sno = 0;
                        foreach ($clients as $c) {
                            $id = $c['client_id'];
                            $display .= '<tr>
						<td>' . ( ++$sno) . '.</td>
						<td>' . $c['client_name'] . '</td>
                        <td>' . $c['client_code'] . '</td>
                        <!--<td>' . $c['client_email'] . '</td>-->
                        <td>' . ($c['client_computecommission'] == 1 ? '<label class="label label-success">Yes</label>' : '<label class="label label-danger">No</label>') . '</td>
                        <td>' . ($c['client_enabled'] == '1' ? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>') . '</td>
                        <td>' . $c['client_dateadded'] . '</td>
                        <td>'
                            . '<div class="btn-group btn-space">
                                    <button type="button" class="btn btn-primary">Actions</button>
                                    <button type="button" data-toggle="dropdown" class="btn btn-primary btn-shade1 dropdown-toggle" aria-expanded="false"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button>
                                    <ul role="menu" class="dropdown-menu">
                                        <li><a data-toggle="modal" data-target="#form-edit-client" onclick="getClientDeets(' . $id . ')" href="#">View / Edit Client</a></li>
                                        <li class="divider"></li>
                                        <li><a data-toggle="modal" data-target="#form-edit-ips" onclick="getClientIPs(' . $id . ')" href="#">View / Edit IPs</a></li>
                                        <!--<li><a data-toggle="modal" data-target="#form-edit-accounts" onclick="getClientAccounts(' . $id . ')" href="#">View / Edit Accounts</a></li>-->
                                    </ul>
                                </div>'
//                            . anchor("reports/transactions/$id", '<i class="icon s7-look"></i>Transaction Report', 'class="btn btn-space btn-warning"')
                            . '<!--<div class="switch-button switch-button-md">
                                <input type="checkbox" checked="" name="swt4" id="swt4"><span>
                                  <label for="swt4"></label></span>
                              </div>-->'
                            . '</td>
					</tr>';
                        }
                        echo $display;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div id="form-bp1" tabindex="-1" role="dialog" class="modal fade colored-header">
    <div class="modal-dialog custom-width">
        <div class="modal-content">
            <?php echo form_open(site_url('client/addClient')); ?>
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i
                        class="icon s7-close"></i></button>
                <h3 class="modal-title">Add New Client</h3>
            </div>
            <div id="msg-container"></div>
            <div class="modal-body form" style="overflow: auto; max-height: 400px;">
                <div class="form-group">
                    <label id="clientname">Name</label>
                    <input type="text" required="required" name="clientname" id="clientname" placeholder="Name of the client here" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" required="required" name="email" id="email" placeholder="Email of the client here" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="nibssclientid">NIBSS Client ID</label>
                    <input type="text" required="required" name="nibssclientid" id="nibssclientid" placeholder="Client ID issued by NIBSS to be used for this client" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="updateurl">Status Update URL</label>
                    <input type="text" name="updateurl" id="updateurl" placeholder="URL to push status updates to" class="form-control" />
                </div>
                <p class="spacer2">
                    <input type="checkbox" name="computecommission" id="computecommission" onclick="showCommConfig();" /> <label for="computecommission">Automatically compute commission for this client</label>
                </p>
                <div id="commission-config-container" style="display: none;">
                    <div class="form-group">
                        <label for="commission_type">Commission Type </label>
<?php
    $comm_type_dd = array(
        -1 => '--Select--',
        0 => 'Fixed Value (&#8358;)',
        1 => 'Percentage (%)',
        2 => 'Per Mille (&#8358;)',
    );
    echo form_dropdown('commission_type', $comm_type_dd, -1, 'id="commission_type" class="form-control"');
?>
                    </div>
                    <div class="form-group">
                        <label for="commission">Commission Value (&#8358; / %)</label>
                        <input type="number" required="required" name="commission" id="commission" placeholder="Commission to charge this client" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="commission_mode">How Should The Commission Be Calculated?</label>
<?php
    $comm_mode_dd = array(
        -1 => '--Select--',
        0 => 'Calculate Per Beneficiary',
        1 => 'Calculated based on the commission beneficiary in the XML request',
    );
    echo form_dropdown('commission_mode', $comm_mode_dd, -1, 'id="commission_mode" class="form-control"');
?>
                    </div>
                    <div class="form-group">
                        <label for="commission_deduct">Should The Commission Be Deducted?</label>
<?php
    $comm_deduct_dd = array(
        -1 => '--Select--',
        0 => 'Yes, deduct from individual beneficiaries',
        1 => 'No, don\'t deduct from individual beneficiaries',
    );
    echo form_dropdown('commission_deduct', $comm_deduct_dd, -1, 'id="commission_deduct" class="form-control"');
?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div id="loading-image" style="width: 100%; display: none;" class="progress-bar progress-bar-danger progress-bar-striped active">Please, wait...</div>
                <button type="button" id="close-button" data-dismiss="modal" class="btn btn-default md-close">Close</button>
                <button type="button" id="save-button" class="btn btn-primary" onclick="saveClientDeets(this.form);">Add Client</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>



<div id="form-edit-client" tabindex="-1" role="dialog" class="modal fade colored-header">
    <div class="modal-dialog custom-width">
        <div class="modal-content">
            <?php echo form_open(site_url('client/editClient')); ?>
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i class="icon s7-close"></i></button>
                <h3 class="modal-title">Edit Client</h3>
            </div>
            <div class="modal-body form" style="overflow: auto; max-height: 400px;">
                <div id="loading-image-edit-body" style="width: 100%; display: none;" class="progress-bar progress-bar-danger progress-bar-striped active">Fetching client details...</div>
                <div id="msg-container-edit"></div>
                <div id="edit-client-form-container" style="display:none;">
                <div class="form-group">
                    <label id="clientname">Name</label>
                    <input type="text" required="required" name="clientname" id="clientname-edit" placeholder="Name of the client here" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="email-edit">Email address</label>
                    <input type="email" required="required" name="email" id="email-edit" placeholder="Email of the client here" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="nibssclientid-edit">NIBSS Client ID</label>
                    <input type="text" required="required" name="nibssclientid" id="nibssclientid-edit" placeholder="Client ID issued by NIBSS to be used for this client" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="updateurl-edit">Status Update URL</label>
                    <input type="text" name="updateurl" id="updateurl-edit" placeholder="URL to push status updates to" class="form-control" />
                </div>
                <p class="spacer2">
                    <input type="checkbox" name="computecommission" id="computecommission-edit" onclick="showCommConfig(true);" /> <label for="computecommission-edit">Automatically compute commission for this client</label>
                </p>
                <div id="commission-config-container-edit" style="display: none;">
                    <div class="form-group">
                        <label for="commission_type-edit">Commission Type </label>
<?php
    $comm_type_dd = array(
        -1 => '--Select--',
        0 => 'Fixed Value (&#8358;)',
        1 => 'Percentage (%)',
        2 => 'Per Mille (&#8358;)',
    );
    echo form_dropdown('commission_type', $comm_type_dd, -1, 'id="commission_type-edit" class="form-control"');
?>
                    </div>
                    <div class="form-group">
                        <label for="commission-edit">Commission Value (&#8358; / %)</label>
                        <input type="number" required="required" name="commission" id="commission-edit" placeholder="Commission to charge this client" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="commission_mode-edit">How Should The Commission Be Calculated?</label>
<?php
    $comm_mode_dd = array(
        -1 => '--Select--',
        0 => 'Calculate Per Beneficiary',
        1 => 'Calculated based on the commission beneficiary in the XML request',
    );
    echo form_dropdown('commission_mode', $comm_mode_dd, -1, 'id="commission_mode-edit" class="form-control"');
?>
                    </div>
                    <div class="form-group">
                        <label for="commission_deduct-edit">Should The Commission Be Deducted?</label>
<?php
    $comm_deduct_dd = array(
        -1 => '--Select--',
        0 => 'Yes, deduct from individual beneficiaries',
        1 => 'No, don\'t deduct from individual beneficiaries',
    );
    echo form_dropdown('commission_deduct', $comm_deduct_dd, -1, 'id="commission_deduct-edit" class="form-control"');
?>
                    </div>
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <div id="loading-image-edit" style="width: 100%; display: none;" class="progress-bar progress-bar-danger progress-bar-striped active">Please, wait...</div>
                <button type="button" id="close-button-edit" data-dismiss="modal" class="btn btn-default md-close">Close</button>
<?php
    echo $dis_sys->hasAccess('client-update') ? '<button type="button" id="save-button-edit" class="btn btn-primary" onclick="updateClientDeets(this.form);">Update Client</button>' : '';
?>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>



<div id="form-edit-ips" tabindex="-1" role="dialog" class="modal fade colored-header">
    <div class="modal-dialog custom-width">
        <div class="modal-content">
            <?php echo form_open('', "id='edit-ips-form'"); ?>
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i class="icon s7-close"></i></button>
                <h3 class="modal-title">Edit Client IPs</h3>
            </div>
            <div class="modal-body form" style="overflow: auto; max-height: 400px;">
                <div id="loading-image-edit-ips-body" style="width: 100%; display: none;" class="progress-bar progress-bar-danger progress-bar-striped active">Fetching details...</div>
                <div id="msg-container-edit-ips"></div>
                
<?php
    echo $dis_sys->hasAccess('client-ip-update') ? '<div id="edit-ips-add-button" style="margin-bottom: 10px;"><button class="btn btn-warning" type="button" onclick="addNewIP();">Add a New IP</button></div>' : '';
?>
                
                <div id="edit-ips-form-container" style="display:none;"></div>
            </div>
            <div class="modal-footer">
                <div id="loading-image-edit-ips" style="width: 100%; display: none;" class="progress-bar progress-bar-danger progress-bar-striped active">Please, wait...</div>
                <button type="button" id="close-button-edit-ips" data-dismiss="modal" class="btn btn-default md-close">Close</button>
<?php
    echo $dis_sys->hasAccess('client-ip-update') ? '<button type="button" id="save-button-edit-ips" class="btn btn-primary" onclick="updateIPs(this.form);">Update IPs</button>' : '';
?>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>



<style type="text/css">
    label { font-weight: bold; }
</style>


<script type="text/javascript">
    ip_count = 0;
    comm_config_hidden = true;
    edit_id = 0;
    ip_edit_id = 0;
    var dis_timer = 0;
    function showCommConfig(do_edit = false){
        dis_id = do_edit ? 'commission-config-container-edit' : 'commission-config-container';
        if(comm_config_hidden){
            $('#' + dis_id).fadeIn()
        } else {
            $('#' + dis_id).fadeOut()
        }
        comm_config_hidden = !comm_config_hidden;
    }
    
    
    function saveClientDeets(dis_form){
        form_data = $(dis_form).serializeArray();
        dis_url = '<?php echo site_url('client/addClient'); ?>';
        $('#close-button').fadeOut();
        $('#save-button').fadeOut();
        $('#loading-image').fadeIn();
        $.ajax({
            type: 'POST',
            url: dis_url,
            data_type: 'json',
            data: form_data,
            success: function(data){
                $('#loading-image').fadeOut();
                $('#msg-container').html(data.msg);
                $('#msg-container').fadeIn();
                if(data.status){
//                    dis_timer = setInterval(reloadClients, 2000);
                    window.location = '<?php echo site_url('client'); ?>';
                } else {
                    $('#close-button').fadeIn();
                    $('#save-button').fadeIn();
                }
            }
        });
    }
    
    
    function getClientDeets(id){
        edit_id = id;
        dis_url = '<?php echo site_url('client/getClients') . '/'; ?>' + id;
        $('#save-button-edit').fadeOut();
        $('#loading-image-edit-body').fadeIn();
        $('#edit-client-form-container').fadeOut();
        $('#msg-container-edit').fadeOut();
        $.ajax({
            type: 'GET',
            url: dis_url,
            data_type: 'json',
            success: function(data){
                $('#loading-image-edit-body').fadeOut();
                if(data.status){
                    $('#clientname-edit').val(data.name);
                    $('#email-edit').val(data.email);
                    $('#nibssclientid-edit').val(data.nibss_clientid);
                    $('#updateurl-edit').val(data.url);
                    $('#commission_type-edit').val(data.charge_type);
                    $('#commission-edit').val(data.charge);
                    $('#commission_mode-edit').val(data.charge_mode);
                    $('#commission_deduct-edit').val(data.deductcharge);
                    if(data.computecommission == 1){
                        comm_config_hidden = false;
                        $('#commission-config-container-edit').fadeIn();
//                        $('#computecommission-edit').checked = "checked";
                        document.getElementById('computecommission-edit').checked = "checked";
                    } else {
                        comm_config_hidden = true;
                        $('#commission-config-container-edit').fadeOut();
//                        $('#computecommission-edit').checked = '';
                        document.getElementById('computecommission-edit').checked = false;
                    }
                    $('#edit-client-form-container').fadeIn();
                    $('#close-button-edit').fadeIn();
                    $('#save-button-edit').fadeIn();
                } else {
                    $('#msg-container-edit').html('Unknown Client!!!');
                }
            }
        });
    }
    

    
    function getClientIPs(id){
        ip_edit_id = id;
        dis_url = '<?php echo site_url('ips/getClientIps') . '/'; ?>' + id;
        $('#save-button-edit-ips').fadeOut();
        $('#loading-image-edit-ips-body').fadeIn();
        $('#edit-ips-form-container').fadeOut();
        $('#msg-container-edit-ips').fadeOut();
        $.ajax({
            type: 'GET',
            url: dis_url,
            data_type: 'json',
            success: function(data){
                $('#loading-image-edit-ips-body').fadeOut();
                if(data.status){
                    ip_count = data.dis_data.length;
                    new_content = '';
                    for(counter = 0; counter < ip_count; counter++){
                        new_content = new_content + '<div id="ip-div-' + counter + '" class="form-group"><input class="form-control" type="text" name="ip[]" value="' + data.dis_data[counter].ip + '" /><button class="btn btn-danger" type="button" onclick="removeIP(' + counter + ');">X</button></div>';
                    }
                    $('#edit-ips-form-container').html(new_content);
                    $('#edit-ips-form-container').fadeIn();
                    $('#close-button-edit-ips').fadeIn();
                    $('#save-button-edit-ips').fadeIn();
                } else {
                    $('#msg-container-edit-ips').html(data.msg ? data.msg : 'Unable to load IPs');
                    $('#msg-container-edit-ips').fadeIn();
                }
            }
        });
    }
    
    
    function initAddBox(){
        comm_config_hidden = true;
        document.getElementById('computecommission').checked = false;
        $('#commission-config-container').fadeOut();
    }
    
    
    function updateClientDeets(dis_form){
        form_data = $(dis_form).serializeArray();
        dis_url = '<?php echo site_url('client/updateClient') . '/'; ?>' + edit_id;
        $('#close-button-edit').fadeOut();
        $('#save-button-edit').fadeOut();
        $('#loading-image-edit').fadeIn();
        $.ajax({
            type: 'POST',
            url: dis_url,
            data_type: 'json',
            data: form_data,
            success: function(data){
                $('#loading-image-edit').fadeOut();
                $('#msg-container-edit').html(data.msg);
                $('#msg-container-edit').fadeIn();
                if(data.status){
//                    dis_timer = setInterval(reloadClients, 2000);
                    window.location = '<?php echo site_url('client'); ?>';
                } else {
                    $('#close-button-edit').fadeIn();
                    $('#save-button-edit').fadeIn();
                }
            }
        });
    }
    
    
//    function reloadClients(){
//        dis_url = '<?php //echo site_url('client/getClients'); ?>';
//        clearTimeout(dis_timer);
//        $.ajax({
//            type: 'POST',
//            url: dis_url,
//            data_type: 'json',
//            data: form_data,
//            success: function(data){
//                console.log(data);
//                
////                if(data.status){
////
////                } else {
////                }
//            }
//        });
//    }
    
    
    function removeIP(id_counter){
        $('#ip-div-' + id_counter).fadeOut();
        $('#ip-div-' + id_counter).remove();
    }
    
    
    function addNewIP(){
        counter = ++ip_count;
        new_ip = '<div id="ip-div-' + counter + '" class="form-group"><input class="form-control" type="text" name="ip[]" value="" /><button class="btn btn-danger" type="button" onclick="removeIP(' + counter + ');">X</button></div>';
        $('#edit-ips-form-container').html($('#edit-ips-form-container').html() + new_ip);
    }
    
    
    function updateIPs(){
        form_data = $('#edit-ips-form').serializeArray();
        dis_url = '<?php echo site_url('ips/updateClientIps') . '/'; ?>' + ip_edit_id;
        $('#close-button-edit-ips').fadeOut();
        $('#save-button-edit-ips').fadeOut();
        $('#loading-image-edit-ips').fadeIn();
        $.ajax({
            type: 'POST',
            url: dis_url,
            data_type: 'json',
            data: form_data,
            success: function(data){
                $('#loading-image-edit-ips').fadeOut();
                $('#msg-container-edit-ips').html(data.msg);
                $('#msg-container-edit-ips').fadeIn();
                $('#close-button-edit-ips').fadeIn();
                $('#save-button-edit-ips').fadeIn();
            }
        });
    }
    
</script>