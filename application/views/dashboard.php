      <div class="am-content">
        <div class="main-content">
          <div class="row">
		    <div class="col-md-5">
              
              <div class="row">
                <div class="col-md-12">
                  <div class="widget widget-tile widget-tile-wide">
                    <div class="tile-info">
                      <div class="icon"><span class="s7-users"></span></div>
                      <div class="data-info">
                        <div class="title">Clients</div>
                        <div class="desc">profiled and active</div>
                      </div>
                    </div>
                    <div class="tile-value"><span data-toggle="counter" data-decimals="2" data-end="<?php echo $clients_active; ?>" data-prefix="&#8358;"><?php echo anchor('client', ' ' . number_format($clients_active)); ?></span></div>
                  </div>
                </div>
<!--                <div class="col-md-12">
                  <div class="widget widget-tile widget-tile-wide">
                    <div class="tile-info">
                      <div class="icon"><span class="s7-cash"></span></div>
                      <div class="data-info">
                        <div class="title">Projects</div>
                        <div class="desc">profiled and active</div>
                      </div>
                    </div>
                    <div class="tile-value"><span data-toggle="counter" data-decimals="2" data-end="<?php echo $projects; ?>" data-prefix="&#8358;"><?php echo anchor('project', $projects); ?></span></div>
                  </div>
                </div>-->
                <div class="col-md-12">
                  <div class="widget widget-tile widget-tile-wide">
                    <div class="tile-info">
                      <div class="icon"><span class="s7-users"></span></div>
                      <div class="data-info">
                        <div class="title">Accounts</div>
                        <div class="desc">paid successfully</div>
                      </div>
                    </div>
                    <div class="tile-value"><span data-toggle="counter" data-decimals="2" data-end="<?php echo $beneficiaries; ?>" data-prefix="&#8358;"><?php echo anchor('reports/transactions', ' ' . number_format($beneficiaries)); ?></span></div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="widget widget-tile widget-tile-wide">
                    <div class="tile-info">
                      <div class="icon"><span class="s7-cash"></span></div>
                      <div class="data-info">
                        <div class="title">Commission</div>
                        <div class="desc">total generated</div>
                      </div>
                    </div>
                    <div class="tile-value"><span data-toggle="counter" data-decimals="2" data-end="<?php echo $commission; ?>" data-prefix="&#8358;"><?php echo anchor('reports/comms', '&#8358;' . number_format($commission, 2)); ?></span></div>
                  </div>
                </div>
<!--                <div class="col-md-12">
                  <div class="widget widget-tile widget-tile-wide">
                    <div class="tile-info">
                      <div class="icon"><span class="s7-cash"></span></div>
                      <div class="data-info">
                        <div class="title">Collection Account</div>
                        <div class="desc">Current Balance</div>
                      </div>
                    </div>
                    <div class="tile-value"><span data-toggle="counter" data-decimals="2" data-end="<?php echo $coll_acc_balance; ?>" data-prefix="&#8358;"><?php echo anchor('#', '&#8358;' . number_format($coll_acc_balance, 2)); ?></span></div>
                  </div>
                </div>-->
				
<!--                <div class="col-md-12">
                  <div class="widget widget-tile widget-tile-wide">
                    <div class="tile-info">
                      <div class="icon"><span class="s7-graph1"></span></div>
                      <div class="data-info">
                        <div class="title"></div>
                        <div class="desc"></div>
                      </div>
                    </div>
                    <div class="tile-value"><span><?php // echo anchor('reports', 'Detailed Reports'); ?></span></div>
                  </div>
                </div>-->
				
              </div>
            </div>
          <div class="col-md-7">
            <div class="widget widget-fullwidth line-chart">
              <div class="widget-head">
                <span class="title">Commission (In the last 30 days)</span>
              </div>
              <div class="chart-container">
                <div class="counter">
                  <div class="desc"></div>
                </div>
                <div id="all-accounts-bar-chart"></div>
              </div>
            </div>
          </div>
          </div>
          
        </div>
      </div>