
		</div>
	</div>
    <script src="<?php echo base_url('assets/lib/jquery/jquery.min.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/lib/jquery.nanoscroller/javascripts/jquery.nanoscroller.min.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/js/main.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/lib/bootstrap/dist/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/lib/jquery-ui/jquery-ui.min.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/lib/skycons/skycons.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/lib/jquery.sparkline/jquery.sparkline.min.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/lib/countup/countUp.min.js'); ?>" type="text/javascript"></script>

<?php
    //Load custom CSS files for the current page
    if (!empty($css_files)){
        $css_files_display = '';
        foreach ($css_files as $css){
            $css_files_display .= link_tag(base_url($css));
        }
        echo $css_files_display;
    }
	
	
	//Load custom JS files for the current page
    if (!empty($js_files)){
        $js_files_display = '';
        foreach ($js_files as $js){
            $js_files_display .= '<script src="' . base_url($js) . '" type="text/javascript"></script>';
        }
        echo $js_files_display;
    }
?>

	<script type="text/javascript">
      $(document).ready(function(){
      	//initialize the javascript
      	App.init();
<?php
	if (!empty($additional_js)){
        echo $additional_js;
    }
?>
      });
    </script>
	
	
  </body>

</html>