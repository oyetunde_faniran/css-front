<?php
//    echo ('<pre>' . print_r($acc_cats, true));
    $tab_links = $tab_contents = '';
    $first = TRUE;
    //die('<pre>' . print_r($view_data['accs'], true));
    $cat_dropdown = array('0' => '--Select Account Category--');
    foreach ($acc_cats as $cat){
        $active_link = $first ? ' class="active" ' : '';
        $active_tab_class = $first ? ' active ' : '';
        $first = FALSE;
        $link_name = url_title($cat['accat_name']);
        $tab_links .= '<li ' . $active_link . '><a href="#' . $link_name . '" data-toggle="tab">' . $cat['accat_name'] . '</a></li>';
        
        //Form the category dropdown array
        $cat_dropdown[$cat['accat_id']] = $cat['accat_name'];
        
        //Format the list of accounts (tab contents) if any
        if(!empty($accs[$cat['accat_id']])){
            $sno = 0;
            $dis_acc_list = '<table class="table table-striped table-border">
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>S/NO</td>
                                    <td>ACCOUNT NAME</td>
                                    <td>ACCOUNT NUMBER</td>
                                    <td>BANK NAME</td>
                                </tr>';
            
            foreach ($accs[$cat['accat_id']] as $acc){
                $dis_acc_id = 'acc-cats-' . $acc['acc_id'];
                $dis_acc_list .= '<tr>
                                    <td><input name="acc_cats[]" type="checkbox" id="' . $dis_acc_id . '" value="' . $acc['acc_id'] . '" /></td>
                                    <td>' . (++$sno) . '.</td>
                                    <td><label for="' . $dis_acc_id . '">' . $acc['acc_name'] . '</label></td>
                                    <td>' . $acc['acc_number'] . '</td>
                                    <td>' . $acc['bank_name'] . '</td>
                                </tr>';
            }   //END foreach()
            $dis_acc_list .= '</table>';
        } else {
            $dis_acc_list = 'No accounts in this category';
        }
        
        $tab_contents .= '<div id="' . $link_name . '" class="tab-pane cont ' . $active_tab_class . '">' . $dis_acc_list . '</div>';
        
    }   //END foreach
?>
<form method="post" action="<?php echo site_url('account/categorize'); ?>">
    <div class="row">
        <div class="col-md-8" style="margin: 30px 0px;">
            <?php echo !empty($msg) ? $msg : ''; ?>
            Add selected accounts to <?php echo form_dropdown('newcategory', $cat_dropdown); ?> category.
            <input type="submit" value="Change Category" class="btn btn-primary" />
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
          <div class="tab-container">
            <ul class="nav nav-tabs">
                <?php echo $tab_links; ?>
            </ul>
            <div class="tab-content">
                <?php echo $tab_contents; ?>
            </div>
          </div>
        </div>
    </div>
</form>