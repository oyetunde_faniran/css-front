<?php
    //die ('<pre>' . print_r($users, true) . '</pre>');
    $display = '<div style="margin:10px 0px;">' . anchor('account/add', '<button class="btn btn-primary"><i class="fa fa-plus-circle"></i> Add New Account</button>') . '</div>';
    if (!empty($users)){
        $display .= '<table class="table table-striped table-hover table-fw-widget dataTable no-footer" role="grid" style="margin-bottom:0;">
                        <thead>
                            <tr>
                                <th>S/NO</th>
                                <th>BANK NAME</th>
                                <th>ACCOUNT NAME</th>
                                <th>ACCONT NUMBER</th>
                                <th>BVN</th>
                                <th>ACTIVE</th>
                                <th><em>Edit</em></th>
                            </tr>
                        </thead>
                        <tbody>';
        $s_no = 0;
        foreach ($users as $u){
            $display .= '<tr>
                            <td>' . (++$s_no) . '.</td>
                            <!--<td>' . (!empty($u['lga_name']) ? $u['lga_name'] : 'All') . '</td>-->
                            <td>' . stripslashes($u['user_firstname']) . '</td>
                            <td>' . stripslashes($u['user_surname']) . '</td>
                            <td>' . stripslashes($u['user_phone']) . '</td>
                            <td>' . stripslashes($u['userlog_email']) . '</td>
                            <td>' . (isset($u['usergroup_name']) ? stripslashes($u['usergroup_name']) : '-') . '</td>
                            <td>' . $u['datecreated'] . '</td>
                            <td>' . $u['lastlogin'] . '</td>
                            <td>' . ($u['user_active'] == 1 ? 'Yes' : 'No') . '</td>
                            <td>' . anchor('sys-admin/user/edit/' . $u['user_id'], '<i class="fa fa-edit"></i> Edit', 'class="btn btn-primary"') . '</td>
                         </tr>';
        }
        $display .= '<tfoot>
                            <tr>
                                <th>S/NO</th>
                                <th>BANK NAME</th>
                                <th>ACCOUNT NAME</th>
                                <th>ACCONT NUMBER</th>
                                <th>BVN</th>
                                <th>ACTIVE</th>
                                <th><em>Edit</em></th>
                            </tr>
                        </tfoot>';
        $display .= '</tbody></table>';
    } else {
        $display = 'No account created yet. Please, ' . anchor('account/add', 'click here to add an account.') . '.';
    }
    echo $display;