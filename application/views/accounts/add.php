<?php
	$bank_dd[0] = '--Select Bank--';
	//die('<pre>' . print_r($banks, true));
	foreach ($banks as $b){
		$bank_dd[$b['bank_id']] = $b['bank_name'];
	}
?>
  <div class="panel panel-default col-md-6">
	<div class="panel-body">
	  <form role="form" action="#" method="post">
		<div class="form-group">
		  <label>Bank</label>
		  <?php echo form_dropdown('bank', $bank_dd, null, 'class="form-control"'); ?>
		</div>
		<div class="form-group">
		  <label>Account Number</label>
		  <input type="text" name="acc_no" placeholder="Enter Account Number" class="form-control" oldautocomplete="remove" autocomplete="off">
		</div>
		<div class="form-group">
		  <label>Account Name</label>
		  <input type="text" name="acc_name" placeholder="Enter Account Name" class="form-control" oldautocomplete="remove" autocomplete="off">
		</div>
		<div class="form-group">
		  <label>Bank Verification Number (BVN)</label>
		  <input type="text" name="acc_name" placeholder="Enter BVN" class="form-control" oldautocomplete="remove" autocomplete="off">
		</div>
		<div class="spacer text-right">
		  <button type="submit" class="btn btn-space btn-primary">Submit</button>
		  <button class="btn btn-space btn-default">Cancel</button>
		</div>
	  </form>
	</div>
  </div>