      <div class="am-content">
        <div class="main-content">
          <div class="row">
		    <div class="col-md-5">
              <div class="row">
                <div class="col-md-6">
					<div class="widget widget-tile">
					  <div class="data-info">
						<div data-toggle="counter" data-end="<?php echo $acc_count ?>" data-decimals="0" data-suffix=" accounts" class="value"><?php echo anchor('reports/do_report/account/total', "$acc_count accounts"); ?></div>
						<div class="desc">profiled and in use</div>
					  </div>
					  <div class="icon"><span class="s7-drawer"></span></div>
					</div>
                </div>
                <div class="col-md-6">
					<div class="widget widget-tile">
					  <div class="data-info">
						<div data-toggle="counter" data-end="<?php echo "$bank_count banks"; ?>" data-suffix=" banks" class="value"><?php echo anchor('reports/do_report/bank/total', "$bank_count banks"); ?></div>
						<div class="desc">with active accounts</div>
					  </div>
					  <div class="icon"><span class="s7-culture"></span></div>
					</div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="widget widget-tile widget-tile-wide">
                    <div class="tile-info">
                      <div class="icon"><span class="s7-cash"></span></div>
                      <div class="data-info">
                        <div class="title">Total Amount</div>
                        <div class="desc">in all accounts</div>
                      </div>
                    </div>
                    <div class="tile-value"><span data-toggle="counter" data-decimals="2" data-end="<?php echo $aggregate['total']; ?>" data-prefix="&#8358;"><?php echo anchor('reports/do_report/account/total', '&#8358;' . number_format($aggregate['total'], 2)); ?></span></div>
                  </div>
                </div>
				
				<div class="col-md-12">
                  <div class="widget widget-tile widget-tile-wide">
                    <div class="tile-info">
                      <div class="icon"><span class="s7-graph1"></span></div>
                      <div class="data-info">
                        <div class="title"></div>
                        <div class="desc"></div>
                      </div>
                    </div>
                    <div class="tile-value"><span><?php echo anchor('reports', 'Detailed Reports'); ?></span></div>
                  </div>
                </div>
				
              </div>
            </div>
            <div class="col-md-7">
              <div class="widget widget-fullwidth line-chart">
                <div class="widget-head">
				  <span class="title">All Accounts Stats</span>
                </div>
                <div class="chart-container">
                  <div class="counter">
                    <div class="desc">Balance in each account</div>
                  </div>
				  <div id="all-accounts-pie-chart"></div>
                </div>
              </div>
            </div>
          </div>
		  
		  
		  
		<div class="row">
			<div class="col-md-12">
              <div class="widget widget-fullwidth line-chart">
                <div class="widget-head">
				  <span class="title">All Accounts Stats</span>
                </div>
                <div class="chart-container">
                  <div class="counter">
                    <div class="value"></div>
                    <div class="desc"></div>
                  </div>
                  <div id="all-accounts-bar-chart"></div>
                </div>
              </div>
            </div>
		</div>
          
        </div>
      </div>