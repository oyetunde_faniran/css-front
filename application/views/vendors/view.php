
<?php
$dis_sys = $this->system_authorization;
$msg = !empty($msg) ? $msg : "";
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $title ?></h3>
    </div>
    <?php
    if (!empty($vendor)) {
        $vendorId = $vendor['vendorId'];
        $encryptedVendorId = $this->basic_functions->encryptGetData($vendorId);
        $dLink = "";
        if ($vendor['status'] == "1") {
            $dStatus = '<span  class="label label-success">Active</span>';
            if ($dis_sys->hasAccess('activate-vendor')) {
                $dLink = '<a id="disable" class="btn btn-danger" href="' . base_url("vendors/vendor/disable/$encryptedVendorId") . '"><i class="fa fa-times"></i>Deactivate Vendor</a>';
            }
        } else if ($vendor['status'] == "0") {
            $dStatus = '<span class="label label-danger label-form">Inactive</span>';
            if ($dis_sys->hasAccess('activate-vendor')) {
                $dLink = '<a id="enable" class="btn btn-success" href="' . site_url("vendors/vendor/enable/$encryptedVendorId") . '"><i class="fa fa-check-square-o"></i>Activate Vendor</a>';
            }
        } else if ($vendor['status'] == "2") {
            $dStatus = '<span class="label label-danger label-form">Inactive</span>';
            if ($dis_sys->hasAccess('activate-vendor')) {
                $dLink = '<a id="enable" class="btn btn-success" href="' . site_url("vendors/vendor/enable/$encryptedVendorId") . '"><i class="fa fa-check-square-o"></i>Activate User</a>';
            }
        }

        if ($authorize == '0') {
            $dLink = "";
        }
        ?>
        <div class="panel-body">
            <?php echo $msg ?>
            <div class="col-md-6 center-block">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-actions">
                        <tbody>
                        <tr>
                            <td><strong>Vendor Name</strong></td>
                            <td><?php echo stripslashes($vendor['vendorName']) ?></td>
                        </tr>
                        <tr>
                            <td><strong>Account Name</strong></td>
                            <td><?php echo stripslashes($vendor['accountName']) ?></td>
                        </tr>
                        <tr>
                            <td><strong>Account No</strong></td>
                            <td><?php echo stripslashes($vendor['accountNo']) ?></td>
                        </tr>
                        <tr>
                            <td><strong>Bank</strong></td>
                            <td><?php echo stripslashes($vendor['bankName']) ?></td>
                        </tr>
                        <tr>
                            <td><strong>Status</strong></td>
                            <td><?php echo $dStatus ?></td>
                        </tr>
                        <tr>
                            <td><strong>Created By</strong></td>
                            <td><?php echo stripslashes($vendor['editor']) ?></td>
                        </tr>
                        <tr>
                            <td><strong>Date Created</strong></td>
                            <td><?php echo $this->basic_functions->formatDate($vendor['dateModified']) ?></td>
                        </tr>
                        <tr>
                            <td><strong>Activated By</strong></td>
                            <td><?php echo stripslashes($vendor['authorizer']) ?></td>
                        </tr>
                        <tr>
                            <td><strong>Date Activated</strong></td>
                            <td><?php echo $this->basic_functions->formatDate($vendor['activatedDate']) ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="pull-right"><?php echo $dLink ?></div>
            <div class="pull-left">
                <a href='<?php echo site_url("vendors/vendor/adminList") ?>'>
                    <button class="btn btn-primary" data-toggle="tooltip" data-placement="top" title=""
                            data-original-title="View All Vendors"><i class="fa fa-list"></i>View all Vendors
                    </button>
                </a>
                <?php if ($dis_sys->hasAccess('edit-vendor')) { ?>
                    &nbsp;
                    <a href='<?php echo site_url("vendors/vendor/edit/$encryptedVendorId") ?>'>
                        <button class="btn btn-info" data-toggle="tooltip" data-placement="top" title=""
                                data-original-title="Edit Vendor"><i class="fa fa-pencil-square-o"></i>Edit Vendor
                        </button>
                    </a>
                <?php } ?>
            </div>
            <div class="clearfix"></div>
        </div>

    <?php } else {
        echo "Invalid Vendor";
    } ?>
</div>
