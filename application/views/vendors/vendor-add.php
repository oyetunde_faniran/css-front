<?php
$form_display = '';

$bank_array = array('' => '--Select Bank--');
if (!empty($banks)) {
    foreach ($banks as $g) {
        $bank_array[$g['bank_id']] = stripslashes($g['bank_name']);
    }
}
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $title ?></h3>
    </div>
    <div class="panel-body">
        <?php
        //Show success/error messages if any
        $form_display .= !empty($msg) ? $msg : '';
        //Show the actual form
        $attributes = array('class' => 'form-horizontal', 'id' => 'validate');
        ?>
        <div class="col-md-12">
            <?php
            try {
                $form_display .= form_open('', $attributes)
                    . '<div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Vendor Name<span class="star">*</span>:</label>
                            <div class="col-md-4 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span class="fa fa-pencil"></span>
                                    </span>'
                    . form_input('vendorName', set_value('vendorName'), 'class="validate[required] form-control" id="vendorName"') .
                    '</div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Account Name<span class="star">*</span>:</label>
                                        <div class="col-md-4 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>' . form_input('accountName', set_value('accountName'), 'class="validate[required] form-control" id="accountName"') .
                    '</div>
                                        </div>
                                    </div>
                                <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Account Number<span class="star">*</span>:</label>

                <div class="col-md-4 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon"><span
                                class="fa fa-pencil"></span></span>' . form_input('accountNo', set_value('accountNo'), 'class="validate[required] form-control" id="accountNo"') . '
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Bank<span class="star">*</span>:</label>
                <div class="col-md-4 col-xs-12">
                    <div class="input-group">
                        ' . form_dropdown('bank', $bank_array, set_value('bankId'), 'class=" validate[required] form-control select" id="bank"') . '
                    </div>
                </div>
            </div>
              <div class="form-group">
                                    <button type="submit" class="btn btn-info pull-right"><span class="fa fa-plus-circle"></span> Create Vendor</button>
                                </div>'
                    . form_close();

            } catch (Exception $ex) {
                $form_display = $ex->getMessage();
            }
            echo $form_display;
            ?>
        </div>
    </div>
</div>