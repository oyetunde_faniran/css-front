<?php $msg = !empty($msg) ? $msg : ""; ?>

<form action="" method="POST">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $title ?></h3>
        </div>
        <?php
        if (!empty($vendor)) {
            ?>
            <div class="panel-body">
                <?php echo $msg ?>
                <div class="col-md-10 center-block">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-actions">
                            <tbody>
                            <tr>
                                <td width="30%"><strong>Vendor Name</strong></td>
                                <td><?php echo $vendor['vendorName'] ?></td>
                            </tr>
                            <tr>
                                <td><strong>Account Name</strong></td>
                                <td><?php echo $vendor['accountName'] ?></td>
                            </tr>
                            <tr>
                                <td><strong>Account Number</strong></td>
                                <td><?php echo $vendor['accountNo'] ?></td>
                            </tr>
                            <tr>
                                <td><strong>Bank</strong></td>
                                <td><?php echo $vendor['bankName'] ?></td>
                            </tr>
                            <tr>
                                <td><strong>Amount</strong></td>
                                <td><input type="number" min="1" step=".01" name="amount" value="">&nbsp;<span class="help">In Naira</span>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Description</strong></td>
                                <td><input class=" form-control" type="text" name="description" value=""></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="panel-footer">

                <div class="pull-right">
                    <input type="submit" value="Save Payment" class="btn btn-success" data-toggle="tooltip"
                           data-placement="top" title="Save Payment" data-original-title="Save Payment">
                </div>
                <div class="pull-left">
                    <a href='<?php echo site_url("vendors/vendor/payList") ?>'>
                        <button class="btn btn-primary" data-toggle="tooltip" data-placement="top" title=""
                                data-original-title="View Vendors">View Vendors
                        </button>
                    </a>
                </div>
                <div class="clearfix"></div>
            </div>
        <?php } else {
            echo "Invalid Step";
        } ?>
    </div>
</form>
