<div class="row">
    <div class="col-md-12">
        <?php
        $msg = !empty($msg) ? $msg : '';
        echo $msg;
        ?>
        <div class="panel panel-default">

            <div class="panel-heading">
                <h3 class="panel-title"><?php echo $title ?></h3>
            </div>

            <div class="panel-body">

                <?php
                if (empty($payment_file_group)) {
                    echo "No Payment Schedule";
                } else {
                    $encryptedPaymentFileGroupId = $this->basic_functions->encryptGetData($payment_file_group['id']);
                    $aLink = "";
                    
                    if ($payment_file_group['status'] == PAYMENT_AUTHORIZED) {
                        $file_group_status = "<span class='label label-info'>AUTHORIZED</span>";
                    } elseif ($payment_file_group['status'] == PAYMENT_APPROVED) {
                        $file_group_status = "<span class='label label-info'>APPROVED</span>";
                        $aLink = anchor('payments/schedule/doauth/' . $encryptedPaymentFileGroupId, '<button class="btn btn-success"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Authorize">Authorize Payment</button>');
                    } elseif ($payment_file_group['status'] == PAYMENT_REJECTED) {
                        $file_group_status = "<span class='label label-danger'>REJECTED</span>";
                    } elseif ($payment_file_group['status'] == PAYMENT_FORWARDED) {
                        $file_group_status = "<span class='label label-warning'>FORWARDED</span>";
                    } else {
                        $file_group_status = "<span class='label label-default'>PENDING</span>";
                        $aLink = anchor('payments/schedule/forward/' . $encryptedPaymentFileGroupId, '<button class="btn btn-success"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Forward">Forward Payment</button>');
                    }
                    ?>
                    <div class="row">
                        <div class="col-md-5"><h4>NARRATION: <?php echo $payment_file_group['narration'] ?></h4></div>
                        <div class="col-md-5"><h4>PAYMENT TYPE: <?php echo $payment_file_group['payment_files'][0]['paymentFileType'] == "2" ? "Vendor Payment" : "Fund Sweeping"; ?></h4>
                        </div>
                        <div class="col-md-2"><h4>STATUS: <?php echo $file_group_status ?></h4></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5"><h4><?php echo $desc . ": " . $payment_file_group['initiator'] ?></h4>
                        </div>
                    </div>

                    <div class="table-responsive" id="customers2">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Source Accounts</th>
                                <th>Beneficiaries</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $count = 1;
                            $total = 0;
                            foreach ($payment_file_group['payment_files'] as $file) {
                                ?>
                                <tr>
                                    <td><?php echo $file['source_account']['acc_name'] . " - " . $file['source_account']['bank_name'] . " - " . $file['source_account']['acc_number'] ?></td>
                                    <td>
                                        <?php
                                        $display = array();
                                        foreach ($file['beneficiaries'] as $beneficiary) {
                                            $str = stripslashes($beneficiary['beneficiarySurname']) . " - " . stripslashes($beneficiary['bankName']) . " - " . stripslashes($beneficiary['beneficiaryAccountNo']) . " - &#8358;" . number_format($beneficiary['amount'], 2);
                                            $display[] = $str;
                                        }
                                        echo implode("<br>", $display);
                                        ?>
                                    </td>
                                </tr>
                                <?php
                                $count++;
                            } ?>
                            </tbody>
                        </table>
                    </div>
                <?php } ?>
            </div>
            <div class="panel-footer">
                <div class="pull-right"><?php echo $aLink ?></div>
                <br class="clearfix">
            </div>
        </div>
    </div>
</div>