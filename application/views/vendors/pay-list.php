<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo $title ?></h3>

                <div class="btn-group pull-right">
                    <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i>
                        Export Data
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="#" onClick="$('#customers2').tableExport({type:'csv',escape:'false'});"><img
                                    src=<?php echo site_url('img/icons/csv.png') ?> width="24"/> CSV</a></li>
                        <li><a href="#" onClick="$('#customers2').tableExport({type:'txt',escape:'false'});"><img
                                    src=<?php echo site_url('img/icons/txt.png') ?> width="24"/> TXT</a></li>
                        <li class="divider"></li>
                        <li><a href="#" onClick="$('#customers2').tableExport({type:'excel',escape:'false'});"><img
                                    src=<?php echo site_url('img/icons/xls.png') ?> width="24"/> XLS</a></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="panel-body">
                <?php if (empty($vendors)) {
                    echo "No Report";
                } else { ?>
                    <div class="table-responsive" id="customers2">
                        <table class="table table-bordered table-striped datatable">
                            <thead>
                            <tr>
                                <th>S/No</th>
                                <th>Vendor Name</th>
                                <th>Status</th>
                                <th>Pay</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $count = 1;
                            foreach ($vendors as $u) {
                                $vendorId = $u['vendorId'];
                                $vendorName = $u['vendorName'];
                                $dStatus = ($u['status'] == 1 ? '<span  class="label label-success">Active</span>' : '<span class="label label-danger label-form">Inactive</span>');
                                $encryptedVendorId = $this->basic_functions->encryptGetData($u['vendorId']);
                                $dLink = anchor('vendors/vendor/initiate/' . $encryptedVendorId, '<button class="btn btn-success"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Pay Vendor">Pay Vendor</button>');
                                ?>
                                <tr id="trow_<?php echo $count ?>">
                                    <td><?php echo $count ?></td>
                                    <td><?php echo $vendorName ?></td>
                                    <td><?php echo $dStatus ?></td>
                                    <td><?php echo $dLink ?></td>
                                </tr>
                                <?php $count++;
                            } ?>
                            </tbody>
                        </table>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>