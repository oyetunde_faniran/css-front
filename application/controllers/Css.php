<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Bams
 *
 * @author oyetunde.faniran
 */
class Css extends UPL_Controller {
 
    
    function __construct(){
        parent::__construct();
        $this->load->model('Accountbalance_model', 'accbalm');
    }   //END __construct()
    
    
    public function index(){
        if($_SESSION['usergroup_id'] == 2){
            redirect('reports/ben_comms');
        }
		//Load the necessary models
//		$this->load->model('Account_model', 'acc');
//		$this->load->model('Bank_model', 'bank');
//		$this->load->model('Report_model', 'rep');
		$this->load->library('Report_formatter');
		
		//Fetch data for the dashboard
		$view_data['clients_active'] = $this->db->count_all_results('api_clients');//count($this->acc->getAccounts());
		$view_data['beneficiaries'] = $this->db->where('ben_status', '2')->count_all_results('schedules_beneficiaries');//count($this->bank->getAccountBanks());
                $view_data['projects'] = $this->db->where('prj_enabled', '1')->count_all_results('comm_projects');//count($this->bank->getAccountBanks());
                $view_data['coll_acc_balance'] = 10000000;//$this->accbalm->getAccountBalance(SUSPENSE_ACC_NO, SUSPENSE_ACC_BANK_CODE, SUSPENSE_ACC_NAME);
                
                //Total commission generated
		$comm_result = $this->db->select_sum('schcomm_amount')->get('schedules_commission');//$this->rep->getTotalTotal();
                $comm_row = $comm_result->row_array();
                $view_data['commission'] = !empty($comm_row['schcomm_amount']) ? $comm_row['schcomm_amount'] : 0;
                
                //Total value processed
                $comm_result = $this->db->select_sum('schcomm_amount')->get('schedules_commission');//$this->rep->getTotalTotal();
                $comm_row = $comm_result->row_array();
                $view_data['total_volume'] = !empty($comm_row['schcomm_amount']) ? $comm_row['schcomm_amount'] : 0;
                
		$view_data['last_update_time'] = '';//$this->rep->getLastUpdateTime();
		
		
		//Fetch comm trend over the last 30 days
                $this->load->model('Commission_model', 'comm_model');
		$report = $this->comm_model->commTrends();
//                die('<pre>' . print_r($report, true));
//        $report = array(
//            array('date' => 'July 1, 2017', 'trans_value' => 1023400, 'latest_date' => date('F d, Y')),
//            array('date' => 'July 2, 2017', 'trans_value' => 2953145),
//            array('date' => 'July 3, 2017', 'trans_value' => 2513047),
//            array('date' => 'July 4, 2017', 'trans_value' => 800236),
//            array('date' => 'July 5, 2017', 'trans_value' => 1999632),
//            array('date' => 'July 6, 2017', 'trans_value' => 3000111),
//            array('date' => 'July 7, 2017', 'trans_value' => 2700033),
//        );
		$view_data['report_data'] = $this->report_formatter->_prepareAccountTotal($report);
		
		//$view_data['additional_js'] = 'App.dashboard2();';
		$view_data['additional_js'] = '';
		
		//Extra JS files to import
		$view_data['js_files'] = array(
            /* 'assets/lib/jquery-flot/jquery.flot.js',
            'assets/lib/jquery-flot/jquery.flot.pie.js',
			'assets/lib/jquery-flot/jquery.flot.resize.js',
            'assets/lib/jquery-flot/plugins/jquery.flot.orderBars.js',
			'assets/lib/jquery-flot/plugins/curvedLines.js', */
			'assets/lib/raphael/raphael-min.js',
			'assets/lib/morrisjs/morris.min.js'
        );
		
		//Extra CSS files to import
		$view_data['css_files'] = array(
			'assets/lib/morrisjs/morris.css'
		);
		
		//Extra JS code to execute (if any)
		$view_data['additional_js'] .= !empty($view_data['report_data']['additional_js']) ? $view_data['report_data']['additional_js'] : '';
		
		//die('<pre>' . print_r($view_data, true));
		
		$view_data['title'] = 'Dashboard';
		$view_data['skip_main_container_class'] = true;
        $this->_doRender('dashboard', $view_data);
    }   //END index()
	
	
	public function login($error = ''){
		$view_data = array();
		if (!empty($error)){
			$view_data['msg'] = $this->_renderErrorMsg('Invalid E-Mail/Password');
		}
		$this->load->view('login', $view_data);
	}
	
	
	public function forgot_password(){
		$this->load->view('forgot_password');
	}
	
	
	public function logout(){
		session_destroy();
		redirect('/');
	}
	
	
	public function putDummyData(){
		die('ready to insert dummy data');
		//Get all the list of accounts
		$this->load->model('Account_model', 'acc');
		$this->load->model('Accountbalance_model', 'accbal');
		$all_accounts = $this->acc->loadMultiple();
		$date = '2015-10-31 ';
		//die('<pre>ACCS --> ' . print_r($all_accounts, true));
		
		//Insert into the account balance table
		foreach ($all_accounts as $acc){
			for ($k = 0; $k <= 23; $k++){
				$datetime = $date . str_pad($k, 2, '0', STR_PAD_LEFT) . ':00:00';
				$data_array = array(
					'acc_id' => $acc['acc_id'],
					'accbal_amount' => (rand(1000000, 99999999)) * 100,
					'accbal_datetime' => "$datetime"
				);
				$this->accbal->insert($data_array);
			}
		}
		
		echo 'DONE INSERTING DATA';
	}	//END putDummyData()
    
}   //END class