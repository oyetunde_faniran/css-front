<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Bankaccount
 *
 * @author tundefaniran
 */
class Bankaccount extends UPL_Controller {
    
    
    function __construct() {
        parent::__construct();
        $this->load->model('Bank_model', 'bank');
        $this->load->model('Commission_model', 'comm');
        $this->load->model('Bankaccount_model', 'bam');
        $this->load->model('Projects_model', 'prj');
        $this->load->model('Deductions_model', 'dedm');
        
        $this->load->library('General_tools');
        $this->max_size = 1024 * 1024 * 5;
        $this->max_size_display = '5MB';
    }
    
    
    public function upload($success = '', $deposit_id_get = 0){
        $posted_data = $this->input->post();
        if(!empty($posted_data)){
            
            try {
                //Confirm that the fields were filled
                $this->load->library('form_validation');
//                $this->form_validation->set_rules('totalamount', 'Total amount', 'required');
                $this->form_validation->set_rules('accno', 'Account number', 'required');
                $this->form_validation->set_rules('accname', 'Account name', 'required');
                $this->form_validation->set_rules('depositdate', 'Deposit date', 'required');
                $this->form_validation->set_rules('bank', 'Bank', 'required|greater_than[0]');
                $this->form_validation->set_message('greater_than', '"{field}" field is required.');
                
                if($this->form_validation->run() === FALSE){
                    throw new Exception(validation_errors());
                }
                
                //Confirm that a file was successfully uploaded
                if($_FILES['breakdown']['error'] == 4){
                    throw new Exception('Please, upload a CSV file containing the breakdown of payment.');
                }
                
                $csv = $this->_getKeystoneCSVData($_FILES['breakdown']);
//                die('<pre>' . print_r($csv, true));

                //Confirm that the the amount is okay
//                if((string)$csv['total_amount'] != (string)$posted_data['totalamount']){
//                    throw new Exception("Total amount entered (&#8358;" . number_format($posted_data['totalamount'], 2) . ") is not the same as the sum of amount (&#8358;" . number_format($csv['total_amount'], 2) . ") in the uploaded file.");
//                }
                
//                die('<pre>' . print_r($posted_data, true) . print_r($_FILES, true) . print_r($csv, true));

                //Save the deposit summary

                $deposit_id = $this->_saveBankDeposit($posted_data['bank'], $posted_data['accno'], $posted_data['accname'], $csv['total_amount'], $posted_data['depositdate'], $csv['filename'], $_SESSION['user_id']);
                
                //Save the individual records in the uploaded file
//                $csv['data']['bdeposit_id'] = $deposit_id;
//                die('<pre>' . print_r($csv['data'], true));
                $this->bam->saveKeystoneBankStatement($csv['data'], $deposit_id);
                
                //Save the commission per project/client
//                $invalid_projects = $this->_saveCommission($csv['data'], $deposit_id, $posted_data['bank'], $posted_data['accno']);
//                if(!empty($invalid_projects)){
//                    throw new Exception('The following project keys in the uploaded file are not valid - (' . implode('', $invalid_projects) . ')');
//                }
//                redirect("bankaccount/upload/success/$deposit_id");
                redirect("bankaccount/assign2project/$deposit_id");
                
//                $this->file_uploaded->doUpload($_FILES['breakdown'], '');
            } catch (Exception $ex) {
                $view_data['msg'] = $this->_renderErrorMsg($ex->getMessage());
            }
        }
        
        if($success == 'success' && $deposit_id_get > 0 && !$_POST && empty($view_data['msg'])){
            $view_data['msg'] = $this->_renderSuccessMsg('Upload completed successfully. ' . anchor("reports/comms/{$deposit_id_get}", 'Click here to view the list of commission') . ' created from it.');
        }
        
        $view_data['js_files'] = array(
            'assets/lib/raphael/raphael-min.js',
            'assets/lib/morrisjs/morris.min.js',
            'assets/lib/datatables/js/jquery.dataTables.min.js',
            'assets/lib/datatables/js/dataTables.bootstrap.min.js',
            'assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.html5.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.flash.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.print.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.colVis.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js',
            'assets/lib/jquery.niftymodals/js/jquery.modalEffects.js',
            'assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js'
        );

        //Extra CSS files to import
        $view_data['css_files'] = array(
            'assets/lib/morrisjs/morris.css',
            'assets/lib/datatables/css/dataTables.bootstrap.min.css',
            'assets/lib/jquery.niftymodals/css/component.css',
            'assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css'
        );

        $view_data['additional_js'] = <<<JS
$("#report-table").dataTable({buttons:["copy","excel","pdf","print"],lengthMenu:[[25,50,100,200,-1],[25,50,100,200,"All"]],dom:"<'row am-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B>><'row am-datatable-body'<'col-sm-12'tr>><'row am-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"});
$(document).ready(function(){
    $(".datetimepicker").datetimepicker({
        autoclose:1,
        format: 'yyyy-mm-dd',
        componentIcon:".s7-date",
        navIcons:{
            rightIcon:"s7-angle-right",
            leftIcon:"s7-angle-left"
        }
    });
});                
JS;
        
        $view_data['banks'] = $this->bank->loadMultiple(0, 0, '', "\$this->db->order_by('bank_name');");
        $view_data['title'] = 'Bank Account Entries Breakdown Upload';
        $view_file = 'bankacc-entries/upload';
        $this->_doRender($view_file, $view_data);
    }
    
    
    
    protected function _getKeystoneCSVData($file){
//        die('asdf');
        //Error State
        if ($file['error'] != 0){
            throw new Exception('File not uploaded successfully.');
        }

        //File Size
        if ($file['size'] > $this->max_size){
            throw new Exception('The file size cannot be more than ' . $this->max_size_display . '.');
        }
        
        $csv = file_get_contents($file['tmp_name']);
        $records = explode("\n", $csv);
        $line_count = count($records);
//        die("<pre> ---'$line_count'" . print_r($records, true));
        $data = array();
        $total_amount = 0.00;
        $record_cursor = 0;
        $project_list = $this->_initProjectList();
        foreach($records as $record){
            $cols_temp = str_getcsv($record);
//            $cols_new = str_getcsv($record);
//            if($record_cursor == 19)
//            die("<pre>--$record--" . print_r($cols_temp, true) . "\n\n" . print_r($cols_new, true));
            $cols[1] = !empty($cols_temp[1]) ? trim($cols_temp[1], '') : '';    //Trans Date
            $cols[2] = !empty($cols_temp[2]) ? trim($cols_temp[2], '') : '';    //Value Date
            $cols[3] = !empty($cols_temp[3]) ? trim($cols_temp[3], '') : '';    //Reference
            $cols[4] = !empty($cols_temp[4]) ? trim($cols_temp[4], '') : '';    //Details
            $cols[5] = !empty($cols_temp[5]) ? trim(str_replace(',', '', $cols_temp[5])) : 0;    //Debit
            $cols[6] = !empty($cols_temp[6]) ? trim(str_replace(',', '', $cols_temp[6])) : 0;    //Credit
            $cols[7] = !empty($cols_temp[7]) ? trim(str_replace(',', '', $cols_temp[7])) : 0;    //Balance
            $project_key = $cols[8] = !empty($cols_temp[8]) ? trim($cols_temp[8], '') : '';    //Project Key
            
            //Ignore row 1 (column headers), row 2 (opening balance), last row (closing balance) and rows without any amount
            ++$record_cursor;
            if($record_cursor == 1 || $record_cursor == 2 || $record_cursor == $line_count || ($cols[5] == 0 && $cols[6] == 0)){
                continue;
            }
            
            if($cols[5] == 0){ //Debit Amount = 0 means CREDIT
                $dis_amount = $cols[6];
                $transtype = "1";
            } else {    //Credit Amount = 0 means DEBIT
                $dis_amount = $cols[5];
                $transtype = "2";
            }
            
            $data[] = [
                'transdate' => date('Y-m-d', strtotime($cols[1])),
                'valuedate' => date('Y-m-d', strtotime($cols[2])),
                'reference' => $cols[3],
                'details' => $cols[4],
                'amount' => $dis_amount,
                'balance' => $cols[7],
                'transtype' => "$transtype",
                'created_at' => date('Y-m-d H:i:s'),
                'project_id' => !empty($project_list[$project_key]) ? $project_list[$project_key] : 0
            ];
            $total_amount += $transtype == "1" ? $dis_amount : 0;
        }
        $filename_new = date('YmdHis-') . rand(1, 1000000) . '.csv';
        move_uploaded_file($file['tmp_name'], BANKDEPOSIT_UPLOAD_DIR . $filename_new);
        $ret_val = array(
            'data' => $data,
            'filename' => $filename_new,
            'total_amount' => $total_amount
        );
//        die('<pre>' . print_r($ret_val, true));
        return $ret_val;
    }
    
    
    
    protected function _getCSVData_old($file){
        //Error State
        if ($file['error'] != 0){
            throw new Exception('File not uploaded successfully.');
        }

        //File Size
        if ($file['size'] > $this->max_size){
            throw new Exception('The file size cannot be more than ' . $this->max_size_display . '.');
        }
        
        $csv = file_get_contents($file['tmp_name']);
        $records = explode("\n", $csv);
        $data = array();
        $total_amount = 0.00;
        $record_count = 0;
        foreach($records as $record){
            $cols_temp = explode(',', $record);
            $cols[1] = !empty($cols_temp[1]) ? trim($cols_temp[1]) : '';
            $cols[2] = !empty($cols_temp[2]) ? trim($cols_temp[2]) : '';
            if(++$record_count == 1 || empty($cols[1]) || empty($cols[2])){
                continue;
            }
            $data[trim($cols[1])] = !empty($data[$cols[1]]) ? ($data[$cols[1]] + trim($cols[2])) : trim($cols[2]);
            $total_amount += trim($cols[2]);
        }
        $filename_new = date('YmdHis-') . rand(1, 1000000) . '.csv';
        move_uploaded_file($file['tmp_name'], BANKDEPOSIT_UPLOAD_DIR . $filename_new);
        $ret_val = array(
            'data' => $data,
            'filename' => $filename_new,
            'total_amount' => $total_amount
        );
        return $ret_val;
    }
    
    
    
    
    protected function _saveCommission($data, $bankdeposit_id, $bank_code, $accno){
//        die('<pre>' . print_r($data, true) . "---$bank_code, $accno");
        foreach ($data as $prj_id => $amount){
            $this->comm->saveCustomCommission($prj_id, $amount, $accno, $bank_code, $bankdeposit_id);
        }
        return true;
    }



//    protected function _createCommissionFromStatement($data, $bankdeposit_id, $bank_code, $accno){
//        die('<pre>' . print_r($data, true));
//
//        //Apply any applicable deductions
//
//        foreach ($data as $prj_id => $amount){
//            $this->comm->saveCustomCommission($prj_id, $amount, $accno, $bank_code, $bankdeposit_id);
//        }
//        return true;
//    }
    
    
    
    protected function _saveCommission_old($data, $bankdeposit_id, $bank, $accno){
        $projects = $this->_initProjectList();
        $bank_deets = $this->bank->getBank($bank);
        if(empty($bank_deets)){
            throw new Exception('Invalid bank supplied');
        }
        
        $invalid_projects = array();
        foreach ($data as $project_key => $amount){
            if(empty($projects[$project_key])){
                $invalid_projects[] = $project_key;
                continue;
            }
            $prj_id = $projects[$project_key];
            $this->comm->saveCustomCommission($prj_id, $amount, $accno, $bank_deets['bank_code'], $bankdeposit_id);
        }
        return $invalid_projects;
    }
    
    
    
    
    protected function _initProjectList(){
        //Get the list of project keys as index pointing to the project IDs
        $this->load->model('Projects_model', 'prjm');
        $projects = $this->prjm->getProjects();
        $ret_val = array();
        foreach ($projects as $project){
            $ret_val[$project['prj_key']] = $project['prj_id'];
        }
        return $ret_val;
    }
    
    
    
    protected function _saveBankDeposit($bank, $accno, $accname, $amount, $deposit_date, $filename, $user_id){
        $date_logged = date('Y-m-d H:i:s');
        $data_array = array(
            'bank_id' => "$bank",
            'bdeposit_accno' => "$accno",
            'bdeposit_accname' => "$accname",
            'bdeposit_amount' => "$amount",
            'bdeposit_datedeposited' => "$deposit_date",
            'bdeposit_filename' => "$filename",
            'user_id' => "$user_id",
            'bdeposit_timelogged' => "$date_logged",
            'user_id_approver' => 0
        );
        $this->db->insert('bankdeposits', $data_array);
        $ret_val = $this->db->insert_id();
//        die('saved');
        return $ret_val;
    }
    
    
    
    public function listall(){
        $where_clause = '';
        
        //Get posted data if any
        $view_data['fromdate'] = $fromdate_temp = $this->input->post('fromdate');
        $view_data['todate'] = $todate_temp = $this->input->post('todate');

        //Date
        if(!empty($fromdate_temp) && !empty($todate_temp)){
            $where_clause .= " AND bd.bdeposit_timelogged BETWEEN '$fromdate_temp 00:00:00' AND '$todate_temp 23:59:59' ";
        } else {
            $today = date('Y-m-d');
            $where_clause .= " AND bd.bdeposit_timelogged BETWEEN '$today 00:00:00' AND '$today 23:59:59' ";
        }

        $where_clause_final = !empty($where_clause) ? 'TRUE ' . $where_clause : '';
            
        
        $view_data['trans'] = $this->bam->getBankDeposits($where_clause_final);
        
        $view_data['js_files'] = array(
            'assets/lib/raphael/raphael-min.js',
            'assets/lib/morrisjs/morris.min.js',
            'assets/lib/datatables/js/jquery.dataTables.min.js',
            'assets/lib/datatables/js/dataTables.bootstrap.min.js',
            'assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.html5.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.flash.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.print.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.colVis.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js',
            'assets/lib/jquery.niftymodals/js/jquery.modalEffects.js',
            'assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js'
        );

        //Extra CSS files to import
        $view_data['css_files'] = array(
            'assets/lib/morrisjs/morris.css',
            'assets/lib/datatables/css/dataTables.bootstrap.min.css',
            'assets/lib/jquery.niftymodals/css/component.css',
            'assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css'
        );

        $view_data['additional_js'] = <<<JS
$("#report-table").dataTable({buttons:["copy","excel","pdf","print"],lengthMenu:[[25,50,100,200,-1],[25,50,100,200,"All"]],dom:"<'row am-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B>><'row am-datatable-body'<'col-sm-12'tr>><'row am-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"});
$(document).ready(function(){
    $(".datetimepicker").datetimepicker({
        autoclose:!0,
        componentIcon:".s7-date",
        navIcons:{
            rightIcon:"s7-angle-right",
            leftIcon:"s7-angle-left"
        }
    });
});                
JS;
        
        $view_data['title'] = 'Bank Account Entries';
        $view_file = 'bankacc-entries/listall';
        $this->_doRender($view_file, $view_data);
    }
    
    
    
    public function assign2project($bdeposit_id_temp = 0){
        //Get the main bank statement record
        $bdeposit_id = (int)$bdeposit_id_temp;
        $view_data['bank_deposit'] = $bank_deposit = $this->bam->getBankDeposits(['bdeposit_id' => "$bdeposit_id"]);
//        die('<pre>' . print_r($bank_deposit, true));
        
        //Save project assignment if in the allowed state
        $posted_projects = $this->input->post('project');
        $current_status = isset($bank_deposit[0]['bdeposit_status']) ? $bank_deposit[0]['bdeposit_status'] : '0';
        $allowed_editable_status = [1, 3];
        $view_data['allow_edit'] = in_array($current_status, $allowed_editable_status);
        if(!empty($posted_projects) && $view_data['allow_edit']){
//            die('<pre>' . print_r($posted_data, true));
            foreach ($posted_projects as $bank_statement_record_id => $project_id){
                $this->bam->saveBankStatementProjectAssignment($bank_statement_record_id, $project_id);
            }
            redirect(current_url());
        }
        
        //Get the contents of the account statement
        $view_data['statement_records'] = $this->bam->getKeystoneBankStatement($bdeposit_id);
//        die('<pre>' . print_r($view_data['statement_records'], true));
        
        //Get the list of projects
        $view_data['projects'] = $this->prj->getProjects();
        
        $view_data['js_files'] = array(
            'assets/lib/raphael/raphael-min.js',
            'assets/lib/morrisjs/morris.min.js',
            'assets/lib/datatables/js/jquery.dataTables.min.js',
            'assets/lib/datatables/js/dataTables.bootstrap.min.js',
            'assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.html5.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.flash.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.print.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.colVis.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js',
            'assets/lib/jquery.niftymodals/js/jquery.modalEffects.js',
            'assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js'
        );

        //Extra CSS files to import
        $view_data['css_files'] = array(
            'assets/lib/morrisjs/morris.css',
            'assets/lib/datatables/css/dataTables.bootstrap.min.css',
            'assets/lib/jquery.niftymodals/css/component.css',
            'assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css'
        );

        $view_data['additional_js'] = <<<JS
$("#report-table").dataTable({buttons:["copy","excel","pdf","print"],lengthMenu:[[25,50,100,200,-1],[25,50,100,200,"All"]],dom:"<'row am-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B>><'row am-datatable-body'<'col-sm-12'tr>><'row am-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"});
$(document).ready(function(){
    $(".datetimepicker").datetimepicker({
        autoclose:!0,
        componentIcon:".s7-date",
        navIcons:{
            rightIcon:"s7-angle-right",
            leftIcon:"s7-angle-left"
        }
    });
});                
JS;
        
        if(empty($view_data['msg'])){
            $view_data['msg'] = '';//$this->_renderSuccessMsg('File has been uploaded successfully. Please, assign client/project to each record.');
        }
//        die('<pre>' . print_r($view_data, true));
        switch($current_status){
            case 2: $view_data['current_status_display'] = 'Pending Approval'; break;
            case 3: $view_data['current_status_display'] = 'Disapproved'; break;
            case 4: $view_data['current_status_display'] = 'Approved'; break;
            case 5: $view_data['current_status_display'] = 'Commission Generated'; break;
            case 1:
            default:
                $view_data['current_status_display'] = 'Assignment in Progress'; break;
        }
        $view_data['current_status'] = $current_status;
        $view_data['deposit_id'] = $bdeposit_id;
        $view_data['title'] = 'Assign Client/Project to Bank Statement Records';
        $view_file = 'bankacc-entries/assign2project';
        $this->_doRender($view_file, $view_data);
    }
    
    
    
    /**
     * 
     * @param int $bdeposit_id_temp The ID of the bank deposit
     * @param int $approval_type    1=Approve, 2=Disapprove
     * @return string               A JSON string
     * @throws Exception
     */
    public function approve_assign2project($bdeposit_id_temp = 0, $approval_type = 0){
//        die('set');
        //Get the main bank statement record
        $bdeposit_id = (int)$bdeposit_id_temp;
        $bank_deposit_list = $this->bam->getBankDeposits(['bdeposit_id' => "$bdeposit_id"]);
        try {
            if(empty($bank_deposit_list)){
                throw new Exception('Invalid bank deposit!');
            }
            $bank_deposit = $bank_deposit_list[0];
            $allowed_status4action = [];
            switch($approval_type){
                case 1: //For Approval: Must be in the "2: Pending Approval" or "3: Disapproved" state
                    $allowed_status4action = [2, 3];
                    $new_status = "4";
                    $action_text = "approved";
                    break;
                case 2:
                default: //For Disapproval: Must be in the "2: Pending Approval" state
                    $allowed_status4action = [2];
                    $new_status = "3";
                    $action_text = "disapproved";
                    break;
            }
//            die('<pre>' . $bank_deposit['bdeposit_status'] . '---'  . print_r($allowed_status4action, true));
            if(!in_array($bank_deposit['bdeposit_status'], $allowed_status4action)){
                throw new Exception("This bank statement project assignment can not be {$action_text}.");
            }
            
            //Get records on the bank statement that are yet to be assigned to a project
            if(!$this->_assignmentIsComplete($bdeposit_id)){
                throw new Exception("Assignment of project to individual records has not being completed. So, it can not be {$action_text}.");
            }
            
            //Now do the actual approval/disapproval
            $data_array = [
                'bdeposit_status' => "$new_status",
                'user_id_approver' => $_SESSION['user_id']
            ];
            $this->bam->updateBankStatement($bdeposit_id, $data_array);

            //Create commission for splitting from the approved assignment
            //$bdeposit_id
            $this->createCommissionFromStatement($bdeposit_id);

            $retval = [
                'status' => true,
                'msg' => ucfirst($action_text) . ' successfully.',
            ];
        } catch (Exception $ex) {
            $retval = [
                'status' => false,
                'msg' => $ex->getMessage(),
            ];
        }
        header('Content-type: application/json');
        echo json_encode($retval);
    }
    
    
    
    /**
     * 
     * @param int $bdeposit_id_temp The ID of the bank deposit
     * @param int $approval_type    1=Approve, 2=Disapprove
     * @return string               A JSON string
     * @throws Exception
     */
    public function forward_assign2project($bdeposit_id_temp = 0){
        //Get the main bank statement record
        $bdeposit_id = (int)$bdeposit_id_temp;
        $bank_deposit_list = $this->bam->getBankDeposits(['bdeposit_id' => "$bdeposit_id"]);
        $action_text = "forwarded for approval";
        $new_status = '2';
        try {
            if(empty($bank_deposit_list)){
                throw new Exception('Invalid bank deposit!');
            }
            $bank_deposit = $bank_deposit_list[0];
            $allowed_status4action = [1, 3];    //1=Assignment in Progress, 3=Disapproved
//            die('<pre>' . $bank_deposit['bdeposit_status'] . '---'  . print_r($allowed_status4action, true));
            if(!in_array($bank_deposit['bdeposit_status'], $allowed_status4action)){
                throw new Exception("This bank statement project assignment can not be forwarded.");
            }
            
            //Get records on the bank statement that are yet to be assigned to a project
            if(!$this->_assignmentIsComplete($bdeposit_id)){
                throw new Exception("Assignment of project to individual records has not being completed. So, it can not be {$action_text}.");
            }
            
            //Now do the actual approval/disapproval
            $data_array = ['bdeposit_status' => "$new_status"];
            $this->bam->updateBankStatement($bdeposit_id, $data_array);
            $retval = [
                'status' => true,
                'msg' => ucfirst($action_text) . ' successfully.',
            ];
        } catch (Exception $ex) {
            $retval = [
                'status' => false,
                'msg' => $ex->getMessage(),
            ];
        }
        header('Content-type: application/json');
        echo json_encode($retval);
    }
    
    
    
    protected function _assignmentIsComplete($bdeposit_id){
        $where_array = [
            'project_id' => "0",
            'transtype' => "1"
        ];
        $unassigned_records = $this->bam->getKeystoneBankStatement($bdeposit_id, $where_array);
        $ret_val = empty($unassigned_records);
        return $ret_val;
    }
    
    
    public function createCommissionFromStatement($deposit_id = 0, $project_id = 0, $monthly_aggregation = 0, $dis_start_date = '', $dis_end_date = ''){
//        https://uplfront.css.ng/bankaccount/createCommissionFromStatement/0/1/2020-07-01/2020-07-31
//        ini_set('display_errors', 'on');
        //Get statements that have been approved and are yet to be converted to commission entries
        $deposits = $this->bam->getStatements4CommissionInit($deposit_id, $project_id);
//        die('<pre>' . print_r($deposits, true));
        
        foreach ($deposits as $d){
            //$monthly_aggregation would be true for transactions to be aggregated monthly, which would be triggered by a cron
            //job on the first day of a new month
            if($monthly_aggregation == 1){
                $timestamp = time() - 86400;
                $start_date = !empty($dis_start_date) ? "$dis_start_date 00:00:00" : date('Y-m-01 00:00:00', $timestamp);   //First day of the month in which yesterday falls
                $end_date = !empty($dis_end_date) ? "$dis_end_date 23:59:59" : date('Y-m-d 23:59:59', $timestamp);      //Last day of the month in which yesterday falls
                $where = [
//                    'p.prj_when2split' => '2',
                    'bdc.created_at >=' => "$start_date",
                    'bdc.created_at <=' => "$end_date"
                ];
            } else {
                $where = ['p.prj_when2split' => '1'];
            }
            //die("$deposit_id = 0, $project_id = 0, $monthly_aggregation = 0, $dis_start_date = '', $dis_end_date = ''");
            $deposit_records = $this->bam->getStatementRecords4CommissionInit($d['bdeposit_id'], $project_id, $where, $monthly_aggregation);
//            die('<pre>' . print_r($deposit_records, true) . "\n\n\n</pre>");
            $dis_records = [];
            $deductions = [];
            $bdeposit_content_ids = '';
            foreach($deposit_records as $record){
                //Don't process if the amount is less than a set threshold
                if($record['total_amount'] < MIN_COMM_AMOUNT){
                    continue;
                }
                //Calculate all applicable deductions and remove from the amount
                $after_deduction = $this->applyDeductions($record['project_id'], $record['total_amount']);

                //Prepare a list of the project and amount for saving
                $dis_records[$record['project_id']] = $after_deduction['final_amount'];

                $bdeposit_content_ids .= ',' . $record['bdeets_id'];
                //die("<pre>" . print_r($record, true));

                //Aggregate previously calculated deductions with the newly calculated ones
                $deductions = $this->aggregateDeductions($deductions, $after_deduction['all_deductions'], $deposit_id);
            }
//            die("<pre>DEDUCTIONS: " . print_r($deductions, true) . "\nALL: " . print_r($dis_records, true));

            //Create the actual commission entries
            //($data, $bankdeposit_id, $bank_code, $accno)
            //die('<pre>' . print_r($dis_records, true));

            $this->_saveCommission($dis_records, $d['bdeposit_id'], $d['bank_code'], $d['bdeposit_accno']);

            //Save deductions configured to be processed at month end for processing later
            $deductions_final = $this->saveNecessaryDeductions4Later($deductions, $deposit_id);

            //Create commission entries for deductions too
            if(!empty($deductions_final)){
                $dis_deposit_id = $d['bdeposit_id'] > 0 ? $d['bdeposit_id'] : 1;
                $this->_saveCommission($deductions_final, $d['bdeposit_id'], $d['bank_code'], $d['bdeposit_accno']);
            }

            //Update the bank deposit content records to processed
            $this->bam->updateBankDepositContentStatus($bdeposit_content_ids);
            
            //Update the bank deposit status to "Commission Created"
//            $data_array = ['bdeposit_status' => "5"];
//            $this->bam->updateBankStatement($d['bdeposit_id'], $data_array);
        }
    }


    protected function applyDeductions($project_id, $amount){
        $deductions = $this->dedm->getProjectAssignedDeductions($project_id);
        $total_deductions = 0;
        $all_deductions = [];
        foreach($deductions as $d){
            switch($d['ded_type']){
                case 1: //1=Fixed Amount,2=Percentage,3=per Mille
                    $total_deductions += $all_deductions[$d['prj_id']] = $d['ded_amount'];
                    break;
                case 2: //2=Percentage
                    $total_deductions += $all_deductions[$d['prj_id']] = round($amount * ($d['ded_amount'] / 100), 2);

                    break;
                case 3: //3=per Mille
                    $total_deductions += $all_deductions[$d['prj_id']] = round($d['ded_amount'] * ceil($amount / 1000), 2);
                    break;
            }
        }
        return [
            'all_deductions' => $all_deductions,
            'final_amount' => $amount - $total_deductions
        ];
    }



    protected function aggregateDeductions($deductions, $new_deductions, $deposit_id){
        foreach ($new_deductions as $prj_id => $amount){
            $deductions[$prj_id] = isset($deductions[$prj_id]) ? ($deductions[$prj_id] + $amount) : $amount;
        }
        return $deductions;
    }



    protected function saveNecessaryDeductions4Later($deductions_temp, $deposit_id){
        $deductions = [];
        //Keep deductions to be processed at month end for later.
        foreach ($deductions_temp as $prj_id => $amount){
            $project_deets = $this->prj->getProject($prj_id);

            switch ($project_deets['prj_when2split']){
                case 1: //Spilt per transaction
                    $deductions[$prj_id] = $amount;
                    break;
                case 2: //Split at the end of the month
                    //Insert into the db for processing later
                    $data[] = [
                        'transdate' => date('Y-m-d'),
                        'valuedate' => date('Y-m-d'),
                        'reference' => 'Computed-Deduction-' . date('Y-m-d'),
                        'details' => 'Deduction computed for immediate commission generation from bank statement',
                        'amount' => $amount,
                        'balance' => 0,
                        'transtype' => "1",
                        'created_at' => date('Y-m-d H:i:s'),
                        'project_id' => $prj_id,
                        'is_computed_deduction' => "1"
                    ];
                    $this->bam->saveKeystoneBankStatement($data, $deposit_id);
                    break;
            }
        }
        return $deductions;
    }
    
    
    
}
