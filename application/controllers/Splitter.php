<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Splitter
 *
 * @author oyetunde.faniran
 */
class Splitter extends UPL_Controller {
    
    
    
    function __construct() {
        parent::__construct();
        $this->load->model('Splitting_model', 'splitm');
    }
    
    
    
    public function getBeneficiaries($id = 0){
//        sleep(2);
        if (!$this->input->is_ajax_request()) {
            die(':(');
        }
        if($id > 0){
            $c = $this->clm->getClient($id);
            $clients = array(
                'status' => true,
                'id' => $c['client_id'],
                'name' => $c['client_name'],
                'email' => $c['client_email'],
                'code' => $c['client_code'],
                'nibss_clientid' => $c['client_nibbs_clientid'],
                'computecommission' => $c['client_computecommission'],
                'charge' => $c['client_transcharge'],
                'charge_type' => $c['client_charge_type'],
                'charge_mode' => $c['client_charge_mode'],
                'deductcharge' => $c['client_deductchargefromamount'],
                'url' => $c['client_statusupdate_url'],
                'status' => $c['client_enabled'],
                'date_added' => $c['client_dateadded'],
                'lastupdated' => $c['client_datelastupdated']
            );
        } else {
            $clients_temp = $this->clm->getClients(false);
            $clients = array();
            foreach($clients_temp as $c){
                $clients[] = array(
                    'id' => $c['client_id'],
                    'name' => $c['client_name'],
                    'email' => $c['client_email'],
                    'code' => $c['client_code'],
                    'nibss_clientid' => $c['client_nibbs_clientid'],
                    'computecommission' => $c['client_computecommission'],
                    'charge' => $c['client_transcharge'],
                    'charge_type' => $c['client_charge_type'],
                    'charge_mode' => $c['client_charge_mode'],
                    'deductcharge' => $c['client_deductchargefromamount'],
                    'url' => $c['client_statusupdate_url'],
                    'status' => $c['client_enabled'],
                    'date_added' => $c['client_dateadded'],
                    'lastupdated' => $c['client_datelastupdated']
                );
            }
        }
        
        $ret_val = json_encode($clients);
        header('Content-type: application/json');
        die($ret_val);
    }
    
    
    public function updateSplit($prj_id_temp = 0){
        $prj_id = (int)$prj_id_temp;
        //Get the posted data
        $ben_share_array = $this->input->post('ben_share');
        $type = $this->input->post('type');
        $total = 0.0;
        try {
            $big_data_array = array();
            $date_added = date('Y-m-d H:i:s');
            
            //Get each beneficiary ID and their respective share
            foreach ($ben_share_array as $ben_id => $share_temp){
                $share = (float)$share_temp;
                if($share <= 0){
                    continue;
                }
                $total += $share;
                $big_data_array[] = array(
                    'prj_id' => "$prj_id",
                    'ben_id' => "$ben_id",
                    'form_share' => "$share",
                    'form_type' => "$type",
                    'form_dateadded' => "$date_added"
                );
            }
            
            //Confirm that everything totals 100%
            if($type == 2 && $total != 100.00){
                throw new Exception('Total share must be equal to 100%');
            }
            
            //Clear all the existing config for this project first
            $this->splitm->delete(false, array('prj_id' => $prj_id));
            
//            echo 'deleted';
            
            //Save the new split config
            $this->splitm->insert_batch($big_data_array);
            
            //Return value
            $ret_val_temp = array(
                'status' => true,
                'msg' => $this->_renderSuccessMsg('Split configuration saved successfully.')
            );
        } catch (Exception $ex) {
            $ret_val_temp = array(
                'status' => false,
                'msg' => $this->_renderErrorMsg($ex->getMessage())
            );
        }
        $ret_val = json_encode($ret_val_temp);
        header('Content-type: application/json');
        echo $ret_val;
    }
    
    
}
