<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Client
 *
 * @author oyetunde.faniran
 */
class Beneficiary extends UPL_Controller {
    
    
    function __construct() {
        parent::__construct();
        $this->load->model('Beneficiary_model', 'ben');
        $this->load->model('Bank_model', 'bank');
    }
    
    
    public function index(){
        $this->listall();
    }
    
    
    public function listall(){
        $view_data['records'] = $this->ben->getBeneficiaries(false);
//        die('<pre>' . print_r($view_data['clients'], true));
        
        $view_data['js_files'] = array(
            'assets/lib/raphael/raphael-min.js',
            'assets/lib/morrisjs/morris.min.js',
            'assets/lib/datatables/js/jquery.dataTables.min.js',
            'assets/lib/datatables/js/dataTables.bootstrap.min.js',
            'assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.html5.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.flash.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.print.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.colVis.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js',
            'assets/lib/jquery.niftymodals/js/jquery.modalEffects.js'
        );

        //Extra CSS files to import
        $view_data['css_files'] = array(
            'assets/lib/morrisjs/morris.css',
            'assets/lib/datatables/css/dataTables.bootstrap.min.css',
            'assets/lib/jquery.niftymodals/css/component.css'
        );

        $view_data['additional_js'] = <<<JS
$("#report-table").dataTable({buttons:["copy","excel","pdf","print"],lengthMenu:[[25,50,100,200,-1],[25,50,100,200,"All"]],dom:"<'row am-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B>><'row am-datatable-body'<'col-sm-12'tr>><'row am-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"});
JS;


        $view_data['banks'] = $this->bank->loadMultiple(0, 0, '', "\$this->db->order_by('bank_name');");
        $view_data['title'] = 'List of Beneficiaries';
        $view_file = 'beneficiaries/ben-listall';
        $this->_doRender($view_file, $view_data);
    }
 
    
    
    public function addBeneficiary(){
        if (!$this->input->is_ajax_request()) {
            die(':(');
        }
        
        $data_temp = file_get_contents('php://input');
        $posted_data = array();
        parse_str($data_temp, $posted_data);

        
        try {
            $this->load->library('form_validation');
            $this->form_validation->set_data($posted_data);
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[comm_beneficiaries.ben_email]');
            $this->form_validation->set_rules('accname', 'Account name', 'required');
            $this->form_validation->set_rules('accno', 'Account number', 'required');
            $this->form_validation->set_rules('bank', 'Bank', 'required|greater_than[0]');
            $this->form_validation->set_message('greater_than', 'The {field} field is required.');
            $this->form_validation->set_message('is_unique', 'The {field} you entered has been used by someone else.');
            
            if($this->form_validation->run() === FALSE){
                throw new Exception(validation_errors());
            }
            
            $date_added = date('Y-m-d H:i:s');
            $data_array = array(
                'ben_name' => $posted_data['name'],
                'ben_email' => $posted_data['email'],
                'ben_accname' => $posted_data['accname'],
                'ben_accno' => $posted_data['accno'],
                'bank_id' => $posted_data['bank'],
                'user_id' => (!empty($_SESSION['user_id']) ? $_SESSION['user_id'] : 0),
                'ben_dateadded' => $date_added
            );
            $this->ben->insert($data_array);
            
            $retval_temp = array(
                'status' => true,
                'msg' => $this->_renderSuccessMsg('Beneficiary added successfully. Reloading the list now...')
            );
            
        } catch (Exception $ex) {
            $retval_temp = array(
                'status' => false,
                'msg' => $this->_renderErrorMsg($ex->getMessage())
            );
        }
        $ret_val = json_encode($retval_temp);
        header('Content-type: application/json');
        die($ret_val);
    }
    
    
    
    public function getBeneficiary($id_temp = 0){
//        sleep(2);
        $id = (int)$id_temp;
        if (!$this->input->is_ajax_request()) {
            die(':(');
        }
        $c = $this->ben->getBeneficiary($id);
        if(!empty($c)){
            $data = array(
                'id' => $c['ben_id'],
                'name' => $c['ben_name'],
                'email' => $c['ben_email'],
                'accname' => $c['ben_accname'],
                'accno' => $c['ben_accno'],
                'bank' => $c['bank_id'],
                'enabled' => $c['ben_enabled']
            );
            $ret_val_temp = array(
                'status' => true,
                'msg' => 'OK',
                'bdata' => $data
            );
        } else {
            $ret_val_temp = array(
                'status' => false,
                'msg' => 'Not Found',
                'bdata' => array()
            );
        }
        
        $ret_val = json_encode($ret_val_temp);
        header('Content-type: application/json');
        echo($ret_val);
    }


    public function updateBeneficiary($id_temp = 0){
        if (!$this->input->is_ajax_request()) {
            die(':(');
        }

        $data_temp = file_get_contents('php://input');
        $posted_data = array();
        parse_str($data_temp, $posted_data);
        $id = (int)$id_temp;


        try {
            $this->load->library('form_validation');
            $this->form_validation->set_data($posted_data);
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('accname', 'Account name', 'required');
            $this->form_validation->set_rules('accno', 'Account number', 'required');
            $this->form_validation->set_rules('bank', 'Bank', 'required|greater_than[0]');
            $this->form_validation->set_message('greater_than', 'The {field} field is required.');
//            $this->form_validation->set_message('is_unique', 'The {field} you entered has been used by someone else.');

            if($this->form_validation->run() === FALSE){
                throw new Exception(validation_errors());
            }

            $enabled = isset($posted_data['enabled']) ? ($posted_data['enabled'] == '1' ? "1" : "0") : "0";

            $date_added = date('Y-m-d H:i:s');
            $data_array = array(
                'ben_name' => $posted_data['name'],
                'ben_email' => $posted_data['email'],
                'ben_accname' => $posted_data['accname'],
                'ben_accno' => $posted_data['accno'],
                'bank_id' => $posted_data['bank'],
                'user_id' => (!empty($_SESSION['user_id']) ? $_SESSION['user_id'] : 0),
                'ben_enabled' => "$enabled"
            );
            $this->ben->setValues($data_array);
            $this->ben->ben_id = $id;
            $this->ben->update();

            $retval_temp = array(
                'status' => true,
                'msg' => $this->_renderSuccessMsg('Beneficiary updated successfully. Reloading the list now...')
            );

        } catch (Exception $ex) {
            $retval_temp = array(
                'status' => false,
                'msg' => $this->_renderErrorMsg($ex->getMessage())
            );
        }
        $ret_val = json_encode($retval_temp);
        header('Content-type: application/json');
        die($ret_val);
    }
    
    
}