<?php
/**
 * Handles user management
 *
 * @author Faniran, Oyetunde
 */
class User extends UPL_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('sys-admin/user_model', 'user');
        $this->load->model('sys-admin/usergroup_model', 'ugroup');
    }



    /**
     * Handles adding of a new user
     * @param string $success   Should contain the string 'success' to show that a new user was successfully added
     * @param int $user_id     The ID of the newly added user
     * @throws Exception
     */
    public function add($success = '', $user_id = 0){
//        die('<pre>' . print_r($_SESSION, true));
        $posted_data = $this->input->post(null, false);

        //Confirm that a form was submitted
        if ($posted_data){
            try {
                //Set validation rule(s)
                $this->form_validation->set_rules('firstname', 'first name', 'required');
                $this->form_validation->set_rules('surname', 'surname', 'required');
                //$this->form_validation->set_rules('lga', 'LGA', 'required|greater_than[-1]');
                $this->form_validation->set_rules('usergroup', 'user group', 'required|greater_than[0]');
                $this->form_validation->set_rules('email', 'e-mail', 'required|valid_email|is_unique[' . UPL_DB_TABLE_PREFIX . 'users_login.userlog_email]');
                $this->form_validation->set_message('is_unique', "The %s you entered is already in use by another user.");
                $this->form_validation->set_message('greater_than', "The %s field is required.");

                //Run the validation method and throw an Exception if there's a problem
                if (!$this->form_validation->run()) {
                    throw new Exception(validation_errors());
                }

                //Get the posted data and insert
                $firstname = $this->input->post('firstname', true);
                $surname = $this->input->post('surname', true);
                $username = $email = $this->input->post('email', true);
                $phone = $this->input->post('phone', true);
                $lga = 0;//$this->input->post('lga', true);
                $group_id = $this->input->post('usergroup', true);
                //$court_id = $this->input->post('court_id', true);
                
                //Generate a GA code
                $this->load->library('Google_authenticator');
                $ga_secret = $this->google_authenticator->createSecret();

                //Do the actual insertion here
                $this->load->library('general_tools');
                $password = $this->general_tools->getRandomString(rand(10, 20));
                $user_id = $this->user->addUser($firstname, $surname, $phone, $email, $username, $password, $lga, $group_id, $ga_secret);

                //Show an error message if insertion failed
                if(empty($user_id)){
                    throw new Exception('Unable to add the user. Please, try again.');
                }
                
                //If the user being created is a commission beneficiary, which is sent as an AJAX request,
                //then update the beneficiary table with the user ID
                if($this->input->is_ajax_request()){
                    $where = array(
                        'ben_email' => $email
                    );
                    $data_array = array(
                        'ben_user_id' => "$user_id"
                    );
                    $this->db->where($where)
                             ->update('comm_beneficiaries', $data_array);
                } else {
                    //Redirect to this page to clear the post data and display a success message
                    redirect("sys-admin/user/add/success/$user_id");
                }
            } catch (Exception $e) {
                $msg = $this->_renderErrorMsg($e->getMessage());
            }
        }   //END if form was submitted

        //Get the message to display and format it if any
        if (empty($msg) && !empty($success) && !empty($user_id)){
            $user_deets = $this->user->getUser($user_id);
            $msg_part = isset($user_deets['userlog_email']) ? " ({$user_deets['userlog_email']})" : '';
            $msg_temp = "User created successfully and the password has been sent to the email address specified." . $msg_part;
            $msg = $this->_renderSuccessMsg($msg_temp);
        }
        if(!empty($msg)){
            $view_data['msg'] = $msg;
        }

        //Get the list of available user groups
        $view_data['groups'] = $this->ugroup->getUserGroups();


        //Get the list of LGAs
        //$this->load->model('lga_model', 'lga');
        $view_data['lgas'] = array();//$this->lga->loadMultiple(0, 0, "lga_enabled = '1'", "\$this->db->order_by('lga_name');");


        //Render the page
        $view_data['title'] = 'Add User';
        $this->_doRender('sys-admin/user/user-add', $view_data);
    }   //END add()



    /**
     * Handles listing of existing user groups
     */
    public function view(){
        //Get existing user groups
        $view_data['users'] = $this->user->getUsers(false);
        //die('<pre>' . print_r($view_data['users'], true));

        $view_data['js_files'] = array(
            'assets/lib/datatables/js/jquery.dataTables.min.js',
            'assets/lib/datatables/js/dataTables.bootstrap.min.js',
			'assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.html5.js',
			'assets/lib/datatables/plugins/buttons/js/buttons.flash.js',
			'assets/lib/datatables/plugins/buttons/js/buttons.print.js',
			'assets/lib/datatables/plugins/buttons/js/buttons.colVis.js',
			'assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js'
        );
		
		$view_data['css_files'] = array(
			'assets/lib/datatables/css/dataTables.bootstrap.min.css',
		);
		
		$view_data['additional_js'] = 'App.dataTables();';

        //Render the page
        $view_data['title'] = 'View Users';
        $this->_doRender('sys-admin/user/user-view', $view_data);
    }   //END view




    /**
     * Handles editing of a user whose ID is specified
     * @param int $user_id      The ID of the user to edit
     * @param string $success   Should contain the string 'success' to show that a the edit was successful
     * @throws Exception
     */
    public function edit($user_id = 0, $success = ''){
        $posted_data = $this->input->post(null, false);

        //Confirm that a form was submitted
        if ($posted_data){
            try {
                //Set validation rule(s)
                $this->form_validation->set_rules('firstname', 'first name', 'required');
                $this->form_validation->set_rules('surname', 'surname', 'required');
                //$this->form_validation->set_rules('lga', 'LGA', 'required|greater_than[-1]');
                $this->form_validation->set_rules('usergroup', 'user group', 'required|greater_than[0]');
                $this->form_validation->set_rules('email', 'e-mail', 'required|valid_email');
                $this->form_validation->set_message('is_unique', "The %s you entered is already in use by another user.");
                $this->form_validation->set_message('greater_than', "The %s field is required.");

                //Run the validation method and throw an Exception if there's a problem
                if (!$this->form_validation->run()) {
                    throw new Exception(validation_errors());
                }

                //Get the posted data and update
                $firstname = $this->input->post('firstname', true);
                $surname = $this->input->post('surname', true);
                $username = $email = $this->input->post('email', true);
                $phone = $this->input->post('phone', true);
                $lga = 0;//$this->input->post('lga', true);
                $group_id = $this->input->post('usergroup', true);
                //$court_id = $this->input->post('court_id', true);
                $active_temp = $this->input->post('active', true);
                $active = !empty($active_temp) ? '1' : '0';
                $reset_password = $this->input->post('password_reset', true);


                //Check for the uniqueness of the new email of the user
                $is_unique = $this->user->isUniqueEmail($email, $user_id);
                if (!$is_unique){
                    throw new Exception("The e-mail you entered is already in use by another user.");
                }

                //Generate a new password if need be
                if($reset_password){
                    $this->load->library('general_tools');
                    $password = $this->general_tools->getRandomString(rand(10, 20));
                } else {
                    $password = '';
                }

                //All's well, so update
                $updated = $this->user->updateUser($user_id, $firstname, $surname, $phone, $email, $username, $password, $lga, $group_id, $active);

                //Show an error message if insertion failed
                if (!$updated){
                    throw new Exception('It seems you did not make any change, so nothing was saved.');
                }

                //Redirect to this page to clear the post data and display a success message
                redirect("sys-admin/user/edit/$user_id/success");
            } catch (Exception $e) {
                $msg = $this->_renderErrorMsg($e->getMessage());
            }
        }   //END if form was submitted

        //Get the details of the user
        $view_data['user_deets'] = $this->user->getUser($user_id, false);
        //die('<pre>' . print_r($view_data['user_deets'], true));

        //Get the message to display and format it if any
        if (empty($msg) && !empty($success)){
            $msg_temp = 'Your changes were successfully saved.';
            $msg = $this->_renderSuccessMsg($msg_temp);
        }
        if(!empty($msg)){
            $view_data['msg'] = $msg;
        }

        //Get the list of available user groups for use in a drop-down
        $view_data['groups'] = $this->ugroup->getUserGroups();

        //Get the list of LGAs
        //$this->load->model('lga_model', 'lga');
        $view_data['lgas'] = array();//$this->lga->loadMultiple(0, 0, "lga_enabled = '1'", "\$this->db->order_by('lga_name');");

        //Render the page
        $view_data['title'] = 'Edit User';
        $this->_doRender('sys-admin/user/user-edit', $view_data);
    }   //END edit()




    /**
     * Handles editing of a user whose ID is specified
     * @param int $user_id      The ID of the user to edit
     * @param string $success   Should contain the string 'success' to show that a the edit was successful
     * @throws Exception
     */
    public function update_profile($success = ''){
        $user_id = !empty($_SESSION['user_id']) ? $_SESSION['user_id'] : 0;
        $posted_data = $this->input->post(null, false);
        //die('<!--BEFORE: ' . print_r($_SESSION, true));

        //Confirm that a form was submitted
        if ($posted_data){
            try {
                //Get the posted data and update
                $firstname = $this->input->post('firstname', true);
                $surname = $this->input->post('surname', true);
                $username = $email = $this->input->post('email', true);
                $phone = $this->input->post('phone', true);
                $newpassword = $this->input->post('newpassword', true);
                $cnewpassword = $this->input->post('cnewpassword', true);
                $curpassword = $this->input->post('password', true);

                //Set validation rule(s)
                $this->form_validation->set_rules('firstname', 'first name', 'required');
                $this->form_validation->set_rules('surname', 'surname', 'required');
                $this->form_validation->set_rules('email', 'e-mail', 'required|valid_email');
                //$this->form_validation->set_message('min_length', "The %s field is required.");
                
                if ($_SESSION['first_timer']){
                    $this->form_validation->set_rules('newpassword', 'new password', 'required|min_length[8]');
                }

                //Set more validation rules if the user is trying to change the password
                if (!empty($newpassword)){
                    $this->form_validation->set_rules('newpassword', 'new password', 'min_length[8]');

                    //Confirm that the new password is equal to the confirmation password
                    if ($newpassword != $cnewpassword){
                        throw new Exception("The new password is not the same as the confirmation password.");
                    }
                }

                //Run the validation method and throw an Exception if there's a problem
                if (!$this->form_validation->run()) {
                    throw new Exception(validation_errors());
                }

                //Check for the uniqueness of the new email of the user
                $is_unique = $this->user->isUniqueEmail($email, $user_id);
                if (!$is_unique){
                    throw new Exception("The e-mail you entered is already in use by another user.");
                }

                //Get the current user profile
                $cur_profile = $this->user->getUser($user_id, false);
                
                //Confirm that the current password that was entered is OK
                $user_deets = $this->user->loginUser($cur_profile['userlog_username'], $curpassword, true, false);
                if (empty($user_deets)){
                    //die("<!--\n\n\n\n\nEXCEPTION: " . print_r($_SESSION, true) . print_r($user_deets, true));
                    throw new Exception("The current password you entered is invalid.");
                }
                //die('<!--\n\n\n\n\nAFTER: ' . print_r($_SESSION, true) . print_r($user_deets, true));

                //All's well, so update
                $updated = $this->user->updateUserPersonalProfile($user_id, $firstname, $surname, $phone, $email, $username, $newpassword, $user_deets['usergroup_id']);

                //Show an error message if insertion failed
                if (!$updated){
                    throw new Exception('It seems you did not make any change, so nothing was saved.');
                }
                
                //Update session variables to show that this user is no longer a first timer after successfully updating his/her profile
                $_SESSION['ga_verified'] = $_SESSION['first_timer'] ? false : $_SESSION['ga_verified'];
                $_SESSION['first_timer'] = false;

                //Redirect to this page to clear the post data and display a success message
                redirect("sys-admin/user/update_profile/$user_id/success");
            } catch (Exception $e) {
                $msg = $this->_renderErrorMsg($e->getMessage());
            }
        }   //END if form was submitted

        //Get the details of the user
        $view_data['user_deets'] = $this->user->getUser($user_id, false);
        $this->load->library('Google_authenticator');
        //die($this->google_authenticator->createSecret());
        $view_data['ga_qrcode_url'] = $this->google_authenticator->getQRCodeGoogleUrl(DEFAULT_TITLE, $view_data['user_deets']['user_ga_secretkey'], $title = null);

        //Get the message to display and format it if any
        if (empty($msg)){
            if ($success == 'success'){
                $msg_temp = 'Your changes were successfully saved.';
                $msg = $this->_renderSuccessMsg($msg_temp);
            } elseif ($success == 'firsttimer') {
                $msg_temp = 'Please, create a new password and set-up your Google Authenticator now. You won\'t be able to log in again if you do not do so now.';
                $msg = $this->_renderWarningMsg($msg_temp);
            }
        }
        if(!empty($msg)){
            $view_data['msg'] = $msg;
        }

        //Render the page
        $view_data['title'] = 'Profile Update Page';
        $this->_doRender('sys-admin/user/user-update-profile', $view_data);
    }   //END edit()

    
    
    public function verify_ga($error = ''){
//        die('<pre>' . print_r($_SESSION, true));
        $posted_data = $this->input->post(null, false);

        //die('<pre>' . print_r($posted_data, true));

        //Confirm that a form was submitted
        if ($posted_data) {
            $code = $this->input->post('ga_code');
            try {
                $this->load->library('Google_authenticator', 2);
//                die("{$_SESSION['user_ga_secretkey']}, $code");
                $valid_code = $this->google_authenticator->verifyCode($_SESSION['user_ga_secretkey'], $code);
                if ($valid_code){
                    $_SESSION['ga_verified'] = true;
                    redirect('');
                }
                
                //Track the number of failed attempts at GA code entry. Disable the account if GA_MAX_FAILED_ATTEMPTS is reached
                $_SESSION['ga_attempts'] = !empty($_SESSION['ga_attempts']) ? ($_SESSION['ga_attempts'] + 1) : 1;
                $max_attempts = defined('GA_MAX_FAILED_ATTEMPTS') ? GA_MAX_FAILED_ATTEMPTS : 3;
                if($_SESSION['ga_attempts'] >= $max_attempts){
                    $this->user->lockAccount($_SESSION['user_id']);
                    redirect('logout');
                }
                
                redirect('sys-admin/user/verify_ga/error');
            } catch (Exception $e) {
                //$msg = $this->_renderErrorMsg($e->getMessage());
            }
        }   //END if form was posted
        
        if ($error == 'error'){
            $view_data['msg'] = $this->_renderErrorMsg('Invalid GA Code');
        }
        
        $view_data['page_title'] = 'Verify your GA Code';
        $this->load->view('verify-ga', $view_data);

    }   //END verify_ga()




}   //END class
