<?php
/**
 * Handles user group management
 *
 * @author Faniran, Oyetunde
 */
class Task extends UPL_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('sys-admin/task_model', 'task');
        $this->load->model('sys-admin/module_model', 'module');
        $this->task_table = UPL_DB_TABLE_PREFIX . 'tasks';
    }



    /**
     * Handles adding of a new task
     * @param string $success   Should contain the string 'success' to show that a new task was successfully added
     * @param int $task_id      The ID of the newly added task
     * @throws Exception
     */
    public function add($success = '', $task_id = 0){
        $posted_data = $this->input->post(null, false);

        //Confirm that a form was submitted
        if ($posted_data){
//            die('<pre>' . print_r($posted_data, true));
            try {
                //Set validation rule(s)
                $this->form_validation->set_rules('module', 'Module', 'required');
                $this->form_validation->set_rules('parent', 'Parent Task', 'required');
                $this->form_validation->set_rules('code', 'Task Code', 'required|is_unique[' . $this->task_table . '.task_code]');
                $this->form_validation->set_rules('url', 'URL', 'required');
                $this->form_validation->set_rules('description', 'Description', 'required');
                $this->form_validation->set_message('is_unique', "There's a task with the code you entered already.");

                //Run the validation method and throw an Exception if there's a problem
                if (!$this->form_validation->run()) {
                    throw new Exception(validation_errors());
                }

                //Get the posted data and insert
                $module = $this->input->post('module', true);
                $parent = $this->input->post('parent', true);
                $code = $this->input->post('code', true);
                $url = $this->input->post('url', true);
                $description = $this->input->post('description', true);
                $active = $this->input->post('active', true);
                $navbarshow = $this->input->post('navbarshow', true);
                $task_id = $this->task->addTask($module, $parent, $code, $url, $description, $active, $navbarshow);

                //Show an error message if insertion failed
                if (empty($task_id)){
                    throw new Exception('Unable to add the task. Please, try again.');
                }

                //Redirect to this page to clear the post data and display a success message
                redirect("sys-admin/task/add/success/$task_id");
            } catch (Exception $e) {
                $msg = $this->_renderErrorMsg($e->getMessage());
            }
        }   //END if form was submitted

        //Get the message to display and format it if any
        if (empty($msg) && !empty($success) && !empty($task_id)){
            $msg_temp = 'Task added successfully.';
            $msg = $this->_renderSuccessMsg($msg_temp);
        }
        if(!empty($msg)){
            $view_data['msg'] = $msg;
        }
        //Get the list of available modules and tasks
        $view_data['modules'] = $this->module->getModules();
        $view_data['tasks'] = $this->task->getTasks();

        //Render the page
        $view_data['title'] = 'Add New Task (Development Only)';
        $this->_doRender('sys-admin/task/task-add', $view_data);
    }   //END add()



    /**
     * Handles listing of existing user groups
     */
    public function view(){
        //Get existing tasks
        $view_data['tasks'] = $this->task->getTasks(false);

        //Render the page
        $view_data['title'] = 'View Tasks';
        $this->_doRender('sys-admin/task/task-view', $view_data);
    }   //END view




    /**
     * Handles editing of a task whose ID is specified
     * @param int $task_id     The ID of the task to edit
     * @param string $success  Should contain the string 'success' to show that the edit was successful
     * @throws Exception
     */
    public function edit($task_id = 0, $success = ''){
        $posted_data = $this->input->post(null, false);

        //Confirm that a form was submitted
        if ($posted_data){
            try {
                //Set validation rule(s)
                $this->form_validation->set_rules('module', 'Module', 'required');
                $this->form_validation->set_rules('parent', 'Parent Task', 'required');
                $this->form_validation->set_rules('code', 'Task Code', 'required');
                $this->form_validation->set_rules('url', 'URL', 'required');
                $this->form_validation->set_rules('description', 'Description', 'required');

                //Run the validation method and throw an Exception if there's a problem
                if (!$this->form_validation->run()) {
                    throw new Exception(validation_errors());
                }

                //Get the posted data and insert
                $module = $this->input->post('module', true);
                $parent = $this->input->post('parent', true);
                $code = $this->input->post('code', true);
                $url = $this->input->post('url', true);
                $description = $this->input->post('description', true);
                $active = $this->input->post('active', true);
                $navbarshow = $this->input->post('navbarshow', true);

                //Check for the uniqueness of the task code
                $is_unique = $this->task->isUniqueTaskCode($code, $task_id);
                if (!$is_unique){
                    throw new Exception("There's a task with the task code you entered already.");
                }

                //All's well, so update
                $updated = $this->task->updateTask($task_id, $module, $parent, $code, $url, $description, $active, $navbarshow);

                //Show an error message if insertion failed
                if (empty($updated)){
                    throw new Exception('It seems you did not make any change, so nothing was saved.');
                }

                //Redirect to this page to clear the post data and display a success message
                redirect("sys-admin/task/edit/$task_id/success");
            } catch (Exception $e) {
                $msg = $this->_renderErrorMsg($e->getMessage());
            }
        }   //END if form was submitted

        //Get the details of the user group
        $view_data['task_deets'] = $this->task->getTask($task_id, false);
        $view_data['task_id'] = $task_id;

        //Get the list of available modules and tasks
        $view_data['modules'] = $this->module->getModules();
        $view_data['tasks'] = $this->task->getTasks();

        //Get the message to display and format it if any
        if (empty($msg) && !empty($success)){
            $msg_temp = 'Your changes were successfully saved.';
            $msg = $this->_renderSuccessMsg($msg_temp);
        }
        if(!empty($msg)){
            $view_data['msg'] = $msg;
        }

        //Render the page
        $view_data['title'] = 'Edit Task';
        $this->_doRender('sys-admin/task/task-edit', $view_data);
    }   //END edit()





}   //END class
