<?php
/**
 * Contains a method for showing an ACCESS DENIED page
 *
 * @package System_Administration
 *
 * @author Faniran, Oyetunde
 */
class Access_denied extends UPL_Controller {
    public function index(){
        $view_data['title'] = 'Access Denied';
        $this->_doRender('404', $view_data);
//        $this->load->view('404');
//        $header_data['title'] = 'Access Denied';
//        $this->load->view('parts/head', $header_data);
//        $this->load->view('sys-admin/access_denied');
//        $this->load->view('parts/foot');
    }
}
