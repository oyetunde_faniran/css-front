<?php

/**
 * Contains methods for pulling audit trail logs
 *
 * @author Zed
 */
class Audit_trail extends UPL_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('sys-admin/audit-trail/audit_trail_model');
        $this->load->model('sys-admin/usergroups/usergroup_model');
        $this->load->model('sys-admin/users/user_model');
        $this->load->model('sys-admin/permissions/permission_model');
        $this->load->library('pagination');
    }

//END __construct()

    function load_view($header_data = '') {
        $header_data['title'] = 'Audit Trail Filter';
        $header_data['usergrp_list'] = $this->usergroup_model->load_usergrouplist();
        $header_data['user_list'] = $this->user_model->load_userlist();
        $header_data['module'] = $this->permission_model->load_modulelist();
        $header_data['task_permodule'] = $this->permission_model->load_tasksPerModule();
        unset($_SESSION["usercheckboxes"]);
        unset($_SESSION["taskcheckboxes"]);
        unset($_SESSION["audit_strtDate"]);
        unset($_SESSION["audit_endDate"]);

        $this->load->view('header', $header_data);
        $this->load->view('sys-admin/audit-trail/audit-trail-filter');
        $this->load->view('footer');
    }

    public function audit_trail_view() {
        $this->load->helper('form'); // helper library default of codeigniter
        $posted_data = $this->input->post();

        if (!empty($posted_data)) {
            $_SESSION["usercheckboxes"] = $this->input->post('userscheckbox');
            $_SESSION["taskcheckboxes"] = $this->input->post('taskscheckbox');
            $_SESSION["audit_strtDate"] = $this->input->post('fromdate');
            $_SESSION["audit_endDate"] = $this->input->post('todate');
            $audit_records2 = $this->audit_trail_model->
                    getAudit_TrailbyUserDateDurationAndTask($_SESSION["usercheckboxes"], $_SESSION["taskcheckboxes"], $_SESSION["audit_strtDate"], $_SESSION["audit_endDate"]);
            $audit_count = count($audit_records2);
            $config = array();
            $config['base_url'] = base_url() . 'index.php/sys-admin/audit_trail/audit_trail_view';
            $config['total_rows'] = $audit_count;
            $config['per_page'] = RECORDS_PER_PAGE;
            $config["uri_segment"] = 4;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = round($choice);
            $this->pagination->initialize($config);

            $pageseg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data['audit_records'] = $this->audit_trail_model->
                    getAudit_TrailbyUserDateDurationAndTaskPagination($pageseg, $config['per_page'], $_SESSION["usercheckboxes"], $_SESSION["taskcheckboxes"], $_SESSION["audit_strtDate"], $_SESSION["audit_endDate"]);
            $data["pagination"] = $this->pagination->create_links();
            $data["pg_no"] = $pageseg + 1;
        } else {
            $audit_records2 = $this->audit_trail_model->
                    getAudit_TrailbyUserDateDurationAndTask($_SESSION["usercheckboxes"], $_SESSION["taskcheckboxes"], $_SESSION["audit_strtDate"], $_SESSION["audit_endDate"]);
            $audit_count = count($audit_records2);

            $config['base_url'] = base_url() . 'index.php/sys-admin/audit_trail/audit_trail_view';
            $config['total_rows'] = $audit_count;
            $config['per_page'] = 50;
            $config["uri_segment"] = 4;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = round($choice);
            $this->pagination->initialize($config);

            $pageseg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data['audit_records'] = $this->audit_trail_model->
                    getAudit_TrailbyUserDateDurationAndTaskPagination($pageseg, $config['per_page'], $_SESSION["usercheckboxes"], $_SESSION["taskcheckboxes"], $_SESSION["audit_strtDate"], $_SESSION["audit_endDate"]);
            $data["pagination"] = $this->pagination->create_links();
            $data["pg_no"] = $pageseg + 1;
        }
        $this->load->view('header', $data);
        $this->load->view('sys-admin/audit-trail/audit-trail-result', $data);
        $this->load->view('footer');
    }

//END audit_trail_filter()

    function audit_trail_advanced($advanced_audit_id) {
        $audit_data = $this->audit_trail_model->get_audit_trail($advanced_audit_id);
        $audit_val_before = unserialize($audit_data['value_before']);
        $audit_val_after = unserialize($audit_data['value_after']);
        $user_logged = $this->user_model->get_user($audit_data['user_id']);
        $task_logged = $this->permission_model->get_taskbyId($audit_data['task_id']);
        //die ('USER:' . print_r ($task_logged, TRUE));
        $popup_info = '<div>
                        <p><h6> <strong>User</strong>:  <span>' . $user_logged['user_fullname'] . '<span></h6><br/>
                        <h6> Task Performed:    <span>' . $task_logged['task_description'] . '</span></h6><br/>
                        <h6> Time Logged:  <span>' . $audit_data['audit_trail_time_logged'] . '</span></h6><br/>
                         </p></div>';
        $popup = '<table class="data display datatable" >
                    <thead class="shade" >
                    <th></th>
                        <th>Value Before</th>
                        <th>Value After</th>
                    </thead>
                    <tbody>';

        if (is_array($audit_val_before) && is_array($audit_val_after)) {
            foreach ($audit_val_after as $key => $value) {

                //Get value before
                $value_before_display = isset($audit_val_before[$key]) ? $audit_val_before[$key] : '';
                if (is_array($value_before_display)) {
                    $value_before_display = $this->_split_array($value_before_display);
                }

                //Get value after
                $value_after_display = (is_array($value)) ? $this->_split_array($value) : $value;

                $popup .= '<tr>
                                <td><label></label><label></label></td>
                                <td>
                                    <span><label><strong>' . $key . ':</strong></label></span>
                                    <span class="sys-admin-val-space">' . $value_before_display . '</span>
                                </td>
                                <td>
                                    <span><label><strong>' . $key . ':</strong></label></span>
                                    <span class="sys-admin-val-space">' . $value_after_display . '</span>
                                </td>
                            </tr>';
            }
        }


        $header_data['dialog_popup'] = $popup_info .$popup . '</tbody></table>';
        $header_data['title'] = 'Technical Details';
        //die ('USER:' . print_r ($header_data['dialog_popup'], TRUE));
       // $this->load->view('header', $header_data);
        $this->load->view('sys-admin/audit-trail/audit-trail-details', $header_data);
        //$this->load->view('footer');
    }

    protected function _split_array($arrayvalue) {
        $ret_val = '';
        if (is_array($arrayvalue)) {
            $ret_val = '<table class="data display datatable sys-admin-inner-table" >
                            <tbody>';
            foreach ($arrayvalue as $key => $value) {
                $ret_val .= '<tr>
                                <td>
                                    <span><label><strong>' . $key . ':</strong></label></span>
                                    <span class="sys-admin-val-space">' . (is_array($value) ? $this->_split_array($value) : $value) . '</span>
                                </td>
                            </tr>';
            }
            $ret_val .= '   </tbody>
                         </table>';
        }
        return $ret_val;
    }

    protected function _showTechDetails($audit_val_before = array(), $audit_val_after = array()) {
        $popup = '';
        if (is_array($audit_val_before) && is_array($audit_val_after)) {
            foreach ($audit_val_after as $key => $value) {
                if (!is_array($value)) {
                    $popup .= '<tr class="even gradeA" >
                                    <td>
                                        <span><label><strong>' . $key . ':</strong></label></span>
                                        <span class="val_space">' . (isset($audit_val_before[$key]) ? $audit_val_before[$key] : '') . '</span>
                                    </td>';
                    $popup .= '     <td>
                                        <span><label><strong>' . $key . ':</strong></label></span>
                                        <span class="val_space">' . $value . '</span>
                                    </td>
                                </tr>';
                } else {
                    $popup .= $this->_showTechDetails($audit_val_before[$key], $value);
                }
            }
            //$popup .=$combine;
        }
    }

    public function load_audit_trailrecords() {
        $this->load->helper('form'); // helper library default of codeigniter
        $usercheckboxes = $this->input->post('userscheckbox');
        $taskcheckboxes = $this->input->post('taskscheckbox');
        $audit_strtDate = $this->input->post('fromdate');
        $audit_endDate = $this->input->post('todate');

        $audit_records = $this->audit_trail_model->getAudit_TrailbyUserDateDurationAndTask($usercheckboxes, $taskcheckboxes, $audit_strtDate, $audit_endDate);
        return $audit_records;
    }

    function result_view($records = '') {
        $header_data['audit_records'] = $records;
        $this->load->library('pagination');

//        $config['base_url'] = base_url().'/audit_trail/result_view';
//        $config['total_rows'] = 40;
//        $config['per_page'] = 20;
//        $this->pagination->initialize($config);
//        echo $this->pagination->create_links();

        $this->load->view('header', $header_data);
        $this->load->view('sys-admin/audit-trail/audit-trail-result');
        $this->load->view('footer');
    }

}

//END class
