<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of privilege
 *
 * @author Tunde
 */
class Privilege extends UPL_Controller {

    function __construct(){
        parent::__construct();
        //$this->load->model('sys-admin/privilege_model', 'priv');
        $this->load->model('sys-admin/task_model', 'task');
        $this->load->model('sys-admin/Usergroup_model', 'ugroup');
        $this->load->model('sys-admin/Privilege_model', 'priv');
    }


    public function configure($group_id = 0, $success = '', $priv_count = 0){
        try {
            //Get existing tasks
            $view_data['tasks'] = $this->task->getTasks(false);

            //Get the details of the user group under consideration
            $usergroup_deets = $this->ugroup->getUserGroup($group_id, false);
            $usergroup_name = isset($usergroup_deets['usergroup_name']) ? $usergroup_deets['usergroup_name'] : 'Unknown';
            if (empty($usergroup_deets)){
                throw new Exception('Unknown User Group');
            }
            $show_privileges = true;

            $posted_data = $this->input->post();
            if ($posted_data){
                //die('<pre>' . print_r($posted_data, true));
                $privileges = $this->input->post('privilege', true);
                $priv_count = $this->priv->configure($group_id, $privileges);

                //Show an error message if configuration failed
                if (empty($priv_count)){
                    throw new Exception('Unable to configure privileges for the selected user group. Please, try again.');
                }

                //Redirect to this page to clear the post data and display a success message
                redirect("sys-admin/privilege/configure/$group_id/success/$priv_count");

            }


        } catch (Exception $e) {
            $msg = $e->getMessage();
            $show_privileges = false;
        }
        $view_data['show_privileges'] = $show_privileges;
        $view_data['group_id'] = $group_id;

        //Get existing privileges for the user group
        $privileges = $this->priv->getPrivileges($group_id);

        $view_data['privileges'] = array();
        foreach ($privileges as $priv){
            $view_data['privileges'][$priv['task_id']] = 1;
        }
        //die ('<pre>...' . print_r($view_data['privileges'], true) . '</pre>---');

        //Get the message to display and format it if any
        if (empty($msg) && !empty($success) && !empty($group_id) && !empty($priv_count)){
            $msg_temp = "Privilege configuration saved successfully -- $priv_count privilege(s) configured.";
            $msg = $this->_renderSuccessMsg($msg_temp);
        }
        if(!empty($msg)){
            $view_data['msg'] = $msg;
        }

        //Render the page
        $view_data['title'] = "Configuration of Privileges for User Group ($usergroup_name)";
        $this->_doRender('sys-admin/privilege/privilege-configuration', $view_data);
    }   //END configure()


}   //END class
