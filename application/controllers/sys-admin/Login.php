<?php

/**
 * Handles user management
 *
 * @author Faniran, Oyetunde
 */
class Login extends UPL_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('sys-admin/user_model', 'user');
        //$this->load->model('sys-admin/usergroup_model', 'ugroup');
    }


    public function index()
    {
        redirect('sign_in');
    }


    /**
     * Handles the login page
     */
    public function login_page()
    {
        $posted_data = $this->input->post(null, false);

        //Confirm that a form was submitted
        if ($posted_data) {
            try {
                //Set validation rule(s)
                $this->form_validation->set_rules('email', 'E-Mail', 'required|valid_email');
                $this->form_validation->set_rules('password', 'Password', 'required');

                //Run the validation method and throw an Exception if there's a problem
                if (!$this->form_validation->run()) {
                    throw new Exception(validation_errors());
                }

                $email = $this->input->post('email', true);
                $password = $this->input->post('password', true);
                $user_deets = $this->user->loginUser($email, $password);
//                die('<pre>--' . print_r($user_deets, true));

                if (empty($user_deets)) {
                    throw new Exception('Invalid email/password');
                }

                //Load the privileges of the user group
                $this->load->model('sys-admin/Privilege_model', 'priv');
                $dis_privs = $this->priv->getPrivileges($user_deets['usergroup_id']);
//                die('<pre>' . print_r($dis_privs, true));
                $privileges_array = array();
                foreach ($dis_privs as $priv) {
                    $privileges_array[$priv['task_code']] = array(
                        'id' => $priv['task_id'],
                        'code' => $priv['task_code'],
                        'url' => $priv['task_url'],
                        'moduleid' => $priv['module_id'],
                    );
                }
                //die('<pre>' . print_r($dis_privs, true) . "\n" . print_r($privileges_array, true));
                //die('<pre>' . print_r($user_deets, true));

                //Set the required session variables
                $_SESSION['user_id'] = $user_deets['user_id'];
                $_SESSION['active_branch_id'] = 1;
                $_SESSION['active_branch_name'] = '';
                $_SESSION['usergroup_id'] = $user_deets['usergroup_id'];
                $_SESSION['myuserId'] = $user_deets['user_id'];
                $_SESSION['mymerchantId'] = 1;
                //$_SESSION['lga_id'] = $user_deets['lga_id'];
                //$_SESSION['lga_name'] = $user_deets['lga_name'];
                $_SESSION['usergroup'] = stripslashes($user_deets['usergroup']);
                $_SESSION['firstname'] = substr(stripslashes($user_deets['user_firstname']), 0, 30);
                $_SESSION['surname'] = stripslashes($user_deets['user_surname']);
                $_SESSION['email'] = stripslashes($user_deets['userlog_email']);
                $_SESSION['phone'] = stripslashes($user_deets['user_phone']);
                $_SESSION['user_privileges'] = $privileges_array;
                $_SESSION['is_applicant'] = false;
                $_SESSION['pass_changed'] = $user_deets['user_password_changed'];
                $_SESSION['ga_enabled'] = $user_deets['user_enable_ga'];
                $_SESSION['user_ga_secretkey'] = $user_deets['user_ga_secretkey'];
                $_SESSION['nav_bar_menu'] = $this->user->loadMenu($user_deets['usergroup_id']);
                $_SESSION['first_timer'] = $_SESSION['ga_verified'] = empty($user_deets['userlog_lastlogin']) || $user_deets['userlog_lastlogin'] == '0000-00-00 00:00:00';
                $_SESSION['ga_verified'] = $_SESSION['ga_enabled'] ? $_SESSION['ga_verified'] : true;

                //die('<pre>' . print_r($_SESSION, true));

                //Redirect to the home page
                if ($_SESSION['first_timer']) {
                    $redirect_url = 'sys-admin/user/update_profile/firsttimer';
                } else {
                    $redirect_url = !$_SESSION['ga_verified'] ? 'sys-admin/user/verify_ga' : '';
                }

                redirect($redirect_url);

            } catch (Exception $e) {
                //$msg = $this->_renderErrorMsg($e->getMessage());
            }
        }   //END if form was posted

        redirect('login/error');

        //Get the message to display and format it if any
//        if(!empty($msg)){
//            $view_data['msg'] = $msg;
//        }

        //Render the page
//        $view_data['title'] = 'Login Page';
//        $this->_doRender('sys-admin/login/login-page', $view_data);
    }   //END login_page()


    /**
     * Handles the forgot password functionality
     */
    public function forgot_password($success = '')
    {
        $posted_data = $this->input->post(null, false);

        //Confirm that a form was submitted
        if ($posted_data) {
            try {
                //Set validation rule(s)
                $this->form_validation->set_rules('email', 'E-Mail', 'required|valid_email');
                $this->form_validation->set_rules('captcha_text', 'Text Below', 'required');

                //Run the validation method and throw an Exception if there's a problem
                if (!$this->form_validation->run()) {
                    throw new Exception(validation_errors());
                }

                $email = $this->input->post('email', true);
                $captcha_text = $this->input->post('captcha_text', true);
                $captcha_token = $this->input->post('captcha_token', true);

                if (!$this->system_authorization->isCaptchaOK($captcha_text, $captcha_token)) {
                    throw new Exception('Invalid CAPTCHA text entered');
                }

                $successful = $this->user->doPasswordRemind($email);

                if (!$successful) {
                    throw new Exception('Unable to run password reminder now. Please, try again later.');
                }

                //Redirect to this page to clear the post data and display a success message
                redirect("sys-admin/login/forgot_password/success");
            } catch (Exception $e) {
                $msg = $this->_renderErrorMsg($e->getMessage());
            }
        }

        //Get the message to display and format it if any
        if (empty($msg) && !empty($success)) {
            $msg_temp = 'A new password has been generated and was successfully sent to your e-mail address.';
            $msg = $this->_renderSuccessMsg($msg_temp);
        }
        if (!empty($msg)) {
            $view_data['msg'] = $msg;
        }

        //Render the page
        $view_data['title'] = 'Password Reminder';
        $this->_doRender('sys-admin/login/forgot-password', $view_data);
    }   //END forgot_password()


    public function logout()
    {
        $redirect_page = $_SESSION['is_applicant'] ? 'sign_in_applicant' : 'sign_in';
        session_destroy();
        redirect($redirect_page);
    }   //END logout()


}   //END class