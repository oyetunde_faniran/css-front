<?php
/**
 * Handles user group management
 *
 * @author Faniran, Oyetunde
 */
class Usergroup extends UPL_Controller {

    function __construct(){
        parent::__construct();
        $this->usergroup_table = UPL_DB_TABLE_PREFIX . 'usergroups';
        $this->load->model('sys-admin/usergroup_model', 'ugroup');
    }



    /**
     * Handles adding of a new user group
     * @param string $success   Should contain the string 'success' to show that a new user group was successfully added
     * @param int $group_id     The ID of the newly added user group
     * @throws Exception
     */
    public function add($success = '', $group_id = 0){
        $posted_data = $this->input->post(null, false);

        //Confirm that a form was submitted
        if ($posted_data){
            try {
                //Set validation rule(s)
                $this->form_validation->set_rules('user_group', 'User Group', 'required|is_unique[' . $this->usergroup_table . '.usergroup_name]');
                $this->form_validation->set_message('is_unique', "There's a user group with the name you entered already.");

                //Run the validation method and throw an Exception if there's a problem
                if (!$this->form_validation->run()) {
                    throw new Exception(validation_errors());
                }

                //Get the posted data and insert
                $name = $this->input->post('user_group', true);
                $description = $this->input->post('user_group_desc', true);
                $group_id = $this->ugroup->addUserGroup($name, $description);

                //Show an error message if insertion failed
                if (empty($group_id)){
                    throw new Exception('Unable to add the user group. Please, try again.');
                }

                //Redirect to this page to clear the post data and display a success message
                redirect("sys-admin/usergroup/add/success/$group_id");
            } catch (Exception $e) {
                $msg = $this->_renderErrorMsg($e->getMessage());
            }
        }   //END if form was submitted

        //Get the message to display and format it if any
        if (empty($msg) && !empty($success) && !empty($group_id)){
            $group_deets = $this->ugroup->getUserGroup($group_id);
            $msg_part = isset($group_deets['usergroup_name']) ? ' Please, ' . anchor("sys-admin/privilege/configure/$group_id", "click here to configure privileges for '{$group_deets['usergroup_name']}'") . ' user group.' : '';
            $msg_temp = 'User group created successfully.' . $msg_part;
            $msg = $this->_renderSuccessMsg($msg_temp);
        }
        if(!empty($msg)){
            $view_data['msg'] = $msg;
        }

        //Render the page
        $view_data['title'] = 'Add User Group';
        $this->_doRender('sys-admin/user-group/usergroup-add', $view_data);
    }   //END add()



    /**
     * Handles listing of existing user groups
     */
    public function view(){
        //Get existing user groups
        $view_data['groups'] = $this->ugroup->getUserGroups(false);

        //Render the page
        $view_data['title'] = 'View User Groups';
        $this->_doRender('sys-admin/user-group/usergroup-view', $view_data);
    }   //END view




    /**
     * Handles editing of a user group whose ID is specified
     * @param int $group_id     The ID of the user group to edit
     * @param string $success   Should contain the string 'success' to show that a the edit was successful
     * @throws Exception
     */
    public function edit($group_id = 0, $success = ''){
        $posted_data = $this->input->post(null, false);

        //Confirm that a form was submitted
        if ($posted_data){
            try {
                //Set validation rule(s)
                $this->form_validation->set_rules('user_group', 'User Group', 'required');

                //Run the validation method and throw an Exception if there's a problem
                if (!$this->form_validation->run()) {
                    throw new Exception(validation_errors());
                }

                //Get the posted data
                $name = $this->input->post('user_group', true);
                $description = $this->input->post('user_group_desc', true);
                $enabled_temp = $this->input->post('active', true);
                $enabled = !empty($enabled_temp) ? '1' : '0';

                //Check for the uniqueness of the new name of the user group
                $is_unique = $this->ugroup->isUnique($name, $group_id);
                if (!$is_unique){
                    throw new Exception("There's a user group with the name you entered already.");
                }

                //All's well, so update
                $updated = $this->ugroup->updateUserGroup($group_id, $name, $description, $enabled);

                //Show an error message if insertion failed
                if (empty($updated)){
                    throw new Exception('It seems you did not make any change, so nothing was saved.');
                }

                //Redirect to this page to clear the post data and display a success message
                redirect("sys-admin/usergroup/edit/$group_id/success");
            } catch (Exception $e) {
                $msg = $this->_renderErrorMsg($e->getMessage());
            }
        }   //END if form was submitted

        //Get the details of the user group
        $view_data['group_deets'] = $this->ugroup->getUserGroup($group_id, false);
        $view_data['group_id'] = $group_id;

        //Get the message to display and format it if any
        if (empty($msg) && !empty($success)){
            $msg_temp = 'Your changes were successfully saved.';
            $msg = $this->_renderSuccessMsg($msg_temp);
        }
        if(!empty($msg)){
            $view_data['msg'] = $msg;
        }

        //Render the page
        $view_data['title'] = 'Edit User Group';
        $this->_doRender('sys-admin/user-group/usergroup-edit', $view_data);
    }   //END edit()





}   //END class
