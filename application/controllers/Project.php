<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Client
 *
 * @author oyetunde.faniran
 */
class Project extends UPL_Controller {
    
    
    function __construct() {
        parent::__construct();
        $this->load->model('Projects_model', 'prj');
        $this->load->model('Clients_model', 'clientm');
        $this->load->model('Commission_model', 'comm');
        $this->load->model('Beneficiary_model', 'benm');
        $this->load->model('Deductions_model', 'dedm');
    }
    
    
    public function index(){
        $this->listall();
    }
    
    
    public function listall(){
        $view_data['records'] = $this->prj->getProjects(false);
        
        //Get the list of all optional deductions
        $view_data['deductions'] = $this->dedm->getDeductions();
//        die('<pre>' . print_r($view_data['clients'], true));
        
        $view_data['js_files'] = array(
            'assets/lib/raphael/raphael-min.js',
            'assets/lib/morrisjs/morris.min.js',
            'assets/lib/datatables/js/jquery.dataTables.min.js',
            'assets/lib/datatables/js/dataTables.bootstrap.min.js',
            'assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.html5.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.flash.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.print.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.colVis.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js',
            'assets/lib/jquery.niftymodals/js/jquery.modalEffects.js'
        );

        //Extra CSS files to import
        $view_data['css_files'] = array(
            'assets/lib/morrisjs/morris.css',
            'assets/lib/datatables/css/dataTables.bootstrap.min.css',
            'assets/lib/jquery.niftymodals/css/component.css'
        );

        $view_data['additional_js'] = <<<JS
$("#report-table").dataTable({buttons:["copy","excel","pdf","print"],lengthMenu:[[25,50,100,200,-1],[25,50,100,200,"All"]],dom:"<'row am-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B>><'row am-datatable-body'<'col-sm-12'tr>><'row am-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"});
JS;

//redirect('reports/do_report/account/total');
//$view_data['accounts'] = $this->acc->loadMultiple(0, 0, '', "\$this->db->order_by('bank_id');");
        $view_data['clients'] = $this->clientm->loadMultiple(0, 0, '', "\$this->db->order_by('client_name');");
        $view_data['bens'] = $this->benm->loadMultiple(0, 0, '', "\$this->db->order_by('ben_name');");
//        die('<pre>' . print_r($view_data['bens'], true));
        $view_data['title'] = 'List of Projects';
        $view_file = 'projects/prj-listall';
        $this->_doRender($view_file, $view_data);
    }
 
    
    
    public function addProject(){
        if (!$this->input->is_ajax_request()) {
            die(':(');
        }
        
        $data_temp = file_get_contents('php://input');
        $posted_data = array();
        parse_str($data_temp, $posted_data);
        
        try {
            $this->load->library('form_validation');
            $this->form_validation->set_data($posted_data);
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('client', 'Client', 'required|greater_than[0]');
            $this->form_validation->set_rules('when2split', 'When to split commission', 'required|greater_than[0]');
            $this->form_validation->set_message('greater_than', 'The {field} field is required.');
            
            if($this->form_validation->run() === FALSE){
                throw new Exception(validation_errors());
            }
            
            //Save the project details
            $date_added = date('Y-m-d H:i:s');
            $project_key = $this->_cleanName4Key($posted_data['name']);
            $data_array = array(
                'prj_name' => $posted_data['name'],
                'prj_key' => "$project_key",
                'prj_description' => $posted_data['desc'],
                'client_id' => $posted_data['client'],
                'user_id' => (!empty($_SESSION['user_id']) ? $_SESSION['user_id'] : 0),
                'prj_default' => ($posted_data['defaultprj'] == '1' ? "1" : "0"),
                'prj_when2split' => $posted_data['when2split'],
                'prj_dateadded' => $date_added
            );
            $prj_id = $this->prj->insert($data_array);
            
            //If there are deduction mappings, clear existing one and save the new one
            if(!empty($posted_data['deductions'])){
                $this->dedm->clearPrjDeductionMapping($prj_id);
                $this->dedm->savePrjDeductionMapping($prj_id, $posted_data['deductions']);
            }
            
            $retval_temp = array(
                'status' => true,
                'msg' => $this->_renderSuccessMsg('Project added successfully. Reloading the list now...')
            );
            
        } catch (Exception $ex) {
            $retval_temp = array(
                'status' => false,
                'msg' => $this->_renderErrorMsg($ex->getMessage())
            );
        }
        $ret_val = json_encode($retval_temp);
        header('Content-type: application/json');
        die($ret_val);
    }
    
    
    
    
    public function getProjects($id = 0){
//        sleep(2);
//        if (!$this->input->is_ajax_request()) {
//            die(':(');
//        }
        if($id > 0){
            $c = $this->prj->getSplitConfig($id);
            $clients = array(
                'status' => true,
                'id' => $c['client_id'],
                'name' => $c['client_name'],
                'email' => $c['client_email'],
                'code' => $c['client_code'],
                'nibss_clientid' => $c['client_nibbs_clientid'],
                'computecommission' => $c['client_computecommission'],
                'charge' => $c['client_transcharge'],
                'charge_type' => $c['client_charge_type'],
                'charge_mode' => $c['client_charge_mode'],
                'deductcharge' => $c['client_deductchargefromamount'],
                'url' => $c['client_statusupdate_url'],
                'status' => $c['client_enabled'],
                'date_added' => $c['client_dateadded'],
                'lastupdated' => $c['client_datelastupdated']
            );
        } else {
            $clients_temp = $this->clm->getClients(false);
            $clients = array();
            foreach($clients_temp as $c){
                $clients[] = array(
                    'id' => $c['client_id'],
                    'name' => $c['client_name'],
                    'email' => $c['client_email'],
                    'code' => $c['client_code'],
                    'nibss_clientid' => $c['client_nibbs_clientid'],
                    'computecommission' => $c['client_computecommission'],
                    'charge' => $c['client_transcharge'],
                    'charge_type' => $c['client_charge_type'],
                    'charge_mode' => $c['client_charge_mode'],
                    'deductcharge' => $c['client_deductchargefromamount'],
                    'url' => $c['client_statusupdate_url'],
                    'status' => $c['client_enabled'],
                    'date_added' => $c['client_dateadded'],
                    'lastupdated' => $c['client_datelastupdated']
                );
            }
        }
        
        $ret_val = json_encode($clients);
        header('Content-type: application/json');
        die($ret_val);
    }
    
    
    
    public function updateProject($client_id_temp = 0){
        die('');
        $client_id = (int)$client_id_temp;
        if (!$this->input->is_ajax_request()) {
            die(':(');
        }
        
        $data_temp = file_get_contents('php://input');
        $posted_data = array();
        parse_str($data_temp, $posted_data);

        
        try {
            $calc_commission = !empty($posted_data['computecommission']) ? '1' : '0';
            $this->load->library('form_validation');
            $this->form_validation->set_data($posted_data);
            $this->form_validation->set_rules('clientname', 'Name', 'required'); // we have to set criteria of each form elemen, it will evaluate automaticallly
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email'); // we have to set criteria of each form elemen, it will evaluate automaticallly
            $this->form_validation->set_rules('nibssclientid', 'NIBSS Client ID', 'required');
            
            if($calc_commission == 1){
                $this->form_validation->set_rules('commission_type', 'Commission Type', 'required|greater_than[-1]');
                $this->form_validation->set_rules('commission', 'Commission Value', 'required|greater_than[0]');
                $this->form_validation->set_rules('commission_mode', 'How Should The Commission Be Calculated?', 'required|greater_than[-1]');
                $this->form_validation->set_rules('commission_deduct', 'Should The Commission Be Deducted?', 'required|greater_than[-1]');
            }
            $this->form_validation->set_message('greater_than', '"{field}" field is required.');
            
            if($this->form_validation->run() === FALSE){
                throw new Exception(validation_errors());
            }
            
//            $this->load->library('General_tools');
//            $client_code = $this->general_tools->getRandomString_AlphaNum(10);
//            $secret_key = $this->general_tools->getRandomString_AlphaNumSigns(30, 100);
//            $date_added = date('Y-m-d H:i:s');
            $data_array = array(
                'client_name' => $posted_data['clientname'],
                'client_email' => $posted_data['email'],
//                'client_code' => $client_code,
//                'client_secretkey' => $secret_key,
                'client_nibbs_clientid' => $posted_data['nibssclientid'],
                'client_computecommission' => (!empty($posted_data['computecommission']) ? '1' : '0'),
                'client_transcharge' => (!empty($posted_data['commission']) ? $posted_data['commission'] : '0'),
                'client_charge_type' => (!empty($posted_data['commission_type']) ? $posted_data['commission_type'] : '0'),
                'client_charge_mode' => (!empty($posted_data['commission_mode']) ? $posted_data['commission_mode'] : '0'),
                'client_deductchargefromamount' => (!empty($posted_data['commission_deduct']) ? $posted_data['commission_deduct'] : '0'),
                'client_statusupdate_url' => (!empty($posted_data['updateurl']) ? $posted_data['updateurl'] : ''),
            );
            $this->clm->client_id = $client_id;
            $this->clm->setValues($data_array);
            $this->clm->update();
//            die ($this->db->last_query());
            
            $retval_temp = array(
                'status' => true,
                'msg' => $this->_renderSuccessMsg('Client was updated successfully. Reloading client list now...')
            );
            
        } catch (Exception $ex) {
            $retval_temp = array(
                'status' => false,
                'msg' => $this->_renderErrorMsg($ex->getMessage())
            );
        }
        $ret_val = json_encode($retval_temp);
        header('Content-type: application/json');
        die($ret_val);
    }
    
    
    
    public function getSplitConfig($id_temp = 0){
//        if (!$this->input->is_ajax_request()) {
//            die(':(');
//        }
        $prj_id = (int)$id_temp;
        $comm_config = $this->comm->getSplitConfig($prj_id);
//        if(!empty($comm_config)){
            foreach($comm_config as $c){
                $ret_val_array['cdata'][] = array(
                    'ben_id' => $c['ben_id'],
                    'share' => $c['form_share'],
                );
                $ret_val_array['type'] = $c['form_type'];
                $ret_val_array['client'] = $c['client_name'];
                $ret_val_array['project'] = $c['prj_name'];
            }
            $ret_val_array['status'] = true;
            $ret_val_array['msg'] = 'OK';
//        } else {
//            $ret_val_array['cdata'] = array();
//            $ret_val_array['status'] = false;
//            $ret_val_array['msg'] = 'Not found';
//        }
        
        $ret_val = json_encode($ret_val_array);
        header('Content-type: application/json');
        die($ret_val);
    }
    
    
    
    protected function _cleanName4Key($name_temp){
        $name = strtolower($name_temp);
        $len = strlen($name);
        $final_str = '';
        for($offset=0; $offset < $len; $offset++){
            $dis_char_temp = substr($name, $offset, 1);
            $dis_char = $dis_char_temp == ' ' ? '-' : (ctype_alnum($dis_char_temp) ? $dis_char_temp : '');
            $final_str .= $dis_char;
        }
        return $final_str;
    }
    
    
    public function downloadProjectKeys(){
        $records = $this->prj->getProjects(false);
        
        $data = "S/NO,CLIENT,PROJECT,KEY\n";
        $sno = 0;
        foreach($records as $r){
            $data .= "\"" . ++$sno . "\",\"" . str_replace("\n", '', $r['client_name']) . "\",\"" . str_replace("\n", '', $r['prj_name']) . "\",\"" . str_replace("\n", '', $r['prj_key']) . "\"\n";
        }
        $this->load->helper('download');
        force_download('css-client-project-keys.csv', $data, "text/csv");
    }
    
    
}