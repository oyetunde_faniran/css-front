<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Bankaccount
 *
 * @author tundefaniran
 */
class Bankaccount extends UPL_Controller {
    
    
    function __construct() {
        parent::__construct();
        $this->load->model('Bank_model', 'bank');
        $this->load->model('Commission_model', 'comm');
        $this->load->model('Bankaccount_model', 'bam');
        
        $this->load->library('General_tools');
        $this->max_size = 1048576;
        $this->max_size_display = '1MB';
    }
    
    
    public function upload($success = '', $deposit_id_get = 0){
        $posted_data = $this->input->post();
        if(!empty($posted_data)){
            
            try {
                //Confirm that the fields were filled
                $this->load->library('form_validation');
                $this->form_validation->set_rules('totalamount', 'Total amount', 'required');
                $this->form_validation->set_rules('accno', 'Account number', 'required');
                $this->form_validation->set_rules('accname', 'Account name', 'required');
                $this->form_validation->set_rules('depositdate', 'Deposit date', 'required');
                $this->form_validation->set_rules('bank', 'Bank', 'required|greater_than[0]');
                $this->form_validation->set_message('greater_than', '"{field}" field is required.');
                
                if($this->form_validation->run() === FALSE){
                    throw new Exception(validation_errors());
                }
                
                //Confirm that a file was successfully uploaded
                if($_FILES['breakdown']['error'] == 4){
                    throw new Exception('Please, upload a CSV file containing the breakdown of payment.');
                }
                
                $csv = $this->_getCSVData($_FILES['breakdown']);
//				die('<pre>' . print_r($csv, true));

                //Confirm that the the amount is okay
                if((string)$csv['total_amount'] != (string)$posted_data['totalamount']){
                    throw new Exception("Total amount entered (&#8358;" . number_format($posted_data['totalamount'], 2) . ") is not the same as the sum of amount (&#8358;" . number_format($csv['total_amount'], 2) . ") in the uploaded file.");
                }
                
//                die('<pre>' . print_r($posted_data, true) . print_r($_FILES, true) . print_r($csv, true));

                //Save the deposit summary
                $deposit_id = $this->_saveBankDeposit($posted_data['bank'], $posted_data['accno'], $posted_data['accname'], $csv['total_amount'], $posted_data['depositdate'], $csv['filename'], $_SESSION['user_id']);
                
                //Save the commission per project/client
                $invalid_projects = $this->_saveCommission($csv['data'], $deposit_id, $posted_data['bank'], $posted_data['accno']);
                if(!empty($invalid_projects)){
                    throw new Exception('The following project keys in the uploaded file are not valid - (' . implode('', $invalid_projects) . ')');
                }
                redirect("bankaccount/upload/success/$deposit_id");
                
//                $this->file_uploaded->doUpload($_FILES['breakdown'], '');
            } catch (Exception $ex) {
                $view_data['msg'] = $this->_renderErrorMsg($ex->getMessage());
            }
        }
        
        if($success == 'success' && $deposit_id_get > 0 && !$_POST && empty($view_data['msg'])){
            $view_data['msg'] = $this->_renderSuccessMsg('Upload completed successfully. ' . anchor("reports/comms/{$deposit_id_get}", 'Click here to view the list of commission') . ' created from it.');
        }
        
        $view_data['js_files'] = array(
            'assets/lib/raphael/raphael-min.js',
            'assets/lib/morrisjs/morris.min.js',
            'assets/lib/datatables/js/jquery.dataTables.min.js',
            'assets/lib/datatables/js/dataTables.bootstrap.min.js',
            'assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.html5.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.flash.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.print.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.colVis.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js',
            'assets/lib/jquery.niftymodals/js/jquery.modalEffects.js',
            'assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js'
        );

        //Extra CSS files to import
        $view_data['css_files'] = array(
            'assets/lib/morrisjs/morris.css',
            'assets/lib/datatables/css/dataTables.bootstrap.min.css',
            'assets/lib/jquery.niftymodals/css/component.css',
            'assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css'
        );

        $view_data['additional_js'] = <<<JS
$("#report-table").dataTable({buttons:["copy","excel","pdf","print"],lengthMenu:[[25,50,100,200,-1],[25,50,100,200,"All"]],dom:"<'row am-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B>><'row am-datatable-body'<'col-sm-12'tr>><'row am-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"});
$(document).ready(function(){
    $(".datetimepicker").datetimepicker({
        autoclose:1,
        format: 'yyyy-mm-dd',
        componentIcon:".s7-date",
        navIcons:{
            rightIcon:"s7-angle-right",
            leftIcon:"s7-angle-left"
        }
    });
});                
JS;
        
        $view_data['banks'] = $this->bank->loadMultiple(0, 0, '', "\$this->db->order_by('bank_name');");
        $view_data['title'] = 'Bank Account Entries Breakdown Upload';
        $view_file = 'bankacc-entries/upload';
        $this->_doRender($view_file, $view_data);
    }
    
    
    
    protected function _getCSVData($file){
        //Error State
        if ($file['error'] != 0){
            throw new Exception('File not uploaded successfully.');
        }

        //File Size
        if ($file['size'] > $this->max_size){
            throw new Exception('The file size cannot be more than ' . $this->max_size_display . '.');
        }
        
        $csv = file_get_contents($file['tmp_name']);
        $records = explode("\n", $csv);
        $data = array();
        $total_amount = 0.00;
        $record_count = 0;
        foreach($records as $record){
            $cols_temp = explode(',', $record);
            $cols[1] = !empty($cols_temp[1]) ? trim($cols_temp[1]) : '';
            $cols[2] = !empty($cols_temp[2]) ? trim($cols_temp[2]) : '';
            if(++$record_count == 1 || empty($cols[1]) || empty($cols[2])){
                continue;
            }
            $data[trim($cols[1])] = !empty($data[$cols[1]]) ? ($data[$cols[1]] + trim($cols[2])) : trim($cols[2]);
            $total_amount += trim($cols[2]);
        }
        $filename_new = date('YmdHis-') . rand(1, 1000000) . '.csv';
        move_uploaded_file($file['tmp_name'], BANKDEPOSIT_UPLOAD_DIR . $filename_new);
        $ret_val = array(
            'data' => $data,
            'filename' => $filename_new,
            'total_amount' => $total_amount
        );
        return $ret_val;
    }
    
    
    
    
    protected function _saveCommission($data, $bankdeposit_id, $bank, $accno){
        $projects = $this->_initProjectList();
        $bank_deets = $this->bank->getBank($bank);
        if(empty($bank_deets)){
            throw new Exception('Invalid bank supplied');
        }
        
        $invalid_projects = array();
        foreach ($data as $project_key => $amount){
            if(empty($projects[$project_key])){
                $invalid_projects[] = $project_key;
                continue;
            }
            $prj_id = $projects[$project_key];
            $this->comm->saveCustomCommission($prj_id, $amount, $accno, $bank_deets['bank_code'], $bankdeposit_id);
        }
        return $invalid_projects;
    }
    
    
    
    
    protected function _initProjectList(){
        //Get the list of project keys as index pointing to the project IDs
        $this->load->model('Projects_model', 'prjm');
        $projects = $this->prjm->getProjects();
        $ret_val = array();
        foreach ($projects as $project){
            $ret_val[$project['prj_key']] = $project['prj_id'];
        }
        return $ret_val;
    }
    
    
    
    protected function _saveBankDeposit($bank, $accno, $accname, $amount, $deposit_date, $filename, $user_id){
        $date_logged = date('Y-m-d H:i:s');
        $data_array = array(
            'bank_id' => "$bank",
            'bdeposit_accno' => "$accno",
            'bdeposit_accname' => "$accname",
            'bdeposit_amount' => "$amount",
            'bdeposit_datedeposited' => "$deposit_date",
            'bdeposit_filename' => "$filename",
            'user_id' => "$user_id",
            'bdeposit_timelogged' => "$date_logged",
        );
        $this->db->insert('bankdeposits', $data_array);
        $ret_val = $this->db->insert_id();
        return $ret_val;
    }
    
    
    
    public function listall(){
        $where_clause = '';
        
        //Get posted data if any
        $view_data['fromdate'] = $fromdate_temp = $this->input->post('fromdate');
        $view_data['todate'] = $todate_temp = $this->input->post('todate');

        //Date
        if(!empty($fromdate_temp) && !empty($todate_temp)){
            $where_clause .= " AND bd.bdeposit_timelogged BETWEEN '$fromdate_temp 00:00:00' AND '$todate_temp 23:59:59' ";
        } else {
            $today = date('Y-m-d');
            $where_clause .= " AND bd.bdeposit_timelogged BETWEEN '$today 00:00:00' AND '$today 23:59:59' ";
        }

        $where_clause_final = !empty($where_clause) ? 'TRUE ' . $where_clause : '';
            
        
        $view_data['trans'] = $this->bam->getBankDeposits($where_clause_final);
        
        $view_data['js_files'] = array(
            'assets/lib/raphael/raphael-min.js',
            'assets/lib/morrisjs/morris.min.js',
            'assets/lib/datatables/js/jquery.dataTables.min.js',
            'assets/lib/datatables/js/dataTables.bootstrap.min.js',
            'assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.html5.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.flash.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.print.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.colVis.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js',
            'assets/lib/jquery.niftymodals/js/jquery.modalEffects.js',
            'assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js'
        );

        //Extra CSS files to import
        $view_data['css_files'] = array(
            'assets/lib/morrisjs/morris.css',
            'assets/lib/datatables/css/dataTables.bootstrap.min.css',
            'assets/lib/jquery.niftymodals/css/component.css',
            'assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css'
        );

        $view_data['additional_js'] = <<<JS
$("#report-table").dataTable({buttons:["copy","excel","pdf","print"],lengthMenu:[[25,50,100,200,-1],[25,50,100,200,"All"]],dom:"<'row am-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B>><'row am-datatable-body'<'col-sm-12'tr>><'row am-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"});
$(document).ready(function(){
    $(".datetimepicker").datetimepicker({
        autoclose:!0,
        componentIcon:".s7-date",
        navIcons:{
            rightIcon:"s7-angle-right",
            leftIcon:"s7-angle-left"
        }
    });
});                
JS;
        
        $view_data['title'] = 'Bank Account Entries';
        $view_file = 'bankacc-entries/listall';
        $this->_doRender($view_file, $view_data);
    }
    
    
    
}
