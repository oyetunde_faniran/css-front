<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Ips
 *
 * @author tundefaniran
 */
class Ips extends UPL_Controller {
    
    
    function __construct() {
        parent::__construct();
        $this->load->model('Ips_model', 'ipm');
    }
    
    
    
    public function getClientIps($client_id_temp = 0){
//        if (!$this->input->is_ajax_request()) {
//            die(':(');
//        }
//        sleep(3);
        $client_id = (int)$client_id_temp;
        try {
            $ips = $this->ipm->getClientIps($client_id);
            if(empty($ips)){
                throw new Exception('No IPs found or client is invalid.');
            }
            $ret_val_temp = array(
                'status' => true,
                'msg' => 'OK',
                'dis_data' => $ips
            );
        } catch (Exception $ex) {
            $ret_val_temp = array(
                'status' => true,
                'msg' => $ex->getMessage(),
                'dis_data' => array()
            );
        }
        header('Content-type: application/json');
        $ret_val = json_encode($ret_val_temp);
        echo $ret_val;
    }
    
    
    
    public function updateClientIps($client_id = 0){
        if (!$this->input->is_ajax_request()) {
            die(':(');
        }
        
        try {
            $ip_post_data = $this->input->post('ip');
            if(empty($ip_post_data)){
                throw new Exception('At least one IP must be configured');
            }
            
            $this->form_validation->set_rules('ip[]', 'IPs', 'valid_ip');
            if(!$this->form_validation->run()){
                throw new Exception('Please, enter only valid IPs.');
            }
            
            $data_array = array();
            foreach ($ip_post_data as $ip){
                if(empty($ip)){
                    throw new Exception('Please, enter only valid IPs.');
                }
                $data_array[] = array(
                    'client_id' => $client_id,
                    'clientip_ip' => $ip
                );
            }
//            die('<pre>' . print_r($data_array, true));
            $this->ipm->clearClientIPs($client_id);
            $this->ipm->insert_batch($data_array);
            
            
            $msg = $this->_renderSuccessMsg('Changes saved successfully');
            $status = true;
        } catch (Exception $ex) {
            $msg = $this->_renderErrorMsg($ex->getMessage());
            $status = false;
        }
        $ret_val_temp = array(
            'status' => $status,
            'msg' => $msg
        );
        $ret_val = json_encode($ret_val_temp);
        header('content-type: application/json');
        echo $ret_val;
    }
    
    
}
