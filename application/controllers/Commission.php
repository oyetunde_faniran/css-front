<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Commission
 *
 * @author oyetunde.faniran
 */
class Commission extends UPL_Controller {


    function __construct() {
        parent::__construct();
        $this->load->library('Css_schedule_pusher');
        $this->load->model('Transaction_model', 'transm');
        $this->load->model('Commission_model', 'comm');
        $this->load->model('Projects_model', 'prj');
        $this->load->model('Splitting_model', 'splitm');
        $this->bankcode = SUSPENSE_ACC_BANK_CODE;
        $this->accno = SUSPENSE_ACC_NO;
        $this->accname = SUSPENSE_ACC_NAME;
        $this->client_id = CSS_CLIENT_ID;
        $this->secretkey = CSS_SECRET_KEY;
    }


    public function processCommission($sch_id = 0, $type = 0){
        if(!$this->input->is_ajax_request()){
            die(':(');
        }
        $trans = $this->transm->getTransactions(array('sch_id' => $sch_id));
        try {
            if(empty($trans)){
                throw new Exception('Transaction not found');
            }


            //Get the beneficiaries
            $ben = array();
            $no_ben = true;
            foreach($trans as $t){
                $no_ben = false;
                if($t['sch_comm_status'] != 0){
                    continue;
                }
                $ben[] = array(
                    'accname' => $this->accname,
                    'amount' => $type == 1 ? $t['sch_commission'] : $t['sch_commission_successful_only'],
                    'accno' => $this->accno,
                    'bankcode' => $this->bankcode,
                    'accno_debit' => $t['sch_source_accno'],
                    'bankcode_debit' => $t['sch_source_bank_code'],
                    'narration' => 'Comm on ' . $t['sch_sched_id'],
                );
            }
            if($no_ben){
                throw new Exception('No beneficiary has been configured.');
            }elseif(empty($ben)){
                throw new Exception('Commission processed already');
            }

            $sched_nibss_id = date('YmdHis') . rand(1000, 1000000);
            $data = array(
                'sched_id' => $sched_nibss_id,
                'bank_code' => $t['sch_source_bank_code'],
                'accno' => $t['sch_source_accno'],
                'beneficiaries' => $ben,
                'narration' => 'Comm on ' . $t['sch_sched_id'],
                'pageno' => 1,
                'client_id' => $this->client_id,
                'secretkey' => $this->secretkey,
            );
            $response = $this->css_schedule_pusher->send2Nibss($data);


            //Get the default project for this client
            $prj_deets = $this->prj->getDefaultClientProject($t['client_id']);
            $prj_id = !empty($prj_deets['prj_id']) ? $prj_deets['prj_id'] : 0;

            $this->comm->saveCommissionPayment($sch_id, $prj_id, $type, $trans[0], $this->accno, $this->bankcode, $ben[0]['accno_debit'], $ben[0]['bankcode_debit'], $response);

            //Update the status of the transaction
            $sch_status = (!empty($response['process_response']['status']) && $response['process_response']['status'] == 16) ? '0' : '2';
            $data_array = array(
                'sch_status' => "$sch_status",
                'sch_comm_status' => "1",
                'sch_datecomm_processed' => date('Y-m-d H:i:s')
            );
            $this->transm->updateTransaction($sch_id, $data_array);

            $retval_temp = array(
                'status' => true,
                'msg' => 'OK'
            );
        } catch (Exception $ex) {
            $retval_temp = array(
                'status' => false,
                'msg' => $ex->getMessage()
            );
        }
        $retval = json_encode($retval_temp);
        header('Content-type: application/json');
        echo $retval;
    }





    public function processSplit($comm_id = 0, $prj_id_temp = 0){
//        die('<pre>' . print_r($post, true));
        if(!$this->input->is_ajax_request()){
//            die(':(');
        }

        $prj_id = (int)$prj_id_temp;

        $where = array(
            'schcomm_status' => "0",
            'schcomm_deleted' => "0",
            'schcomm_split_status' => "1"
        );

        if($prj_id > 0){
            $where['sc.prj_id'] = $prj_id;
            $aggregate = true;
        } else {
            $where['schcomm_id'] = $comm_id;
            $aggregate = false;
        }

        $trans = $this->comm->getCommissionPayments($where, false, $aggregate);

//        $where = array(
//            'schcomm_id' => $comm_id,
//            'schcomm_deleted' => "0"
//        );
//        $trans = $this->comm->getCommissionPayments($where);


//        die('<pre>' . print_r($trans, true));
        try {
            $narration_temp = $this->input->post('narration');
            $narration_temp2 = trim($narration_temp);
//            die($narration);

            if(empty($narration_temp2)){
                throw new Exception('Narration cannot be empty');
            }
            $narration = 'UPL-' . $narration_temp2;

//            die('ready to go');
            if(empty($trans)){
                throw new Exception('Commission not found');
            }


            //Get the beneficiaries
            $ben = array();
            foreach($trans as $t){
                //Confirm that the money has been paid into the settlement acct & that it has not been split before
                if($t['schcomm_status'] != 0 || $t['schcomm_split_status'] == 0){
                    continue;
                }

                //Get the project ID to use
                if(empty($prj_id)){
                    $prj_deets = $this->prj->getDefaultClientProject($t['client_id']);
                    $prj_id = !empty($prj_deets['prj_id']) ? $prj_deets['prj_id'] : 0;
                }

                //Get the split config
                $split_config = $this->comm->getSplitConfig($prj_id);
                if(empty($split_config)){
                    throw new Exception('Split not yet configured for this project');
                }
//                echo('<pre>' . print_r($split_config, true));

                //Get any configured deductions
                $deduction = 0;
                $final_total_amount = $t['schcomm_amount'] - $deduction;

                $split_total = 0.0;
                foreach($split_config as $tconf){
                    $ben_amount = $tconf['form_type'] == 2 ? ($final_total_amount * $tconf['form_share'] * 0.01) : $tconf['form_share'];
                    $split_total += (float)$tconf['form_share'];
                    $ben[] = array(
                        'accname' => $tconf['ben_name'],
                        'amount' => $ben_amount - NIBSS_TRANS_CHARGE,
                        'accno' => $tconf['ben_accno'],
                        'bankcode' => $tconf['bank_code'],
                        'accno_debit' => $this->accno,
                        'bankcode_debit' => $this->bankcode,
//                        'narration' => 'Comm on ' . $t['sch_sched_id'],
                        'narration' => $narration,
                        'share' => $tconf['form_share'],
                        'ben_id' => $tconf['ben_id']
                    );
                }


                //If it is of type fixed, then ensure that the total adds up
                if($tconf['form_type'] == 1 && (string)$split_total != (string)$final_total_amount){
                    throw new Exception('Incorrect split configuration');
                }


            }
            //die("<pre>\n\n****************************\n\n" . print_r($ben, true));
            if(empty($ben)){
                throw new Exception('Split processed already or no split has been configured.');
            }



            //Now, pass to NIBSS
            $sched_nibss_id = date('YmdHis') . rand(1000, 1000000);
            $data = array(
                'sched_id' => $sched_nibss_id,
                'bank_code' => $this->bankcode,
                'accno' => $this->accno,
                'beneficiaries' => $ben,
//                'narration' => 'Commission split on ' . $t['sch_sched_id'],
                'narration' => $narration,
                'pageno' => 1,
                'client_id' => $this->client_id,
                'secretkey' => $this->secretkey,
            );
//            echo("<pre>Data to send to NIBSS: \n\n" . print_r($data, true));
            $response = $this->css_schedule_pusher->send2Nibss($data);

//            echo("\n\n\nResponse after sending to NIBSS: \n\n" . print_r($response, true));

            $comm_id_list = $aggregate ? explode(',', $t['comm_ids']) : [$t['schcomm_id']];

            $this->splitm->saveSplit($t['schcomm_id'], $this->accno, $this->bankcode, $ben, $response, $comm_id_list);
            //$sch_id, $type, $trans, $accno, $bankcode, $accno_debit, $bankcode_debit, $nibss_data

//            echo("\n\n\nSplit Saved!!!");

            //Update the status of the transaction
            $split_status = (!empty($response['process_response']['status']) && $response['process_response']['status'] == 16) ? '0' : '2';
            $dis_data_array = array(
                'schcomm_split_status' => "$split_status",
                'schcomm_date_split_processed' => date('Y-m-d H:i:s'),
                'schcomm_split_sched_id' => $sched_nibss_id
            );
            $this->comm->updateCommission($t['schcomm_id'], $dis_data_array, $comm_id_list);

//            echo("\n\n\nCommission Updated!!!");

            if($response['status'] != '16'){
                throw new Exception ('Unable to successfully push for payment. Please try again later.');
            }

            $retval_temp = array(
                'status' => true,
                'msg' => 'OK'
            );
        } catch (Exception $ex) {
            $retval_temp = array(
                'status' => false,
                'msg' => $ex->getMessage()
            );
        }
        $retval = json_encode($retval_temp);
        header('Content-type: application/json');
        echo $retval;
    }



    public function initSplit($comm_id = 0, $prj_id_temp = 0){
        if(!$this->input->is_ajax_request()){
//            die(':(');
        }

        $prj_id = (int)$prj_id_temp;

        $where = array(
            'schcomm_status' => "0",
            'schcomm_deleted' => "0",
            'sc.schcomm_split_status' => "1"
        );

        if($prj_id > 0){
            $where['sc.prj_id'] = $prj_id;
            $aggregate = true;
        } else {
            $where['schcomm_id'] = $comm_id;
            $aggregate = false;
        }

        $trans = $this->comm->getCommissionPayments($where, false, $aggregate);
//        echo('<pre>FULL TRANSACTION DEETS: ' . print_r($trans, true));

        try {
            if(empty($trans)){
                throw new Exception('Commission not found');
            }


            //Get the beneficiaries
            $ben = array();
            foreach($trans as $t){
                //Confirm that the money has been paid into the settlement acct & that it has not been split before
                if($t['schcomm_status'] != 0 || !in_array($t['schcomm_split_status'], array('1', '2'))){
                    continue;
                }

                //Get the project ID to use
                if(empty($prj_id_temp)){
                    $prj_deets = $this->prj->getDefaultClientProject($t['client_id']);
                    $prj_id = !empty($prj_deets['prj_id']) ? $prj_deets['prj_id'] : 0;
                } else {
//                    echo "\n\nNo need to fetch the default prj_id since it has been specified: $prj_id";
                }

//                echo "\n\nGetting the split config now";

                //Get the split config
                $split_config = $this->comm->getSplitConfig($prj_id);
//                echo('<pre>' . "\n\n\nSPLIT CONFIG: '$prj_id' -- '{$t['client_id']}'" . print_r($split_config, true));

                $split_total = 0.0;
                foreach($split_config as $tconf){
                    $ben_amount = $tconf['form_type'] == 2 ? $t['schcomm_amount'] * $tconf['form_share'] * 0.01 : $tconf['form_share'];
                    $split_total += (float)$tconf['form_share'];
                    $ben[] = array(
                        'accname' => $tconf['ben_name'],
                        'amount' => round($ben_amount - NIBSS_TRANS_CHARGE,2),
                        'accno' => $tconf['ben_accno'],
                        'bankcode' => $tconf['bank_code'],
                        'accno_debit' => $this->accno,
                        'bankcode_debit' => $this->bankcode,
                        'narration' => 'Comm on ' . $t['sch_sched_id'],
                        'share' => $tconf['form_share'],
                        'amount_formatted' => number_format($ben_amount - NIBSS_TRANS_CHARGE, 2),
                    );
                }


//                echo("\n\n\nBEN DETAILS: " . print_r($ben, true));


                //If it is of type fixed, then ensure that the total adds up
                if(empty($tconf) || (!empty($tconf) && $tconf['form_type'] == 1 && (string)$split_total != (string)$t['schcomm_amount'])){
                    throw new Exception('Incorrect split configuration');
                }


            }
//            die("<pre>\n\n****************************\n\n" . print_r($ben, true));
            if(empty($ben)){
                throw new Exception('Split processed already or no split has been configured.');
            }

            $total_charge = count($ben) * NIBSS_TRANS_CHARGE;
            $retval_temp = array(
                'status' => true,
                'msg' => 'OK',
                'beneficiaries' => $ben,
                'total_amount' => number_format($trans[0]['schcomm_amount'], 2),
                'total_charges' => number_format($total_charge, 2),
                'total_amount_less_charges' => number_format(($trans[0]['schcomm_amount'] - $total_charge), 2),
                'total_count' => !empty($trans[0]['total_count']) ? $trans[0]['total_count'] : 1,
            );
        } catch (Exception $ex) {
            $retval_temp = array(
                'status' => false,
                'msg' => $ex->getMessage(),
                'beneficiaries' => array(),
                'total_amount' => 0,
                'total_charges' => 0,
                'total_amount_less_charges' => 0,
                'total_count' => 0
            );
        }
        $retval = json_encode($retval_temp);
        header('Content-type: application/json');
        echo $retval;
    }




    public function showSplit($comm_id_temp = 0){
//        sleep(2);
        if(!$this->input->is_ajax_request()){
            die(':(');
        }
        $comm_id = (int)$comm_id_temp;
        $deets = $this->splitm->getSplitDetails($comm_id);
//        die('<pre>' . print_r($trans, true));
        try {
            if(empty($deets)){
                throw new Exception('Details not found');
            }

            //Get the beneficiaries
            $ben = array();
            foreach($deets as $t){
                $ben[] = array(
                    'accname' => $t['split_accname'],
                    'amount' => $t['split_amount'],
                    'accno' => $t['split_accno'],
                    'bankcode' => $t['split_bankcode'],
                    'amount_formatted' => number_format($t['split_amount'], 2),
                    'share' => $t['split_share']
                );
            }

            $retval_temp = array(
                'status' => true,
                'msg' => 'OK',
                'beneficiaries' => $ben
            );
        } catch (Exception $ex) {
            $retval_temp = array(
                'status' => false,
                'msg' => $ex->getMessage(),
                'beneficiaries' => array()
            );
        }
        $retval = json_encode($retval_temp);
        header('Content-type: application/json');
        echo $retval;
    }



    public function create_custom(){
        $prj_id_temp = $this->input->post('project');
        $amount_temp = $this->input->post('amount');
//        die('<pre>' . print_r($posted_data, true));
        $prj_id = (int)$prj_id_temp;
        $amount = (float)$amount_temp;
        try{
            if(empty($amount) || empty($prj_id)){
                throw new Exception();
            }
            $this->comm->saveCustomCommission($prj_id, $amount, $this->accno, $this->bankcode);
        } catch (Exception $ex) {

        }
        redirect('reports/comms');
    }




    public function deleteCommission($comm_id_temp = 0){
//        sleep(2);
        if(!$this->input->is_ajax_request()){
            die(':(');
        }
        $comm_id = (int)$comm_id_temp;
        try {
            //Do delete
            $deleted = $this->comm->deleteCommission($comm_id);
            if(!$deleted){
                throw new Exception('Unable to delete. Please, try again.');
            }

            $retval_temp = array(
                'status' => true,
                'msg' => 'OK',
            );
        } catch (Exception $ex) {
            $retval_temp = array(
                'status' => false,
                'msg' => $ex->getMessage(),
            );
        }
        $retval = json_encode($retval_temp);
        header('Content-type: application/json');
        echo $retval;
    }



}
