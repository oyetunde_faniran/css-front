<?php


class Neft_status_updater extends CI_Controller
{

    protected $clients;
    protected $schedules;

    function __construct() {
        parent::__construct();
        $this->clients = $this->getClients();
        $this->schedules = [];
        $this->sched_ids = [];
    }


    public function index($is_neft = 1){
        ini_set('max_execution_time', 0);
        $input_file = TRANS_REPORT_UPLOAD_DIR . 'trans-report.csv';
        $output_file = TRANS_REPORT_UPLOAD_DIR . 'trans-report-output.csv';
        $sno = 0;
        if(file_exists($input_file)){
            echo "Creating file handles...<br />";
            $file_in = fopen($input_file, 'r');
            $file_out = fopen($output_file, 'w+');
            echo "File handles created...<br /><hr />";
            while(!feof($file_in)){
                $csv_row = fgetcsv($file_in);
                $sno++;
                if($sno == 1){
                    echo 'Skipping the column header rows<br />';
                    continue;
                }
                //echo "Read CSV row from file: <pre>" . print_r($csv_row, true) . "</pre><br />";
                $dis_output = $this->runUpdates($csv_row, $is_neft);
                fputcsv($file_out, $dis_output);
                echo "Output written to output file: <pre>" . print_r($dis_output, true) . "</pre><hr />";
            }
            fclose($file_in);
            fclose($file_out);

            //Update the record in the main schedule table
            echo "<pre>About to update the main schedule table\n\n";
            foreach($this->sched_ids as $sch_id => $sched){
                $succesful = !empty($sched['successful']) ? $sched['successful'] : 0;
                $failed = !empty($sched['failed']) ? $sched['failed'] : 0;
                $query = "UPDATE schedules
                                    SET sch_ben_count_successful_only = sch_ben_count_successful_only + $succesful,
                                        sch_ben_count_failed_only = sch_ben_count_failed_only + $failed
                                    WHERE sch_id = $sch_id";
                $this->db->query($query);
                echo "$query\n\n";
            }

            echo "I'm done!!!";
        } else {
            die('Input file not found!!!');
        }
    }


    protected function runUpdates_old($csv_row, $is_neft = 1){
        /*
        - Batch ID
        - Serial Number
        - Account Number
        - Sort code
        - Amount
        - Returned Reason
        */
        $batch_id = !empty($csv_row[0]) ? trim($csv_row[0]) : '';
        $serialno = !empty($csv_row[1]) ? trim($csv_row[1]) : '';
        $accno_temp = !empty($csv_row[2]) ? trim($csv_row[2]) : '';
        $sortcode_temp = !empty($csv_row[3]) ? trim($csv_row[3]) : '';
        $amount_temp = !empty($csv_row[4]) ? trim($csv_row[4]) : '';
        $reason_in_file = !empty($csv_row[5]) ? strtolower(trim($csv_row[5])) : '';

        try {
            //Confirm that all the necessary data is in place
            if(empty($batch_id) || empty($serialno) || empty($accno_temp) || empty($sortcode_temp) || empty($amount_temp)){
                throw new Exception('Incomplete Data');
            }

            //Confirm that all the necessary data is in place
//            if(empty($batch_id) || empty($accno_temp) || empty($sortcode_temp) || empty($amount_temp)){
//                throw new Exception('Incomplete Data');
//            }

            //Get the client code & sched ID from the batch ID
            $client_sched = $this->getClientIDAndScheduleID($batch_id);
//            die('<pre>' . print_r($client_sched, true));
            $client_code = $client_sched['client_code'];
            $sched_id_client = $client_sched['sched_id_client'];
//            die("$client_code - $sched_id_client");

            //Get the client details using the client code
            $dis_client = !empty($this->clients[$client_code]) ? $this->clients[$client_code] : [];
            if(empty($dis_client)){
                throw new Exception("Unknown client code: $client_code");
            }

//            die('<pre>CLIENT: ' . print_r($dis_client, true));

            //Get the DB table schedule row ID using the client's schedule ID
            $sch_id = $this->getScheduleID($sched_id_client);
//            die('<pre>SCH ID: ' . print_r($sch_id, true));

            //Clean up the other parameters
            $accno = str_pad($accno_temp, (10 - strlen($accno_temp)), "0", STR_PAD_LEFT);
            $bankcode = substr($sortcode_temp, 0, 3);
            $amount = str_replace(',', '', $amount_temp);

            //Conditions for updating this schedule beneficiary
            $where = [
                'sch_id' => $sch_id,
                'ben_bankcode' => $bankcode,
                'ben_accno' => $accno,
                'ben_amount' => $amount,
//                'ben_serialno' => $serialno
            ];
            //ben_statuscode = '06' || ben_statuscode = ''
            $where_text = "(ben_statuscode = '06' OR ben_statuscode = '') AND sch_id = $sch_id AND ben_bankcode = $bankcode AND ben_accno = $accno AND ben_amount = $amount AND ben_serialno = $serialno";

//            die('<pre>' . print_r($where, true));

            //Get the statuscode & reason
            $status_array = $this->getStatusAndReason($reason_in_file);
            $statuscode = $status_array['statuscode'];
            $reason = $status_array['reason'];

            //Update to be carried out on this beneficiary
            $update_data = [
                'ben_status' => ($statuscode == '00' ? "2" : "3"),
                'ben_statuscode' => "$statuscode",
                'ben_reason' => "$reason",
//                'ben_lastupdated' => date('Y-m-d H:i:s'),
            ];

            $update_log = "WHERE: " . print_r($update_data, true) . " --- UPDATE DATA: " . print_r($update_data, true);

            //Now, update the beneficiary
            $this->db->where($where)
                ->update('schedules_beneficiaries', $update_data);
            if($this->db->affected_rows() == 1){
                $csv_row['update_result'] = "Updated: " . $this->db->last_query();

                //Update the successful / failed count in the main schedules table
                $column = $statuscode == '00' ? 'sch_ben_count_successful_only' : 'sch_ben_count_failed_only';
                $this->db->query("UPDATE schedules SET $column = $column + 1 WHERE sch_id = $sch_id");
            } else {
                $csv_row['update_result'] = "Update Failed: " . $this->db->last_query();
            }
            $csv_row['update_result'] = ($this->db->affected_rows() == 1 ? "Updated: " : "Update Failed: ") . $this->db->last_query();

        } catch (Exception $ex) {
            $csv_row['update_result'] = $ex->getMessage();
        }
        return $csv_row;
    }



    protected function runUpdates($csv_row, $is_neft = 1){
        /*
        - Batch ID
        - Serial Number
        - Account Number
        - Sort code
        - Amount
        - Returned Reason
        */
        $batch_id = !empty($csv_row[0]) ? trim($csv_row[0]) : '';
        $serialno = !empty($csv_row[1]) ? trim($csv_row[1]) : '';
        $accno_temp = !empty($csv_row[2]) ? trim($csv_row[2]) : '';
        $sortcode_temp = !empty($csv_row[3]) ? trim($csv_row[3]) : '';
        $amount_temp = !empty($csv_row[4]) ? trim($csv_row[4]) : '';
        $reason_in_file = !empty($csv_row[5]) ? strtolower(trim($csv_row[5])) : '';

        try {
            //Confirm that all the necessary data is in place
            if(empty($batch_id) || empty($serialno) || empty($accno_temp) || empty($sortcode_temp) || empty($amount_temp)){
                throw new Exception('Incomplete Data');
            }

            //Confirm that all the necessary data is in place
//            if(empty($batch_id) || empty($accno_temp) || empty($sortcode_temp) || empty($amount_temp)){
//                throw new Exception('Incomplete Data');
//            }

            //Get the client code & sched ID from the batch ID
            $client_sched = $this->getClientIDAndScheduleID($batch_id);
//            die('<pre>' . print_r($client_sched, true));
            $client_code = $client_sched['client_code'];
            $sched_id_client = $client_sched['sched_id_client'];
//            die("$client_code - $sched_id_client");

            //Get the client details using the client code
            $dis_client = !empty($this->clients[$client_code]) ? $this->clients[$client_code] : [];
            if(empty($dis_client)){
                throw new Exception("Unknown client code: $client_code");
            }

//            die('<pre>CLIENT: ' . print_r($dis_client, true));

            //Get the DB table schedule row ID using the client's schedule ID
            $sch_id = $this->getScheduleID($sched_id_client);
//            die('<pre>SCH ID: ' . print_r($sch_id, true));

            //Clean up the other parameters
            $accno = str_pad($accno_temp, 10, "0", STR_PAD_LEFT);
            $bankcode = $is_neft ? substr($sortcode_temp, 0, 3) : $this->_getBankCode($sortcode_temp);
            $amount = str_replace(',', '', $amount_temp);

            //Conditions for updating this schedule beneficiary
            $where = [
                'sch_id' => $sch_id,
                'ben_bankcode' => $bankcode,
                'ben_accno' => $accno,
                'ben_amount' => $amount,
//                'ben_serialno' => $serialno
            ];
            //ben_statuscode = '06' || ben_statuscode = ''
            $where_text = "(ben_statuscode = '06' OR ben_statuscode = '') AND sch_id = $sch_id AND ben_bankcode = $bankcode AND ben_accno = $accno AND ben_amount = $amount AND ben_serialno = $serialno";

//            die('<pre>' . print_r($where, true));

            //Get the statuscode & reason
            $status_array = $this->getStatusAndReason($reason_in_file);
            $statuscode = $status_array['statuscode'];
            $reason = $status_array['reason'];

            //Update to be carried out on this beneficiary
            $update_data = [
                'ben_status' => ($statuscode == '00' ? "2" : "3"),
                'ben_statuscode' => "$statuscode",
                'ben_reason' => "$reason",
//                'ben_lastupdated' => date('Y-m-d H:i:s'),
            ];

            $update_log = "WHERE: " . print_r($update_data, true) . " --- UPDATE DATA: " . print_r($update_data, true);

//            die("<pre> WHERE: " . print_r($where, true) . "\n\nDATA: " . print_r($update_data, true));

            //Now, update the beneficiary
            $this->db->where($where)
                ->update('schedules_beneficiaries', $update_data);
//            echo $this->db->last_query() . "\n";
            if($this->db->affected_rows() == 1){
                $csv_row['update_result'] = "Updated";// . $this->db->last_query();

                //Update the count of successful / failed payments
//                $column = $statuscode == '00' ? 'sch_ben_count_successful_only' : 'sch_ben_count_failed_only';
//                $this->db->query("UPDATE schedules SET $column = $column + 1 WHERE sch_id = $sch_id");
                if($statuscode == '00'){
                    $this->sched_ids[$sch_id]['successful'] = empty($this->sched_ids[$sch_id]['successful']) ? 0 : $this->sched_ids[$sch_id]['successful'];
                    $this->sched_ids[$sch_id]['successful']++;
                } else {
                    $this->sched_ids[$sch_id]['failed'] = empty($this->sched_ids[$sch_id]['failed']) ? 0 : $this->sched_ids[$sch_id]['failed'];
                    $this->sched_ids[$sch_id]['failed']++;
                }
            } else {
                $csv_row['update_result'] = "Update Failed";// . $this->db->last_query();
            }
//            $csv_row['update_result'] = ($this->db->affected_rows() == 1 ? "Updated: " : "Update Failed: ");// . $this->db->last_query();

        } catch (Exception $ex) {
            $csv_row['update_result'] = $ex->getMessage();
        }
        return $csv_row;
    }



    protected function _getBankCode($bank_name_temp){
        $bank_name = strtoupper(trim($bank_name_temp));
        switch ($bank_name){
            case 'ACCESS BANK PLC': $ret_val = '044'; break;
            case 'ACCESS BANK PLC (DIAMOND)': $ret_val = '063'; break;
            case 'FIDELITY BANK PLC': $ret_val = '070'; break;
            case 'FIRST BANK OF NIGERIA PLC': $ret_val = '011'; break;
            case 'FIRST CITY MONUMENT BANK PLC': $ret_val = '214'; break;
            case 'GUARANTY TRUST BANK PLC': $ret_val = '058'; break;
            case 'POLARIS BANK': $ret_val = '076'; break;
            case 'UNITED BANK FOR AFRICA PLC': $ret_val = '033'; break;
            case 'UNITY BANK PLC': $ret_val = '215'; break;
            case 'ZENITH INTERNATIONAL BANK PLC': $ret_val = '057'; break;
            case 'ECOBANK NIGERIA PLC': $ret_val = '050'; break;
            case 'HERITAGE BANK PLC': $ret_val = '030'; break;
            case 'KEYSTONE BANK PLC': $ret_val = '082'; break;
            case 'STANBIC IBTC BANK PLC': $ret_val = '221'; break;
            case 'STERLING BANK PLC': $ret_val = '232'; break;
            case 'UNION BANK OF NIGERIA PLC': $ret_val = '032'; break;
            case 'WEMA BANK PLC': $ret_val = '035'; break;
            case 'STANDARD CHARTERED BANK PLC': $ret_val = '068'; break;
            case 'PROVIDUS BANK PLC': $ret_val = '101'; break;
            case 'CITIBANK':
            case 'CITI BANK PLC':
            case 'CITI BANK': $ret_val = '023'; break;
//            case 'asdfasdf': $ret_val = 'asdfasdf'; break;
//            case 'asdfasdf': $ret_val = 'asdfasdf'; break;
        }
        return $ret_val;
    }



    protected function getStatusAndReason($reason_in_file){
        switch ($reason_in_file){
            case '':
            case 'paid':
            case 'payment successful':
            case 'payment successfull':
            case 'successful':
                $statuscode = '00';
                $reason = 'Successful';
                break;

            case 'payment in progress':
            case 'in progress':
                $statuscode = '06';
                $reason = 'In Progress';
                break;

            case 'account attached':
            case 'invalid nuban number':
                $statuscode = '10';
                $reason = 'INVALID NUBAN NUMBER';
                break;
            case 'invalid bank code':
                $statuscode = '22';
                $reason = 'INVALID BANK CODE';
                break;
            case 'account name and account number differ':
            case 'account name and account number differs':
            case 'account non-existent':
            case 'account closed':
            case 'account not valid for clearing/electronic payment':
                $statuscode = '23';
                $reason = 'INVALID BANK ACCOUNT';
                break;
            case 'account dormant':
                $statuscode = '21';
                $reason = 'DORMANT ACCOUNT';
                break;
            case 'payment stopped':
            case "drawer’s confirmation required":
                $statuscode = '20';
                $reason = 'DO NOT HONOR';
                break;
            case 'payment failed':
            default:
                $statuscode = '15';
                $reason = 'PAYMENT FAILED';
                break;
        }
        return [
            'statuscode' => $statuscode,
            'reason' => $reason,
        ];
    }


    protected function getClientIDAndScheduleID($batch_id){
        $batch_array = explode('_', $batch_id);
        $sched_id_nibss_temp = $batch_array[count($batch_array) - 1];
        $sched_id_nibss = str_replace('.csv', '', $sched_id_nibss_temp);
        $sched_id_client = substr(trim($sched_id_nibss), 0, strlen($sched_id_nibss) - 10);
        $client_code = substr($sched_id_nibss, strlen($sched_id_nibss) - 10);
        return [
            'sched_id_nibss' => $sched_id_nibss,
            'sched_id_client' => $sched_id_client,
            'client_code' => $client_code,
        ];
    }



    protected function getClients(){
        $result = $this->db->get('api_clients');
        $clients = $result->result_array();
        $ret_val = [];
        foreach ($clients as $c){
            $ret_val[$c['client_code']] = $c;
        }
        return $ret_val;
    }


    protected function getScheduleID($sched_id_client){
        if(isset($this->schedules[$sched_id_client])){
            $ret_val = $this->schedules[$sched_id_client];
        } else {
            $result = $this->db->where(['sch_sched_id' => "$sched_id_client"])
                ->get('schedules');
//            die('<pre>' . $this->db->last_query());
            $sched_deets = $result->row_array();
            $this->schedules[$sched_id_client] = $ret_val = !empty($sched_deets['sch_id']) ? $sched_deets['sch_id'] : 0;
        }
//        die("<pre>..." . print_r($sched_deets, true) . "---$ret_val");
        return $ret_val;
    }


}