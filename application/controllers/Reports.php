<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Reports
 *
 * @author oyetunde.faniran
 */
class Reports extends UPL_Controller {
    
    
    
    function __construct() {
        parent::__construct();
        $this->load->model('Transaction_model', 'transm');
        $this->load->model('Clients_model', 'clientm');
        $this->load->model('Commission_model', 'comm');
        $this->load->model('Projects_model', 'prj');
    }
    
    
    
    public function transactions(){
        $where_clause = '';
        
        //Get posted data if any
        $view_data['fromdate'] = $fromdate_temp = $this->input->post('fromdate');
        $view_data['todate'] = $todate_temp = $this->input->post('todate');
        $client_temp = $this->input->post('client');

        //Date
        if(!empty($fromdate_temp) && !empty($todate_temp)){
//                $where_clause['s.client_id'] = $client_id;
            $where_clause .= " AND sch_dateadded BETWEEN '$fromdate_temp 00:00:00' AND '$todate_temp 23:59:59' ";
        } else {
            $today = date('Y-m-d');
            $where_clause .= " AND sch_dateadded BETWEEN '$today 00:00:00' AND '$today 23:59:59' ";
        }

        //Client ID
        $client_id = (int)$client_temp;
        if(!empty($client_id)){
//                $where_clause['s.client_id'] = $client_id;
            $where_clause .= " AND s.client_id = '$client_id' ";
        }

        $where_clause_final = !empty($where_clause) ? 'TRUE ' . $where_clause : '';
            
        
        $view_data['trans'] = $this->transm->getTransactions($where_clause_final);
        $view_data['clients'] = $this->clientm->getClients();
        
        $view_data['js_files'] = array(
            'assets/lib/raphael/raphael-min.js',
            'assets/lib/morrisjs/morris.min.js',
            'assets/lib/datatables/js/jquery.dataTables.min.js',
            'assets/lib/datatables/js/dataTables.bootstrap.min.js',
            'assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.html5.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.flash.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.print.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.colVis.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js',
            'assets/lib/jquery.niftymodals/js/jquery.modalEffects.js',
            'assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js'
        );

        //Extra CSS files to import
        $view_data['css_files'] = array(
            'assets/lib/morrisjs/morris.css',
            'assets/lib/datatables/css/dataTables.bootstrap.min.css',
            'assets/lib/jquery.niftymodals/css/component.css',
            'assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css'
        );

        $view_data['additional_js'] = <<<JS
$("#report-table").dataTable({buttons:["copy","excel","pdf","print"],lengthMenu:[[25,50,100,200,-1],[25,50,100,200,"All"]],dom:"<'row am-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B>><'row am-datatable-body'<'col-sm-12'tr>><'row am-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"});
$(document).ready(function(){
    $(".datetimepicker").datetimepicker({
        autoclose:!0,
        componentIcon:".s7-date",
        navIcons:{
            rightIcon:"s7-angle-right",
            leftIcon:"s7-angle-left"
        }
    });
});                
JS;
        
        $view_data['title'] = 'Transaction Reports';
		$view_file = 'reports/report-transaction';
        $this->_doRender($view_file, $view_data);
    }


    public function trans_beneficiaries($sch_id_temp = 0, $success = ''){
        $sch_id = (int)$sch_id_temp;

        //Get posted data if any
        $posted_data = $this->input->post();
        if(!empty($posted_data)) {
            try {
                $status_temp = $this->input->post('status');
                $checked = $this->input->post('ben_checked');

                //Get the status details from the ID and confirm validity
                $status_id = (int)$status_temp;
                $status_array = $this->transm->getNPPResponse($status_id);
                if (empty($status_array)) {
                    throw new Exception('No status selected');
                }

                //Confirm that beneficiaries were checked
                if (empty($checked)) {
                    throw new Exception('No beneficiary selected for status update');
                }

                //Update beneficiary statuses
                $this->transm->updateTransactionBenStatus($checked, $status_array['response_code'], $status_array['description']);


                //Update successful / failed beneficiary count on the schedules table
                switch ($status_array['response_code']){
                    case '00': $column = 'sch_ben_count_successful_only'; break;
                    case '06': $column = ''; break;
                    default: $column = 'sch_ben_count_failed_only'; break;
                }
                if($column != ''){
                    $this->transm->updateTransCompletedBeneficiaryCount($sch_id, count($checked), $column);
                }

                redirect("reports/trans_beneficiaries/$sch_id/success");

            } catch (Exception $ex) {
                $view_data['msg'] = $this->_renderErrorMsg($ex->getMessage());
            }
        }

        if(empty($view_data['msg']) && $success == 'success'){
            $view_data['msg'] = $this->_renderSuccessMsg('Beneficiary transaction status set successfully');
        }

        $trans_temp = $this->transm->getTransactions(['sch_id' => $sch_id]);
        $view_data['trans'] = !empty($trans_temp[0]) ? $trans_temp[0] : [];
        $view_data['npp_responses'] = $this->transm->getNPPResponses();
        $view_data['bens'] = $this->transm->getTransactionBeneficiaries($sch_id);
//        die('<pre>' . print_r($view_data, true));

        $view_data['js_files'] = array(
            'assets/lib/raphael/raphael-min.js',
            'assets/lib/morrisjs/morris.min.js',
            'assets/lib/datatables/js/jquery.dataTables.min.js',
            'assets/lib/datatables/js/dataTables.bootstrap.min.js',
            'assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.html5.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.flash.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.print.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.colVis.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js',
            'assets/lib/jquery.niftymodals/js/jquery.modalEffects.js',
            'assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js'
        );

        //Extra CSS files to import
        $view_data['css_files'] = array(
            'assets/lib/morrisjs/morris.css',
            'assets/lib/datatables/css/dataTables.bootstrap.min.css',
            'assets/lib/jquery.niftymodals/css/component.css',
            'assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css'
        );

        $view_data['additional_js'] = <<<JS
$("#report-table").dataTable({buttons:["copy","excel","pdf","print"],lengthMenu:[[25,50,100,200,-1],[25,50,100,200,"All"]],dom:"<'row am-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B>><'row am-datatable-body'<'col-sm-12'tr>><'row am-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"});
$(document).ready(function(){
    $(".datetimepicker").datetimepicker({
        autoclose:!0,
        componentIcon:".s7-date",
        navIcons:{
            rightIcon:"s7-angle-right",
            leftIcon:"s7-angle-left"
        }
    });
});                
JS;

        $view_data['title'] = 'Transaction Beneficiaries';
        $view_file = 'reports/report-transaction-beneficiaries';
        $this->_doRender($view_file, $view_data);
    }
    
    
    
    public function comms($deposit_id = 0, $date_added = ''){
        $where_clause = '';
        
        //Get posted data if any
        $view_data['fromdate'] = $fromdate_temp = !empty($date_added) ? $date_added : $this->input->post('fromdate');
        $view_data['todate'] = $todate_temp = !empty($date_added) ? $date_added : $this->input->post('todate');
        $client_temp = $this->input->post('client');

        //Date
        if(!empty($fromdate_temp) && !empty($todate_temp)){
//                $where_clause['s.client_id'] = $client_id;
            $where_clause .= " AND schcomm_dateadded BETWEEN '$fromdate_temp 00:00:00' AND '$todate_temp 23:59:59' ";
        } else {
            $today = date('Y-m-d');
            $where_clause .= " AND schcomm_dateadded BETWEEN '$today 00:00:00' AND '$today 23:59:59' ";
        }

        //Client ID
        $client_id = (int)$client_temp;
        if(!empty($client_id)){
//                $where_clause['s.client_id'] = $client_id;
            $where_clause .= " AND s.client_id = '$client_id' ";
        }
        
        //Add the bank deposit ID as a constraint if specified
        if(!empty($deposit_id)){
            $where_clause .= " AND sc.bdeposit_id = '$deposit_id' ";
        }
        

        $where_clause_final = !empty($where_clause) ? 'sc.schcomm_deleted = "0" ' . $where_clause : '';
        
//        echo("<!-- '$client_temp' ... '$client_id'" . "'$where_clause' ... '$where_clause_final'" );
            
        
        $view_data['trans'] = $this->comm->getCommissionPayments($where_clause_final);
//        die('<pre>' . print_r($view_data['trans'], true));
        $view_data['clients'] = $this->clientm->getClients();
        
        $view_data['js_files'] = array(
            'assets/lib/raphael/raphael-min.js',
            'assets/lib/morrisjs/morris.min.js',
            'assets/lib/datatables/js/jquery.dataTables.min.js',
            'assets/lib/datatables/js/dataTables.bootstrap.min.js',
            'assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.html5.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.flash.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.print.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.colVis.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js',
            'assets/lib/jquery.niftymodals/js/jquery.modalEffects.js',
            'assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js'
        );

        //Extra CSS files to import
        $view_data['css_files'] = array(
            'assets/lib/morrisjs/morris.css',
            'assets/lib/datatables/css/dataTables.bootstrap.min.css',
            'assets/lib/jquery.niftymodals/css/component.css',
            'assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css'
        );

        $view_data['additional_js'] = <<<JS
$("#report-table").dataTable({buttons:["copy","excel","pdf","print"],lengthMenu:[[25,50,100,200,-1],[25,50,100,200,"All"]],dom:"<'row am-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B>><'row am-datatable-body'<'col-sm-12'tr>><'row am-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"});
$(document).ready(function(){
    $(".datetimepicker").datetimepicker({
        autoclose:!0,
        componentIcon:".s7-date",
        navIcons:{
            rightIcon:"s7-angle-right",
            leftIcon:"s7-angle-left"
        }
    });
});                
JS;
        
        //Get the list of projects
        $view_data['prjs'] = $this->prj->getProjects();
        
        $view_data['title'] = 'Commission Reports';
		$view_file = 'reports/report-commission';
        $this->_doRender($view_file, $view_data);
//        $client_id = (int)$client_id_temp;
////        die("$client_id");
//        $view_data['title'] = 'Commission Reports';
//		$view_file = 'reports/report-commission';
//        $this->_doRender($view_file, $view_data);
    }
    
    
    
    
    public function ben_comms(){
        $where_clause = '';
        
        //Get posted data if any
        $view_data['fromdate'] = $fromdate_temp = $this->input->post('fromdate');
        $view_data['todate'] = $todate_temp = $this->input->post('todate');
        $client_temp = $this->input->post('client');

        //Date
        if(!empty($fromdate_temp) && !empty($todate_temp)){
//                $where_clause['s.client_id'] = $client_id;
            $where_clause .= " AND split_dateadded BETWEEN '$fromdate_temp 00:00:00' AND '$todate_temp 23:59:59' ";
        } else {
            $today = date('Y-m-d');
            $where_clause .= " AND split_dateadded BETWEEN '$today 00:00:00' AND '$today 23:59:59' ";
        }

        //Client ID
//        $client_id = (int)$client_temp;
//        if(!empty($client_id)){
////                $where_clause['s.client_id'] = $client_id;
//            $where_clause .= " AND s.client_id = '$client_id' ";
//        }
        
        //Get the beneficiary ID of the logged-in user ID from the beneficiaries table
        $this->load->model('Beneficiary_model', 'benm');
        $ben_deets = $this->benm->getBeneficiaryFromUserID($_SESSION['user_id']);
        $ben_id = !empty($ben_deets['ben_id']) ? $ben_deets['ben_id'] : 0;
        
        //Restrict the split to the logged-in user only
        $where_clause .= " AND ben_id = '$ben_id'";

        $where_clause_final = !empty($where_clause) ? 'TRUE ' . $where_clause : '';
            
        
        $view_data['trans'] = $this->comm->getBeneficiaryCommissionPayments($where_clause_final);
        $view_data['clients'] = $this->clientm->getClients();
        
        $view_data['js_files'] = array(
            'assets/lib/raphael/raphael-min.js',
            'assets/lib/morrisjs/morris.min.js',
            'assets/lib/datatables/js/jquery.dataTables.min.js',
            'assets/lib/datatables/js/dataTables.bootstrap.min.js',
            'assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.html5.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.flash.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.print.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.colVis.js',
            'assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js',
            'assets/lib/jquery.niftymodals/js/jquery.modalEffects.js',
            'assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js'
        );

        //Extra CSS files to import
        $view_data['css_files'] = array(
            'assets/lib/morrisjs/morris.css',
            'assets/lib/datatables/css/dataTables.bootstrap.min.css',
            'assets/lib/jquery.niftymodals/css/component.css',
            'assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css'
        );

        $view_data['additional_js'] = <<<JS
$("#report-table").dataTable({buttons:["copy","excel","pdf","print"],lengthMenu:[[25,50,100,200,-1],[25,50,100,200,"All"]],dom:"<'row am-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B>><'row am-datatable-body'<'col-sm-12'tr>><'row am-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"});
$(document).ready(function(){
    $(".datetimepicker").datetimepicker({
        autoclose:!0,
        componentIcon:".s7-date",
        navIcons:{
            rightIcon:"s7-angle-right",
            leftIcon:"s7-angle-left"
        }
    });
});                
JS;
        
        //Get the list of projects
        $view_data['prjs'] = $this->prj->getProjects();
        
        $view_data['title'] = 'Beneficiary Commission Reports';
        $view_file = 'reports/report-commission-ben-only';
        $this->_doRender($view_file, $view_data);
//        $client_id = (int)$client_id_temp;
////        die("$client_id");
//        $view_data['title'] = 'Commission Reports';
//		$view_file = 'reports/report-commission';
//        $this->_doRender($view_file, $view_data);
    }
    
    
    
}
