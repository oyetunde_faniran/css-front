<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SCH_Model
 *
 * @author Tunde
 */
class UPL_Model extends CI_Model {

    protected $table = '';
    protected $pk_column = '1';
    protected $values = '';

    function __construct() {
        parent::__construct();
        $this->values = new stdClass();
    }


    public function insert($data = array()){
        $insert_data = !empty($data) ? $data : $this;
        $this->db->insert($this->table, $insert_data);
        return $this->db->insert_id();
    }   //END insert()



    public function insert_batch($data = array()){
        $insert_data = !empty($data) ? $data : $this;
        return $this->db->insert_batch($this->table, $insert_data);
    }   //END insert_batch()



    /**
     * Loads multiple rows from the tables
     * @param int $offset
     * @param int $limit
     * @param mixed $where
     * @param string $eval_code     A string that contains any custom code to be executed. eg. custom select clause, join clauses. etc.
     * @return type
     */
    public function loadMultiple($offset = 0, $limit = 0, $where = '', $eval_code = ''){
        if (!empty($where)){
            $this->db->where($where);
        }

        if (!empty($offset) && !empty($limit)){
            $this->db->limit($limit, $offset);
        } elseif (!empty($offset)){
            $this->db->limit($offset);
        }

        if (!empty($eval_code)){
            eval($eval_code);
        }

        $result = $this->db->get($this->table);
        return $result->result_array();
    }   //END loadMultiple()


    protected function execute($action = '', $usepk = true, $where = '', $eval_code = ''){
        if ($usepk){
            $pk_value = isset($this->{$this->pk_column}) ? $this->{$this->pk_column} : '';
            $this->db->where($this->pk_column, $pk_value);
        }

        if (!empty($where)){
            $this->db->where($where);
        }

        if (!empty($eval_code)){
            eval($eval_code);
        }

        switch($action){
            case 'load':
                $result = $this->db->get($this->table);
                $ret_val = $result->row_array();
                break;
            case 'update':
                $ret_val = $this->db->update($this->table, $this->values);
                break;
            case 'delete':
                $ret_val = $this->db->delete($this->table);
                break;
        }   //END switch()


        return $ret_val;
    }   //END load()


    public function load($usepk = true, $where = '', $eval_code = ''){
        return $this->execute('load', $usepk, $where, $eval_code);
    }


    public function update($usepk = true, $where = '', $eval_code = ''){
        return $this->execute('update', $usepk, $where, $eval_code);
    }


    public function delete($usepk = true, $where = '', $eval_code = ''){
        return $this->execute('delete', $usepk, $where, $eval_code);
    }


    public function setValues($data_array){
        foreach ($data_array as $key => $value){
            $this->values->$key = $value;
        }
    }

    protected function setTable($table){
        $this->table = $table;
    }

    protected function setTablePK($table_pk){
        $this->pk_column = $table_pk;
    }


    public function startTransaction(){
        $this->db->trans_begin();
    }


    public function commitTransaction(){
        $this->db->trans_commit();
    }


    public function rollbackTransaction(){
        $this->db->trans_rollback();
    }



    protected function _sendEmail($from_email, $from_name, $to, $subject, $msg){
        //Send an email containing the code
        $this->load->library('email');
        $this->email->from($from_email, $from_name);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($msg);
        $this->email->set_alt_message($msg);
        return $this->email->send();
    }   //END _sendEmail()


}   //END class