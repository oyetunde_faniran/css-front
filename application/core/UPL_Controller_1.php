<?php
/**
 * Description of SCH_Controller
 *
 * @author Tunde
 */
class UPL_Controller extends CI_Controller {

    /**
     * The CSS class to use as the main icon of the class/entity
     * @var string
     */
    protected $main_icon;

    /**
     * The name that would be used to refer to the class/entity
     * @var string
     */
    protected $main_title;

    /**
     * The name that would be used to refer to the class/entity in plural form
     * @var string
     */
    protected $main_title_plural;

    /**
     * The folder that would store all view files for this class
     * @var string
     */
    protected $view_folder;

    function __construct($min_uri_segments = 3) {
        parent::__construct();


        //Set Variables for use in sub-classes
        $dis_class = get_class($this);
        $this->main_icon = '';
        $this->main_title = $dis_class;
        $this->main_title_plural = $dis_class . 's';
        $this->view_folder = strtolower($dis_class) . '/';
        $this->controller = $this->view_folder;

//        //Get the sub-domain & subsequently the school details
//        $subdomain = self::_getCurrentSubDomain();
//        $this->load->model('schools/School_model', 'school_model');
//        $school_deets = $this->school_model->getSchoolFromSubDomain($subdomain);
//        if (empty($school_deets)){
//            die('Unknown School!');
//        }
//        !defined('SCHOOL_NAME')? define('SCHOOL_NAME', $school_deets['sch_name']) : '';
//        !defined('SCHOOL_ID')? define('SCHOOL_ID', $school_deets['sch_id']) : '';
//        !defined('SCHOOL_STATUS')? define('SCHOOL_STATUS', $school_deets['sch_status']) : '';
//        !defined('SCHOOL_PER_STUDENT_COST')? define('SCHOOL_PER_STUDENT_COST', $school_deets['schset_perstudentcost']) : '';
//        die('<pre>' . print_r($school_deets, true));

        $dis_url = uri_string();
        //die($dis_url);
        $login_page = 'login';
//        die('<pre>' . print_r($_SESSION, true));

        //Form a list of pages that can be visited without logging in
        $free2view = array(//'', '/',                                    //Home Page
                            'sys-admin/access_denied',            //Access Denied Page
                            'access_denied',
                            'login',
                            'login/error',
                            'logout',
							'forgot_password',
                            'sys-admin/login/login_page',         //Log-In Form-Submission Handler
                            'sys-admin/login/forgot_password',    //Forgot Password
                            'sys-admin/login/forgot_password/success',  //Forgot Password Success Page
                            'sys-admin/login/logout'             //Log Out
                     );

        if (!in_array($dis_url, $free2view)){
            //die($dis_url . '--> bad');
            //Confirm that the person is logged in if he/she is trying to visit a protected page
            if (empty($_SESSION['user_id'])) {
                redirect($login_page);
            }

            //Confirm that the person has access to the requested page
            if (!$this->system_authorization->authorize($min_uri_segments, $free2view)) {
                redirect('sys-admin/access_denied');
            }
        } //else die($dis_url . '--> good');
    }   //END __construct()



    protected static function _getCurrentSubDomain(){
        return 'rsbb';
//        $parts = explode(".", $_SERVER["HTTP_HOST"]);
//        return count($parts) >= 4 ? $parts[0] : "";
    }   //END getCurrentSubDomain()


    protected function _do404(){
        $this->load->view('404');
    }



    /**
     * Load view files for page rendering
     * @param string $view_file     The main view file to load
     * @param string $view_data     The data array to pass to the view
     */
    protected function _doRender($view_file = '', $view_data = array()){
        $this->load->view('header', $view_data);
        !empty($view_file) ? $this->load->view($view_file, $view_data) : null;
        $this->load->view('footer', $view_data);
    }


    /**
     * Does the actual rendering of a success/warning/error message
     * @param string $msg   The message to display
     * @param int $type     1 = Success, 2 = Warning, 3 = Error
     * @return string       The rendered message
     */
    private function _renderMsg($msg, $type){
        switch($type){
            case 1:     //Success
                $style = UPL_MSG_STYLE_SUCCESS;
				$icon = 's7-check';
                $header_text = 'Success!';
                break;
            case 2:     //Warning
                $style = UPL_MSG_STYLE_WARNING;
                $header_text = 'Warning!';
				$icon = 's7-attention';
                break;
            default:     //Error
                $style = UPL_MSG_STYLE_ERROR;
                $header_text = 'Error!';
				$icon = 's7-close-circle';
                break;
        }
//        $ret_val = '<div class="' . $style . '">
//                        <h5>' . $header_text . '</h5>
//                        <p>' . $msg . '</p>
//                     </div>';
        $ret_val = '<div class="' . $style . '">
                        <a class="close" data-dismiss="alert" href="#">&times;</a>
                        <h4><i class="icon-ok-sign"></i> ' . $header_text . '</h4>
                        ' . $msg . '
                      </div>';
		$ret_val = '<div role="alert" class="alert ' . $style . ' alert-icon alert-dismissible">
						<div class="icon"><span class="' . $icon . '"></span></div>
						<div class="message">
							<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="s7-close"></span></button><strong>' . $header_text . '</strong> ' . $msg . '
						</div>
					</div>';
        return $ret_val;
    }   //END _renderSuccessMsg()



    /**
     * Renders a success msg using the specified style UPL_MSG_STYLE_SUCCESS set in constants.php
     * @param string $msg   The success message to display
     * @return string       The rendered success message
     */
    protected function _renderSuccessMsg($msg){
        return $this->_renderMsg($msg, 1);
    }   //END _renderSuccessMsg()



    /**
     * Renders a error msg using the specified style UPL_MSG_STYLE_ERROR set in constants.php
     * @param string $msg   The error message to display
     * @return string       The rendered error message
     */
    protected function _renderErrorMsg($msg){
        return $this->_renderMsg($msg, 3);
    }   //END _renderErrorMsg()



    /**
     * Renders a warning msg using the specified style UPL_MSG_STYLE_WARNING set in constants.php
     * @param string $msg   The warning message to display
     * @return string       The rendered warning message
     */
    protected function _renderWarningMsg($msg){
        return $this->_renderMsg($msg, 2);
    }   //END _renderWarningMsg()


    protected function do404(){
        $this->load->view('404');
    }



    protected function _sendEmail($from_email, $from_name, $to, $subject, $msg){
        //Send an email containing the code
        $this->load->library('email');
        $this->email->from($from_email, $from_name);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($msg);
        $this->email->set_alt_message($msg);
        return $this->email->send();
    }   //END _sendEmail()



}   //END class